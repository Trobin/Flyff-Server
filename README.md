# Visual Studio 2019 Source + Files

## Short Information
Forked from [Denwo22/Flyff-Server](https://github.com/Denwo22/Flyff-Server). I had the idea to port Flyff servers to Linux since I think servers should be runned on Linux :) and it's a good challenge to learn !

## What do i need to run this?
- Microsoft SQL Server
- [Visual Studio 2019](https://www.visualstudio.com/vs/whatsnew/)

## Changes


## How to test
- Build/generate solution
- Execute "Copy Files.bat" script
- Launch servers (1=Account, 2=Database, 3=Core, 4=Certifier, 5=Login, 6=Cache, 7=World)
- Launch Neuz and connect

## Steps to port to Linux
For all application servers :
- Remove unecessary Windows API (GUI, typedefs (BOOL, BYTE, FLOAT, etc))
- Remove/replace necessary Windows API by standard C++ or cross-platform API/libs (Network, MicrosoftSQL -> MySQL, Direct3D -> OpenGL?, DirectX)
- Transform VS Studio project into cmake project