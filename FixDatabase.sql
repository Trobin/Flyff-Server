USE [CHARACTER_01_DBF]
GO
/****** Objet : StoredProcedure [dbo].[MAIL_STR_REALTIME] Date de génération du script : 10/28/2009 19:14:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[MAIL_STR_REALTIME]

@iGu CHAR(255),
@serverindex CHAR(2),
@nMail_Before INT = 0,
@nMail_After INT = 0,
@idPlayer CHAR(7) = '0000000',
@nLevel INT = 0,
@iaccount VARCHAR(32) = '',
@tmCreate INT = 0,
@dwSerialNumber INT = 0,
@nHitPoint INT = 0
as
set nocount on

declare @iserverindex char(2)
set @iserverindex = cast((cast(@serverindex as int) + 50) as char(2))

IF @iGu = 'S1'
BEGIN
SELECT * FROM MAIL_TBL
WHERE serverindex = @iserverindex AND byRead<90
ORDER BY nMail
RETURN
END
ELSE
IF @iGu = 'U1'
BEGIN
UPDATE MAIL_TBL SET nMail = @nMail_After, serverindex = @serverindex, dwSerialNumber = @dwSerialNumber, nHitPoint = @nHitPoint
WHERE serverindex = @iserverindex and nMail = @nMail_Before
RETURN
END
ELSE
IF @iGu = 'I1'
BEGIN

DECLARE @ItemID int
DECLARE @ItemNum int
DECLARE @bBinds int
SET @ItemID = 0
SET @ItemNum = 1
SET @bBinds = 2


IF( @nLevel = 20 ) BEGIN
SET @ItemID = 26112
END
ELSE IF( @nLevel = 40 ) BEGIN
SET @ItemID = 26211
END
ELSE IF( @nLevel = 60 ) BEGIN
SET @ItemID = 26103
END
ELSE IF( @nLevel = 80 ) BEGIN
SET @ItemID = 30135
END
ELSE IF( @nLevel = 100 ) BEGIN
SET @ItemID = 5800
SET @bBinds = 0
END
ELSE IF( @nLevel = 120 ) BEGIN
SET @ItemID = 4703
SET @bBinds = 0
END

IF( @ItemID = 0 ) BEGIN
RETURN
END

-- ?? ??? ?? ??
DECLARE @nMaxMailID int
SELECT @nMaxMailID = MAX(nMail)+1 from MAIL_TBL where serverindex = @iserverindex
SET @nMaxMailID = ISNULL( @nMaxMailID, 0 )

-- ??? ??
DECLARE @szTitle VARCHAR(128)
DECLARE @szText VARCHAR(1024)
SET @szTitle = 'Title'
SET @szText = 'Text'

EXEC dbo.MAIL_STR 'A1', @nMaxMailID, @iserverindex, @idPlayer, '0000000', 0, @tmCreate, 0, @szTitle, @szText,@ItemID, @ItemNum, 0, 0, 0, 0, @bBinds
RETURN
END