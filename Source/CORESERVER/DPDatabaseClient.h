#ifndef __DPDATABASECLIENT_H__
#define	__DPDATABASECLIENT_H__

#include "DPMng.h"
#include "msghdr.h"
#include "misc.h"
#include "guild.h"

#undef	theClass
#define theClass	CDPDatabaseClient
#undef theParameters
#define theParameters	CAr & ar


class CDPDatabaseClient : public CDPMng
{
public:
	//	Constructions
	CDPDatabaseClient();
	virtual	~CDPDatabaseClient();
	//	Operations
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID dpId);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID dpId);

	void	SendCreateGuild(GUILD_MEMBER_INFO* info, int nSize, unsigned long idGuild, const char* szGuild);
	void	SendDestroyGuild(unsigned long idGuild, unsigned long idMaster);
	void	SendAddGuildMember(unsigned long idPlayer, unsigned long idGuild, unsigned long DoidPlayer);
	void	SendRemoveGuildMember(unsigned long idPlayer, unsigned long idGuild, unsigned long DoidPlayer);
	void	SendGuildMemberLv(unsigned long idPlayer, int nMemberLv);
	void	SendGuildClass(unsigned long idPlayer, int nClass);
	void	SendGuildNickName(unsigned long idPlayer, const char* strNickName);
	void	SendChgMaster(unsigned long idPlayer, unsigned long idPlayer2);
	void	SendGuildLogo(unsigned long idGuild, unsigned long dwLogo);
	void	SendGuildContribution(CONTRIBUTION_CHANGED_INFO& info);
	void	SendGuildNotice(unsigned long idGuild, const char* szNotice);
	void	SendGuildAuthority(unsigned long idGuild, unsigned long adwAuthority[]);
	void	SendGuildPenya(unsigned long idGuild, unsigned long adwPenya[]);
	void	SendGuildSetName(unsigned long uidGuild, char* szName);

	void	OnAddVoteResult(CAr& ar);

	void	SendAddVote(unsigned long idGuild, const char* szTitle,
		const char* szQuestion, char szSelections[4][MAX_BYTE_VOTESELECT]);
	void	SendRemoveVote(unsigned long idVote);
	void	SendCloseVote(unsigned long idVote);
	void	SendCastVote(unsigned long idVote, BYTE cbSelection);

	void	SendAcptWar(unsigned long idWar, unsigned long idDecl, unsigned long idAcpt);
	void	SendWarEnd(unsigned long idWar, unsigned long idDecl, unsigned long idAcpt, int nWptDecl, int nWptAcpt, int nType, CTime Time, int nWinPointDecl, int nWinPointAcpt, int nGetPointDecl, int nGetPointAcpt);
	void	SendSurrender(unsigned long idWar, unsigned long idPlayer, unsigned long idGuild);
	void	SendWarDead(unsigned long idWar, unsigned long idGuild);
	void	SendWarMasterAbsent(unsigned long idWar, unsigned long idGuild);
	void	SendSnoopGuild(unsigned long idGuild, unsigned long idPlayer, const char* lpszChat);
	void	OnUpdateGuildRankFinish(CAr& ar);
#ifdef __RT_1025
	void	QueryAddMessenger(unsigned long idPlayer, unsigned long idFriend);
	void	QueryDeleteMessenger(unsigned long idPlayer, unsigned long idFriend);
	void	QueryUpdateMessenger(unsigned long idPlayer, unsigned long idFriend, bool bBlock);
	void	OnRemovePlayerFriend(CAr& ar);
#else	// __RT_1025
	void	SendRemoveFriend(unsigned long uidPlayer, unsigned long uidFriend);
#endif	// __RT_1025
	void	SendAddPartyName(unsigned long uidPlayer, const char* sParty);
	void	SendTag(unsigned long idFrom, unsigned long idTo, const char* lpszString);
	void	SendCTWanted(BYTE byReqType, unsigned long idPlayer, int nGold, long nEnd, const char* szMsg);
#ifdef __SERVERLIST0911
	void	SendServerEnable(unsigned long uKey, long lEnable);
#endif	// __SERVERLIST0911

	USES_PFNENTRIES;
	void	OnPartyName(CAr& ar);
	void	OnGlobalData(CAr& ar);
	void	OnInsertTagResult(CAr& ar);
	void	OnDelPlayer(CAr& ar);
	void	OnGuildMemberTime(CAr& ar);
	void	OnBuyingInfo(CAr& ar);
	void	OnTCList(CAr& ar);
	void	OnSetPlayerName(CAr& ar);
#if __VER >= 11 // __SYS_PLAYER_DATA
	void	OnUpdatePlayerData(CAr& ar);
#endif	// __SYS_PLAYER_DATA
#ifdef __AUTO_NOTICE
	void	OnEventNotice(CAr& ar);
#endif // __AUTO_NOTICE
};


#endif	// __DPDATABASECLIENT_H__