#ifndef __PLAYER_H__
#define __PLAYER_H__

#pragma once

#include "mempooler.h"
#include "CMclCritSec.h"
#ifdef __RT_1025
#include "rtmessenger.h"
#else	// __RT_1025
#include "messenger.h"
#endif	// __RT_1025

enum TAG_RESULT
{
	TAG_NOTFRIEND,
	TAG_BLOCKED,
	TAG_OK
};

class CPlayer
{
public:
	CMclCritSec		m_AccessLock;
	unsigned long			m_dwSerial;
	DPID			dpid;
	DPID			dpidCache;
	DPID			dpidUser;
	unsigned long			uKey;						// uPlayerId
	char			lpszPlayer[MAX_PLAYER];		// name
	char			lpAddr[16];					// ip
	char			lpszAccount[MAX_ACCOUNT];	// account
#ifdef __RT_1025
	CRTMessenger	m_RTMessenger;
#else	// __RT_1025
	CMessenger		m_Messenger;
#endif	// __RT_1025
	unsigned long			m_uPartyId;
	unsigned long			m_idGuild;
	unsigned long			m_idWar;
	unsigned long			m_uIdofMulti;
	CTime			m_tGuildMember;
	unsigned long			m_idSnoop;

public:
	CPlayer(unsigned long idPlayer, const char* pszPlayer, const char* pszAccount);
	virtual	~CPlayer();

	void		Lock() { m_AccessLock.Enter(); }
	void		Unlock() { m_AccessLock.Leave(); }
	TAG_RESULT	IsTagSendable(unsigned long idTo);

public:
#ifndef __VM_0820
#ifndef __MEM_TRACE
	static	MemPooler<CPlayer>* m_pPool;
	void* operator new(size_t nSize) { return CPlayer::m_pPool->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CPlayer::m_pPool->Alloc(); }
	void	operator delete(void* lpMem) { CPlayer::m_pPool->Free((CPlayer*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CPlayer::m_pPool->Free((CPlayer*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820
};


typedef map< unsigned long, CPlayer* >		ULONG2PTR;

class CPlayerMng
{
public:
	CMclCritSec				m_AddRemoveLock;
	map< unsigned long, CPlayer*>	m_players;			// serial value key
	ULONG2PTR				m_ulong2;			// player id key
	set<unsigned long>				m_set;				// set of operators
	unsigned long					m_uCount;

public:
	CPlayerMng();
	virtual	~CPlayerMng();

	void		Free(void);
	bool		AddCache(DPID dpidCache);
	bool		RemoveCache(DPID dpidCache);
	unsigned long		GetCache(DPID dpidCache);
	bool		AddPlayer(unsigned long idPlayer, const char* lpszPlayer, const char* lpszAccount);
	void		RemovePlayer(CPlayer* pPlayer, bool bNotify = true);
	bool		RegisterPlayerInfo(CPlayer* pPlayer);
	bool		UnregisterPlayerInfo(CPlayer* pPlayer, bool bNotify);
	CPlayer* GetPlayerBySerial(unsigned long dwSerial);
	CPlayer* GetPlayerBySocket(DPID dpidSocket);
	CPlayer* GetPlayer(unsigned long uKey);
	unsigned long		GetCount(void);
	void		PackName(CAr& ar);
	void		AddItToSetofOperator(unsigned long uPlayerId);
	bool	IsOperator(unsigned long idPlayer);
#if __VER >= 11 // __SYS_PLAYER_DATA
	void	Logout(CPlayer* pPlayer);
#endif	// __SYS_PLAYER_DATA
};


#endif	// __PLAYER_H__