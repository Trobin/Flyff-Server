#ifndef __WANTEDLIST__H
#define __WANTEDLIST__H

#include "misc.h"		// WANTED_ENTRY
#include "ar.h"

typedef std::map<unsigned long, WANTED_ENTRY*>	WANTED_ENTRY_LIST;


// 포상금 리스트객체 
class CWantedList
{
public:
	virtual ~CWantedList();

protected:
	WANTED_ENTRY_LIST		m_wantedList;
	CMclCritSec				m_AccessLock;
	__int64					m_nMinGold;			// 표시 리스트의 최소 현상금 

public:
	static CWantedList& GetInstance();
	void					Write(CAr& ar);
	void					Read(CAr& ar);
	bool					SetEntry(unsigned long idPlayer, const char* szPlayer, int nGold, const char* szMsg);
	__int64					GetReward(unsigned long idPlayer, LPTSTR lpszReward, bool& bUpdatable);
	void					Rename(unsigned long idPlayer, const char* lpszPlayer);

protected:
	CWantedList();
	bool					IsUpdatable(__int64 nGold);
};


#endif // __WANTEDLIST__H