#ifndef __DPCACHESRVR_H__
#define __DPCACHESRVR_H__

#pragma once

#include "DPMng.h"
#include "ServerDesc.h"
#include "Player.h"
#include "guild.h"

#if __VER >= 11 // __SYS_PLAYER_DATA
#include "playerdata.h"
#endif	// __SYS_PLAYER_DATA

#undef	theClass
#define theClass	CDPCacheSrvr
#undef	theParameters
#define theParameters	CAr & ar, DPID, DPID, unsigned long

class CGuild;

class CDPCacheSrvr : public CDPMng
{
private:
	CServerDescArray	m_apServer;

public:
	//	Constructions
	CDPCacheSrvr();
	virtual	~CDPCacheSrvr();
	//	Overrides
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	//	Operations
	void	SendProcServerList(DPID dpid);

	void	SendHdr(unsigned long dwHdr, DPID dpidCache, DPID dpidUser);
	void	SendWhisper(const char* sPlayerFrom, const char* sPlayerTo, unsigned long idFrom, unsigned long idTo, const char* lpString, CPlayer* pTo, int nSearch = 0);
	void	SendSay(const char* sPlayerFrom, const char* sPlayerTo, unsigned long idFrom, unsigned long idTo, const char* lpString, CPlayer* pTo, int nSearch = 0);
	void	SendGMSay(const char* sPlayerFrom, const char* sPlayerTo, const char* lpString, CPlayer* pTo);
	void	SendFriendState(CPlayer* pTo);
	void	SendFriendJoin(CPlayer* pTo, CPlayer* pFriend);
	void	SendFriendLogOut(CPlayer* pTo, unsigned long uidPlayer);
	void	SendSetFriendState(CPlayer* pTo);
	void	SendFriendNoIntercept(CPlayer* pTo, CPlayer* pFriend, int state);
	void	SendFriendIntercept(CPlayer* pPlayer, CPlayer* pFriend);
	void	SendFriendIntercept(CPlayer* pPlayer, unsigned long uFriendid);
	void	SendFriendNoIntercept(CPlayer* pTo, unsigned long uFriendid, int state);
	void	SendOneFriendState(CPlayer* pTo, unsigned long uidPlayer, unsigned long dwState);
	void	SendModifyMode(unsigned long dwMode, BYTE f, unsigned long idFrom, CPlayer* pTo);
#ifdef __LAYER_1015
	void	SendSummonPlayer(unsigned long idOperator, unsigned long uIdofMulti, unsigned long dwWorldID, const D3DXVECTOR3& vPos, CPlayer* pPlayer, int nLayer);
#else	// __LAYER_1015
	void	SendSummonPlayer(unsigned long idOperator, unsigned long uIdofMulti, unsigned long dwWorldID, const D3DXVECTOR3& vPos, CPlayer* pPlayer);
#endif	// __LAYER_1015
	void	SendTeleportPlayer(unsigned long idOperator, CPlayer* pPlayer);
	void	SendKillPlayer(CPlayer* pPlayer);
	void	SendGetPlayerAddr(const char* lpszPlayer, const char* lpAddr, CPlayer* pOperator);
	void	SendGetPlayerCount(unsigned short uCount, CPlayer* pOperator);
	void	SendGetCorePlayer(CPlayer* pOperator);
	void	SendSystem(const char* lpString);
	void	SendCaption(const char* lpString, unsigned long dwWorldId, bool bSmall);

	void	SendDefinedText(int dwText, DPID dpidCache, DPID dpidUser, LPCSTR lpszFormat, ...);
	void	SendErrorParty(unsigned long dwError, CPlayer* pPlayer);
	void	SendAddFriend(unsigned long uLeader, unsigned long uMember, long nLeaderJob, BYTE nLeaderSex, char* szLeaderName, CPlayer* pMember);
	void	SendAddFriendNotFound(char* szMemberName, CPlayer* pLeader);
	void	SendBlock(BYTE nGu, char* szName, CPlayer* pTo);
	void	SendTagResult(CPlayer* pPlayer, BYTE cbResult);
	void	SendGameRate(float fRate, BYTE nFlag);

	void	SendSetPlayerName(unsigned long idPlayer, const char* lpszPlayer);
	void	SendSnoop(const char* lpszString, CPlayer* pSnoop);

#if __VER >= 11 // __SYS_PLAYER_DATA
	void	SendUpdatePlayerData(unsigned long idPlayer, PlayerData* pPlayerData, CPlayer* pTo);
	void	SendLogout(unsigned long idPlayer, CPlayer* pTo);
#endif	// __SYS_PLAYER_DATA

#ifdef __QUIZ
	void	SendQuizSystemMessage(int nDefinedTextId, bool bAll, int nChannel, int nTime);
#endif // __QUIZ

	USES_PFNENTRIES;
	//	Handlers
	void	OnAddConnection(DPID dpid);
	void	OnRemoveConnection(DPID dpid);

	void	OnAddPlayer(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnQueryRemovePlayer(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);

	void	OnAddPartyMember(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnRemovePartyMember(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnPartyChangeTroup(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnPartyChangeName(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnPartyChangeItemMode(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnPartyChangeExpMode(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);

	void	OnAddFriend(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnGetFriendState(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnSetFrinedState(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnFriendInterceptState(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnRemoveFriend(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
public:
	void	SendUpdateGuildRank();
	void	OnDestroyGuild(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnAddGuildMember(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnRemoveGuildMember(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnGuildMemberLv(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnGuildAuthority(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnGuildPenya(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnGuildSetName(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnGuildRank(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnGuildClass(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnGuildNickName(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnChgMaster(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	SendAddGuildMember(const GUILD_MEMBER_INFO& info, const char* lpszPlayer, unsigned long idGuild, CPlayer* pPlayer);
public:
	void	SendRemoveGuildMember(unsigned long idPlayer, unsigned long idGuild, CPlayer* pPlayer);
	//private:
	void	SendGuildMemberLv(unsigned long idPlayer, int nMemberLv, CPlayer* pPlayer);
	void	SendGuildSetName(unsigned long idGuild, const char* szName);
	void	SendGuildClass(unsigned long idPlayer, int nClass, CPlayer* pPlayer);
	void	SendGuildNickName(unsigned long idPlayer, const char* strNickName, CPlayer* pPlayer);
	void	SendChgMaster(unsigned long idPlayer, unsigned long idPlayer2, CPlayer* pPlayer);
public:
	void	SendGuild(CGuild* pGuild, CPlayer* pPlayer);
	void	SendGuildChat(const char* lpszPlayer, const char* sChat, CPlayer* pPlayer, OBJID objid);
	void	SendGuildMemberLogin(CPlayer* pTo, BYTE nLogin, unsigned long uPlayerId, unsigned long uMultiNo);
	void	SendGuildMemberGameJoin(CPlayer* pTo, int nMaxLogin, unsigned long uLoginPlayerId[], unsigned long uLoginGuildMulti[]);
	void	SendGuildError(CPlayer* pTo, int nError);
	void	SendGuildRank(CPlayer* pTo);
	void	SendGuildMsgControl(GUILD_MSG_HEADER* pHeader, unsigned long pPenya, BYTE cbCloak);

private:
	void	SendDeclWar(unsigned long idDecl, const char* pszMaster, CPlayer* pPlayer);
	void	SendAcptWar(unsigned long idWar, unsigned long idDecl, unsigned long idAcpt);
	void	SendSurrender(unsigned long idWar, CGuild* pDecl, CGuild* pAcpt, unsigned long idPlayer, const char* sPlayer, bool bDecl);	// 1
	void	SendSurrender(unsigned long idWar, unsigned long idPlayer, const char* sPlayer, bool bDecl, CPlayer* pPlayer);	// 2
	void	OnDeclWar(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnAcptWar(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnSurrender(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnQueryTruce(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnAcptTruce(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long);
public:
	void	SendWarEnd(unsigned long idWar, int nWptDecl, int nWptAcpt, int nType);
	void	SendWarDead(unsigned long idWar, const char* lpszPlayer, bool bDecl, CPlayer* pPlayer);
	void	SendQueryTruce(CPlayer* pPlayer);
	void	SendBuyingInfo(PBUYING_INFO2 pbi2, CPlayer* pPlayer);

private:
	void	OnAddVote(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnRemoveVote(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnCloseVote(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnCastVote(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnPartyChangeLeader(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
	void	OnSendTag(CAr& ar, DPID dpidCache, DPID dpidUser, unsigned long uBufSize);
};

inline void CDPCacheSrvr::SendHdr(unsigned long dwHdr, DPID dpidCache, DPID dpidUser)
{
	BEFORESENDSOLE(ar, dwHdr, dpidUser);
	SEND(ar, this, dpidCache);
}

#endif	// __DPCACHESRVR_H__