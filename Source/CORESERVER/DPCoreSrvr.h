#ifndef __DPCORESRVR_H__
#define __DPCORESRVR_H__

#include "DPMng.h"
#include "msghdr.h"
#include "ServerDesc.h"
#include "ObjMap.h"
#include "guild.h"

#if __VER >= 14 // __INSTANCE_DUNGEON
#include "InstanceDungeonBase.h"
#endif // __INSTANCE_DUNGEON

#undef	theClass
#define theClass	CDPCoreSrvr
#undef	theParameters
#define theParameters	CAr & ar, DPID, DPID, DPID, unsigned long

class CDPCoreSrvr : public CDPMng
{
public:
	int					m_nGCState;
public:
	CServerDescArray	m_apSleepServer;
	CServerDescArray	m_apServer;	// active
#ifdef __STL_0402
	map<unsigned long, DPID>	m_toHandle;	// key to dpid
#else	// __STL_0402
	CMyMap<DPID>	m_toHandle;	// key to dpid
#endif	// __STL_0402
	CMclCritSec		m_AccessLock;
	CObjMap		m_objmap;
public:
	// Constructions
	CDPCoreSrvr();
	virtual	~CDPCoreSrvr();

	// Operations
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);

	void	SendRecharge(unsigned long uBlockSize, DPID dpid);
	void	SendDuplicate(unsigned long uWorldSrvr);
	void	SendUnify(unsigned long uWorldSrvr);
	void	SendQueryTickCount(unsigned long dwTime, DPID dpid, double dCurrentTime);
	//	Operator commands
	void	SendShout(unsigned long idPlayer, const char* lpString, DPID dpid);
	void	SendPlayMusic(unsigned long idmusic, unsigned long dwWorldID, DPID dpid);
	void	SendPlaySound(unsigned long idsound, unsigned long dwWorldID, DPID dpid);

#if __VER >= 12 // __JHMA_VER12_1	//12차 극단유료아이템
	void	SendSetPartyMode(unsigned long idParty, int nMode, bool bOnOff, long nPoint = 0, unsigned long dwSkillTime = 0);
#else // //12차 극단유료아이템
	void	SendSetPartyMode(unsigned long idParty, int nMode, bool bOnOff, long nPoint = 0);
#endif // //12차 극단유료아이템
	void	SendRemoveParty(unsigned long idParty, unsigned long idLeader, unsigned long idMember);
	void	SendPartyChangeTroup(unsigned long idParty, const char* szPartyName);
	void	SendPartyChangeName(unsigned long idParty, const char* szPartyName);
	void	SendPartyChangeItemMode(unsigned long idParty, int nItemMode);
	void	SendPartyChangeExpMode(unsigned long idParty, int nExpMode);

	void	SendAddPartyMember(unsigned long uPartyId, unsigned long idLeader, long nLeaderLevel, long nLeaderJob, BYTE nLeaderSex, unsigned long idMember, long nMemberLevel, long nMemberJob, BYTE nMemberSex);
	void	SendAddFriend(unsigned long uidSender, unsigned long uidFriend, BYTE nSenderSex, BYTE nFriendSex, long nSendJob, long nFriendJob);
	void	SendRemoveFriend(unsigned long uidSender, unsigned long uidFriend);
#ifdef __ENVIRONMENT_EFFECT
	void	SendEnvironmentEffect();
#else // __ENVIRONMENT_EFFECT
	void	SendEnvironmentSnow(bool bSnow);
	void	SendEnvironmentRain(bool bRain);
	void	SendEnvironment(bool bRain, bool bSnow);
#endif // __ENVIRONMENT_EFFECT
	void	SendPartyChat(unsigned long idParty, const char* lpName, const char* lpString, OBJID objid);
	void	SendSetPlayerName(unsigned long idPlayer, const char* lpszPlayer, unsigned long dwData, bool f);

	void	SendDestroyPlayer(unsigned long idPlayer, DPID dpid);

#ifdef __EVENT0913
	void	SendEvent0913(bool f);
#endif	// __EVENT0913
#ifdef __EVENT1206
	void	SendEvent1206(bool f);
#endif	// __EVENT1206
	void	SendEvent(unsigned long dwEvent);
	void	SendGuildCombatState(void);
	void	SendSetSnoop(unsigned long idPlayer, unsigned long idSnoop, bool bRelease);

	// Handlers
	USES_PFNENTRIES;
	void	OnAddConnection(CAr& ar, DPID dpid, DPID, DPID, unsigned long);
	void	OnRemoveConnection(DPID dpid);
	void	OnRecharge(CAr& ar, DPID dpid, DPID, DPID, unsigned long);
	void	OnJoin(CAr& ar, DPID dpid, DPID, DPID, unsigned long);		// idPlayer, szPlayer, bOperator

//	Operator commands
	void	OnWhisper(CAr& ar, DPID dpidFrom, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnSay(CAr& ar, DPID dpidFrom, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnModifyMode(CAr& ar, DPID dpidFrom, DPID dpidCache, DPID dpidUser, unsigned long);
	void	OnShout(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGMSay(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnPlayMusic(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnPlaySound(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnSummonPlayer(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnTeleportPlayer(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnKillPlayer(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGetPlayerAddr(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGetPlayerCount(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGetCorePlayer(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnSystem(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnCaption(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnAddPartyExp(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnRemovePartyPoint(CAr& ar, DPID, DPID, DPID, unsigned long);
#if __VER < 11 // __SYS_PLAYER_DATA
	void	OnMemberLevel(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnMemberJob(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnFriendChangeJob(CAr& ar, DPID, DPID, DPID, unsigned long);
#endif	// __SYS_PLAYER_DATA
	void	OnFallSnow(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnFallRain(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnStopSnow(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnStopRain(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnPartySkillUse(CAr& ar, DPID, DPID, DPID, unsigned long uBufSize);
	void	OnPartyChat(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnPartyLevel(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnLoadConstant(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGameRate(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnSetMonsterRespawn(CAr& ar, DPID, DPID, DPID, unsigned long);

	void    OnGuildLogo(CAr& ar, DPID, DPID, DPID, unsigned long);
	void    OnGuildContribution(CAr& ar, DPID, DPID, DPID, unsigned long);
	void    OnGuildNotice(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGuildRealPenya(CAr& ar, DPID, DPID, DPID, unsigned long);
#if __VER < 11 // __SYS_PLAYER_DATA
	void	OnChangeGuildJobLevel(CAr& ar, DPID, DPID, DPID, unsigned long);
#endif	// __SYS_PLAYER_DATA

	void	OnSetSnoop(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnSetSnoopGuild(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnChat(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnPing(CAr& ar, DPID dpid, DPID, DPID, unsigned long);

	void	OnGCState(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGCRemoveParty(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGCAddParty(CAr& ar, DPID, DPID, DPID, unsigned long);

private:
	DPID	GetWorldSrvrDPID(unsigned long uWorldSrvr);
private:
	DPID	GetWorldSrvrDPID(unsigned long uIdofMulti, unsigned long dwWorldID, const D3DXVECTOR3& vPos);
	unsigned long	GetIdofMulti(DPID dpid);

public:
	void	OnGuildMsgControl(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnCreateGuild(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnGuildChat(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	SendCreateGuild(GUILD_MEMBER_INFO* pInfo, int nSize, unsigned long idGuild, const char* szGuild);
	void	SendDestroyGuild(unsigned long idGuild);
	void	SendAddGuildMember(const GUILD_MEMBER_INFO& info, unsigned long idGuild);
	void	SendRemoveGuildMember(unsigned long idMember, unsigned long idGuild);
	void	SendGuildMemberLv(unsigned long idPlayer, int nMemberLv, unsigned long idGuild);

	void	SendGuildClass(unsigned long idPlayer, int nClass, unsigned long idGuild);
	void	SendGuildNickName(unsigned long idPlayer, const char* strNickName, unsigned long idGuild);
	void	SendGuildMemberLogOut(unsigned long idGuild, unsigned long idPlayer);
	void	SendGuildAuthority(unsigned long uGuildId, unsigned long dwAuthority[]);
	void	SendGuildPenya(unsigned long uGuildId, unsigned long dwType, unsigned long dwPenya);
	void	SendChgMaster(unsigned long idPlayer, unsigned long idPlayer2, unsigned long idGuild);
	void	SendGuildSetName(unsigned long idGuild, const char* lpszGuild, unsigned long idPlayer = 0, BYTE nId = 0xff, BYTE nError = 0);

	void	SendAddVoteResult(VOTE_INSERTED_INFO& info);
	void	SendAddRemoveVote(unsigned long idGuild, unsigned long idVote);
	void	SendAddCloseVote(unsigned long idGuild, unsigned long idVote);
	void	SendAddCastVote(unsigned long idGuild, unsigned long idVote, BYTE cbSelection);

	void	SendAcptWar(unsigned long idWar, unsigned long idDecl, unsigned long idAcpt);
	void	SendWarDead(unsigned long idWar, bool bDecl);
	void	SendWarEnd(unsigned long idWar, int nWptDecl, int nWptAcpt, int nType);
	void	SendSurrender(unsigned long idWar, unsigned long idPlayer, bool bDecl);
	void	SendWarMasterAbsent(unsigned long idWar, bool bDecl);
	void	OnWarDead(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnWarMasterAbsent(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnWarTimeout(CAr& ar, DPID, DPID, DPID, unsigned long);

public:
	void	SendSetFriendState(unsigned long uidPlayer, unsigned long dwState);
	void	SendChangeLeader(unsigned long uPartyId, unsigned long uidChangeLeader);
	void	SendFriendInterceptState(unsigned long uIdPlayer, unsigned long uIdFriend);

	void	OnAddFriendNameReqest(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnBlock(CAr& ar, DPID, DPID, DPID, unsigned long);

	void	UpdateWantedList();
	void	SendCWWantedList(DPID dpid = DPID_ALLPLAYERS);
	void	OnWCWantedGold(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnWCWantedReward(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnSetPartyDuel(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnQuerySetGuildName(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	SendRemoveUser(unsigned long dwSerial);
#if __VER >= 14 // __INSTANCE_DUNGEON
public:
	void	SendInstanceDungeonAllInfo(int nType, CInstanceDungeonBase* pID, DPID dpId);
	void	SendInstanceDungeonCreate(int nType, unsigned long dwDungeonId, ID_INFO& ID_Info);
	void	SendInstanceDungeonDestroy(int nType, unsigned long dwDungeonId, ID_INFO& ID_Info);
	void	SendInstanceDungeonSetCoolTimeInfo(unsigned long uKey, int nType, unsigned long dwPlayerId, COOLTIME_INFO CT_Info);
	void	SendInstanceDungeonDeleteCoolTimeInfo(int nType, unsigned long dwPlayerId);
private:
	void	OnInstanceDungeonCreate(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnInstanceDungeonDestroy(CAr& ar, DPID, DPID, DPID, unsigned long);
	void	OnInstanceDungeonSetCoolTimeInfo(CAr& ar, DPID, DPID, DPID, unsigned long);
#endif // __INSTANCE_DUNGEON
#ifdef __QUIZ
private:
	void	OnQuizSystemMessage(CAr& ar, DPID, DPID, DPID, unsigned long);
#endif // __QUIZ
};

inline DPID CDPCoreSrvr::GetWorldSrvrDPID(unsigned long uWorldSrvr)
{
#ifdef __STL_0402
	map<unsigned long, DPID>::iterator i = m_toHandle.find(uWorldSrvr);
	if (i != m_toHandle.end())
		return i->second;
	return DPID_UNKNOWN;
#else	// __STL_0402
	DPID dpid;
	if (m_toHandle.Lookup(uWorldSrvr, dpid))
		return dpid;
	return DPID_UNKNOWN;
#endif	// __STL_0402
}

inline DPID CDPCoreSrvr::GetWorldSrvrDPID(unsigned long uIdofMulti, unsigned long dwWorldID, const D3DXVECTOR3& vPos)
{
	if (uIdofMulti == NULL_ID)
		return DPID_UNKNOWN;

#ifdef __STL_0402
	for (CServerDescArray::iterator i = m_apServer.begin(); i != m_apServer.end(); ++i)
	{
		CServerDesc* pServer = i->second;
		if (pServer->GetIdofMulti() == uIdofMulti && pServer->IsUnderJurisdiction(dwWorldID, vPos))
			return (DPID)i->first;
	}
	return DPID_UNKNOWN;
#else	// __STL_0402
	CMyBucket<CServerDesc*>* pBucket = m_apServer.GetFirstActive();
	while (pBucket)
	{
		CServerDesc* pServer = pBucket->m_value;
		if (pServer->GetIdofMulti() == uIdofMulti && pServer->IsUnderJurisdiction(dwWorldID, vPos))
			return (DPID)pBucket->m_dwKey;
		pBucket = pBucket->pNext;
	}
	return DPID_UNKNOWN;
#endif	// __STL_0402

}

inline unsigned long CDPCoreSrvr::GetIdofMulti(DPID dpid)
{
#ifdef __STL_0402
	CServerDescArray::iterator i = m_apServer.find(dpid);
	if (i != m_apServer.end())
		return i->second->GetIdofMulti();
	return NULL_ID;
#else	// __STL_0402
	CServerDesc* pServerDesc;
	if (m_apServer.Lookup(dpid, pServerDesc))
		return pServerDesc->GetIdofMulti();
	return NULL_ID;
#endif	// __STL_0402
}
#endif	// __DPCORESRVR_H__