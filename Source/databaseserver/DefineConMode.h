#define CONVER_ITEM_START	(unsigned long)0x00000001	// 아이템 컨버트 시작( 아래것을 사용하려면 꼭 있어야함
#define SAVE_TEXT			(unsigned long)0x00000002	// 퀘리문으로 안돌리고 Text에 쓰기
#define REMOVE_ITEM_ALL		(unsigned long)0x00000004	// 모든 아이템 지우기
#define REMOVE_ITEM_ALL_PAY	(unsigned long)0x00000008	// 아이템 값 지불
#define PIERCING_CONFIRM	(unsigned long)0x00000010	// 피어싱 확인