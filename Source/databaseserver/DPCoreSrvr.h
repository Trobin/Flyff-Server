#ifndef __DPCORESRVR_H__
#define __DPCORESRVR_H__

#pragma once

#include "DPMng.h"
#include "MsgHdr.h"
#include "guild.h"

#undef	theClass
#define theClass	CDPCoreSrvr
#undef	theParameters
#define theParameters	CAr & ar, LPBYTE lpBuf, unsigned long uBufSize

#if __VER >= 11 // __SYS_PLAYER_DATA
#include "playerdata.h"
#endif	// __SYS_PLAYER_DATA

class CDPCoreSrvr : public CDPMng
{
public:
	//	Constructions
	CDPCoreSrvr();
	virtual	~CDPCoreSrvr();

	//	Operations
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID dpid);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID dpid);

	void	SendPartyName(DPID dpid);
	void	SendTagResult(unsigned long idFrom, BYTE cbResult);
	void	OnAddConnection(DPID dpid);
	void	OnRemoveConnection(DPID dpid);
	void	SendAddVoteResult(VOTE_INSERTED_INFO& info);
	void	SendDelPlayer(unsigned long idPlayer, unsigned long idGuild);
	void	SendMemberTime(unsigned long idPlayer, const char* tGuildMember);
	void	SendBuyingInfo(PBUYING_INFO2 pbi2);	// 3
	void	SendSetPlayerName(unsigned long idPlayer, const char* lpszPlayer, unsigned long dwData, bool f);

#if __VER >= 11 // __SYS_PLAYER_DATA
	void	SendUpdatePlayerData(unsigned long idPlayer, PlayerData* pPlayerData);
#endif	// __SYS_PLAYER_DATA
#ifdef __AUTO_NOTICE
	void	SendEventLuaNotice();
#endif // __AUTO_NOTICE
#ifdef __GUILD_HOUSE_MIDDLE
	void	SendNoticeMessage(const char* szNotice);
#endif // __GUILD_HOUSE_MIDDLE

	//	Handlers	
	USES_PFNENTRIES;

	void	OnCreateGuild(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnDestroyGuild(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnAddGuildMember(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRemoveGuildMember(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildMemberLv(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildClass(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildNickName(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnChgMaster(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildLogo(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildNotice(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnAddVote(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRemoveVote(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnCloseVote(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnCastVote(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildPenya(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildAuthority(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildSetName(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);

	void	OnAcptWar(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnWarEnd(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSurrender(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnWarDead(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnWarMasterAbsent(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
#ifdef __RT_1025
	void	OnAddMessenger(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnDeleteMessenger(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnUpdateMessenger(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
public:
	void	SendRemovePlayerFriend(unsigned long uPlayerId, unsigned long uFriendId);
private:
#else	// __RT_1025
	void	OnRemoveFriend(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
#endif	// __RT_1025
	void	OnAddPartyName(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnTag(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnWanted(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSnoopGuild(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
#ifdef __SERVERLIST0911
	void	OnServerEnable(CAr& ar, LPBYTE lpBuf, unsigned long uBufSize);
#endif	// __SERVERLIST0911
};

#endif	// __DPLOGINSRVR_H__