// CampusDBCtrl.h: interface for the CCampusDBCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAMPUSDBCTRL_H__D25D11CF_A207_47BA_A35B_5AD4D63DAAB9__INCLUDED_)
#define AFX_CAMPUSDBCTRL_H__D25D11CF_A207_47BA_A35B_5AD4D63DAAB9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if __VER >= 15 // __CAMPUS
#include "dbcontroller.h"
#include "Campus.h"

//////////////////////////////////////////////////////////////////////
// CCampusDBCtrl Construction/Destruction
//////////////////////////////////////////////////////////////////////

enum
{
	CAMPUS_LOAD,
	CAMPUS_SEND,
	CAMPUS_ADD_MEMBER,
	CAMPUS_REMOVE_MEMBER,
	CAMPUS_UPDATE_POINT,
};

class CCampusDBCtrl : public CDbController
{
public:

	CCampusDBCtrl();
	virtual ~CCampusDBCtrl();

	virtual void Handler(LPDB_OVERLAPPED_PLUS pov, unsigned long dwCompletionKey);

private:
	void	CreateLogQuery();

	void	AddCampusMember(CAr& ar);
	void	RemoveCampusMember(CAr& ar);
	void	UpdateCampusPoint(CAr& ar);

	void	LoadAllCampus();
	void	SendAllCampus(DPID dpId);
	void	InsertCampus(unsigned long idCampus);
	void	DeleteCampus(unsigned long idCampus);
	void	InsertCampusMember(unsigned long idCampus, unsigned long idPlayer, int nMemberLv);
	void	DeleteCampusMember(unsigned long idPlayer, int nMemberLv);
	int		UpdateCampusPoint(unsigned long idPlayer, int nCampusPoint);
	void	UpdateCampusId(unsigned long idPlayer, unsigned long idCampus);

	void	LogUpdateCampusMember(unsigned long idCampus, unsigned long idMaster, unsigned long idPupil, char chState);
	void	LogUpdateCampusPoint(unsigned long idPlayer, int nPrevPoint, int nCurrPoint, char chState);

	CQuery* m_pLogQuery;
};

//////////////////////////////////////////////////////////////////////
// CCampusHelper Construction/Destruction
//////////////////////////////////////////////////////////////////////

class CCampusHelper
{
public:
	CCampusHelper();
	~CCampusHelper();
	static CCampusHelper* GetInstance();

	bool PostRequest(int nQuery, BYTE* lpBuf = NULL, int nBufSize = 0, unsigned long dwCompletionKey = 0)
	{
		return m_CampusDBCtrl.PostRequest(nQuery, lpBuf, nBufSize, dwCompletionKey);
	}

	void	Serialize(CAr& ar) { m_pCampusMng->Serialize(ar); }
	unsigned long	AddCampus(CCampus* pCampus) { return m_pCampusMng->AddCampus(pCampus); }
	bool	RemoveCampus(unsigned long idCampus) { return m_pCampusMng->RemoveCampus(idCampus); }
	bool	IsEmpty() { return m_pCampusMng->IsEmpty(); }

	CCampus* GetCampus(unsigned long idCampus) { return m_pCampusMng->GetCampus(idCampus); }

	void	Clear() { m_pCampusMng->Clear(); }
	bool	AddPlayerId2CampusId(unsigned long idPlayer, unsigned long idCampus) { return m_pCampusMng->AddPlayerId2CampusId(idPlayer, idCampus); }
	void	RemovePlayerId2CampusId(unsigned long idPlayer) { m_pCampusMng->RemovePlayerId2CampusId(idPlayer); }
	unsigned long	GetCampusIdByPlayerId(unsigned long idPlayer) { return m_pCampusMng->GetCampusIdByPlayerId(idPlayer); }

	int		GetMaxPupilNum(int nCampusPoint);

private:
	CCampusDBCtrl	m_CampusDBCtrl;
	CCampusMng* m_pCampusMng;
};

#endif // __CAMPUS
#endif // !defined(AFX_CAMPUSDBCTRL_H__D25D11CF_A207_47BA_A35B_5AD4D63DAAB9__INCLUDED_)