#ifndef __DPDATABASESRVR_H__
#define __DPDATABASESRVR_H__

#pragma once

#include "DPMng.h"
#include "MsgHdr.h"

#if __VER >= 12 // __LORD
#include "lord.h"
#endif	// __LORD

#undef	theClass
#define theClass	CDPTrans
#undef	theParameters
#define theParameters	CAr & ar, DPID, DPID, DPID, LPBYTE lpBuf, unsigned long uBufSize

class CMail;
class CMailBox;

#if __VER >= 11 // __SYS_PLAYER_DATA
#include "playerdata.h"
#endif	// __SYS_PLAYER_DATA

#if __VER >= 13 // __HOUSING
#include "Housing.h"
#endif // __HOUSING

#if __VER >= 12 // __LORD
class CLordSkill;
#endif	// __LORD
#if __VER >= 13 // __COUPLE_1117
class CCoupleMgr;
#endif	// __COUPLE_1117

#ifdef __QUIZ
#include "Quiz.h"
#endif // __QUIZ
class CDPTrans : public CDPMng
{
public:
	//	Constructions
	CDPTrans();
	virtual	~CDPTrans();
	//	Overrides
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	//	Operations
	void	SendAllGuildCombat(DPID dpId);

	static	CDPTrans* GetInstance(void);
#if __VER >= 11 // __SYS_PLAYER_DATA
	void	SendAllPlayerData(DPID dpid);
	void	SendAddPlayerData(unsigned long idPlayer, PlayerData* pPlayerData);
	void	SendDeletePlayerData(unsigned long idPlayer);
	void	SendUpdatePlayerData(unsigned long idPlayer, PlayerData* pPlayerData);
#else	// __SYS_PLAYER_DATA
	//{{AFX
	void	SendAllPlayerID(DPID dpId);
	void	SendPlayerID(unsigned long idPlayer, const char* lpszPlayer);
	void	SendRemovePlayerID(unsigned long idPlayer);
	//}}AFX
#endif	// __SYS_PLAYER_DATA
	void	SendHdr(unsigned long dwHdr, DPID dpid);

	void	SendUpdateGuildRankFinish();
	void	SendBaseGameSetting(bool bFirst, DPID dpid);
	void	SendMonsterRespawnSetting(int nRemoveSize, int nRemoveRespawn[], DPID dpid);
	void	SendMonsterPropSetting(bool bFirst, DPID dpid);
	void	SendGMChat(int nCount);
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	void	SendGC1to1Open(void);
	void	SendGC1to1TenderGuild(vector<CGuildCombat1to1Mng::__GC1TO1TENDER>& vecT, vector<CGuildCombat1to1Mng::__GC1TO1TENDER>& vecF, DPID dpId);
#endif // __GUILD_COMBAT_1TO1

	void	OnQueryMailBox(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);


	//////////////////////////////////////////////////////////////////////////
	void	OnQueryMailBoxCount(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnQueryMailBoxReq(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendMailBoxReq(unsigned long idReceiver, DPID dpid, bool bHaveMailBox, CMailBox* pMailBox);
	//////////////////////////////////////////////////////////////////////////


	void	SendMailBox(CMailBox* pMailBox, DPID dpid);
	void	OnQueryPostMail(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnQueryRemoveMail(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnQueryGetMailItem(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnQueryGetMailGold(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnReadMail(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendAllMail(DPID dpid);
	void	SendPostMail(bool bResult, unsigned long idReceiver, CMail* pMail);
	void	SendRemoveMail(unsigned long idReceiver, unsigned long nMail);
	void	SendGetMailItem(unsigned long idReceiver, unsigned long nMail, unsigned long uQuery);
	void	SendGetMailGold(unsigned long idReceiver, unsigned long nMail, unsigned long uQuery);
	void	SendReadMail(unsigned long idreceiver, unsigned long nMail);
	void	SendEventFlag(unsigned long dwFlag);
	void	SendEventGeneric(DPID dpid);

#if __VER >= 9 // __EVENTLUA
	void	SendEventLuaState(map<BYTE, bool> mapState, bool bLuaChanged, DPID dpid = DPID_ALLPLAYERS);
	void	SendEventLuaChanged(void);
	void	OnEventLuaChanged(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __EVENTLUA

#ifdef __S_RECOMMEND_EVE
	void	OnEveRecommend(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __S_RECOMMEND_EVE
	// 나중에 USES_PFNENTRIES; 밑으로 옮기자 
	void	OnSaveConcurrentUserNumber(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);

	USES_PFNENTRIES;
	//	Handlers
	void	OnRemoveConnection(DPID dpid);
	void	OnJoin(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSavePlayer(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#ifdef __S_NEW_SKILL_2
	void	OnSaveSkill(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __S_NEW_SKILL_2
	//
	void	OnLogItem(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogQuest(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);

	void	OnCalluspXXXMultiServer(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogExpBox(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);

	void	OnLogPlayConnect(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogPlayDeath(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogLevelUp(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogServerDeath(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogUniqueItem(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnChangeBankPass(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGammaChat(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogPkPvp(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogSchool(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnPing(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogSkillPoint(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// WorldServer에게 길드창고를 쿼리하라는 패킷을 받고 쿼리를 진행하는 함수
	void	OnQueryGuildBank(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildBankUpdate(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildRealPay(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildContribution(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnUpdateGuildRanking(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnUpdateGuildRankingDB(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);

	void	OnSchoolReport(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnBuyingInfo(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnOpenBattleServer(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnCloseBattleServer(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);

	void	OnQueryGuildQuest(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnInsertGuildQuest(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnUpdateGuildQuest(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);

	void	OnQuerySetPlayerName(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnItemTBLUpdate(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnPreventLogin(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);

#if __VER >= 9	// __PET_0410
	void	OnCalluspPetLog(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif	// __PET_0410

#if __VER >= 11 // __GUILD_COMBAT_1TO1
	void	OnGC1to1StateToDBSrvr(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGC1to1Tender(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGC1to1LineUp(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGC1to1WarPerson(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGC1to1WarGuild(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __GUILD_COMBAT_1TO1

#if __VER >= 11 // __MA_VER11_04	// 길드 창고 로그 기능 world,database,neuz
	void	OnGuildBankLogView(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif //__MA_VER11_04	// 길드 창고 로그 기능 world,database,neuz
#if __VER >= 11 // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz
	void	OnSealChar(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSealCharConm(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSealCharGet(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSealCharSet(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz

#if __VER >= 11 // __SYS_PLAYER_DATA
	void	OnUpdatePlayerData(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif	// __SYS_PLAYER_DATA

	void	OnQueryRemoveGuildBankTbl(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnCalluspLoggingQuest(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
public:
	void	OnGuidCombatInGuild(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuidCombatOutGuild(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnResultGuildCombat(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnStartGuildCombat(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGCGetPenyaGuild(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGCGetPenyaPlayer(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGCContinue(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendInGuildCombat(unsigned long idGuild, unsigned long dwPenya, unsigned long dwExistingPenya);
	void	SendOutGuildCombat(unsigned long idGuild);
	void	SendResultGuildCombat(void);
	void	SendPlayerPointGuildCombat(void);
	void	SendGetPenyaGuildGC(unsigned long uidPlayer, int nGuildCombatID, unsigned long uidGuild);
	void	SendGetPenyaPlayerGC(unsigned long uidPlayer, int nGuildCombatID, unsigned long uidGuild);
	void	SendContinueGC(void);
	void	SendQueryRemoveGuildBankTbl(int nNo, unsigned long idGuild, unsigned long dwItemId, SERIALNUMBER iSerialNumber, unsigned long dwItemNum);

#if __VER >= 12 // __LORD
public:
	// 월드 서버에 군주입찰 결과 전송
	void	SendElectionAddDeposit(unsigned long idPlayer, __int64 iDeposit, time_t tCreate, bool bRet);
	// 월드 서버에 공약 설정 결과 전송
	void	SendElectionSetPledge(unsigned long idPlayer, const char* szPledge, bool bRet);
	// 월드 서버에 투표 결과 전송
	void	SendElectionIncVote(unsigned long idPlayer, unsigned long idElector, bool bRet);
	// 월드 서버에 입후보 시작 상태 전송
	void	SendElectionBeginCandidacy(void);
	// 월드 서버에 투표 시작 상태 전송
	void	SendElectionBeginVote(int nRequirement);
	// 월드 서버에 투표 종료 상태 전송
	void	SendElectionEndVote(unsigned long idPlayer);
	// 모든 군주 시스템 정보를 월드 서버에 전송
	void	SendLord(DPID dpid);
	// 월드 서버에 군주 이벤트 현재 상태를 전송
	void	SendLEventCreate(CLEComponent* pComponent, bool bResult);
	// 월드 서버에 군주 이벤트 초기화를 전송
	void	SendLEventInitialize(void);
	// 월드 서버에 군주 스킬 사용 결과를 전송
	// 결과가 참이면 월드 서버는 실제 군주 스킬 효과를 적용
	void	SendLordSkillUse(unsigned long idPlayer, unsigned long idTarget, int nSkill, int nRet);
	// 해당 군주 스킬의 재사용 대기 시간을 전송
	void	SendLordSkillTick(CLordSkill* pSkills);
	// 해당 군주 이벤트의 남아있는 틱을 전송
	void	SendLEventTick(ILordEvent* pEvent);
private:
	// /군주입찰
	void	OnElectionAddDeposit(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /공약설정
	void	OnElectionSetPledge(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /군주투표
	void	OnElectionIncVote(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /군주이벤트시작
	void	OnLEventCreate(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /군주스킬
	void	OnLordSkillUse(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /군주프로세스
	void	OnElectionProcess(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /입후보시작
	void	OnElectionBeginCandidacy(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /투표시작
	void	OnElectionBeginVote(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /투표종료
	void	OnElectionEndVote(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	// /군주이벤트초기화
	void	OnLEventInitialize(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif	// __LORD

#if __VER >= 12 // __TAX
public:
	void	SendTaxInfo(DPID dpId, bool bConnect = false, bool bToAllClient = false);
private:
	void	OnSetSecretRoomWinGuild(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSetLord(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnTaxRate(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnAddTax(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnApplyTaxRateNow(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __TAX

#if __VER >= 12 // __SECRET_ROOM
public:
	void	SendSecretRoomInfoClear(DPID dpId);
	void	SendSecretRoomTenderInfo(BYTE nContinent, unsigned long dwGuildId, int nPenya, vector<unsigned long>& vecMemberId, DPID dpId);
private:
	void	OnSecretRoomInsertToDB(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSecretRoomUpdateToDB(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSecretRoomLineUpToDB(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnSecretRoomClosed(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __SECRET_ROOM

#if __VER >= 13 // __RAINBOW_RACE
public:
	void	SendRainbowRaceInfo(vector<unsigned long>& vec_dwNowPlayerId, vector<unsigned long>& vec_prevRanking, DPID dpId);

private:
	void	OnRainbowRaceLoadInfo(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRainbowRaceApplication(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRainbowRaceFailedUser(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRainbowRaceRanking(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __RAINBOW_RACE0

#if __VER >= 13 // __HOUSING
public:
	void	SendHousingLoadInfo(unsigned long dwPlayerId, CHousing* pHousing, DPID dpId);
	void	SendHousingFurnitureList(unsigned long dwPlayerId, HOUSINGINFO& housingInfo, bool bAdd, DPID dpId);
	void	SendHousingSetupFurniture(unsigned long dwPlayerId, HOUSINGINFO housingInfo, DPID dpId);
	void	SendHousingSetVisitAllow(unsigned long dwPlayerId, unsigned long dwTargetId, bool bAllow, DPID dpId);
	void	SendHousingDBFailed(unsigned long dwPlayerId, unsigned long dwItemId, DPID dpId);
private:
	void	OnHousingLoadInfo(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnHousingReqSetFurnitureList(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnHousingReqSetupFurniture(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnHousingReqSetVisitAllow(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnHousingReqGMRemoveAll(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __HOUSING

#if __VER >= 13 // __COUPLE_1117
public:
	void	OnPropose(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnCouple(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnDecouple(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnClearPropose(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendCouple(CCoupleMgr* pMgr, DPID dpid);
	void	SendProposeResult(unsigned long idProposer, unsigned long idTarget, int nResult, time_t t, DPID dpid);
	void	SendCoupleResult(unsigned long idProposer, unsigned long idTarget, int nResult);
	void	SendDecoupleResult(unsigned long idPlayer, int nResult);
#if __VER >= 13 // __COUPLE_1202
	void	OnQueryAddCoupleExperience(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendAddCoupleExperience(unsigned long idPlayer, int nExperience);
#endif	// __COUPLE_1202
#endif	// __COUPLE_1117

#if __VER >= 13 // __HONORABLE_TITLE			// 달인
	void	OnLogGetHonorTime(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif	// __HONORABLE_TITLE			// 달인

#ifdef __FUNNY_COIN
	void	OnFunnyCoinReqUse(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendFunnyCoinAckUse(unsigned long dwPlayerId, unsigned long dwItemId, SERIALNUMBER dwSerialNumber, int nResult, DPID dpId);
#endif // __FUNNY_COIN
#if __VER >= 14 // __PCBANG
	void	SendPCBangSetApply(DPID dpId);
	bool	m_bPCBangApply;
#endif // __PCBANG

#ifdef __VTN_TIMELIMIT
	void	OnTimeLimitInfoReq(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendTimeLimitAck(unsigned long dwPlayerId, int nPlayTime, DPID dpId);
	void	OnTimeLimitUpdate(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendTimeLimitReset();
#endif // __VTN_TIMELIMIT

#if __VER >= 14 // __INSTANCE_DUNGEON
	void	OnLogInstanceDungeon(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __INSTANCE_DUNGEON

#ifdef __QUIZ
	void	OnQuizEventOpen(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnQuizEventState(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnQuizEventEntrance(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnQuizEventSelect(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnPostPrizeItem(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	SendQuizEventOpen(DPID dpId);
	void	SendQuizList(DPID dpId, CQuiz::QUIZLIST& QL, int nSize);
	void	SendQuizEventNotice(DPID dpId);
	void	SendQuizEventChanged();
#endif // __QUIZ
#ifdef __ERROR_LOG_TO_DB
	void	OnErrorLog(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __ERROR_LOG_TO_DB
#if __VER >= 15 // __GUILD_HOUSE
	void	OnBuyGuildHouse(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildHousePacket(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnLogGuildFurniture(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#ifdef __GUILD_HOUSE_MIDDLE
	void	OnGuildHouseTenderJoin(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGuildHouseLevelUpdate(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
#endif // __GUILD_HOUSE_MIDDLE
#endif // __GUILD_HOUSE
#if __VER >= 15 // __CAMPUS
private:
	void	OnAddCampusMember(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRemoveCampusMember(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnUpdateCampusPoint(CAr& ar, DPID dpid, DPID dpidCache, DPID dpidUser, LPBYTE lpBuf, unsigned long uBufSize);
public:
	void	SendAllCampus(DPID dpId);
	void	SendAddCampusMember(unsigned long idCampus, unsigned long idMaster, unsigned long idPupil);
	void	SendRemoveCampusMember(unsigned long idCampus, unsigned long idPlayer);
	void	SendUpdateCampusPoint(unsigned long idPlayer, int nCampusPoint);
#endif // __CAMPUS
};

#endif	// __DPDATABASESRVR_H__