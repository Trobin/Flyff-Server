#pragma once

#if __VER >= 12 // __SECRET_ROOM
#include "dbcontroller.h"

enum {
	QUERY_SECRETROOM_LOAD = 0,
	QUERY_SECRETROOM_TENDER_INSERT,
	QUERY_SECRETROOM_TENDER_UPDATE,
	QUERY_SECRETROOM_LINEUP_INSERT,
	QUERY_SECRETROOM_CLOSED,
};

class CSecretRoomDbController : public CDbController
{
public:
	CSecretRoomDbController(void);
	~CSecretRoomDbController(void);

	virtual	void	Handler(LPDB_OVERLAPPED_PLUS pov, unsigned long dwCompletionKey);

private:
	void LoadSecretRoom(DPID dpId);
	void InsertTenderToDB(BYTE nContinent, unsigned long dwGuildId, int nPenya);
	void UpdateTenderToDB(BYTE nContinent, unsigned long dwGuildId, int nPenya, char chState, unsigned long dwWorldId, int nWarState, int nKillCount);
	void InsertLineUpMemberToDB(BYTE nContinent, unsigned long dwGuildId, vector<unsigned long>& vecMemberId);

	int m_nTimes;
};

class CSecretRoomDBMng
{
public:
	CSecretRoomDBMng(void);
	~CSecretRoomDBMng(void);
	static CSecretRoomDBMng* GetInstance(void);

	bool	PostRequest(int nQuery, BYTE* lpBuf = NULL, int nBufSize = 0, unsigned long dwCompletionKey = 0);

private:
	CSecretRoomDbController m_SRDbController;

};

#endif // __SECRET_ROOM