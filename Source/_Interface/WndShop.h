#ifndef __SHOP_H
#define __SHOP_H

class CWndItemCtrlVendor : public CWndItemCtrl
{
public:
	CWndItemCtrlVendor();
	~CWndItemCtrlVendor();
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);

};
class CWndConfirmBuy : public CWndNeuz
{
public:
	CWndConfirmBuy();
	~CWndConfirmBuy();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

private:
	void		OnOK();
	void		OnChangeBuyCount(unsigned long dwBuy);

public:
	CMover* m_pMover;
	CItemElem* m_pItemElem;
	unsigned long			m_dwItemId;
	CWndEdit* m_pEdit;
	CWndStatic* m_pStatic;
	CWndStatic* m_pStaticGold;
#if __VER >= 11 // __CSC_VER11_3
	int				m_nBuyType;
#endif //__CSC_VER11_3
};

class CWndWarning : public CWndNeuz
{
public:
	CMover* m_pMover;
	CItemElem* m_pItemElem;
	CWndWarning();
	~CWndWarning();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndConfirmSell : public CWndNeuz
{
public:
	CMover* m_pMover;
	CItemElem* m_pItemElem;
	CWndEdit* m_pEdit;
	CWndStatic* m_pStatic;
	CWndStatic* m_pStaticGold;
	CWndConfirmSell();
	~CWndConfirmSell();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
#if __VER >= 14 // __DROP_CONFIRM_BUG
	virtual bool Process(void);
#endif // __DROP_CONFIRM_BUG
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
class CWndShop : public CWndNeuz
{
public:
	CWndConfirmSell* m_pWndConfirmSell;
	CWndWarning* m_pWndWarning;
public:
	bool m_bSexSort;
	bool m_bLevelSort;

	CMover* m_pMover;
	CWndItemCtrlVendor m_wndItemCtrl[MAX_VENDOR_INVENTORY_TAB];
	CWndStatic m_static;
	CWndShop();
	~CWndShop();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnDestroyChildWnd(CWndBase* pWndChild);
	virtual	void OnDestroy(void);
};

#if __VER >= 8 //__CSC_VER8_4
class CWndBeautyShopConfirm;
#ifdef __NEWYEARDAY_EVENT_COUPON
class CWndUseCouponConfirm;
#endif //__NEWYEARDAY_EVENT_COUPON
#endif //__CSC_VER8_4

class CWndBeautyShop : public CWndNeuz
{
public:
	CTexture         m_Texture;
	CWndConfirmSell* m_pWndConfirmSell;
	CModelObject* m_pModel;

	unsigned long			 m_dwHairMesh;
	bool			 m_bLButtonClick;
	CRect			 m_ColorRect[3];
	float			 m_fColor[3];
	int              m_nHairCost;
#if __VER >= 8 //__CSC_VER8_4
	int				 m_nHairColorCost;
#else
	int              m_nHairColorCostR;
	int              m_nHairColorCostG;
	int              m_nHairColorCostB;
#endif //__CSC_VER8_4
	CPoint			 m_ColorScrollBar[3];
	CPoint			 m_OriginalColorScrollBar[3];

#ifdef __Y_BEAUTY_SHOP_CHARGE
	bool			 m_bChange;
#endif //__Y_BEAUTY_SHOP_CHARGE
#if __VER >= 8 //__CSC_VER8_4
	CModelObject* m_pApplyModel;
	CWndEdit* m_pRGBEdit[3];
	int				 m_ChoiceBar;
	CModelObject* m_pHairModel;
	unsigned long			 m_nHairNum[4];
	unsigned long			 m_dwSelectHairMesh;
	CWndBeautyShopConfirm* m_pWndBeautyShopConfirm;
#endif //__CSC_VER8_4

#ifdef __NEWYEARDAY_EVENT_COUPON
	bool m_bUseCoupon;
	CWndUseCouponConfirm* m_pWndUseCouponConfirm;
#endif //__NEWYEARDAY_EVENT_COUPON

public:
	CWndBeautyShop();
	~CWndBeautyShop();
	void    ReSetBar(float r, float g, float b);

	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	virtual void OnMouseWndSurface(CPoint point);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnDestroyChildWnd(CWndBase* pWndChild);
	virtual	void OnDestroy(void);

#if __VER >= 8 //__CSC_VER8_4
	void SetRGBToEdit(float color, int editnum);
	void SetRGBToBar(int editnum);
	void DrawHairKind(C2DRender* p2DRender, D3DXMATRIX matView);
	void UpdateModels();
#endif //__CSC_VER8_4

#ifdef __NEWYEARDAY_EVENT_COUPON
	void UseHairCoupon(bool isUse);
#endif //__NEWYEARDAY_EVENT_COUPON
};

#if __VER >= 8 //__CSC_VER8_4

#ifdef __NEWYEARDAY_EVENT_COUPON
class CWndUseCouponConfirm : public CWndNeuz
{
public:
	bool  m_bUseCoupon;
	unsigned long m_TargetWndId;
	bool  m_checkClose;
	int   m_MainFlag;
public:
	CWndUseCouponConfirm();
	virtual ~CWndUseCouponConfirm();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

	void SetInfo(unsigned long targetWndId, int flag);
};
#endif //__NEWYEARDAY_EVENT_COUPON

class CWndBeautyShopConfirm : public CWndNeuz
{
public:
	int m_ParentId;

public:
	CWndBeautyShopConfirm();
	virtual ~CWndBeautyShopConfirm();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndFaceShop : public CWndNeuz
{
public:
	CModelObject* m_pMainModel;
	CModelObject* m_pApplyModel;
	CModelObject* m_pFriendshipFace;
	CModelObject* m_pNewFace;

	unsigned long m_dwFriendshipFace;
	unsigned long m_dwNewFace;
	unsigned long m_nSelectedFace;
	unsigned long m_nFriendshipFaceNum[4];
	unsigned long m_nNewFaceNum[4];
	int m_nCost;
	int m_ChoiceBar;

	CWndBeautyShopConfirm* m_pWndBeautyShopConfirm;

#ifdef __NEWYEARDAY_EVENT_COUPON
	bool m_bUseCoupon;
	CWndUseCouponConfirm* m_pWndUseCouponConfirm;
#endif //__NEWYEARDAY_EVENT_COUPON

public:
	CWndFaceShop();
	~CWndFaceShop();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void OnDestroy(void);
	virtual void OnDestroyChildWnd(CWndBase* pWndChild);

	void DrawFaces(int ChoiceFlag, C2DRender* p2DRender, D3DXMATRIX matView);
	void UpdateModels();

#ifdef __NEWYEARDAY_EVENT_COUPON
	void UseFaceCoupon(bool isUse);
#endif //__NEWYEARDAY_EVENT_COUPON
};
#endif //__CSC_VER8_4

#endif