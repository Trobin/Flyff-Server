// WndArcane.h: interface for the CWndNeuz class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDMESSAGEBOX_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)
#define AFX_WNDMESSAGEBOX_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CWndMessageBox : public CWndNeuz
{
	CWndButton m_wndButton1;
	CWndButton m_wndButton2;
	CWndButton m_wndButton3;

protected:
	CWndText   m_wndText;
	unsigned int       m_nType;

public:
	CWndMessageBox();
	virtual ~CWndMessageBox();

	bool Create(const char* lpszMessage, unsigned int nType, const RECT& rect, unsigned int nID, CWndBase* pWndParent = NULL);
	virtual void OnEnter(unsigned int nChar);
	bool IsDisable();

	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool Initialize(const char* lpszMessage, CWndBase* pWndParent, unsigned long nType = MB_OK);
	//bool Initialize(CString strMessage,unsigned int nType = MB_OK);

	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	//	virtual bool OnCommand(unsigned int nID,unsigned long dwMessage);
};
class CWndMessageBoxUpper : public CWndNeuz
{
protected:
	CWndText   m_wndText;
	unsigned int       m_nType;
	bool	   m_bPostLogoutMsg;

public:
	CWndButton m_wndButton1;
	CWndButton m_wndButton2;
	CWndButton m_wndButton3;

	CWndMessageBoxUpper();
	~CWndMessageBoxUpper();

	bool Create(const char* lpszMessage, unsigned int nType, const RECT& rect, unsigned int nID, CWndBase* pWndParent = NULL);

	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool Initialize(const char* lpszMessage, CWndBase* pWndParent, unsigned long nType = MB_OK, bool bPostLogoutMsg = false);
	//bool Initialize(CString strMessage,unsigned int nType = MB_OK);

	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	//	virtual bool OnCommand(unsigned int nID,unsigned long dwMessage);
};
#define DECLARE_WNDMESSAGEBOX( class_name ) \
	class class_name : public CWndMessageBox \
	{ \
		public: class_name() { } ~class_name() { } \
		virtual bool Initialize( CWndBase* pWndParent = NULL, unsigned long nType = MB_OK ); \
		bool OnChildNotify(unsigned int message,unsigned int nID,LRESULT* pLResult); \
	};

#endif // !defined(AFX_WNDMESSAGEBOX_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)