#ifndef __FUNCTEXTCMD_H
#define __FUNCTEXTCMD_H

struct TextCmdFunc
{
	bool(*m_pFunc)(CScanner& scanner);
	TCHAR* m_pCommand;
	TCHAR* m_pAbbreviation;
	TCHAR* m_pKrCommand;
	TCHAR* m_pKrAbbreviation;
	unsigned long  m_nServer; // 0 = client, 1 = server, 2 = �Ѵ� 
	unsigned long m_dwAuthorization;
	TCHAR* m_pszDesc;
};
extern TextCmdFunc m_textCmdFunc[];

#endif