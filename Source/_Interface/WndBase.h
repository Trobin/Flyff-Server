// WndBase.h: interface for the CWndBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDBASE_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_)
#define AFX_WNDBASE_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*
#ifdef __CLIENT
#define CLIENT_WIDTH  ( g_Neuz.m_d3dsdBackBuffer.Width )
#define CLIENT_HEIGHT ( g_Neuz.m_d3dsdBackBuffer.Height )
#define RECT_CLIENT   CRect( 0, 0, CLIENT_WIDTH, CLIENT_HEIGHT )
#endif
*/

#define LAYOUT_NONE    0
#define LAYOUT_RULERS  1
#define LAYOUT_GRID    2

#define WSIZE_WINDOW   0
#define WSIZE_MIN      1
#define WSIZE_MAX      2

#include "WndStyle.h"

#define D3DCOLOR_TEMP(a,b,g,r) \
    ((D3DCOLOR)((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff))) 

// 클라이언트의 좌표 얻기 
#define GET_CLIENT_POINT( hwnd, pt ) CPoint pt; ::GetCursorPos( &pt ); ::ScreenToClient( hwnd, &pt );

// 좌표를 맞추기 위한 다이얼로그 : 사용하면 아주 유용함 
#define COORDI_DLG(c,x1,y1,x2,y2)	static CCoordiDlg c(x1,y1,x2,y2); if(c.m_hWnd == NULL) c.Create(CCoordiDlg::IDD);

class C2DRender;

#include "ResManager.h"

//_CHILD   
class CWndBase;

typedef struct tagWNDMESSAGE
{
	CWndBase* m_pWndBase;
	unsigned int      m_message;
	WPARAM    m_wParam;
	LPARAM    m_lParam;
} WNDMESSAGE, * LPWNDMESSAGE;

//////////////////////////////////////////////////////////////////////////////
// CWndBase 
//////////////////////////////////////////////////////////////////////////////

class CWndBase
{
	LPDIRECT3DVERTEXBUFFER9 m_pVB;

	static CPoint    m_pointOld;
	static CRect     m_rectOld;

public:
	static CWndBase* m_pWndCapture;
	//bool      m_bCapture;
	bool      m_bPush; // 포커스 윈도가 눌렸나?
	int       m_nResizeDir;

	//oid AddWnd(CWndBase* pWnd); 
		//void RemoveWnd(CWndBase* pWnd);
	void DestroyAllWnd(CWndBase* pWndRoot); // 모든 윈도를 강제 삭제 ; 종료할때 호출 
	void SetChildFocus(CWndBase* pWndBase, POINT point);
	CWndBase* GetChildFocus(CWndBase* pWndBase, POINT point);
	//protected:
public:
	static CResManager m_resMng;
	static CMapStringToPtr m_strWndTileMap;
	CD3DApplication* m_pApp;
	CTheme* m_pTheme;
	CWndBase* m_pParentWnd;
	CPtrArray m_wndArray;
	unsigned int      m_nIdWnd;
	bool      m_bVisible;
	CSize     m_sizeTile;
	unsigned long     m_dwStyle;
	bool      m_bLButtonUp;
	bool      m_bLButtonDown;
	bool      m_bRButtonDown;
	bool      m_bMButtonDown;
	bool	  m_bLButtonDowned;
	CString   m_strTitle;
	CString   m_strToolTip;
	C2DRender* m_p2DRender;
	CRect     m_rectCurrentWindow;
	CRect     m_rectCurrentClient;
	CRect     m_rectWindow; // Window ; 패어런트부터의 좌표 
	CRect     m_rectClient; // Client ; 패어런트부터의 좌표 ( 보통 에찌 때문에 m_rectWindow 보다는 조금 안쪽 좌표를 갖는다. )
	CRect     m_rectLayout; // Client ; 패어런트부터의 좌표 ( 보통 에찌 때문에 m_rectWindow 보다는 조금 안쪽 좌표를 갖는다. )
	CPoint    m_ptMouse;
	bool      m_bGroup;
	CD3DFont* m_pFont;
	bool      m_bAutoFree;
	bool      m_bEnable;
	CWndBase* m_pWndFocusChild;
	bool      m_bTabStop;
	bool      m_bDefault;
	bool      m_bActive;
	int       m_nToolTipPos;
	BYTE      m_byWndType;
	bool      m_bKeyButton;
	unsigned long     m_dwColor;
	int       m_nAlphaCount;
	bool      m_bTile;
	CString   m_strTexture;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	bool      m_bNoCloseButton;
#endif // __IMPROVE_QUEST_INTERFACE

	static bool           m_bCling;
	static bool           m_bEdit;
	static bool           m_bFullWnd;
	static CWndBase* m_pWndRoot;
	static CWndBase* m_pWndFocus; // 다이얼로그 윈도 포커스 
	static CWndBase* m_pCurFocus; // 다이얼로그, 차일드 중 최종 현재 포커스 

#ifndef __VS2003
	static CPtrArray      m_wndOrder;
#endif

	static CPtrArray      m_wndRemove;
	static CPtrArray      m_postMessage;
	//static CTexturePack   m_texturePack     ;
	static CTexture* m_pTexForbid;
	static CTimer         m_timerForbid;
	static bool           m_bForbid;
	static CPoint         m_ptForbid;
	static HWND           m_hWnd;
	static HCURSOR        m_hDefaultCursor;
	static CWndBase* m_pWndOnMouseMove;
	static CWndBase* m_pWndOnSetCursor;
	static int            m_nAlpha;
	static CTheme         m_Theme;

	static bool SetForbidTexture(LPDIRECT3DDEVICE9 pd3dDevice, const char* lpszFileName);
	static void SetForbid(bool bForbid);
	static bool IsForbid() { return m_pTexForbid && m_bForbid; }

	CTexture* m_pTexture;
	//	CString m_strTexture;

	bool IsPush() { return m_bPush; }
	CRect GetScreenRect();

public:
	static CTextureMng    m_textureMng;
	static SHORTCUT       m_GlobalShortcut;
	bool m_bPickup; // 포커스 윈도를 들었나?

	void AddWnd(CWndBase* pWnd);
	void RemoveWnd(CWndBase* pWnd);

	//	CString m_strSndEffect;
	int       m_nWinSize; // 0 = nomal, 1 - minimaize, 2 = maximize

	void FitTextureSize();
	static void FreeTileTexture();

	CWndBase();
	virtual ~CWndBase();
	bool Create(unsigned long dwStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void SetTexture(LPDIRECT3DDEVICE9 pd3dDevice, const char* lpszFileName, bool bMyLoader = false);
	void SetTexture(LPDIRECT3DDEVICE9 pd3dDevice, const char* lpKey, CTexture* m_pTexture);

	void RemoveDestroyWnd();
	void MoveParentCenter(); // 윈도를 부모 윈도의 중앙으로 옮긴다.
	void MoveRectCenter(CRect rect); // 윈도를 rect의 중앙으로 옮긴다.
	void MovePointCenter(CPoint pt); // 윈도를 pt의 중앙으로 옮긴다.
	void Move(CPoint pt); // 왼도의 left,top을 pt로 
	void Move(int x, int y) { Move(CPoint(x, y)); } // 왼도의 left,top을 pt로 
	bool IsVisible() { return m_bVisible; } // 윈도가 보이는 상태인가.
	void SetVisible(bool bVisible) { m_bVisible = (bVisible != false); } // 윈도를 보이거나 감춘다.
	void SetCapture();
	void ReleaseCapture();
	CRect  MakeCenterRect(int nWidth, int nHeight);
	CPoint GetClientCenter(); // Client의 중앙 좌표를 돌려준다.
	CPoint GetStrCenter(C2DRender* p2DRender, const char* str); // 화면 중심에서 스트링의 중앙에 출력하기 위한 좌표를 돌려준다.
	CPoint GetStrRectCenter(C2DRender* p2DRender, CRect rect, const char* str); // rect의 중심에서 스트링의 중앙에 출력하기 위한 좌표를 돌려준다.
	CString GetTitle() { return m_strTitle; }
	void SetTitle(CString strTitle) { m_strTitle = strTitle; }
	unsigned long GetStyle() { return m_dwStyle; }
	CWndBase* GetParentWnd() { return m_pParentWnd; }
	CRect GetClientRect(bool bParent = false);
	CRect GetWindowRect(bool bParent = false);
	CRect GetLayoutRect(bool bParent = false);
	CRect GetWndRect() { return m_rectWindow; }
	bool IsWndRoot() { return this == m_pWndRoot; }
	bool IsOnWndBase(CPoint pt);  // 포인트가 윈도 위에 있는가?
#ifndef __VS2003
	bool IsOpenAnywnd() { return m_wndOrder.GetSize() ? true : false; }
#endif
	bool IsOpenModalWnd();
	CWndBase* GetFocusChild() { return m_pWndFocusChild; }
	CWndBase* GetFocusWnd() { return m_pWndFocus; }
	void GetLogFont(C2DRender* p2DRender, LOGFONT* pLogFont);
	int  GetFontHeight();
	bool IsWndStyle(unsigned long dwStyle) { return (m_dwStyle & dwStyle) ? true : false; }
	int  GetWndStyle() { return m_dwStyle; }
	void SetWndStyle(unsigned long dwStyle) { m_dwStyle = dwStyle; }
	int  GetWndId() { return m_nIdWnd; }
	void SetWndId(unsigned long dwWndId) { m_nIdWnd = dwWndId; }
	void AddWndStyle(unsigned long dwStyle) { m_dwStyle |= dwStyle; }
	void DelWndStyle(unsigned long dwStyle) { m_dwStyle &= ~dwStyle; }
	CWndBase* GetWndBase(unsigned int idWnd);
	CWndBase* GetWndBase();
	CWndBase* FindWnd(unsigned int idWnd) { return GetWndBase(idWnd); }
	CWndBase* GetChildWnd(unsigned int nID);
	CWndBase* GetDlgItem(unsigned int nID)
	{
		CWndBase* pWnd = GetChildWnd(nID);
		if (pWnd == NULL)
			Error("GetDlgItem : nID=%d not Found.", nID);
		return pWnd;
	}
	bool IsOpenWnd(unsigned int nId);
	bool IsOpenWnd(CWndBase* pWnd);
	bool IsOpenWnd() { return m_nIdWnd ? true : false; }
	bool IsFocusWnd() { return m_pWndFocus == this; }
	bool IsFocusChild() { return m_pParentWnd ? m_pParentWnd->m_pWndFocusChild == this : false; }
	CPoint GetMousePoint() { return m_ptMouse; }
	void SetGroup(bool bGroup) { m_bGroup = bGroup; }
	bool IsGroup() { return m_bGroup; }
	CPtrArray* GetWndArray() { return &m_wndArray; }
	void SetFont(CD3DFont* pFont) { m_pFont = pFont; }
	CD3DFont* GetFont() { return m_pFont; }
	void SetAutoFree(bool bFree) { m_bAutoFree = (bFree != false); }
	bool IsAutoFree() { return m_bAutoFree; }
	bool IsParentWnd(CWndBase* pParentWnd);
	bool IsWindowEnabled() const { return m_bEnable; }
	void EnableWindow(bool bEnable = true) { m_bEnable = (bEnable != false); }
	LRESULT SendMessage(unsigned int message, WPARAM wParam = 0, LPARAM lParam = 0);
	bool PostMessage(unsigned int message, WPARAM wParam = 0, LPARAM lParam = 0);
	void  SetToolTip(const char* lpszToolTip, int nPos = 0) { m_strToolTip = lpszToolTip; m_nToolTipPos = nPos; }
	void SetTabStop(bool bTabStop) { m_bTabStop = (bTabStop != false); }
	virtual void SetFocus();
	void KillFocus(CWndBase* pWndFocus, CWndBase* pWndNew);
	void SetDefault(bool bDefault) { m_bDefault = (bDefault != false); }
	bool IsDefault() { return m_bDefault; }
	bool IsFullWnd() { return m_bFullWnd; }
	CWndBase* FindFullWnd();
	void GradationRect(C2DRender* p2DRender, CRect rect, unsigned long dwColor1t, unsigned long dwColor1b, unsigned long dwColor2b, int nMidPercent = 40);
	bool WindowToScreen(LPPOINT lpPoint);
	bool WindowToScreen(LPRECT lpRect);
	bool ClientToScreen(LPPOINT lpPoint);
	bool ClientToScreen(LPRECT lpRect);
	bool ScreenToWindow(LPPOINT lpPoint);
	bool ScreenToWindow(LPRECT lpRect);
	bool ScreenToClient(LPPOINT lpPoint);
	bool ScreenToClient(LPRECT lpRect);

	CWndBase* GetFrameWnd();
	bool AdjustMinRect(CRect* pRect, int nWidth, int nHeight);
	bool AdjustMaxRect(CRect* pRect, int nWidth, int nHeight);

	void SetWndType(int nWndType) { m_byWndType = (BYTE)nWndType; }
	unsigned long GetWndType() { return (unsigned long)m_byWndType; }
	LPWNDCTRL GetWndCtrl(unsigned long dwWndId) { return m_resMng.GetAtControl(m_nIdWnd, dwWndId); }
	LPWNDAPPLET GetWndApplet() { return m_resMng.GetAt(m_nIdWnd); }

	static void ClipStrArray(C2DRender* p2DRender, CRect rect, int nLineSpace,
		CStringArray* pStringArray, CStringArray* pNewStringArray);

	virtual void AlighWindow(CRect rcOld, CRect rcNew);

	bool IsDestroy();
	void Destroy(bool bAutoFree = false); // 윈도를 파괴한다.
	void PaintChild(C2DRender* p2DRender);
	void Paint(C2DRender* p2DRender, bool bPaintChild = true);
	void SetWndSize(int cx, int cy);
	virtual void OnMouseWndSurface(CPoint point);
	virtual void PaintRoot(C2DRender* p2DRender);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual int  GetResizeDir(CPoint ptClient);
	virtual bool IsPickupSpace(CPoint point);
	virtual	bool Process();
	virtual	void OnDraw(C2DRender* p2DRender);
	virtual	bool OnDrawIcon(CWndBase* pWndBase, C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual	void OnUpdate();
	virtual void OnDestroyChildWnd(CWndBase* pWndChild);
	virtual bool OnSetCursor(CWndBase* pWndBase, unsigned int nHitTest, unsigned int message);
	// message
//Protected:
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnMButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnMButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnNonClientLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnChar(unsigned int nChar);
	virtual void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnPaint();
	virtual void OnClose();
	virtual void OnDestroy();
	virtual void OnTimer(unsigned int nIDEvent);
	virtual void OnMove(int x, int y);
	virtual void OnSetFocus(CWndBase* pOldWnd);
	virtual void OnKillFocus(CWndBase* pNewWnd);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);

public:
	virtual bool OnSystemNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnParentNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnChildNotify(unsigned int nCode, unsigned int nID, LRESULT* pLResult);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	///virtual LRESULT WndMsgProc( unsigned int message, WPARAM wParam, LPARAM lParam );

	LRESULT WindowRootProc(unsigned int message, WPARAM wParam, LPARAM lParam);
#ifdef __V050823_JAPAN_ATOK
	virtual LRESULT WindowProc(unsigned int message, WPARAM wParam, LPARAM lParam);
#else
	LRESULT WindowProc(unsigned int message, WPARAM wParam, LPARAM lParam);
#endif
	virtual LRESULT DefWindowProc(unsigned int message, WPARAM wParam, LPARAM lParam);

	//virtual HRESULT OneTimeSceneInit();
	virtual HRESULT InitDeviceObjects();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
	//virtual HRESULT FinalCleanup() { return S_OK; }
	//virtual HRESULT Render(); 
	//virtual HRESULT FrameMove();
	virtual void AfterSkinTexture(LPWORD pDest, CSize size, D3DFORMAT d3dFormat = D3DFMT_A4R4G4B4);
	virtual void AdditionalSkinTexture(LPWORD pDest, CSize size, D3DFORMAT d3dFormat = D3DFMT_A4R4G4B4);
	virtual void AdjustWndBase(D3DFORMAT d3dFormat = D3DFMT_A4R4G4B4);

	void MakeVertexBuffer();
	void RenderWnd();

	friend class CWndButton;
};
#endif // !defined(AFX_WNDBASE_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_)