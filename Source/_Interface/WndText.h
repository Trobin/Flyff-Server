#ifndef __WNDTEXT__H
#define __WNDTEXT__H

class CWndTextQuest : public CWndMessageBox
{
public:
	CItemBase* m_pItemBase;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndTextBook : public CWndNeuz
{
public:
	CItemBase* m_pItemBase;

	CWndTextBook();
	~CWndTextBook();

	void SetItemBase(CItemBase* pItemBase);

	virtual bool Initialize(CWndBase* pWndParent = NULL, CItemBase* pItemBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndTextScroll : public CWndNeuz
{
public:
	CItemBase* m_pItemBase;

	CWndTextScroll();
	~CWndTextScroll();

	void SetItemBase(CItemBase* pItemBase);

	virtual bool Initialize(CWndBase* pWndParent = NULL, CItemBase* pItemBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndTextLetter : public CWndNeuz
{
public:
	CItemBase* m_pItemBase;

	CWndTextLetter();
	~CWndTextLetter();

	void SetItemBase(CItemBase* pItemBase);

	virtual bool Initialize(CWndBase* pWndParent = NULL, CItemBase* pItemBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif