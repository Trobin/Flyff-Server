#ifndef __WNDVENDORBUY__H
#define __WNDVENDORBUY__H

#include "WndRegVend.h"

class CWndVendorBuy : public CWndNeuz
{
	CItemBase* m_pItemBase;
	int	m_iIndex;
public:
	CWndVendorBuy(CItemBase* pItemBase, int iIndex);
	~CWndVendorBuy();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	unsigned long			m_dwFocus;
	CPoint			m_pt[2];
	bool	        m_bIsFirst;
	CALC_DATA       m_Calc;
	virtual	void	OnChar(unsigned int nChar);
	int				ProcessCalc(CALC_DATA calc, int num);
};

#endif
