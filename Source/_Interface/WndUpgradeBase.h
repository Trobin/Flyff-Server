#ifndef __WNDUPGRADEBASE__H
#define __WNDUPGRADEBASE__H

#define MAX_UPGRADE      6

class CWndUpgradeBase : public CWndNeuz
{
public:
	unsigned long      m_dwReqItem[2];
	int        m_nCost;
	CItemElem* m_pItemElem[MAX_UPGRADE];
	CRect      m_Rect[MAX_UPGRADE];

	int        m_nCount[2];
	int        m_nMaxCount;

	CWndUpgradeBase();
	~CWndUpgradeBase();

	virtual bool    OnDropIcon(LPSHORTCUT pShortcut, CPoint point);

	virtual bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void	OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void	OnDestroyChildWnd(CWndBase* pWndChild);
	virtual	void	OnDestroy(void);
	virtual void	OnRButtonUp(unsigned int nFlags, CPoint point);
};

#endif