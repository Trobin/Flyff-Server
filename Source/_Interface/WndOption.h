// WndOption.h: interface for the CWndNeuz class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDOPTION_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)
#define AFX_WNDOPTION_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WndOptionGame.h"	 // ���� �ɼ�

class CWndOption : public CWndNeuz
{
public:
	CWndOption();
	~CWndOption();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

//////////////////////////////////////////////////////////////////////////////////////
// Sound
//

class CWndOptSound : public CWndNeuz
{
public:
	CTexture         m_Texture;
	CTexture         m_TexturePt;
	bool			 m_bLButtonClick;
	bool			 m_bLButtonClick2;
	int				 m_nStep[2];
	CWndOptSound();
	~CWndOptSound();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);

	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);

	void GetRangeSlider(unsigned long dwWndId, int& nStep, CPoint point);
	int GetSliderStep(unsigned long dwWndId, int& nStep, CPoint point);
	CPoint GetStepPos(int nStep, int nWidth, int nDivision);

	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();

};
class CWndOptVideo : public CWndNeuz
{
public:
	void GetRangeSlider(unsigned long dwWndId, int& nStep, CPoint point);
	int GetSliderStep(unsigned long dwWndId, int& nStep, CPoint point);
	void GetRangeSlider(unsigned long dwWndId, int& nStep, CPoint point, int nDivision);
	int GetSliderStep(unsigned long dwWndId, int& nStep, CPoint point, int nDivision);
	CPoint GetStepPos(int nStep, int nWidth, int nDivision);
	CWndOptVideo();
	~CWndOptVideo();

#if __VER >= 8 //__Y_GAMMA_CONTROL_8	
	int				 m_nStep[5];
	int				 m_nBrightTable[11];

	bool			 m_bLButtonClick3;
	bool			 m_bLButtonClick4;
	bool			 m_bLButtonClick5;
#else //__Y_GAMMA_CONTROL_8
	int				 m_nStep[2];
#endif //__Y_GAMMA_CONTROL_8

	CTexture         m_Texture;
	CTexture         m_TexturePt;
	bool			 m_bLButtonClick;
	bool			 m_bLButtonClick2;
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
};



class CWndOptMyInfo : public CWndNeuz
{
public:
	CWndOptMyInfo();
	~CWndOptMyInfo();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndOptWindow : public CWndNeuz
{
public:
	CWndOptWindow();
	~CWndOptWindow();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


class CWndTotalOption : public CWndNeuz
{
public:
	CWndOptVideo		m_OptTabVideoSnd;
	CWndOptionGame		m_OptTabGame;
#if __VER < 12 // __UPDATE_OPT
	CWndOption			m_OptTabEtc;
	CWndOptSound		m_OptTabSound;
#endif
	CWndTotalOption();
	~CWndTotalOption();

	void UpdateDataAll();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

//BEGIN_WNDCLASS( CWndOptVideo )
//END_WNDCLASS

//BEGIN_WNDCLASS( CWndOptTheme )
//	CWndButton    m_wndPaper1;
//	CWndButton    m_wndPaper2;
//	CWndButton    m_wndPaper3;
//	CWndButton    m_wndPaper4;
//	CWndButton    m_wndPaper5;
//END_WNDCLASS

//BEGIN_WNDCLASS( CWndOptSound )
//END_WNDCLASS

//BEGIN_WNDCLASS( CWndOptMusic )
//END_WNDCLASS


//BEGIN_WNDCLASS( CWndOptMouse )
//END_WNDCLASS

//BEGIN_WNDCLASS( CWndOptHotkey )
//END_WNDCLASS

/*
class CWndSound : public CWndNeuz
{
	CWndButton m_wndMusic;
	CWndButton m_wndSound;
public:
	CWndSound();
	virtual ~CWndSound();
//	virtual CItem* GetFocusItem() { return NULL; }
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL,unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand( unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase );
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

*/
#endif // !defined(AFX_WNDOPTION_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)