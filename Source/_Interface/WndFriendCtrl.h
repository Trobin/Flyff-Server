// WndBase.h: interface for the CWndBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDFRIENDCTRL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_)
#define AFX_WNDFRIENDCTRL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef __RT_1025
#include "rtmessenger.h"
#else	// __RT_1025
#include "messenger.h"
#endif	// __RT_1025

class C2DRender;
class CItemElem;

class CWndFriendCtrl : public CWndBase
{
	void InterpriteScript(CScanner& scanner, CPtrArray& ptrArray);
	int           m_nCurSelect;
	int           m_nFontHeight;
	CWndScrollBar m_wndScrollBar;

	unsigned long		  m_uParent;
	int			  m_nSelectServer;
	int			  m_nSex;
	char		  m_szId[MAX_PATH];
	int			  m_nServerCount[11];
	unsigned long		  m_uServerPlayerId[11][1024];

	int			  m_nDrawCount;

	CTexture m_texGauEmptyNormal;
	CTexture m_texGauFillNormal;

	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;


	// Constructors
public:
	CWndMenu m_menu;

	CRect         m_rect;
	bool          m_bDrag;
	unsigned long         m_dwListCtrlStyle;


	void SetScrollBar();
#ifdef __RT_1025
	int		GetSelect(CPoint point, unsigned long& idPlayer, Friend** ppFriend);
	void	GetSelectFriend(int SelectCount, unsigned long& idPlayer, Friend** ppFriend);
#else	// __RT_1025
	int GetSelect(CPoint point, LPFRIEND* lppFriend);
	void	GetSelectFriend(int SelectCount, LPFRIEND* lppFriend);
#endif	// __RT_1025
	unsigned long GetSelectId(int SelectCount);

	//	void InitItem( int nJob, int nExpert, LPSKILL apExpertSkill, unsigned long uParent = 0 );
	void SetSearch(int nSelectServer, int nSex, const char* strId);

	CWndFriendCtrl();
	~CWndFriendCtrl();

	void GetFriendName();
	void Create(unsigned long m_dwListCtrlStyle, RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void LoadListBoxScript(const char* lpFileName);
	// Attributes
	COLORREF GetBkColor() const;
	bool SetBkColor(COLORREF cr);
	int GetItemCount() const;
	bool GetItem(LVITEM* pItem) const;
	bool SetItem(const LVITEM* pItem);
	bool SetItem(int nItem, int nSubItem, unsigned int nMask, const char* lpszItem,
		int nImage, unsigned int nState, unsigned int nStateMask, LPARAM lParam);

	// Operations
	int InsertItem(const LVITEM* pItem);

	// Overridables
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	//	virtual bool OnDropIcon( LPSHORTCUT pShortcut, CPoint point = 0 );
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);

	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	// Implementation
public:
	int InsertItem(unsigned int nMask, int nItem, const char* lpszItem, unsigned int nState,
		unsigned int nStateMask, int nImage, LPARAM lParam);

	void ScrollBarPos(int nPos);
	int	 GetDrawCount(void);

protected:
	void RemoveImageList(int nImageList);
protected:
	////{{AFX_MSG(CListCtrl)
	//afx_msg void OnNcDestroy();
	////}}AFX_MSG
	//DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_WNDSKILLCTRL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_)

