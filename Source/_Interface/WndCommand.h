#ifndef __WNDCOMMAND__H
#define __WNDCOMMAND__H

class CWndCommand : public CWndListBox //CWndNeuz 
{
public:
	CWndBase* m_pWndEdit;

	CWndCommand();
	~CWndCommand();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnSetFocus(CWndBase* pOldWnd);
	virtual void OnKillFocus(CWndBase* pNewWnd);
};
#endif