#ifndef __WNDRANKGUILD__H
#define __WNDRANKGUILD__H

class CWndRankTabBest : public CWndNeuz
{
public:
	CWndRankTabBest();
	~CWndRankTabBest();

	int		m_nCurrentList;			// ��µ� �������Ʈ�� ���� �ε���.
	int		m_nMxOld, m_nMyOld;			// ���� ��ǥ.

	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndRankTabUnited : public CWndNeuz
{
public:
	CWndRankTabUnited();
	~CWndRankTabUnited();

	int		m_nCurrentList;			// ��µ� �������Ʈ�� ���� �ε���.
	int		m_nMxOld, m_nMyOld;			// ���� ��ǥ.

	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndRankTabPenya : public CWndNeuz
{
public:
	CWndRankTabPenya();
	~CWndRankTabPenya();

	int		m_nCurrentList;			// ��µ� �������Ʈ�� ���� �ε���.
	int		m_nMxOld, m_nMyOld;			// ���� ��ǥ.

	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndRankGuild : public CWndNeuz
{
public:
	CWndRankGuild();
	~CWndRankGuild();

	CWndRankTabBest		m_WndRankTabBest;
	CWndRankTabUnited	m_WndRankTabUnited;
	CWndRankTabPenya	m_WndRankTabPenya;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif