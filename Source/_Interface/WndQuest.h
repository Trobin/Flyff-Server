#ifndef __WNDQUEST__H
#define __WNDQUEST__H

#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
#include "TreeInformation.h"

extern CTreeInformationManager g_QuestTreeInfoManager;

class CWndQuestTreeCtrl : public CWndTreeCtrl
{
public:
	CWndQuestTreeCtrl(void);
	virtual ~CWndQuestTreeCtrl(void);

public:
	virtual bool OnChildNotify(unsigned int nCode, unsigned int nID, LRESULT* pLResult);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif // __IMPROVE_QUEST_INTERFACE

class CWndRemoveQuest : public CWndMessageBox
{
public:
	int m_nRemoveQuestId;
	CWndRemoveQuest() { m_nRemoveQuestId = -1; }
	virtual ~CWndRemoveQuest() { }
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndQuest : public CWndNeuz
{
public:
	CWndQuest();
	~CWndQuest();

#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	enum { MAX_CHECK_NUMBER = 5 };
#endif // __IMPROVE_QUEST_INTERFACE

	int m_idSelQuest;
	CWordArray m_aOpenTree;
	void Update(int nNewQuestId = -1);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	void RemoveQuest(void);
#else // __IMPROVE_QUEST_INTERFACE
#if __VER >= 13 // __QUEST_HELPER
	void UpdateText(bool bClick = false);
#else //__QUEST_HELPER
	void UpdateText();
#endif //__QUEST_HELPER
#endif // __IMPROVE_QUEST_INTERFACE
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	void ControlOpenTree(const LPTREEELEM lpTreeElem);
#endif // __IMPROVE_QUEST_INTERFACE

	void TreeOpen();
	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	CString MakeQuestString(CString& string, bool bCond);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
#endif // __IMPROVE_QUEST_INTERFACE
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
private:
	CWndQuestTreeCtrl* GetQuestTreeSelf(const unsigned long dwQuestID);
	void AddOpenTree(CWordArray& raOpenTree, CPtrArray& rPtrArray);
	void OpenTreeArray(CPtrArray& rPtrArray, bool bOpen = true);
	void InsertQuestItem(const unsigned long dwQuestID, CDWordArray& raOldHeadQuestID, const bool bCompleteQuest, const int nNewQuestId = -1);
	unsigned long FindOldHeadQuest(const CDWordArray& raOldHeadQuestID, const unsigned long dwNowHeadQuestID) const;
	bool IsCheckedQuestID(unsigned long dwQuestID);

private:
	CWndQuestTreeCtrl m_WndScenario;
	CWndQuestTreeCtrl m_WndNormal;
	CWndQuestTreeCtrl m_WndRequest;
	CWndQuestTreeCtrl m_WndEvent;
#endif // __IMPROVE_QUEST_INTERFACE
};

#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
class CWndQConditionTreeCtrl : public CWndTreeCtrl
{
public:
	CWndQConditionTreeCtrl(void);
	virtual ~CWndQConditionTreeCtrl(void);

public:
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndQuestDetail : public CWndNeuz
{
public:
	CWndQuestDetail(unsigned long dwQuestID = -1);
	virtual ~CWndQuestDetail(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual	void PaintFrame(C2DRender* p2DRender);

public:
	void UpdateQuestText(bool bClick = false);
	void UpdateQuestDetailText(unsigned long dwQuestID, LPQUEST lpQuest, bool bComplete, bool bClick = false);
	void SetQuestID(unsigned long dwQuestID);
	unsigned long GetQuestID(void) const;

private:
	unsigned long m_dwQuestID;
	CWndQConditionTreeCtrl m_WndQConditionTreeCtrl;
};

void MakeQuestConditionItems(unsigned long dwQuestID,
	CWndTreeCtrl* pWndTreeCtrl,
	bool bClick = false,
	LPTREEELEM lpTreeElem = NULL,
	unsigned long dwStartColor = D3DCOLOR_ARGB(255, 0, 0, 0),
	unsigned long dwEndColor = D3DCOLOR_ARGB(255, 0, 0, 0),
	unsigned long dwSelectColor = D3DCOLOR_ARGB(255, 0, 0, 255));
const CString MakeString(const CString& string, bool bCond);
unsigned long MakeTextColor(unsigned long dwStartColor = D3DCOLOR_ARGB(255, 0, 0, 0), unsigned long dwEndColor = D3DCOLOR_ARGB(255, 0, 0, 0), int nCurrentNumber = 0, int nCompleteNumber = 0);
unsigned long GetRootHeadQuest(unsigned long dwHeadQuest);
unsigned long SetQuestDestinationInformation(unsigned long dwQuestID, unsigned long dwGoalIndex);
void ProcessQuestDestinationWorldMap(unsigned long dwGoalTextID);
#endif // __IMPROVE_QUEST_INTERFACE

#endif