#ifndef __WNDOPTIONGAME__H
#define __WNDOPTIONGAME__H

class CWndOptionGame : public CWndNeuz
{
public:
	CWndOptionGame();
	~CWndOptionGame();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
#if __VER >= 12 // __UPDATE_OPT
	CTexture         m_Texture;
	CTexture         m_TexturePt;
	bool			 m_bLButtonClick;
	bool			 m_bLButtonClick2;
	int				 m_nStep[2];

	virtual void OnMouseMove(unsigned int nFlags, CPoint point);

	void GetRangeSlider(unsigned long dwWndId, int& nStep, CPoint point);
	int GetSliderStep(unsigned long dwWndId, int& nStep, CPoint point);
	CPoint GetStepPos(int nStep, int nWidth, int nDivision);

	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
#endif
};
#endif
