#ifndef __WNDMESSENGER__H
#define __WNDMESSENGER__H

#include "WndFriendCtrl.h"
#include "WndPartyCtrl.h"
#include "WndGuildCtrl.h"
#if __VER >= 11 // __CSC_VER11_4
#include "WndMessengerCtrl.h"
#endif //__CSC_VER11_4

class CWndMessenger : public CWndNeuz
{
public:
	CWndMessenger();
	~CWndMessenger();

	CWndMenu		m_menuState;
	CWndFriendCtrl	m_wndFriend;
	CWndPartyCtrl	m_wndParty;
	CWndGuildCtrl	m_wndGuild;
	CString			m_strTooltip;	// 접속표시 툴팁문자열 
	CTexture		m_TexMail;
	int				m_nFlashCounter;
	int				m_nSwitch;

	virtual	bool Process();
	virtual void SetWndRect(CRect rectWnd, bool bOnSize);
	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
};

#if __VER >= 11 // __CSC_VER11_4
class CWndMessengerEx : public CWndNeuz
{
public:
	CWndMessengerEx();
	~CWndMessengerEx();

	CWndMenu			m_menuState;
	CWndFriendCtrlEx	m_wndFriend;
	CWndGuildCtrlEx		m_wndGuild;
#if __VER >= 15 // __CAMPUS
	CWndCampus			m_WndCampus;
#endif // __CAMPUS
	CTexture			m_TexMail;
	int					m_nFlashCounter;
	int					m_nSwitch;

	virtual	bool Process();
	//	virtual void SetWndRect( CRect rectWnd, bool bOnSize );
	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	//	virtual void OnSize( unsigned int nType, int cx, int cy ); 
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

	void UpdateFriendList();
	void UpdateGuildMemberList();
#if __VER >= 15 // __CAMPUS
	void UpdateCampusMemberList();
#endif // __CAMPUS
};
#endif //__CSC_VER11_4

class CWndInstantMsg : public CWndNeuz
{
public:
	CTimer m_timer;
	CString m_strMessage;
	CWndInstantMsg();
	~CWndInstantMsg();

	CString m_strPlayer;

	void AddMessageJoin(const char* lpszJoinName);
	void AddMessage(const char* lpszFrom, const char* lpszMessage);
	void AddPostMessage(const char* lpszSendName);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnDestroy();
};

class CWndMessage : public CWndNeuz
{
public:
	CWndMessage();
	~CWndMessage();

	CString m_strPlayer;

	void InitSize(void);
	void AddMessage(const char* lpszFrom, const char* lpszMessage);
	void OnInputString();

	virtual void SetWndRect(CRect rectWnd, bool bOnSize);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnDestroy();
};

class CWndMessageNote : public CWndNeuz
{
public:
	TCHAR m_szName[64];
	unsigned long m_dwUserId;

	CWndText* m_pWndText;
	CWndEdit* m_pEdit;
public:
	CWndMessageNote();
	~CWndMessageNote();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndMessengerNote : public CWndNeuz
{
public:
	CWndMessengerNote();
	~CWndMessengerNote();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif