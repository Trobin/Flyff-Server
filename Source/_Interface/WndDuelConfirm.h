#ifndef __WNDDUELCONFIRM__H
#define __WNDDUELCONFIRM__H

class CWndDuelConfirm : public CWndNeuz
{
public:
	char	m_szSrc[MAX_NAME];	// 듀얼 신청자이름
	OBJID	m_idSrc;

	bool	m_bDuelParty;			// 파티 듀얼시 true

	CWndDuelConfirm();
	~CWndDuelConfirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#if __VER >= 8     // 8차 듀얼존에 관계없이 PVP가능하게함   Neuz, World
class CWndDuelResult : public CWndNeuz
{
public:
	char	m_szSrc[MAX_NAME];	// 듀얼 신청자이름
	OBJID	m_idSrc;

	bool	m_bDuelWin;			//  듀얼시 이겼을때true

	CWndDuelResult();
	~CWndDuelResult();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif	// __VER >= 8  

#endif