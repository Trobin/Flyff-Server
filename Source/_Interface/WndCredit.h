#ifndef __WNDCREDIT__H
#define __WNDCREDIT__H

class CWndCredit : public CWndNeuz
{
	void LoadScreenShot();
public:
	float   m_fOldMusicVolume;
	CTimer m_Starttimer;
	CSound* m_pSound;
	bool   m_bPlayVoice;
	CWndCredit();
	~CWndCredit();

	CStringArray m_strArray;
	int m_nLine;
	CTimer m_timer;
	CString m_strWord;
	CPoint m_ptWord;
	int m_nStyle;
	int m_nParam1;
	int m_nParam2;
	int m_nParam3;

	int m_nTexCount;
	CTexture m_texScreenShot;
	CTexture m_aTexScreenShot[18];
	CTimer m_timerScreenShot;

	CD3DFont* m_pFont;

	virtual	bool Process();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void SetWndRect(CRect rectWnd, bool bOnSize);

	virtual HRESULT InitDeviceObjects();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
	virtual	void	OnDestroy(void);

};

class CWndAbout : public CWndNeuz
{
public:
	CD3DFont* m_pFont;
	CWndAbout();
	~CWndAbout();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual HRESULT InitDeviceObjects();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
	virtual void OnDestroy();
};
#endif
