#ifndef __WNDINVENREMOVEITEM__H
#define __WNDINVENREMOVEITEM__H

class CWndInvenRemoveItem : public CWndNeuz
{
public:
	CItemElem* m_pItemElem;
	LPWNDCTRL		m_pWndItemCtrl;
	CWndEdit* m_pWndEditNum;

	int				m_nRemoveNum;
	void InitItem(CItemElem* pItemElem);
	bool OnButtonOK(void);

	CWndInvenRemoveItem();
	~CWndInvenRemoveItem();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
};
#endif
