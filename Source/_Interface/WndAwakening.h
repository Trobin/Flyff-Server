#ifndef __WND__H
#define __WND__H

#if __VER >= 11 // __SYS_IDENTIFY
class CWndAwakening : public CWndNeuz
{
public:

	CWndText* m_pText;
	CItemElem* m_pItemElem;
	ItemProp* m_pEItemProp;
	CTexture* m_pTexture;

	CWndAwakening();
	~CWndAwakening();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual void OnDestroy();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual bool process();

	void SetDescription();
};
#endif

#ifdef __PROTECT_AWAKE
class CWndSelectAwakeCase : public CWndNeuz
{
	// 각성 보호 선택창 ( 두가지중에 하나 고름 < 각성전, 각성후 > 0
public:

	CWndSelectAwakeCase();
	virtual ~CWndSelectAwakeCase();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	//	virtual void OnDestroy();
	//	virtual bool OnCommand( unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase ); 
	//	virtual void OnLButtonDown( unsigned int nFlags, CPoint point ); 
	//	virtual bool OnDropIcon( LPSHORTCUT pShortcut, CPoint point );
	virtual bool process();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	void SetItemIndex(const unsigned long index) { m_dwItemIndex = index; }
	void SetData(BYTE byObjID, unsigned long dwSerialNum, __int64 n64NewOption);
	void OutputOptionString(C2DRender* p2DRender, CItemElem* pItemElem, bool bNew = false);

protected:
	unsigned long m_dwOldTime;
	unsigned long m_dwDeltaTime;

	unsigned long m_dwItemIndex;
	CTexture* m_pTexture;

	BYTE m_byObjID;
	unsigned long m_dwSerialNum;
	__int64 m_n64NewOption;

	CTexture* m_pTexGuage;
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBufferGauge;
};

#endif //__PROTECT_AWAKE

#endif
