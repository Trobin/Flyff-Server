#ifndef __WNDREGVEND__H
#define __WNDREGVEND__H

typedef struct CALC_DATA
{
	int  m_nValue;
	char m_ch;
} CALC_DATA;


class CWndRegVend : public CWndNeuz
{
private:
	CWndEdit* m_pWndEdit;

	unsigned long		m_dwOldFocus;

	CItemBase* m_pItemBase;
	int		m_iIndex;
public:
	CWndRegVend();
	~CWndRegVend();

	//	Operations
	void	SetItem(int iIndex, CItemBase* pItemBase);

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual	void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);

	unsigned long			m_dwFocus;
	CPoint			m_pt[2];
	bool	        m_bIsFirst;
	CALC_DATA       m_Calc;
	virtual	void	OnChar(unsigned int nChar);
	int				ProcessCalc(CALC_DATA calc, int num);
	CTexture* m_pTex;
};

#endif