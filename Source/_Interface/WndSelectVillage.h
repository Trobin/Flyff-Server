#ifndef __WNDSELECTVILLAGE__H
#define __WNDSELECTVILLAGE__H


class CReturnScrollMsgBox : public CWndMessageBox
{
private:
	unsigned long m_dwItemId;
public:
	void SetItemID(unsigned long dwItemId) { m_dwItemId = dwItemId; }
	unsigned long GetItemID() { return m_dwItemId; }
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndSelectVillage : public CWndNeuz
{
public:
	CWndSelectVillage();
	~CWndSelectVillage();

	unsigned long	m_dwItemId;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif	// __WNDSELECTVILLAGE__H

