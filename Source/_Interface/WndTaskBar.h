// WndArcane.h: interface for the CWndNeuz class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDTASKBAR_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)
#define AFX_WNDTASKBAR_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CWndShortcut : public CWndButton
{
public:
	CWndShortcut();
	~CWndShortcut();

	//bool Create(const char* lpszCaption,unsigned long dwStyle,const RECT& rect,CWndBase* pParentWnd,unsigned int nID,CSprPack* pSprPack,int nSprIdx,int nColorTable = 0);
	//bool Create(const char* lpszCaption,unsigned long dwStyle,const RECT& rect,CWndBase* pParentWnd,unsigned int nID);// { return Create(lpszCaption,dwStyle,rect,pParentWnd,nID,NULL,0); }
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	bool Process();
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);

	/*
		void SetMenu(CWndMenu* pWndMenu);

		// Attributes
		//bool IsButtonStyle( unsigned long dwStyle ) { return ( m_dwButtonStyle & dwStyle ) ? true : false; }
		void SetPushTime(int nTime); // 버튼을 누르고 nTime이후에 계속 OnCommand를 패어런트에게 보낸다.
		unsigned int GetState() const; // Retrieves the check state, highlight state, and focus state of a button control.
		void SetState(bool bHighlight); // Sets the highlighting state of a button control
		int  GetCheck() const; // Retrieves the check state of a button control.
		void SetCheck(int nCheck); // Sets the check state of a button control.
		//unsigned int GetButtonStyle() const; // Retrieves information about the button control style.
		//void SetButtonStyle(unsigned int nStyle, bool bRedraw = true); // Changes the style of a button.
		void SetString(CString strSndEffect);
		bool IsHighLight() { return m_bHighLight; }
		void SetFontColor     (unsigned long dwColor) { m_nFontColor      = (unsigned long)dwColor; }
		void SetPushColor     (unsigned long dwColor) { m_nPushColor      = (unsigned long)dwColor; }
		void SetDisableColor  (unsigned long dwColor) { m_nDisableColor   = (unsigned long)dwColor; }
		void SetHighLightColor(unsigned long dwColor) { m_nHighlightColor = (unsigned long)dwColor; }
		unsigned long GetFontColor     () { return m_nFontColor     ; }
		unsigned long GetPushColor     () { return m_nPushColor     ; }
		unsigned long GetDisableColor  () { return m_nDisableColor  ; }
		unsigned long GetHighLightColor() { return m_nHighlightColor; }
		void SetWndExecute(CWndBase* pWndBase);
		CWndBase* GetWndExecute() { return m_pWndExecute; }
		void SetPushPoint(int x,int y) { m_ptPush = CPoint(x,y); }
		*/
};
class CWndQuickList : public CWndNeuz
{
public:
	CWndQuickList();
	~CWndQuickList();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

//////////////////////////////////////////////////////////////////////////////////////
// 작업 윈도 
// 메뉴 버튼, 명령 아이콘, 단축 아이콘, 활성화 기능 등등
//
class CWndTaskBar : public CWndNeuz
{
	unsigned long m_dwHighAlpha;

public:
	bool		   IsShortcut(LPSHORTCUT lpShortcut, unsigned long dwShortcut, unsigned long dwId);
	CWndMenu       m_menuShortcut;
	CWndQuickList* m_pWndQuickList;
	CTexturePack   m_texPack;
	CWndButton     m_wndMenu;
	CWndButton     m_wndQuickList;
	CWndButton     m_wndQuickListUp;
	CWndButton     m_wndQuickListDn;
	SHORTCUT       m_aSlotApplet[MAX_SLOT_APPLET]; // 1 ~ 20
	SHORTCUT       m_aSlotItem[MAX_SLOT_ITEM_COUNT][MAX_SLOT_ITEM]; // 1 ~ 0(10)
	SHORTCUT       m_aSlotQueue[MAX_SLOT_QUEUE];
	LPSHORTCUT     m_paSlotItem;
	LPSHORTCUT     m_pSelectShortcut;
	SHORTCUT       m_aSlotSkill;
	CTexture* m_pTexture;
	int            m_nMaxSlotApplet;
	bool           m_bStartTimeBar;
	int            m_nUsedSkillQueue;
	int            m_nPosition;
	int            m_nSkillBar;
	int            m_nCurQueueNum;
	int            m_nCurQueue;
	CTimer         m_timer;
	int			   m_nExecute;		// 0: 실행중이지 않음 1:실행대기중(스킬쓰러 달려가는중) 2:실행중.
	OBJID		   m_idTarget;		// 스킬사용대상.
	int            m_nSlotIndex;
	int		       m_nActionPoint;		// 액션 포인트 - 시리얼라이즈 대상.
#if __VER >= 12 // __LORD
	void RenderLordCollTime(CPoint pt, unsigned long dwSkillId, C2DRender* p2DRender);
#endif
	void RenderCollTime(CPoint pt, unsigned long dwSkillId, C2DRender* p2DRender);
	void UpdateItem();
	void PutTooTip(LPSHORTCUT pShortcut, CPoint point, CRect* pRect);
	void SetItemSlot(int nSlot);
	bool CheckAddSkill(int nSkillStyleSrc, int nQueueDest);
	bool UseSkillQueue(CCtrl* pTargetObj);
	LPSKILL GetCurrentSkillQueue();
	bool SetSkillQueue(int nIndex, unsigned long dwType, unsigned long dwId, CTexture* pTexture = NULL);
	bool SetShortcut(int nIndex, unsigned long dwShortcut, unsigned long dwType, unsigned long dwId, CTexture* pTexture = NULL, int nWhere = 0);
	void OnEndSkill(void);
	void OnCancelSkill(void);
	LPSHORTCUT Select(CPoint point);
	void RemoveSkillQueue(int nIndex, bool bSend = true);
	bool RemoveDeleteObj();
	void InitTaskBar();

	CWndTaskBar();
	virtual ~CWndTaskBar();
	//	virtual CItem* GetFocusItem() { return NULL; }
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual bool Process();
	virtual void OnMouseWndSurface(CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual	void OnDestroyChildWnd(CWndBase* pWndChild);
	void	Serialize(CAr& ar);
	void	SetTaskBarTexture(LPSHORTCUT pShortcut);

	HRESULT	RestoreDeviceObjects();
	HRESULT InvalidateDeviceObjects();
	HRESULT DeleteDeviceObjects();
protected:
	void RenderOutLineLamp(int x, int y, int num, unsigned long size);
};
//////////////////////////////////////////////////////////////////////////////////////
// 태스크바의 매뉴 
//
class CWndTaskMenu : public CWndMenu //Neuz
{
public:
	CWndTaskMenu();
	virtual ~CWndTaskMenu();
	void SetTexture(CWndButton* pWndButton);
	//	virtual CItem* GetFocusItem() { return NULL; }
	virtual CWndButton* AppendMenu(CWndMenu* pWndMenu, unsigned int nFlags, unsigned int nIDNewItem, const char* lpszNewItem);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void PaintFrame(C2DRender* p2DRender);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool Process();
	virtual void OnKillFocus(CWndBase* pNewWnd);
private:
	CWndMenu* m_pMenu1;
	CWndMenu* m_pMenu2;
	CWndMenu* m_pMenu3;
	CWndMenu* m_pMenu4;
	CWndMenu* m_pMenu5;
	CWndMenu* m_pMenu6;
	CWndMenu* m_pMenu7;
};

#endif // !defined(AFX_WNDTASKBAR_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)