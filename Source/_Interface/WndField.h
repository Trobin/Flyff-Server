#if !defined(AFX_WNDFIELD_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)
#define AFX_WNDFIELD_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "post.h"
#include "guild.h"

#if __VER >= 13 // __CSC_VER13_2
#include "couple.h"
#include "couplehelper.h"
#endif //__CSC_VER13_2

extern 	int g_nSkillCurSelect;
const int MAX_WANTED_LIST = 100;

#if __VER >= 13 // __HONORABLE_TITLE
#include "honor.h"
#endif	// __HONORABLE_TITLE

#ifdef __MAIL_REQUESTING_BOX
#include "WndMailRequestingBox.h"
#endif // __MAIL_REQUESTING_BOX
//////////////////////////////////////////////////////////////////////////////////////
// 애플랫 윈도 
//
class CWndApplet : public CWndNeuz
{
public:
	CWndListBox m_wndList;
	CWndButton m_wndButton1;
	CWndButton m_wndButton2;
	CWndButton m_wndButton3;

	CWndApplet();
	virtual ~CWndApplet();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

};

//////////////////////////////////////////////////////////////////////////////////////
// 드롭아이템 
//
class CWndDropItem : public CWndNeuz
{
public:
	CItemElem* m_pItemElem;
	D3DXVECTOR3 m_vPos;
	CWndEdit* m_pEdit;

	CWndDropItem();
	virtual ~CWndDropItem();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
#if __VER >= 14 // __DROP_CONFIRM_BUG
	virtual bool Process(void);
#endif // __DROP_CONFIRM_BUG
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndDropConfirm : public CWndNeuz
{
public:
	CItemElem* m_pItemElem;
	D3DXVECTOR3 m_vPos;

	CWndDropConfirm();
	virtual ~CWndDropConfirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
#if __VER >= 14 // __DROP_CONFIRM_BUG
	virtual bool Process(void);
#endif // __DROP_CONFIRM_BUG
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndRandomScrollConfirm : public CWndNeuz
{
public:
	bool bFlag;
	OBJID objid, objid1;
	void SetItem(OBJID objidBuf, OBJID objid1Buf, bool bFlagBuf = false);
public:
	CWndRandomScrollConfirm();
	virtual ~CWndRandomScrollConfirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndQuestItemWarning : public CWndNeuz
{
public:
	CItemElem* m_pItemElem;
	D3DXVECTOR3 m_vPos;

	CWndQuestItemWarning();
	virtual ~CWndQuestItemWarning();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


//////////////////////////////////////////////////////////////////////////////////////
//  퀘스트 아이템 정보 창
//
class CWndQuestItemInfo : public CWndNeuz
{
public:
	CWndQuestItemInfo();
	virtual ~CWndQuestItemInfo();

	CItemBase* m_pItemBase;
	CPoint		m_Position;

	inline void SetItemBase(CItemBase* pItemBase = NULL) { m_pItemBase = pItemBase; };

	bool Create(CItemBase* pItemBase, unsigned int nID, CWndBase* pWndParent = NULL);

	virtual bool Initialize(CWndBase* pWndParent = NULL, CItemBase* pItemBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

//////////////////////////////////////////////////////////////////////////////////////
// 인벤토리 
//
#if __VER >= 9 // __CSC_VER9_1
class CWndRemoveJewelConfirm;
#endif //__CSC_VER9_1

class CWndGold : public CWndButton
{
public:
	CTexture m_texture;
	CWndGold();
	virtual ~CWndGold();
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual	void OnInitialUpdate();
};

class CWndConfirmBuy;
class CWndInventory : public CWndNeuz
{
	CPoint				m_OldPos;
	float				m_fRot;
	CModelObject* m_pModel;
	bool				m_bLButtonDownRot;

	CRect		 m_InvenRect[MAX_HUMAN_PARTS];

	CModelObject m_meshStage;
	CItemElem* m_pSelectItem;
	bool m_bReport;

	CTexture* m_TexRemoveItem;
public:
	void UpDateModel();

	void UpdateParts();
	CModelObject* GetModel() { return m_pModel; }
	void BaseMouseCursor();
	void RunUpgrade(CItemBase* pItem);
	void SetEnchantCursor();

	CSfx* m_pSfxUpgrade;
	CItemBase* m_pUpgradeItem;
	CItemBase* m_pUpgradeMaterialItem;
	bool		m_bIsUpgradeMode;
	unsigned long		m_dwEnchantWaitTime;

	CWndConfirmBuy* m_pWndConfirmBuy;
	CWndGold     m_wndGold;
	CWndItemCtrl m_wndItemCtrl;
#if __VER >= 9 // __CSC_VER9_1
	bool		m_bRemoveJewel;
	CWndRemoveJewelConfirm* m_pWndRemoveJewelConfirm;
#endif //__CSC_VER9_1
	CWndInventory();
	virtual ~CWndInventory();
	virtual bool Process();
	virtual void OnMouseWndSurface(CPoint point);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnDestroyChildWnd(CWndBase* pWndChild);
	virtual void OnDestroy(void);
	virtual bool OnSetCursor(CWndBase* pWndBase, unsigned int nHitTest, unsigned int message);

#if __VER >= 9 // __CSC_VER9_1
public:
	void UpdateTooltip(void) { m_wndItemCtrl.UpdateTooltip(); }
#endif //__CSC_VER9_1

};

class CWndQueryEquip : public CWndNeuz
{
	CPoint				m_OldPos;
	float				m_fRot;
	CModelObject* m_pModel;
	bool				m_bLButtonDownRot;
	unsigned long		 m_ObjID;
	CRect		 m_InvenRect[MAX_HUMAN_PARTS];

	EQUIP_INFO_ADD	m_aEquipInfoAdd[MAX_HUMAN_PARTS];
public:

	CMover* GetMover()
	{
		if (m_ObjID != NULL_ID)
			return prj.GetMover(m_ObjID);

		return NULL;
	}
	void	SetMover(unsigned long		 ObjID);
	void	SetEquipInfoAdd(EQUIP_INFO_ADD* aEquipInfoAdd);

	CWndQueryEquip();
	virtual ~CWndQueryEquip();
	virtual bool Process();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnMouseWndSurface(CPoint point);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


//////////////////////////////////////////////////////////////////////////////////////
// 상태 
//
class CWndStateConfirm : public CWndNeuz
{
public:
	unsigned int m_nId;
	void OnSetState(unsigned int nId);
	void SendYes(void);
public:
	CWndStateConfirm();
	virtual ~CWndStateConfirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
class CWndCharacterBase : public CWndBase
{
	CTexture m_txInfo;

public:
	CWndCharacterBase();
	virtual ~CWndCharacterBase();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndCharacterDetail : public CWndBase
{
	CWndButton m_wndStr;
	CWndButton m_wndSta;
	CWndButton m_wndDex;
	CWndButton m_wndInt;

	int			m_nCnt;

public:
	bool	m_fWaitingConfirm;
	CWndCharacterDetail();
	virtual ~CWndCharacterDetail();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);

protected:
	void RenderATK(C2DRender* p2DRender, int x, int y);
};

#if __VER >= 9 // __CSC_VER9_2
class CWndCharacterDetail2 : public CWndBase
{
public:
	CWndButton m_wndStrPlus, m_wndStrMinus;
	CWndButton m_wndStaPlus, m_wndStaMinus;
	CWndButton m_wndDexPlus, m_wndDexMinus;
	CWndButton m_wndIntPlus, m_wndIntMinus;

	CWndButton m_wndApply, m_wndReset;

	CWndEdit m_editStrCount;
	CWndEdit m_editStaCount;
	CWndEdit m_editDexCount;
	CWndEdit m_editIntCount;

	int m_nStrCount;
	int m_nStaCount;
	int m_nDexCount;
	int m_nIntCount;

	int m_nGpPoint;

	int m_nATK;
	int m_nDEF;
	int m_nCritical;
	int m_nATKSpeed;

public:
	CWndCharacterDetail2();
	virtual ~CWndCharacterDetail2();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	virtual bool Process();

	void RefreshStatPoint();
	void GetVirtualATK(int* pnMin, int* pnMax);
	int GetVirtualDEF();
	int GetVirtualCritical();
	float GetVirtualATKSpeed();

protected:
	void RenderATK(C2DRender* p2DRender, int x, int y);
};
#endif //__CSC_VER9_2

class CWndChangeJob;

class CWndPvpBase : public CWndBase
{
	CTexture m_txInfo;

public:
	CWndButton m_wndChangeJob;
	CWndChangeJob* m_pWndChangeJob;
	bool	m_fWaitingConfirm;
	bool m_bExpert;
	int m_nDisplay;			// 직업창 화면 옵션

	CWndPvpBase();
	virtual ~CWndPvpBase();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndChangeJob : public CWndNeuz
{
public:
	int m_nJob;
	CWndButton m_wndExpert[6];
	CWndButton m_wndOk;
	CWndButton m_wndCancel;

	CWndChangeJob(int nJob);
	virtual ~CWndChangeJob();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#if __VER >= 13 // __RENEW_CHARINFO
class CWndCharInfo : public CWndBase
{

public:
	CWndButton		m_wndChangeJob;
	CWndChangeJob* m_pWndChangeJob;
	bool			m_fWaitingConfirm;
	bool			m_bExpert;
	int				m_nDisplay;

	CWndButton m_wndStrPlus, m_wndStrMinus;
	CWndButton m_wndStaPlus, m_wndStaMinus;
	CWndButton m_wndDexPlus, m_wndDexMinus;
	CWndButton m_wndIntPlus, m_wndIntMinus;

	CWndButton m_wndApply, m_wndReset;

	CWndEdit m_editStrCount;
	CWndEdit m_editStaCount;
	CWndEdit m_editDexCount;
	CWndEdit m_editIntCount;

	int m_nStrCount;
	int m_nStaCount;
	int m_nDexCount;
	int m_nIntCount;

	int m_nGpPoint;

	int m_nATK;
	int m_nDEF;
	int m_nCritical;
	int m_nATKSpeed;

public:

	CWndCharInfo();
	virtual ~CWndCharInfo();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	virtual bool Process();
	void RefreshStatPoint();
	void GetVirtualATK(int* pnMin, int* pnMax);
	int GetVirtualDEF();
	int GetVirtualCritical();
	float GetVirtualATKSpeed();

protected:
	void RenderATK(C2DRender* p2DRender, int x, int y);
};

class CWndHonor : public CWndNeuz
{
private:
	int						m_nSelectedId;
	int						m_nCount;
	vector<EarnedTitle>		m_vecTitle;
public:

	CWndHonor();
	virtual ~CWndHonor();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	void RefreshList();

};
#endif // __RENEW_CHARINFO

class CWndCharacter : public CWndNeuz
{
public:
#if __VER >= 13 // __RENEW_CHARINFO
	CWndCharInfo		m_wndCharInfo;
	CWndHonor			m_wndHonor;
#else // __RENEW_CHARINFO
#if __VER >= 9 // __CSC_VER9_2
	CWndCharacterDetail2 m_wndStateDetail;
#else //__CSC_VER9_2
	CWndCharacterDetail m_wndStateDetail;
#endif
	CWndCharacterBase   m_wndStateBase;
	CWndPvpBase         m_wndPvp;
#endif // __RENEW_CHARINFO
public:
	CWndCharacter();
	virtual ~CWndCharacter();
	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


class CWndReSkillControl : public CWndNeuz
{
public:
	CWndReSkillControl();
	virtual ~CWndReSkillControl();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndReSkillWarning : public CWndNeuz
{
	bool	m_bParentDestroy;
public:
	CWndReSkillWarning();
	virtual ~CWndReSkillWarning();

	void InitItem(bool bParentDestroy);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnDestroy();
};



//////////////////////////////////////////////////////////////////////////////////////
// 스킬 EX 
//
class CWndSkillTreeEx : public CWndNeuz
{
protected:
	int			  m_nCount;					//현재 직업에 해당하는 스킬 총 갯수

	int           m_nCurSelect;			//잉 안쓰고있는데?
	bool          m_bDrag;					//마우스로 클릭했는데 스킬아이콘 영역 안 인 경우 true
	LPSKILL       m_pFocusItem;				//선택된 아이템 : 즉 스킬을 더블클릭하거나 해서 레벨을 올릴수있는 상태로 만든 스킬아이템
	int				m_nCurrSkillPoint;		//현재 남은 스킬포인트
	CTexturePack	m_textPackNum;			//스킬 레벨숫자 표시용 그림 ( 1, 2,..... max )
	unsigned long			m_dwMouseSkill;			//마우스에 위치한 스킬
	CWndButton* m_pWndButton[4];		//+, -, reset, finish
	LPSKILL       m_apSkill;				//스킬 목록
	CTexture* m_atexSkill[MAX_SKILL_JOB];
	CTexture* m_aSkillLevel[3];
	//CTexture*     m_atexJobPannel[ 2 ];
	LPIMAGE       m_atexJobPannel[2];
#if __VER >= 10 // __CSC_VER9_1
	CString		  m_strHeroSkilBg;			//히어로 이미지 파일 이름
#endif //__CSC_VER9_1
	int           m_nJob;			//class 번호

	CTexture m_texGauEmptyNormal;
	CTexture m_texGauFillNormal;

	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;

	CTexture* m_atexTopDown[2];
	int 	m_nTopDownGap;
#if __VER >= 10 // __CSC_VER9_1
	bool	m_bSlot[4];
#else
	bool	m_bSlot[3];
#endif //__CSC_VER9_1

#if __VER >= 10 // __CSC_VER9_1
	bool m_bLegend;							//전승
	CWndStatic* m_pWndHeroStatic[2];
#endif //__CSC_VER9_1

public:
	bool	IsReSkillPointDone();
	int		GetCurrSkillPoint() { return m_nCurrSkillPoint; }
	void	SubSkillPointDown(LPSKILL lpSkill);
	LPSKILL GetSkill() { return m_apSkill; }
	bool	IsDownPoint(unsigned long skill);
	int		GetCalcImagePos(int nIndex);
	int		GetCurSelect() { return g_nSkillCurSelect; }
	bool	GetSkillPoint(unsigned long dwSkillID, CRect& rect);
	LPSKILL GetSkill(int i);
	LPSKILL GetdwSkill(unsigned long dwSkill);
	bool	CheckSkill(int i);
	void LoadTextureSkillicon();
	void InitItem(int nJob, LPSKILL apSkill, bool bReset = false);
	CWndSkillTreeEx();
	virtual ~CWndSkillTreeEx();

	void AfterSkinTexture(LPWORD pDest, CSize size, D3DFORMAT d3dFormat = D3DFMT_A4R4G4B4);
	void SetJob(int nJob);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool Process();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	void SetActiveSlot(int nSlot, bool bFlag);
	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
};


//////////////////////////////////////////////////////////////////////////////////////
// 매신저  
//

//////////////////////////////////////////////////////////////////////////////////////
// Navigator
//

class CWndNavigator : public CWndNeuz
{
	CWndButton m_wndPlace;
	CWndButton m_wndMover;
	CWndButton m_wndZoomIn;
	CWndButton m_wndZoomOut;
	CWndMenu   m_wndMenuPlace;
	CWndMenu   m_wndMenuMover;
	BILLBOARD  m_billboard;
	CTexture   m_texArrow;
	CTexture   m_texDunFog;
	CTexturePack m_texNavObjs;
	unsigned long		 m_iFrame;
	unsigned long		 m_iPastTime;
	CTexturePack m_texNavPos;
	inline void	 AccuFrame() {
		unsigned long CurTime = g_tmCurrent;
		if (CurTime - m_iPastTime > 120)
		{
			m_iFrame++;
			m_iPastTime = CurTime;
		}
		if (m_iFrame >= 4)
			m_iFrame = 0;
	};
	void RenderMark(C2DRender* p2DRender, CMover* Player);	//	참조되는 변수는 플레이어와 파티플레이어를 얻을 수 있는 것이어야 하지만 
																//	현재는 자신만을 찍는 것을 하고 그 다음에 파티 플레이어들을 찾을수 있는 방법을 찾아 보자꾸나
	void RenderMarkAll(C2DRender* p2DRender, CMover* Player);
	CBillboard m_billArrow;
	CSize      m_size;
	int        m_nSizeCnt;
	TCHAR      m_szName[64];
	void ResizeMiniMap();

	CTexture m_GuildCombatTextureMask;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	CTexture* m_pDestinationPositionTexture;
#endif // __IMPROVE_QUEST_INTERFACE

public:
	bool m_bObjFilterPlayer;
	bool m_bObjFilterParty;
	bool m_bObjFilterNPC;
	bool m_bObjFilterMonster;


	void RenderPartyMember(C2DRender* p2DRender, TEXTUREVERTEX** pVertices, CRect rect, D3DXVECTOR3 vPos, unsigned long uIdPlayer, const char* lpStr);
	void SetRegionName(TCHAR* tszName);		// 지역

	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);

	CWndNavigator();
	virtual ~CWndNavigator();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
};
//////////////////////////////////////////////////////////////////////////////////////
// CWndStatus
//

class CWndStatus : public CWndNeuz
{
	bool m_bHPVisible;
	bool m_bExpVisible;
public:
	CModel* m_pModel;
	int		m_nDisplay;
public:
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	CTexture m_texGauEmptyNormal;
	CTexture m_texGauEmptySmall;
	CTexture m_texGauFillNormal;
	CTexture m_texGauFillSmall;

	LPDIRECT3DVERTEXBUFFER9 m_pVBHPGauge;
	LPDIRECT3DVERTEXBUFFER9 m_pVBMPGauge;
	LPDIRECT3DVERTEXBUFFER9 m_pVBFPGauge;
	LPDIRECT3DVERTEXBUFFER9 m_pVBEXPGauge;

	bool m_bVBHPGauge;
	bool m_bVBMPGauge;
	bool m_bVBFPGauge;
	bool m_bVBEXPGauge;

#if __VER >= 15 // __GUILD_HOUSE
	bool m_bShowTip_AEXP;
	bool m_bVBAEXPGauge;
	LPDIRECT3DVERTEXBUFFER9 m_pVBAEXPGauge;
#endif

	int m_nHPWidth;
	int m_nMPWidth;
	int m_nFPWidth;
	int m_nEXPWidth;
	int m_nPXPWidth;

	void MakeGaugeVertex();

	CWndStatus();
	virtual ~CWndStatus();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual	void PaintFrame(C2DRender* p2DRender);

#if __VER >= 15 // __GUILD_HOUSE
	virtual void OnMouseWndSurface(CPoint point);
#endif
};

class CWndQuit : public CWndMessageBox
{
	bool	m_bFlag;
public:
	CWndQuit() { }
	virtual ~CWndQuit() { }
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};
class CWndLogOut : public CWndMessageBox
{
public:
	CWndLogOut() { }
	virtual ~CWndLogOut() { }
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};


class CWndLogOutWaitting : public CWndNeuz
{
	bool  m_bIsLogOut;
	unsigned long dwTime;
public:
	CWndLogOutWaitting();
	virtual ~CWndLogOutWaitting();
	void	SetIsLogOut(bool bFlag) { m_bIsLogOut = bFlag; }
	virtual bool Process();
	virtual bool Initialize(CWndBase* pWndParent);
	virtual	void OnInitialUpdate();
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

//DECLARE_WNDMESSAGEBOX( CWndLogOut    ) // 로그아웃 처리하는 메시지 박스 클레스 

//BEGIN_WNDCLASS( CWndMacro )
//END_WNDCLASS

//BEGIN_WNDCLASS( CWndStyle )
//END_WNDCLASS

class CWndTradeGold : public CWndNeuz
{
public:
	unsigned int m_nIdWndTo;
	SHORTCUT m_Shortcut;
	D3DXVECTOR3 m_vPos;
	unsigned long m_dwGold;
	CWndBase* m_pWndBase;
	CWndStatic* pStatic;
	CWndStatic* pStaticCount;
#if __VER >= 11 // __SYS_POCKET
	char m_nSlot;
	char m_nPutSlot;
#else
	BYTE m_nSlot;
	BYTE m_nPutSlot;
#endif
	CWndTradeGold();
	virtual ~CWndTradeGold();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool Process(void);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndConfirmTrade : public CWndNeuz
{
	OBJID m_objid;
public:
	CWndConfirmTrade();
	virtual ~CWndConfirmTrade();

	void OnSetName(const char* szName, OBJID objid);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndTradeConfirm : public CWndNeuz
{
public:
	bool bMsg;
	CWndTradeConfirm();
	virtual ~CWndTradeConfirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

BEGIN_WNDCLASS(CWndTrade)

CWndTradeCtrl m_wndItemCtrlYou;
CWndTradeCtrl m_wndItemCtrlI;
CWndStatic    m_wndGold;
int           m_nGoldI, m_nGoldYou;

void DoCancel();

END_WNDCLASS

//////////

BEGIN_WNDCLASS(CWndEmotion)
CWndTreeCtrl  m_wndViewCtrl;
CWndEdit      m_wndEdit;
CWndButton    m_wndDefault;
LPTREEELEM    m_lpSelectTreeElem;
END_WNDCLASS


class CWndRevival : public CWndNeuz
{
public:
	CWndButton* m_pLodeLight;
	CWndButton* m_pLodeStar;
	CWndButton* m_pRevival;
#if __VER >= 9 // __S_9_ADD
	CWndButton* m_pShop;
#endif // __S_9_ADD
	CWndRevival();
	virtual ~CWndRevival();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	void	EnableButton(int nButton, bool bEnable);
};

class CWndReWanted : public CWndNeuz
{
public:
	CWndReWanted();
	virtual ~CWndReWanted();

private:
	CString      m_strWanted;

public:
	bool         CheckWantedInfo(int nGold, const char* szMsg);
	void		 SetWantedName(const char* szWanted);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndResurrectionConfirm : public CWndNeuz
{
public:
	CWndResurrectionConfirm();
	virtual ~CWndResurrectionConfirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndWantedConfirm : public CWndNeuz
{
	int		 m_nGold;
	char	 m_szName[MAX_NAME];
public:
	CWndWantedConfirm();
	virtual ~CWndWantedConfirm();

	void		 SetInfo(const char szName[], const int nGold);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


struct WANTEDLIST
{
	char	 szName[MAX_NAME];		// 이름.
	__int64	 nGold;						// 현상금
	char     szDate[32];
	char	 szMsg[WANTED_MSG_MAX + 1];	// 십자평 
};

class CWndWanted : public CWndNeuz
{
private:
	CWndWantedConfirm* m_pWantedConfirm;
	time_t				m_recvTime;
	CWndScrollBar		m_wndScrollBar;
	int					m_nMax;
	WANTEDLIST			m_aList[MAX_WANTED_LIST];
	int					m_nSelect;

public:
	CWndWanted();
	virtual ~CWndWanted();

	int          GetSelectIndex(const CPoint& point);
	void		 Init(time_t lTime);
	void		 InsertWanted(const char szName[], __int64 nGold, int nTime, const char szMsg[]);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseWndSurface(CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
};


class CWndCommItemDlg : public CWndNeuz
{
public:
	CWndEdit* m_pWndEdit;
	unsigned long			m_dwObjId;
	unsigned long			m_dwCtrlId;
	void			SetItem(unsigned long dwDefindText, unsigned long dwObjId, unsigned long dwCtrlId);
	CWndCommItemDlg();
	virtual ~CWndCommItemDlg();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndChangeClass1 : public CWndNeuz
{
public:
	int			nJob;
	void		SetJob();

	CWndChangeClass1();
	virtual ~CWndChangeClass1();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndChangeClass2 : public CWndNeuz
{
public:
	int			nJob;
	void		SetJob();

	CWndChangeClass2();
	virtual ~CWndChangeClass2();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndPostSendConfirm;

class CWndPostSend : public CWndNeuz
{
	int		m_nCost;
	int		m_nCount;
	BYTE    m_nItem;

public:
	void ClearData();
	CWndPostSend();
	virtual ~CWndPostSend();


	BYTE GetItemId() { return m_nItem; }
	void SetItemId(BYTE nId);
	void SetCost(int nCost) { m_nCost = nCost; }
	void SetCount(int nCount) { m_nCount = nCount; }
	void SetReceive(char* pchar);
	void SetTitle(char* pchar);
	void SetText(char* pchar);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void AdditionalSkinTexture(LPWORD pDest, CSize sizeSurface, D3DFORMAT d3dFormat = D3DFMT_A4R4G4B4);
};

class CWndPostItemWarning : public CWndNeuz
{
public:
	int	   m_nMailIndex;

	CWndPostItemWarning();
	virtual ~CWndPostItemWarning();

	void	SetIndex(int nIndex) { m_nMailIndex = nIndex; }
	void	SetString(char* string);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndPostDeleteConfirm;
class CWndPostRead : public CWndNeuz
{
	int					m_nMailIndex;
	CWndGold			m_wndGold;
	bool				m_bDrag;
	CWndPostDeleteConfirm* m_pDeleteConfirm;

public:
	CWndPostItemWarning* m_pWndPostItemWarning;
	void MailReceiveItem();
	void MailReceiveGold();
	void ClearData();
	CWndPostRead();
	virtual ~CWndPostRead();

	void SetValue(int nMailIndex);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
};


class CWndPostReceive : public CWndNeuz
{
	CTexture			m_Texture[3];
	CWndScrollBar		m_wndScrollBar;
	int					m_nSelect;
	int					m_nMax;
	CWndPostRead* m_wndPostRead;
public:
	void UpdateScroll();

	CWndPostReceive();
	virtual ~CWndPostReceive();

	int          GetSelectIndex(const CPoint& point);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual	void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual HRESULT DeleteDeviceObjects();
	virtual void OnMouseWndSurface(CPoint point);
};
class CWndPost : public CWndNeuz
{
public:
	CWndPostSend		m_PostTabSend;
	CWndPostReceive		m_PostTabReceive;

	CWndPost();
	virtual ~CWndPost();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

#ifdef __MAIL_REQUESTING_BOX
public:
	void LoadLastMailBox(void);
	void SaveLastMailBox(void);
	void CloseMailRequestingBox(void);

private:
	CWndMailRequestingBox* m_pWndMailRequestingBox;
#endif // __MAIL_REQUESTING_BOX
};
class CWndPostSendConfirm : public CWndNeuz
{
protected:
	CWndText	m_wndText;

	BYTE		m_nItem;
	short		m_nItemNum;
	int			m_nGold;
	char		m_szReceiver[MAX_NAME];
	char		m_szTitle[MAX_MAILTITLE];
	char		m_szText[MAX_MAILTEXT];
public:
	CWndPostSendConfirm();
	virtual ~CWndPostSendConfirm();

	void			SetValue(BYTE nItem, short nItemNum, char* lpszReceiver, int nGold, char* lpszTitle, char* lpszText);
	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndPostDeleteConfirm : public CWndNeuz
{
protected:
	CWndText	m_wndText;
	int		m_nIndex;
public:
	CWndPostDeleteConfirm();
	virtual ~CWndPostDeleteConfirm();

	void			SetValue(int nIndex) { m_nIndex = nIndex; }
	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
};

struct GUILDLIST
{
	char	 szGuild[MAX_NAME];		// 이름.
	char	 szName[MAX_NAME];		// 이름.
	int		 nNum;
};

class CWndGuildCombatOffer : public CWndNeuz
{
protected:
	unsigned long			m_dwReqGold;
	unsigned long			m_dwMinGold;
	unsigned long			m_dwBackupGold;

public:
	CWndGuildCombatOffer();
	virtual ~CWndGuildCombatOffer();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
	void			SetGold(unsigned long nCost);
	void			SetTotalGold(__int64 nCost);
	void			SetMinGold(unsigned long dwMinGold) { m_dwMinGold = dwMinGold; }
	void			SetReqGold(unsigned long dwReqGold) { m_dwReqGold = dwReqGold; }
	void			SetBackupGold(unsigned long dwBackupGold) { m_dwBackupGold = dwBackupGold; }
	void			EnableAccept(bool bFlag);
};

class CWndGuildCombatBoard : public CWndNeuz
{
protected:
#if __VER >= 11 // __GUILD_COMBAT_1TO1
public:
	int m_nCombatType;
#endif	//__GUILD_COMBAT_1TO1
public:
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	CWndGuildCombatBoard(int m_nCombatType);
#else //__GUILD_COMBAT_1TO1
	CWndGuildCombatBoard();
#endif //__GUILD_COMBAT_1TO1
	virtual ~CWndGuildCombatBoard();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	virtual void	PaintFrame(C2DRender* p2DRender);
#endif //__GUILD_COMBAT_1TO1
	void			SetString(char* szChar);
};

class CGuildCombatSelectionClearMessageBox : public CWndMessageBox
{
public:
	CString m_strMsg;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CGuildCombatInfoMessageBox : public CWndNeuz
{
#if __VER >= 11 // __GUILD_COMBAT_1TO1
public:
	int	m_nCombatType;
#endif //__GUILD_COMBAT_1TO1
public:
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	CGuildCombatInfoMessageBox(int nCombatType);
#else //__GUILD_COMBAT_1TO1
	CGuildCombatInfoMessageBox();
#endif //__GUILD_COMBAT_1TO1
	virtual ~CGuildCombatInfoMessageBox();
	void	SetString(char* szChar);
	virtual	void OnInitialUpdate();
	virtual	bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	virtual void PaintFrame(C2DRender* p2DRender);
	void	SetString(CString strMsg);
#endif //__GUILD_COMBAT_1TO1
};

class CGuildCombatInfoMessageBox2 : public CWndNeuz
{
public:
	CGuildCombatInfoMessageBox2();
	virtual ~CGuildCombatInfoMessageBox2();
	void	SetString(char* szChar);
	virtual	void OnInitialUpdate();
	virtual	bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};



class CWndGuildCombatSelection : public CWndNeuz
{
protected:
	multimap<int, CGuildMember*>	m_mapSelectPlayer;   // 길드리스트...레벨소팅

	vector<unsigned long>					m_vecGuildList;   // 길드 리스트
	vector<unsigned long>					m_vecSelectPlayer;   // 참가자 리스트..

	unsigned long							m_uidDefender;
	CTexture						m_TexDefender;
	int								m_nDefenderIndex;

	int								nMaxJoinMember;
	int								nMaxWarMember;

public:
	void Reset();
	CWndGuildCombatSelection();
	virtual ~CWndGuildCombatSelection();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
	void			EnableFinish(bool bFlag);

	void			SetDefender(unsigned long uiPlayer);
	void			UpDateGuildListBox();

	void			AddCombatPlayer(unsigned long uiPlayer);
	void			AddGuildPlayer(unsigned long uiPlayer);

	void			RemoveCombatPlayer(int nIndex);
	void			RemoveGuildPlayer(int nIndex);

	unsigned long			FindCombatPlayer(unsigned long uiPlayer);
	unsigned long			FindGuildPlayer(unsigned long uiPlayer);

	void			SetMemberSize(int nMaxJoin, int nMaxWar);
};

class CWndGuildCombatState : public CWndNeuz
{
private:
	CTimeSpan		m_ct;
	time_t    		m_tEndTime; // timegettime+
	time_t    		m_tCurrentTime; // timegettime+

#if __VER >= 11 // __GUILD_COMBAT_1TO1
public:
	int				m_nCombatType;
#endif //__GUILD_COMBAT_1TO1

public:
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	CWndGuildCombatState(int nCombatType);
#else //__GUILD_COMBAT_1TO1
	CWndGuildCombatState();
#endif //__GUILD_COMBAT_1TO1
	virtual ~CWndGuildCombatState();

	void		 InsertTitle(const char szTitle[]);
	int          GetSelectIndex(const CPoint& point);
	void		 Init(time_t lTime);
	void		 InsertGuild(const char szGuild[], const char szName[], int nNum);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	virtual void	PaintFrame(C2DRender* p2DRender);
#endif //__GUILD_COMBAT_1TO1
	virtual bool Process();
#if __VER < 11 // __GUILD_COMBAT_1TO1
	void		 SetTotalGold(__int64 nGold);
#endif //__GUILD_COMBAT_1TO1
	void		 SetGold(int nGold);
	void		 SetRate(int nRate);
	void	 	 SetTime(time_t tTime) { m_tCurrentTime = 0; m_tEndTime = time_null() + tTime; }
};

class CWndGuildCombatJoinSelection : public CWndNeuz
{
protected:
	int	   m_nMapNum;
	CTimer m_timerInputTimeOut;
	unsigned long  m_dwOldTime;

public:
	CWndGuildCombatJoinSelection();
	virtual ~CWndGuildCombatJoinSelection();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool	Process();

	void			SetTimeOut(unsigned long dwSec) { m_dwOldTime = g_tmCurrent + dwSec; }
	void			SetMapNum(int nMap) { m_nMapNum = nMap; }
};

class CWndGuildWarAppConfirm : public CWndNeuz
{
protected:
	CWndText	m_wndText;
public:
	CWndGuildWarAppConfirm();
	virtual ~CWndGuildWarAppConfirm();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
};
class CWndGuildWarCancelConfirm : public CWndNeuz
{
protected:
	CWndText	m_wndText;
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	int			m_nCombatType;
#endif //__GUILD_COMBAT_1TO1
public:
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	CWndGuildWarCancelConfirm(int nCombatType);
#else //__GUILD_COMBAT_1TO1
	CWndGuildWarCancelConfirm();
#endif //__GUILD_COMBAT_1TO1
	virtual ~CWndGuildWarCancelConfirm();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	virtual void	PaintFrame(C2DRender* p2DRender);
#endif //__GUILD_COMBAT_1TO1
};
class CWndGuildWarJoinConfirm : public CWndNeuz
{
#if __VER >= 11 // __GUILD_COMBAT_1TO1
public:
	int			m_nCombatType;
#endif //__GUILD_COMBAT_1TO1
protected:
	CWndText	m_wndText;
public:
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	CWndGuildWarJoinConfirm(int nCombatType);
#else //__GUILD_COMBAT_1TO1
	CWndGuildWarJoinConfirm();
#endif //__GUILD_COMBAT_1TO1
	virtual ~CWndGuildWarJoinConfirm();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	virtual void	PaintFrame(C2DRender* p2DRender);
#endif //__GUILD_COMBAT_1TO1
};

class CWndGuildWarState : public CWndNeuz
{
private:
	CWndScrollBar		m_wndScrollBar;
	int					m_nMax;
	GUILDLIST			m_aList[MAX_WANTED_LIST];
	int					m_nSelect;

public:
	CWndGuildWarState();
	virtual ~CWndGuildWarState();

	void		 InsertTitle(const char szTitle[]);
	int          GetSelectIndex(const CPoint& point);
	void		 Init(time_t lTime);
	void		 InsertGuild(const char szGuild[], const char szName[], int nNum);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseWndSurface(CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
};


struct GUILDNAME
{
	char	 szGuild[MAX_NAME];		// 이름.
};

class CWndGuildCombatRanking : public CWndNeuz
{
private:
	CWndScrollBar		m_wndScrollBar;
	int					m_nMax;
	multimap< int, GUILDNAME > m_multimapRanking;
	int					m_nSelect;

public:
	void SortRanking();
	CWndGuildCombatRanking();
	virtual ~CWndGuildCombatRanking();

	int          GetSelectIndex(const CPoint& point);
	void		 Init(time_t lTime);
	void		 InsertGuild(const char szGuild[], int nWinCount);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseWndSurface(CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
};

// 길드 결과 로그 - 길드
class CWndGuildCombatTabResultRate : public CWndNeuz
{
public:
	CWndGuildCombatTabResultRate();
	virtual ~CWndGuildCombatTabResultRate();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
};
// 길드 결과 로그 - 개인
class CWndGuildCombatTabResultLog : public CWndNeuz
{
public:
	CWndGuildCombatTabResultLog();
	virtual ~CWndGuildCombatTabResultLog();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
};

// 길드 결과 로그창
class CWndGuildCombatResult : public CWndNeuz
{
public:
	void InsertLog(CString str);
	void InsertPersonRate(CString str);
	void InsertGuildRate(CString str);
	CWndGuildCombatResult();
	virtual ~CWndGuildCombatResult();

	CWndGuildCombatTabResultRate   m_WndGuildCombatTabResultRate;
	CWndGuildCombatTabResultLog    m_WndGuildCombatTabResultLog;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
};



typedef struct __GUILDCOMBAT_RANK_INFO
{
	unsigned long	uidPlayer;
	int		nJob;
} __GUILDCOMBAT_RANK_INFO;

typedef struct __GUILDCOMBAT_RANK_INFO2
{
	CString strName;
	CString strJob;
	unsigned long	uidPlayer;
	int		nPoint;
} __GUILDCOMBAT_RANK_INFO2;

#define MAX_GUILDCOMBAT_RANK_PER_PAGE 11
#define MAX_GUILDCOMBAT_RANK		  100


// 길드 랭킹 탭- 직업별
class CWndGuildCombatRank_Class : public CWndNeuz
{
public:
	CWndScrollBar		m_wndScrollBar;
	int					m_nMax;
	int					m_nSelect;

	int		m_nRate;
	int		m_nOldRate;

	__GUILDCOMBAT_RANK_INFO2		m_listRank[MAX_GUILDCOMBAT_RANK];

	CWndGuildCombatRank_Class();
	virtual ~CWndGuildCombatRank_Class();

	void		 InsertRank(int nJob, unsigned long uidPlayer, int nPoint);
	int          GetSelectIndex(const CPoint& point);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual	void OnLButtonDown(unsigned int nFlags, CPoint point);
};

// 길드 랭킹 - 직업별
class CWndGuildCombatRank_Person : public CWndNeuz
{
public:
	multimap< int, __GUILDCOMBAT_RANK_INFO > m_mTotalRanking;

	CWndGuildCombatRank_Person();
	virtual ~CWndGuildCombatRank_Person();

	CWndGuildCombatRank_Class    m_WndGuildCombatTabClass_Tot;
	CWndGuildCombatRank_Class    m_WndGuildCombatTabClass_Mer;
	CWndGuildCombatRank_Class    m_WndGuildCombatTabClass_Mag;
	CWndGuildCombatRank_Class    m_WndGuildCombatTabClass_Acr;
	CWndGuildCombatRank_Class    m_WndGuildCombatTabClass_Ass;

	CWndGuildCombatRank_Class* __GetJobKindWnd(int nJob);
	void						InsertRank(int nJob, unsigned long	uidPlayer, int nPoint);
	void						UpdateList();
	void						DivisionList();
	void						UpdatePlayer(int nJob, unsigned long idPlayer);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);

};

class CWndFontEdit : public CWndNeuz
{
	CTexture* m_pTexture;
	CPoint			 m_ColorScrollBar[3];
	CPoint			 m_OriginalColorScrollBar[3];

	CRect			 m_ColorRect[3];
	float			 m_fColor[3];

	bool			 m_bLButtonClick;

public:
	CWndText* m_pWndText;
	CWndFontEdit();
	virtual ~CWndFontEdit();

	void	ReSetBar(float r, float g, float b);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
};

#if __VER >= 8 //__CSC_VER8_3

#include "WndWorld.h"

class CWndBuffStatus : public CWndNeuz
{
public:
	vector< multimap<unsigned long, BUFFSKILL> > m_pBuffTexture;
	list<BUFFICON_INFO> m_pBuffIconInfo;

	int m_BuffIconViewOpt;
public:
	CWndBuffStatus();
	virtual ~CWndBuffStatus();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	virtual	void PaintFrame(C2DRender* p2DRender);

#ifdef __BUFF_1107
	void RenderBuffIcon(C2DRender* p2DRender, IBuff* pBuff, bool bPlayer, BUFFICON_INFO* pInfo, CPoint ptMouse);
#else	// __BUFF_1107
	void RenderBuffIcon(C2DRender* p2DRender, SKILLINFLUENCE* pSkill, bool bPlayer, BUFFICON_INFO* pInfo, CPoint ptMouse);
#endif	// __BUFF_1107
	void RenderOptBuffTime(C2DRender* p2DRender, CPoint& point, CTimeSpan& ct, unsigned long dwColor);
	void SetBuffIconInfo();
	bool GetHitTestResult();
};
#endif //__CSC_VER8_3

#if __VER >= 9 // __CSC_VER9_1
/*******************************
	제련 시스템 관련 Window
********************************/
#include "WndSummonAngel.h"

class CWndMixJewelConfirm : public CWndNeuz
{
public:
	CWndMixJewelConfirm();
	virtual ~CWndMixJewelConfirm();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndMixJewel : public CWndNeuz
{
public:
	CWndText* m_pText;
	GENMATDIEINFO m_MatJewel[MAX_JEWEL];	// cr : uw :
	ItemCountSet m_ItemInfo[MAX_JEWEL];		// cr : uw :

	int m_nitemcount;
	int m_nSelectCtrl;
	int m_nOrichalcum;
	int m_nMoonstone;

	bool m_bStart;

	CWndInventory* m_pWndInventory;
	CWndMixJewelConfirm* m_pConfirm;
public:

	CWndMixJewel();
	~CWndMixJewel();

	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual	void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual void OnDestroy(void);
	virtual	void	OnDestroyChildWnd(CWndBase* pWndChild);	// cr : uw :

	void SetDescription(char* szChar);
	int HitTest(CPoint point);
	void ReceiveResult(int nResult);
	void SetJewel(CItemElem* pItemElem);
	void SetStartBtn(bool buse);
	void SetConfirmInit();
};

class CWndExtraction : public CWndNeuz
{
public:
	CWndText* m_pText;
	CItemElem* m_pItemElem;
	ItemProp* m_pEItemProp;

public:
	CWndExtraction();
	virtual ~CWndExtraction();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);

	void SetDescription(char* szChar);
	void ReceiveResult(int result, int nCount);
	void SetWeapon(CItemElem* pItemElem);
};

class CWndSmeltJewel : public CWndNeuz
{
public:
	CModelObject* m_pMainItem;
	CItemElem* m_pItemElem;
	CItemElem* m_pJewelElem;
	CWndText* m_pText;

	unsigned long m_dwJewel[5];
	int m_nJewelSlot[5];
	int m_nJewelCount;
	int m_nUsableSlot;
	OBJID m_objJewelId;

	float m_fRotate;
	float m_fAddRot;

	int m_nStatus;
	int m_nCount;
	int m_nDelay;
	int m_nAlpha;
	float m_nEyeYPos;
	bool m_bFlash;

public:
	CWndSmeltJewel();
	~CWndSmeltJewel();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual	void OnDestroy(void);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual bool Process();

	void SetDescription(char* szChar);
	void ReceiveResult(int result);
	void InitializeJewel(CItemElem* pItemElem);
	void SetJewel(CItemElem* pItemElem);
};

class CWndChangeWeapon : public CWndNeuz
{
public:
	CWndText* m_pText1;
	CWndText* m_pText2;
	CItemElem* m_pWItemElem;
	CItemElem* m_pJItemElem[2];
	int m_nWeaponType;
	bool m_bIsSendChange;

public:
	CWndChangeWeapon(int nType);
	virtual ~CWndChangeWeapon();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual bool Process();

	void SetDescription(char* szChar);
	void ReceiveResult(int result);
	void SetItem(CItemElem* pItemElem);
};

class CWndRemoveJewelConfirm : public CWndNeuz
{
public:
	CWndInventory* m_pInventory;
	CItemBase* m_pUpgradeItem;

public:
	CWndRemoveJewelConfirm();
	virtual ~CWndRemoveJewelConfirm();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

	void SetItem(CItemBase* m_pItem);
};
#endif //__CSC_VER9_1

#if __VER >= 10 // __CSC_VER9_1 -> __LEGEND
/*******************************
	전승 시스템 관련 Window
********************************/
class CWndHeroSkillUp : public CWndNeuz
{
public:
	CWndText* m_pText;
	CItemElem* m_pItemElem[5];
	int m_JewelID[5];
	int m_SlotID[5];
	int m_PicJewel[5];
	bool m_bSendHeroSkillup;

public:
	CWndHeroSkillUp();
	virtual ~CWndHeroSkillUp();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);

	int HitTest(CPoint point);
	void SetDescription(char* szChar);
	void ReceiveResult(int nresult);
	void SetJewel(CItemElem* pItemElem);
};

#endif //__CSC_VER9_1 -> __LEGEND

#ifdef __TRADESYS

#define MAX_LIST_COUNT 15

class CWndDialogEvent : public CWndNeuz
{
public:
	vector<int> m_nVecList;
	vector<int> m_nDescList;

	int m_nChoiceNum;
	int m_nListCount;
	int m_nDescCount;
	int m_nCurrentPage;
	int m_nMMI;
	int m_nGap;			//List Gap
	int m_nListOffset;	//Selected List Rect Offset

public:
	CWndDialogEvent();
	virtual ~CWndDialogEvent();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();

	void SetDescription(char* szChar);
	void SetMMI(int mmi);
	void ReceiveResult(int result);
};
#endif //__TRADESYS

#if __VER >= 12 // __HEAVEN_TOWER
#define MAX_FLOOR_COUNT 15

class CWndHeavenTower : public CWndNeuz
{
public:
	vector<int> m_nVecList;
	vector<int> m_nDescList;
	vector<int> m_nMsgList;
	vector<int>	m_nCost;

	int m_nChoiceNum;
	int m_nListCount;
	int m_nDescCount;
	int m_nCurrentPage;
	int m_nGap;			//List Gap
	int m_nListOffset;	//Selected List Rect Offset

public:
	CWndHeavenTower();
	virtual ~CWndHeavenTower();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();

	void InitText();
	void InitWnd();
};

class CWndHeavenTowerEntranceConfirm : public CWndMessageBox
{
public:
	unsigned long	m_nFloor;

public:
	void	SetValue(CString str, unsigned long nFloor);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

#endif //__HEAVEN_TOWER

#if __VER >= 10 // __REMOVE_ATTRIBUTE

class CWndRemoveAttributeConfirm : public CWndNeuz
{
public:
	CWndRemoveAttributeConfirm();
	virtual ~CWndRemoveAttributeConfirm();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndRemoveAttribute : public CWndNeuz
{
public:
	CWndText* m_pText;
	CItemElem* m_pItemElem;
	ItemProp* m_pEItemProp;
	CTexture* m_pTexture;

	CWndRemoveAttributeConfirm* m_pWndConfirm;

public:
	CWndRemoveAttribute();
	virtual ~CWndRemoveAttribute();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);

	void SetDescription(char* szChar);
	void ReceiveResult(bool result);
	void SetWeapon(CItemElem* pItemElem);
};

#endif //__REMOVE_ATTRIBUTE

#if __VER >= 11 // __PIERCING_REMOVE
class CWndRemovePiercing : public CWndNeuz
{
public:
	CWndText* m_pText;
	CItemElem* m_pItemElem;
	ItemProp* m_pEItemProp;
	CTexture* m_pTexture;
#if __VER >= 12 // __CSC_VER12_4
	int			m_nInfoSlot[10];
#endif //__CSC_VER12_4

public:
	CWndRemovePiercing();
	virtual ~CWndRemovePiercing();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);

	void SetDescription(char* szChar);
#if __VER >= 12 // __CSC_VER12_4
	virtual void OnMouseWndSurface(CPoint point);
	void SetItem(CItemElem* pItemElem);
#else //__CSC_VER12_4
	void SetSuit(CItemElem* pItemElem);
#endif //__CSC_VER12_4
};
#endif //__PIERCING_REMOVE

#if __VER >= 12 // __CSC_VER12_4
class CWndRemoveJewel : public CWndNeuz
{
public:
	CWndText* m_pText;
	CItemElem* m_pItemElem;
	CItemElem* m_pMoonstone;
	ItemProp* m_pEItemProp;
	CTexture* m_pTexture;
	CTexture* m_pMoonstoneTex;
	int			m_nJewelCount;
	int			m_nJewelID[5];
	CTexture* m_pJewelTex[5];
	int			m_nJewelSlot[5];
	int			m_nInfoSlot[5];

public:
	CWndRemoveJewel();
	virtual ~CWndRemoveJewel();

	virtual void OnDestroy();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);

	void SetDescription(char* szChar);
	void SetItem(CItemElem* pItemElem);

private:
	void ResetJewel();
};
#endif //__CSC_VER12_4

#if __VER >= 13 // __EXT_ENCHANT
class CWndChangeAttribute : public CWndNeuz
{
public:
	CWndText* m_pText;
	int m_nAttributeNum;
	int m_nAttributeStaticID[5];
	int m_nTooltipTextIndx[6];
	CItemElem* m_pItemElem;
	CItemElem* m_pChangeItem;
	CTexture* m_pTexture;

public:
	CWndChangeAttribute();
	virtual ~CWndChangeAttribute();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual bool OnSetCursor(CWndBase* pWndBase, unsigned int nHitTest, unsigned int message);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual bool Process();

public:
	void SetChangeItem(CItemElem* pItemElem);
	void FillRect(C2DRender* p2DRender, CRect rectBg, unsigned long dwColorstart, unsigned long dwColorend);
};
#endif //__EXT_ENCHANT

#if __VER >= 13 // __CSC_VER13_2
class CWndCoupleMessage : public CWndMessageBox
{
public:
	enum { CM_SENDPROPOSE = 0, CM_RECEIVEPROPOSE, CM_CANCELCOUPLE };

	CString m_strText;
	int m_nMode;
	CObj* m_pTargetObj;

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

public:
	void SetMessageMod(CString strText, int nMode, CObj* pTargetObj = NULL);
};

class CWndCoupleTabInfo : public CWndNeuz
{
public:
	CCouple* m_pCouple;

	CTexture m_texGauEmptyNormal;
	CTexture m_texGauFillNormal;
	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;

public:
	CWndCoupleTabInfo();
	virtual ~CWndCoupleTabInfo();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
};

class CWndCoupleTabSkill : public CWndNeuz
{
public:
	CWndText* m_pText;
	CTexture* m_pSkillBgTexture;

public:
	CWndCoupleTabSkill();
	virtual ~CWndCoupleTabSkill();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
};

class CWndCoupleManager : public CWndNeuz
{
public:
	CWndCoupleTabInfo	m_wndCoupleTabInfo;
	CWndCoupleTabSkill	m_wndCoupleTabSkill;
public:
	CWndCoupleManager();
	virtual ~CWndCoupleManager();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
};
#endif //__CSC_VER13_2

#ifdef __FUNNY_COIN
class CWndFunnyCoinConfirm : public CWndNeuz
{
public:
	unsigned long m_dwItemId;
	CItemElem* m_pItemElem;

public:
	CWndFunnyCoinConfirm();
	virtual ~CWndFunnyCoinConfirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	void SetInfo(unsigned long dwItemId, CItemElem* pItemElem);
};
#endif //__FUNNY_COIN

#if __VER >= 14 // __SMELT_SAFETY

class CWndSmeltSafety : public CWndNeuz
{
public:
#if __VER >= 15 // __15_5TH_ELEMENTAL_SMELT_SAFETY
	enum WndMode { WND_NORMAL, WND_ACCESSARY, WND_PIERCING, WND_ELEMENT };
#else // __15_5TH_ELEMENTAL_SMELT_SAFETY
	enum WndMode { WND_NORMAL, WND_ACCESSARY, WND_PIERCING };
#endif // __15_5TH_ELEMENTAL_SMELT_SAFETY
	enum { SMELT_MAX = 10 };
	enum { ENCHANT_TIME = 2 };
	enum { EXTENSION_PIXEL = 32, HALF_EXTENSION_PIXEL = EXTENSION_PIXEL / 2 };
#if __VER >= 15 // __15_5TH_ELEMENTAL_SMELT_SAFETY
	enum { GENERAL_NON_USING_SCROLL2_LEVEL = 7, ELEMENTAL_NON_USING_SCROLL2_LEVEL = 10 };
#endif // __15_5TH_ELEMENTAL_SMELT_SAFETY

private:
	WndMode m_eWndMode;
	CItemElem* m_pItemElem;
	CTexture* m_pItemTexture;
	CTexture* m_pNowGaugeTexture;
	CTexture* m_pSuccessTexture;
	CTexture* m_pFailureTexture;
	int m_nMaterialCount;
	int m_nScroll1Count;
	int m_nScroll2Count;
	int m_nResultCount;
	bool m_bStart;
	bool m_bResultSwitch;
	unsigned long m_dwEnchantTimeStart;
	unsigned long m_dwEnchantTimeEnd;
	float m_fGaugeRate;
	int m_nValidSmeltCounter;
	int m_nCurrentSmeltNumber;
	int m_nResultStaticID[SMELT_MAX];
	GENMATDIEINFO m_Material[SMELT_MAX];
	GENMATDIEINFO m_Scroll1[SMELT_MAX];
	GENMATDIEINFO m_Scroll2[SMELT_MAX];
	bool m_bResultStatic[SMELT_MAX];
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBufferGauge;
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBufferSuccessImage;
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBufferFailureImage;
#if __VER >= 15 // __15_5TH_ELEMENTAL_SMELT_SAFETY
	ItemProp* m_pSelectedElementalCardItemProp;
#endif // __15_5TH_ELEMENTAL_SMELT_SAFETY

public:
	CWndSmeltSafety(WndMode eWndMode);
	virtual ~CWndSmeltSafety();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate();
	virtual bool Process();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	void SetItem(CItemElem* pItemElem);
	void RefreshInformation(void);
	void RefreshText(void);
	void RefreshValidSmeltCounter(void);
	void StopSmelting(void);
	void DisableScroll2(void);
	void ResetData(void);
	void AddListItem(GENMATDIEINFO* pListItem, CItemElem* pItemElem);
	void SubtractListItem(GENMATDIEINFO* pListItem);
	void DrawListItem(C2DRender* p2DRender);
	bool IsDropMaterialZone(CPoint point);
	bool IsDropScroll1Zone(CPoint point);
	bool IsDropScroll2Zone(CPoint point);
	bool IsAcceptableMaterial(ItemProp* pItemProp);
	bool IsAcceptableScroll1(ItemProp* pItemProp);
	bool IsAcceptableScroll2(ItemProp* pItemProp);
	int GetNowSmeltValue(void);
	int GetDefaultMaxSmeltValue(void);
	void SetResultSwitch(bool bResultSwitch) { m_bResultSwitch = bResultSwitch; }
	void SetCurrentSmeltNumber(int nCurrentSmeltNumber) { m_nCurrentSmeltNumber = nCurrentSmeltNumber; }
	void SetResultStatic(bool bResultStatic, int nIndex) { m_bResultStatic[nIndex] = bResultStatic; }
	CItemElem* GetItemElement(void) const { return m_pItemElem; }
	bool GetResultSwitch(void) const { return m_bResultSwitch; }
	int GetCurrentSmeltNumber(void) const { return m_nCurrentSmeltNumber; }
	int GetResultStaticID(int nIndex) const { return m_nResultStaticID[nIndex]; }
};

class CWndSmeltSafetyConfirm : public CWndNeuz
{
public:
	enum ErrorMode { WND_ERROR1, WND_ERROR2, WND_ERROR3 };

private:
	ErrorMode m_eErrorMode;
	CItemElem* m_pItemElem;

public:
	CWndSmeltSafetyConfirm(ErrorMode eErrorMode);
	virtual ~CWndSmeltSafetyConfirm();

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate();
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDestroy(void);

	void SetWndMode(CItemElem* pItemElem);
};

#endif //__SMELT_SAFETY

#if __VER >= 14 // __EQUIP_BIND
class CWndEquipBindConfirm : public CWndNeuz
{
public:
	enum EquipAction { EQUIP_DOUBLE_CLICK, EQUIP_DRAG_AND_DROP };

public:
	CWndEquipBindConfirm(EquipAction eEquipAction);
	virtual ~CWndEquipBindConfirm(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

public:
	void SetInformation(CItemBase* pItemBase, unsigned long dwObjId);
	void SetInformation(CItemElem* pItemElem);

private:
	void EquipItem(void);

private:
	EquipAction m_eEquipAction;
	CItemBase* m_pItemBase;
	unsigned long m_dwObjId;
	CItemElem* m_pItemElem;
};
#endif // __EQUIP_BIND

#if __VER >= 14 // __RESTATE_CONFIRM
class CWndRestateConfirm : public CWndNeuz
{
public:
	CWndRestateConfirm(unsigned long dwItemID);
	virtual ~CWndRestateConfirm(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

public:
	void SetInformation(unsigned long dwItemObjID, OBJID m_ObjID, int nPart = -1);

private:
	unsigned long m_dwItemID;
	unsigned long m_dwItemObjID;
	OBJID m_ObjID;
	int m_nPart;
};
#endif // __RESTATE_CONFIRM

#if __VER >= 15 // __CAMPUS
class CWndCampusInvitationConfirm : public CWndNeuz
{
public:
	CWndCampusInvitationConfirm(unsigned long idSender = 0, const CString& rstrSenderName = _T(""));
	virtual ~CWndCampusInvitationConfirm(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

private:
	unsigned long m_idSender;
	CString m_strSenderName;
};

class CWndCampusSeveranceConfirm : public CWndNeuz
{
public:
	CWndCampusSeveranceConfirm(unsigned long idTarget = 0, const CString& rstrTargetName = _T(""));
	virtual ~CWndCampusSeveranceConfirm(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

private:
	unsigned long m_idTarget;
	CString m_strTargetName;
};
#endif // __CAMPUS

#endif // !defined(AFX_WNDFIELD_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)