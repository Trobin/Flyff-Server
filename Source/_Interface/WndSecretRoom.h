#if __VER >= 12 // __SECRET_ROOM

#ifndef __WNDGUILDSECRETROOM__H
#define __WNDGUILDSECRETROOM__H

//////////////////////////////////////////////////////////////////////////
// Secret Room 
//////////////////////////////////////////////////////////////////////////

class CWndSecretRoomSelection : public CWndNeuz
{
protected:
	multimap<int, CGuildMember*>	m_mapSelectPlayer;   // 정렬된 길드원 리스트

	vector<unsigned long>					m_vecGuildList;   // 길드원 리스트
	vector<unsigned long>					m_vecSelectPlayer;   // 참가자 리스트

public:
	void Reset();
	CWndSecretRoomSelection();
	virtual ~CWndSecretRoomSelection();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
	void			EnableFinish(bool bFlag);

	void			UpDateGuildListBox();

	void			AddCombatPlayer(unsigned long uiPlayer);
	void			AddGuildPlayer(unsigned long uiPlayer);

	void			RemoveCombatPlayer(int nIndex);
	void			RemoveGuildPlayer(int nIndex);

	unsigned long			FindCombatPlayer(unsigned long uiPlayer);
	unsigned long			FindGuildPlayer(unsigned long uiPlayer);

	//	void			SetMaster();
};

class CWndSecretRoomOffer : public CWndNeuz
{
protected:
	unsigned long			m_dwReqGold;
	unsigned long			m_dwMinGold;
	unsigned long			m_dwBackupGold;

public:
	CWndSecretRoomOffer();
	virtual ~CWndSecretRoomOffer();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
	void			SetGold(unsigned long nCost);
	void			SetMinGold(unsigned long dwMinGold) { m_dwMinGold = dwMinGold; }
	void			SetReqGold(unsigned long dwReqGold) { m_dwReqGold = dwReqGold; }
	void			SetBackupGold(unsigned long dwBackupGold) { m_dwBackupGold = dwBackupGold; }
	void			EnableAccept(bool bFlag);
};

class CWndSecretRoomOfferState : public CWndNeuz
{
private:
	CTimeSpan		m_ct;
	time_t    		m_tEndTime;		// timegettime+
	time_t    		m_tCurrentTime; // timegettime+

public:
	CWndSecretRoomOfferState();
	virtual ~CWndSecretRoomOfferState();

	void		 InsertTitle(const char szTitle[]);
	int          GetSelectIndex(const CPoint& point);
	void		 Init(time_t lTime);
	void		 InsertGuild(const char szGuild[], const char szName[], int nNum);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual bool Process();
	void		 SetGold(int nGold);
	void		 SetRate(int nRate);
	void	 	 SetTime(time_t tTime) { m_tCurrentTime = 0; m_tEndTime = time_null() + tTime; }
};

class CWndSecretRoomBoard : public CWndNeuz
{
public:
	CWndSecretRoomBoard();
	virtual ~CWndSecretRoomBoard();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
	void			SetString();
};
/*
class CWndSecretRoomGuildMember : public CWndNeuz
{
public:
	bool m_bSelect;
	unsigned long m_uGuildMemberId;
	CTexture m_texGauHPEmptyNormal;
	CTexture m_texGauHPFillNormal;

	LPDIRECT3DVERTEXBUFFER9 m_pVBHPGauge;
public:
	CWndSecretRoomGuildMember();
	~CWndSecretRoomGuildMember();

	virtual	bool	Initialize( CWndBase* pWndParent = NULL, unsigned long nType = MB_OK );
	virtual	bool	OnChildNotify( unsigned int message, unsigned int nID, LRESULT* pLResult );
	virtual	void	OnDraw( C2DRender* p2DRender );
	virtual	void	OnInitialUpdate();
	virtual void	OnLButtonUp( unsigned int nFlags, CPoint point );
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
	virtual void	OnDestroy();
	virtual void	OnMouseWndSurface( CPoint point );

	void SetSelectColor(bool bSet) {m_bSelect = bSet;};
};

#define MAX_SECRETROOM_MEMBER 11

typedef struct __SECRETROOM_GUILDMEMBER
{
	unsigned long m_uGuildMemberId;
	CWndSecretRoomGuildMember* m_pWndGuildMember;
} __SECRETROOM_GUILDMEMBER;

class CWndSecretRoomGuildMemMng : public CWndNeuz
{
private:
	CWndListBox* m_pWndListBox;
	int m_nGuildMemCount;
	CWndSecretRoomGuildMember* m_pWndSelectMember;
	CWndSecretRoomGuildMember* m_pWndLastMember;
	vector<__SECRETROOM_GUILDMEMBER> m_vecGuildMember;

public:
	CWndSecretRoomGuildMemMng();
	~CWndSecretRoomGuildMemMng();

	virtual	bool Initialize( CWndBase* pWndParent = NULL, unsigned long nType = MB_OK );
	virtual	bool OnChildNotify( unsigned int message, unsigned int nID, LRESULT* pLResult );
	virtual	void OnDraw( C2DRender* p2DRender );
	virtual	void OnInitialUpdate();

	void SetGuildMemCount(int nGuildMemCount) {m_nGuildMemCount = nGuildMemCount;};
	void SetGuildMember(unsigned long uGuildMemberId);
	void SetSelect(CWndSecretRoomGuildMember* pWndSelectMember);
	bool IsSelect(CWndSecretRoomGuildMember* pWndMember);
	void DestroyMemberWnd(unsigned long uMember);
	CWndSecretRoomGuildMember* GetBeforeWnd();
	void SetVisibleMng(bool bVisible);
};
*/

#define MAX_SECRETROOM_MEMBER 11

class CWndSecretRoomQuick : public CWndNeuz
{
public:
	int m_StaticID[MAX_SECRETROOM_MEMBER];
	CWndStatic* m_pWndMemberStatic[MAX_SECRETROOM_MEMBER];
	CTexture m_texGauEmptyNormal;
	CTexture m_texGauFillNormal;

	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;

	vector<unsigned long> m_vecGuildMemberId;
	unsigned long m_FocusMemberid;
	int m_MemberCount;
	int m_nWndHeight;
	bool m_bMini;
public:

	CWndSecretRoomQuick();
	~CWndSecretRoomQuick();

	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	void SetActiveMember(int MemberNum);
	void SetGuildMemCount(int nGuildMemCount) { m_MemberCount = nGuildMemCount; };
	void SetGuildMember(unsigned long uGuildMemberId);
	void SortMemberList();
};

class CWndSecretRoomChangeTaxRate : public CWndNeuz
{
public:
	int m_nDefaultSalesTax;
	int m_nDefaultPurchaseTax;
	int m_nChangeSalesTax;
	int m_nChangePurchaseTax;
	int m_nMinTax;
	int m_nMaxTax;

	BYTE m_nCont;

public:
	CWndSecretRoomChangeTaxRate();
	virtual ~CWndSecretRoomChangeTaxRate();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();

	void SetDefaultTax(int nMinTax, int nMaxTax, BYTE nCont);
};

class CWndSecretRoomCheckTaxRate : public CWndNeuz
{
public:
	int m_nSalesTax;
	int m_nPurchaseTax;
	unsigned long m_dwGuildId;

public:
	CWndSecretRoomCheckTaxRate();
	virtual ~CWndSecretRoomCheckTaxRate();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
};

//////////////////////////////////////////////////////////////////////////
// Message Box Class
//////////////////////////////////////////////////////////////////////////

class CWndSecretRoomSelectionResetConfirm : public CWndMessageBox
{
public:
	CString m_strMsg;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndSecretRoomOfferMessageBox : public CWndMessageBox
{
public:
	unsigned long m_nCost;
	void	SetValue(CString str, unsigned long nCost);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndSecretRoomInfoMsgBox : public CWndNeuz
{
public:
	CWndSecretRoomInfoMsgBox();

	virtual ~CWndSecretRoomInfoMsgBox();
	void	SetString(char* szChar);
	virtual	void OnInitialUpdate();
	virtual	bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

	void	SetString(CString strMsg);
};

class CWndSecretRoomChangeTaxRateMsgBox : public CWndMessageBox
{
public:
	int m_nSalesTax;
	int m_nPurchaseTax;
	BYTE m_nCont;

public:
	void	SetValue(CString str, int nSalesTax, int nPurchaseTax, BYTE nCont);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndSecretRoomCancelConfirm : public CWndNeuz
{
public:
	CWndSecretRoomCancelConfirm();
	virtual ~CWndSecretRoomCancelConfirm();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif //__WNDGUILDSECRETROOM__H
#endif //__SECRET_ROOM