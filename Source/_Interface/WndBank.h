#ifndef __WNDBANK__H
#define __WNDBANK__H

class CWndBank : public CWndNeuz
{
public:
	CWndGold     m_wndGold[3];
	CWndItemCtrl m_wndItemCtrl[3];
	CRect RectItemCtrl[3];
	CRect RectGold[3];
	CWndStatic* pCost[3];
	bool  bUse[3];
	CTexture* m_pTexture;

	CWndBank();
	~CWndBank();
	void ReSetBank(void);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndConfirmBank : public CWndNeuz
{
public:
	unsigned long	m_dwId;
	unsigned long	m_dwItemId;
	void	SetItem(unsigned long dwId, unsigned long dwItemId) { m_dwId = dwId;	m_dwItemId = dwItemId; }
public:
	CWndConfirmBank();
	~CWndConfirmBank();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndBankPassword : public CWndNeuz
{
public:
	int m_nFlags;
	unsigned long	m_dwId;
	unsigned long	m_dwItemId;
	void	SetItem(unsigned long dwId, unsigned long dwItemId) { m_dwId = dwId;		m_dwItemId = dwItemId; }
public:
	CWndBankPassword();
	~CWndBankPassword();

	void SetBankPassword(int nFlags);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


#endif
