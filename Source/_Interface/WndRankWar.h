#ifndef __WNDRANKWAR__H
#define __WNDRANKWAR__H

class CWndRankWarTabGiveUp : public CWndNeuz
{
public:
	CWndRankWarTabGiveUp();
	~CWndRankWarTabGiveUp();

	int		m_nCurrentList;			// ��µ� �������Ʈ�� ���� �ε���.
	int		m_nMxOld, m_nMyOld;			// ���� ��ǥ.

	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndRankWarTabLose : public CWndNeuz
{
public:
	CWndRankWarTabLose();
	~CWndRankWarTabLose();

	int		m_nCurrentList;			// ��µ� �������Ʈ�� ���� �ε���.
//	int		m_nSelect;
	int		m_nMxOld, m_nMyOld;			// ���� ��ǥ.

	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndRankWarTabWin : public CWndNeuz
{
public:
	CWndRankWarTabWin();
	~CWndRankWarTabWin();

	int		m_nCurrentList;			// ��µ� �������Ʈ�� ���� �ε���.
//	int		m_nSelect;
	int		m_nMxOld, m_nMyOld;			// ���� ��ǥ.

	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndRankWar : public CWndNeuz
{
public:
	CWndRankWar();
	~CWndRankWar();

	CWndRankWarTabGiveUp	m_WndRankWarTabGiveUp;
	CWndRankWarTabLose		m_WndRankWarTabLose;
	CWndRankWarTabWin		m_WndRankWarTabWin;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif