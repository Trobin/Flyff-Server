#if __VER >= 13 // __RAINBOW_RACE

#ifndef __WNDRAINBOWRACE__H
#define __WNDRAINBOWRACE__H

class CWndRainbowRaceOffer : public CWndNeuz
{
public:
	CWndText* m_pText;
	int m_nOfferCount;

public:
	CWndRainbowRaceOffer();
	virtual ~CWndRainbowRaceOffer();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();

	void SetOfferCount(int nCount) { m_nOfferCount = nCount; };
};

class CWndRainbowRaceInfo : public CWndNeuz
{
public:
	CWndText* m_pText;

public:
	CWndRainbowRaceInfo();
	virtual ~CWndRainbowRaceInfo();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
};

class CWndRainbowRaceRule : public CWndNeuz
{
public:
	CWndText* m_pText;

public:
	CWndRainbowRaceRule();
	virtual ~CWndRainbowRaceRule();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
};

class CWndRainbowRaceRanking : public CWndNeuz
{
public:
	unsigned long m_dwPlayerId[5];

public:
	CWndRainbowRaceRanking();
	virtual ~CWndRainbowRaceRanking();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);

	void SetRankingPlayer(unsigned long* dwPlayerId);
};

class CWndRainbowRacePrize : public CWndNeuz
{
public:
	CWndText* m_pText;

public:
	CWndRainbowRacePrize();
	virtual ~CWndRainbowRacePrize();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
};

class CWndRainbowRaceMiniGameInfo : public CWndNeuz
{
public:
	CWndText* m_pText;
	CString m_strFileName;

public:
	CWndRainbowRaceMiniGameInfo();
	virtual ~CWndRainbowRaceMiniGameInfo();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();

	void SetFileName(CString strFileName) { m_strFileName = strFileName; };
};

class CWndRainbowRaceMiniGameButton : public CWndNeuz
{
public:
	CTexturePack m_BtnTexture;
	bool m_bLoadTexMap;
	bool m_bFocus;
	bool m_bPush;
	//	bool m_bMove;
	CPoint m_ptPush;
	int m_nAlpha;
	bool m_bReverse;

public:
	CWndRainbowRaceMiniGameButton();
	virtual ~CWndRainbowRaceMiniGameButton();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	//	virtual bool OnChildNotify( unsigned int message, unsigned int nID, LRESULT* pLResult ); 
	virtual	void OnInitialUpdate();
	virtual void CWndRainbowRaceMiniGameButton::PaintFrame(C2DRender* p2DRender);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	//	virtual void OnMouseWndSurface( CPoint point );
	//	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool Process();
};

class CWndRainbowRaceMiniGame : public CWndNeuz
{
public:
	CString m_strMiniGameInfo[7];
	CString m_strPathCheck[7];
	int m_TitleStaticID[7];
	int m_CheckStaticID[7];
	int m_nTextID[7];
	unsigned long m_dwColor[7];
	bool m_bIsCompletedGame[7];
	int m_nCurrentGame;
	bool m_bGameInfoRefresh;

	bool  m_bReverse;
	float m_fLerp;

	CWndRainbowRaceMiniGameInfo* m_pWndMiniGameInfo;

public:
	CWndRainbowRaceMiniGame();
	virtual ~CWndRainbowRaceMiniGame();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual bool OnSetCursor(CWndBase* pWndBase, unsigned int nHitTest, unsigned int message);
	virtual bool Process();

	void FillRect(C2DRender* p2DRender, CRect rectBg, unsigned long dwColorstart, unsigned long dwColorend, bool bLamp);

public:
	void SetCurrentGameInfo();
};

class CWndRainbowRaceMiniGameEnd : public CWndMessageBox
{
public:
	int m_nGameID;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDestroy();

public:
	void SetGameID(int nGameID) { m_nGameID = nGameID; };
};

class CWndRRMiniGameKawiBawiBo : public CWndNeuz
{
	enum MyPosition { KAWI, BAWI, BO };
public:
	CWndText* m_pText;
	int m_nWinningCount;			//연승 수
	int m_nMyChoice;				//내가 선택한 카드 (가위, 바위, 보)
	int m_nComChoice;				//Com이 선택한 카드
	int m_nCount;
	int m_nDelay;					//Com 상태를 회전시키는 Delay값
	int m_nResult;					//서버로 부터 결과가 온 상태
	int m_nStatus;					//현재의 상태 (Delay관련 적용 위해 사용)
	int m_nItemCount;
	int m_nNextItemCount;

	unsigned long m_dwRItemId;
	unsigned long m_dwRNextItemId;

	CString m_strChoice[3];			//가위, 바위, 보 Text

	CWndStatic* m_pStWinningCount;	//연승 표시
	CWndStatic* m_pStMyChoice;		//내가 선택한 카드 표시
	CWndStatic* m_pStComChoice;		//Com이 선택한 카드 표시

public:
	CWndRRMiniGameKawiBawiBo();
	virtual ~CWndRRMiniGameKawiBawiBo();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Process();

	void ReceiveResult(int nResult) { m_nResult = nResult; };
	void RefreshInfo();
	void DisableAllBtn();
	void EnableAllBtn();
	int GetKawiBawiBoComResult(int nMyChoice, int nResult);
};

class CWndRRMiniGameDice : public CWndNeuz
{
public:
	CWndText* m_pText;
	int m_nTargetNum;
	int m_nTargetNumBackup;
	int m_nDiceNum1, m_nDiceNum2;
	int m_nResultDiceNum1, m_nResultDiceNum2;	//서버로 부터 받은 결과 값
	CString m_strPathDice[6];
	int m_nResult;
	int m_nCount1, m_nCount2;
	int m_nDelay1, m_nDelay2;					//Com 상태를 회전시키는 Delay값
	int m_nStatus1, m_nStatus2;					//현재의 상태 (Delay관련 적용 위해 사용)
	bool m_bFinishStatus1;

	CTexturePack m_texTargetNum;
public:
	CWndRRMiniGameDice();
	virtual ~CWndRRMiniGameDice();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual bool Process();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	void SetTargetNumber(int nNum) { m_nTargetNum = nNum; };
	void ReceiveResult(int nResult, int nDice1, int nDice2);
};

class CWndRRMiniGameArithmeticTimeOver : public CWndMessageBox
{
public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDestroy();
};

class CWndRRMiniGameArithmetic : public CWndNeuz
{
public:
	CWndText* m_pText;
	CString m_strQuestion;
	unsigned long m_dwTime;
	int m_nCorrectCount;

	CWndRRMiniGameArithmeticTimeOver* m_pWndRRMiniGameArithmeticTimeOver;

public:
	CWndRRMiniGameArithmetic();
	virtual ~CWndRRMiniGameArithmetic();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();

	void SetNextQuestion(CString strQuestion, int nCorrectCount);
	virtual bool Process();
};

class CWndRRMiniGameStopWatch : public CWndNeuz
{
public:
	CWndText* m_pText;
	unsigned long m_dwTargetTime;
	unsigned long m_dwStartTime;
	unsigned long m_dwTime;
	bool m_bStart;
	time_t m_timeSec;
	int m_nUSec;
	CTexturePack m_texStopWatchNum;

public:
	CWndRRMiniGameStopWatch();
	virtual ~CWndRRMiniGameStopWatch();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual bool Process();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

public:
	void SetTargetTime(unsigned long dwTargetTime);
};

class  CWndRRMiniGameTyping : public CWndNeuz
{
public:
	CWndText* m_pText;

	int m_QuestionStaticID[3];
	int m_TypingStaticID[3];

public:
	CWndRRMiniGameTyping();
	virtual ~CWndRRMiniGameTyping();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();

public:
	void SetQuestion(int nQuestion, CString strQuestion);
	void ResetQuestion();
};

typedef struct __CARD_INFO
{
	CTexture* m_pTexture;
	int m_nCardNum;
	bool m_bCheck;
} __CARD_INFO;

class CWndRRMiniGameCard : public CWndNeuz
{
public:
	CWndText* m_pText;
	CString m_strCard[9];
	CTexture* m_texCardBack;
	__CARD_INFO m_stCard[18];
	int m_nCustomID[18];
	int m_nPair[2];
	unsigned long m_dwPenaltyTime;
	int m_nPenaltySec;
	CTexturePack m_texPenaltyNum;

public:
	CWndRRMiniGameCard();
	virtual ~CWndRRMiniGameCard();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual bool Process();
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual bool OnSetCursor(CWndBase* pWndBase, unsigned int nHitTest, unsigned int message);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

public:
	void SetQuestion(CString strQuestion);
	void ReceiveResult(int nResult);
};

class CWndRRMiniGameLadderFail : public CWndMessageBox
{
public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDestroy();
};

class CWndRRMiniGameLadder : public CWndNeuz
{
	enum { GO_DOWN, GO_LEFT, GO_RIGHT };
public:
	int m_nResult;
	int m_nTarget;
	int m_nStart;
	int m_nLadder[15][10];
	CWndText* m_pText;
	int m_nStartCustomID[15];
	bool m_bStart;
	bool m_bMakeLadder;
	bool m_bFailMsg;
	CPoint m_ptPoint;
	int m_nGoTo;
	int m_nLevel;
	int m_nPoint;
	int m_nNextPosY;

	CTexture* m_pTexBg1;
	CTexture* m_pTexBg2;
	CTexture* m_pTexHorizon;
	CTexture* m_pTexChoice;
	CTexture* m_pTexPoint;
	CTexture* m_pTexGoal;
	CTexture* m_pTexFail;

	CRect m_rectLine[21];
	CRect m_rectPointLine;
	int m_nLineCount;
	bool  m_bReverse;
	float m_fLerp;

	CWndRRMiniGameLadderFail* m_pWndRRMiniGameLadderFail;

public:
	CWndRRMiniGameLadder();
	virtual ~CWndRRMiniGameLadder();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual bool Process();
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual bool OnSetCursor(CWndBase* pWndBase, unsigned int nHitTest, unsigned int message);

public:
	bool MakeLadder();
	void ReceiveResult(int nResult);
	void SetFillRectValue();
	void FillRect(C2DRender* p2DRender, CRect rectBg, unsigned long dwColorstart, unsigned long dwColorend);
	void Reset();
};
#endif //__WNDRAINBOWRACE__H
#endif //__RAINBOW_RACE