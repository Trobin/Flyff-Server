#ifndef __WNDDIALOG2__H
#define __WNDDIALOG2__H

class CWndAnswer : public CWndButton
{
public:
	LPVOID m_pWordButton;
};

class CWndDialog : public CWndNeuz
{
	CWndAnswer* m_apWndAnswer[6];
public:
	CTimer m_timer;
	CTexture m_texChar;
	bool m_bWordButtonEnable;
	int m_nWordButtonNum;
	int m_nKeyButtonNum;
	int m_nContextButtonNum;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	int m_nNewQuestListNumber;
	int m_nCurrentQuestListNumber;
	int m_nSelectKeyButton;
#endif // __IMPROVE_QUEST_INTERFACE
	CUIntArray m_aContextMark[32];
	struct WORDBUTTON
	{
		bool bStatus;
		CRect rect;
		TCHAR szWord[64];
		TCHAR szKey[64];
		unsigned long dwParam;
		unsigned long dwParam2;
		int nLinkIndex; // 줄바꿈으로 단어가 끊어진 경우 연결하기 위한 인덱스 
	};
	WORDBUTTON m_aWordButton[32];
	WORDBUTTON m_aKeyButton[32];
	WORDBUTTON m_aContextButton[32];
	CEditString m_string;

	unsigned long m_dwQuest;// context 버튼에서 사용함 
	bool m_bSay;
	int m_nCurArray;
	CPtrArray m_strArray;
	OBJID m_idMover;
	CMapStringToString m_mapWordToOriginal;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
private:
	CWndListBox m_WndNewQuestListBox;
	CWndListBox m_WndCurrentQuestListBox;
	CTexture* m_pNewQuestListIconTexture;
	CTexture* m_pExpectedQuestListIconTexture;
	CTexture* m_pCurrentQuestListIconTexture;
	CTexture* m_pCompleteQuestListIconTexture;
public:
#endif // __IMPROVE_QUEST_INTERFACE

	CWndDialog();
	~CWndDialog();

	void RemoveAllKeyButton();
	void RemoveKeyButton(const char* lpszKey);
	void AddAnswerButton(const char* lpszWord, const char* lpszKey, unsigned long dwParam, unsigned long dwQuest);
	void AddKeyButton(const char* lpszWord, const char* lpszKey, unsigned long dwParam, unsigned long dwQuest);
	void ParsingString(const char* lpszString);
	void Say(const char* lpszString, unsigned long dwQuest);
	void EndSay();
	void BeginText();
	void MakeKeyButton();
	void MakeContextButton();
	void MakeAnswerButton();
	void UpdateButtonEnable();
	bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	void RunScript(const char* szKey, unsigned long dwParam, unsigned long dwQuest);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	void AddNewQuestList(const char* lpszWord, const char* lpszKey, const unsigned long dwParam, const unsigned long dwQuest);
	void AddCurrentQuestList(const char* lpszWord, const char* lpszKey, const unsigned long dwParam, const unsigned long dwQuest);
	void MakeQuestKeyButton(const CString& rstrKeyButton);

private:
	void RenderNewQuestListIcon(C2DRender* p2DRender);
	void RenderCurrentQuestListIcon(C2DRender* p2DRender);
	void AddQuestList(CWndListBox& pWndListBox, int& nQuestListNumber, const char* lpszWord, const char* lpszKey, const unsigned long dwParam, const unsigned long dwQuest);

public:
#endif // __IMPROVE_QUEST_INTERFACE

	virtual bool Process();
	virtual bool OnSetCursor(CWndBase* pWndBase, unsigned int nHitTest, unsigned int message);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
};
#endif
