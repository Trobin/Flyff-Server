#if __VER >= 11 // __GUILD_COMBAT_1TO1

#ifndef __WNDGUILDCOMBAT1TO1__H
#define __WNDGUILDCOMBAT1TO1__H

//////////////////////////////////////////////////////////////////////////
// 1:1 GuildCombat Class
//////////////////////////////////////////////////////////////////////////

class CWndGuildCombat1to1Selection : public CWndNeuz
{
protected:
	multimap<int, CGuildMember*>	m_mapSelectPlayer;   // 정렬된 길드원 리스트

	vector<unsigned long>					m_vecGuildList;   // 길드원 리스트
	vector<unsigned long>					m_vecSelectPlayer;   // 참가자 리스트

//	unsigned long							m_uidDefender;
//	CTexture						m_TexDefender;
//	int								m_nDefenderIndex;

//	int								nMaxJoinMember;
//	int								nMaxWarMember;

public:
	void Reset();
	CWndGuildCombat1to1Selection();
	virtual ~CWndGuildCombat1to1Selection();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
	void			EnableFinish(bool bFlag);

	//	void			SetDefender( unsigned long uiPlayer );
	void			UpDateGuildListBox();

	void			AddCombatPlayer(unsigned long uiPlayer);
	void			AddGuildPlayer(unsigned long uiPlayer);

	void			RemoveCombatPlayer(int nIndex);
	void			RemoveGuildPlayer(int nIndex);

	unsigned long			FindCombatPlayer(unsigned long uiPlayer);
	unsigned long			FindGuildPlayer(unsigned long uiPlayer);

	//	void			SetMemberSize( int nMaxJoin,  int nMaxWar );
};

class CWndGuildCombat1to1Offer : public CWndNeuz
{
protected:
	unsigned long			m_dwReqGold;
	unsigned long			m_dwMinGold;
	unsigned long			m_dwBackupGold;

public:
	int				m_nCombatType; // 0 : 길드대전 , 1 : 1:1길드대전

public:
	CWndGuildCombat1to1Offer(int nCombatType);
	virtual ~CWndGuildCombat1to1Offer();

	virtual	bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual	bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void	PaintFrame(C2DRender* p2DRender);
	void			SetGold(unsigned long nCost);
	//	void			SetTotalGold( __int64 nCost );
	void			SetMinGold(unsigned long dwMinGold) { m_dwMinGold = dwMinGold; }
	void			SetReqGold(unsigned long dwReqGold) { m_dwReqGold = dwReqGold; }
	void			SetBackupGold(unsigned long dwBackupGold) { m_dwBackupGold = dwBackupGold; }
	void			EnableAccept(bool bFlag);
};

//////////////////////////////////////////////////////////////////////////
// Message Box Class
//////////////////////////////////////////////////////////////////////////

class CGuildCombat1to1SelectionResetConfirm : public CWndMessageBox
{
public:
	CString m_strMsg;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndGuildCombat1to1OfferMessageBox : public CWndMessageBox
{
public:
	unsigned long m_nCost;
	void	SetValue(CString str, unsigned long nCost);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};
#endif //__WNDGUILDCOMBAT1TO1__H
#endif //__GUILD_COMBAT_1TO1