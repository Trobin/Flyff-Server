#ifndef __WND_HELPER_WEB_BOX_H__
#define __WND_HELPER_WEB_BOX_H__

#ifdef __NEW_WEB_BOX
#ifdef __CLIENT

class CWndHelperWebBox : public CWndNeuz
{
public:
	enum { TASKBAR_HEIGHT = 48 };

public:
	CWndHelperWebBox(void);
	~CWndHelperWebBox(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool Process(void);
	virtual HRESULT RestoreDeviceObjects(void);

public:
	void Release(void);
};

#endif // __CLIENT
#endif // __NEW_WEB_BOX

#endif // __WND_HELPER_WEB_BOX_H__