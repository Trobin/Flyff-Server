#ifndef __WNDGUILD__H
#define __WNDGUILD__H

#include "WndGuildTabApp.h"
#include "WndGuildTabInfo.h"
#include "WndGuildTabMember.h"
#include "WndGuildTabWar.h"

#if __VER >= 15 // __GUILD_HOUSE
#include "WndGuildTabPower.h"
#endif


class CWndGuild : public CWndNeuz
{
public:
	CWndGuild();
	~CWndGuild();

	CWndGuildTabApp     m_WndGuildTabApp;
	CWndGuildTabInfo    m_WndGuildTabInfo;
	CWndGuildTabMember  m_WndGuildTabMember;
	CWndGuildTabWar		m_WndGuildTabWar;

#if __VER >= 15 // __GUILD_HOUSE
	CWndGuildTabPower m_WndGuildTabPower;

	void SetCurTab(int index);		//���õ� �� ���� 
#endif

	void UpdateDataAll();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
};


class CWndGuildConfirm : public CWndNeuz
{
public:
	CWndGuildConfirm();
	~CWndGuildConfirm();

	unsigned long	m_idMaster;
	CString strGuildName;
	void	SetGuildName(char* pGuildName);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif