#ifndef __WNDLORD__H
#define __WNDLORD__H
#if __VER >= 12 // __LORD
#include "clord.h"


class CWndLordConfirm : public CWndNeuz
{
private:
	CString		m_pPledge;
public:
	CWndLordConfirm(CString strStr) { m_pPledge = strStr; };
	~CWndLordConfirm() {};

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndLordPledge : public CWndNeuz
{
private:
	SPC					m_pRanker;
	bool				m_bIsFirst;
	CWndLordConfirm* m_pWndConfirm;

public:
	CWndLordPledge();
	CWndLordPledge(int nRank);
	~CWndLordPledge();

	void SetPledge(const char* strPledge);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	bool SetRanker(int nRank);
};

class CWndLordState : public CWndNeuz
{
private:
	unsigned long				m_tmRefresh;
	CString				strName[10];
	CString				strClass[10];
	int					m_nSelect;
public:
	CWndLordPledge* m_pWndPledge;

	CWndLordState();
	~CWndLordState();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool Process();
};



class CWndLordTender : public CWndNeuz
{
public:
	CWndLordTender();
	~CWndLordTender();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	void	RefreshDeposit();
};

class CWndLordVote : public CWndNeuz
{
private:
	unsigned long				m_tmRefresh;

public:
	CWndLordVote();
	~CWndLordVote();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool Process();
};

class CWndLordEvent : public CWndNeuz
{
private:
	int m_nEEvent;	// 사용자가 선택한 경험치 이벤트 인덱스, 초기값 0
	int m_nDEvent;	// 사용자가 선택한 드롭률 이벤트 인덱스, 초기값 0
public:
	CWndLordEvent();
	~CWndLordEvent();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndLordSkill : public CWndNeuz
{
private:
	bool          m_bDrag;
	CTexture* m_aTexSkill[9];
	int			  m_nCurSelect;
	LPWNDCTRL	  m_aWndCtrl[9];
public:
	CWndLordSkill();
	~CWndLordSkill();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
};

#define LORDSKILL_RECALL	6
#define LORDSKILL_TELEPORT	7

class CWndLordSkillConfirm : public CWndNeuz
{
private:
	int m_nType;

public:
	CWndLordSkillConfirm() { m_nType = 0; };
	~CWndLordSkillConfirm() {};
	CWndLordSkillConfirm(int nType) { m_nType = nType; };

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
};

class CWndLordInfo : public CWndNeuz
{

public:
	CWndLordInfo() {};
	~CWndLordInfo() {};

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
};

class CWndLordRPInfo : public CWndNeuz
{

public:
	CWndLordRPInfo() {};
	~CWndLordRPInfo() {};

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
};

#endif
#endif