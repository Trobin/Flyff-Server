#include "stdafx.h"
#include "resData.h"
#include "WndChangeName.h"
#include "dpclient.h"
#include "definetext.h"
extern		CDPClient		g_DPlay;


/*
  WndId : APP_CHANGENAME
  CtrlId : WIDC_STATIC1
  CtrlId : WIDC_OK
  CtrlId : WIDC_CANCEL
  CtrlId : WIDC_EDIT_CHANGENAME
*/

CWndChangeName::CWndChangeName()
{

}

CWndChangeName::~CWndChangeName()
{

}

void CWndChangeName::OnDraw(C2DRender* p2DRender)
{
#ifdef __S_SERVER_UNIFY
	if (g_WndMng.m_bAllAction == false)
	{
		CRect rectRoot = m_pWndRoot->GetLayoutRect();
		CRect rectWindow = GetWindowRect();
		CPoint point(rectRoot.right - rectWindow.Width(), 110);
		Move(point);
		MoveParentCenter();
	}
#endif // __S_SERVER_UNIFY
}

void CWndChangeName::OnInitialUpdate()
{
	CWndNeuz::OnInitialUpdate();

	m_dwData = 0;

	CRect rectRoot = m_pWndRoot->GetLayoutRect();
	CRect rectWindow = GetWindowRect();
	CPoint point(rectRoot.right - rectWindow.Width(), 110);
	Move(point);
	MoveParentCenter();
}

bool CWndChangeName::Initialize(CWndBase* pWndParent, unsigned long /*dwWndId*/)
{
	return CWndNeuz::InitDialog(g_Neuz.GetSafeHwnd(), APP_CHANGENAME, 0, CPoint(0, 0), pWndParent);
}

bool CWndChangeName::OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase)
{
	return CWndNeuz::OnCommand(nID, dwMessage, pWndBase);
}

void CWndChangeName::OnSize(unsigned int nType, int cx, int cy)
{
	CWndNeuz::OnSize(nType, cx, cy);
}

void CWndChangeName::OnLButtonUp(unsigned int nFlags, CPoint point)
{

}

void CWndChangeName::OnLButtonDown(unsigned int nFlags, CPoint point)
{

}

void CWndChangeName::SetData(unsigned short wId, unsigned short wReset)
{
	m_dwData = MAKELONG(wId, wReset);
}

extern unsigned long IsValidPlayerName(CString& strName);

bool CWndChangeName::OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult)
{
	if (nID == WIDC_OK)
	{
		CWndEdit* pEdit = (CWndEdit*)GetDlgItem(WIDC_EDIT_CHANGENAME);
		CString string = pEdit->GetString();

		unsigned long dwError = ::IsValidPlayerName(string);
		if (dwError > 0)
		{
			g_WndMng.OpenMessageBox(prj.GetText(dwError));
			pEdit->SetFocus();
			return true;
		}

		if (prj.IsInvalidName(string)
#ifdef __RULE_0615
			|| prj.IsAllowedLetter(string) == false
#endif	// __RULE_0615
			)
		{
			g_WndMng.OpenMessageBox(_T(prj.GetText(TID_DIAG_0020)));
			return true;
		}
		g_DPlay.SendQuerySetPlayerName(m_dwData, (LPSTR)(LPCSTR)string);
		Destroy();
	}
	else if (nID == WIDC_CANCEL || nID == WTBID_CLOSE)
	{
#ifdef __S_SERVER_UNIFY
		if (g_WndMng.m_bAllAction == false)
			return true;
#endif // __S_SERVER_UNIFY
		Destroy();
	}

	return CWndNeuz::OnChildNotify(message, nID, pLResult);
}

#ifdef __PET_1024

CWndChangePetName::CWndChangePetName()
{
	m_dwId = 0;
}

CWndChangePetName::~CWndChangePetName()
{

}

void CWndChangePetName::OnDraw(C2DRender* p2DRender)
{
#ifdef __S_SERVER_UNIFY
	if (g_WndMng.m_bAllAction == false)
	{
		CRect rectRoot = m_pWndRoot->GetLayoutRect();
		CRect rectWindow = GetWindowRect();
		CPoint point(rectRoot.right - rectWindow.Width(), 110);
		Move(point);
		MoveParentCenter();
	}
#endif // __S_SERVER_UNIFY
}

void CWndChangePetName::OnInitialUpdate()
{
	CWndNeuz::OnInitialUpdate();

	m_dwData = 0;

	CRect rectRoot = m_pWndRoot->GetLayoutRect();
	CRect rectWindow = GetWindowRect();
	CPoint point(rectRoot.right - rectWindow.Width(), 110);
	Move(point);
	MoveParentCenter();
}

bool CWndChangePetName::Initialize(CWndBase* pWndParent, unsigned long /*dwWndId*/)
{
	return CWndNeuz::InitDialog(g_Neuz.GetSafeHwnd(), APP_CHANGENAME, 0, CPoint(0, 0), pWndParent);
}

bool CWndChangePetName::OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase)
{
	return CWndNeuz::OnCommand(nID, dwMessage, pWndBase);
}

void CWndChangePetName::OnSize(unsigned int nType, int cx, int cy)
{
	CWndNeuz::OnSize(nType, cx, cy);
}

void CWndChangePetName::OnLButtonUp(unsigned int nFlags, CPoint point)
{

}

void CWndChangePetName::OnLButtonDown(unsigned int nFlags, CPoint point)
{

}

void CWndChangePetName::SetData(unsigned short wId, unsigned short wReset)
{
	m_dwData = MAKELONG(wId, wReset);
}

//extern unsigned long IsValidPlayerName( CString& strName );

bool CWndChangePetName::OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult)
{
	if (nID == WIDC_OK)
	{
		CWndEdit* pEdit = (CWndEdit*)GetDlgItem(WIDC_EDIT_CHANGENAME);
		CString string = pEdit->GetString();

		unsigned long dwError = ::IsValidPlayerName(string);
		if (dwError > 0)
		{
			g_WndMng.OpenMessageBox(prj.GetText(dwError));
			pEdit->SetFocus();
			return true;
		}

		if (prj.IsInvalidName(string)
#ifdef __RULE_0615
			|| prj.IsAllowedLetter(string) == false
#endif	// __RULE_0615
			)
		{
			g_WndMng.OpenMessageBox(_T(prj.GetText(TID_DIAG_0020)));
			return true;
		}
		// 펫 이름을 바꾸도록 요청하는 함수를 호출한다
		g_DPlay.SendDoUseItemInput(m_dwId, (LPSTR)(LPCSTR)string);
		Destroy();
	}
	else if (nID == WIDC_CANCEL || nID == WTBID_CLOSE)
	{
#ifdef __S_SERVER_UNIFY
		if (g_WndMng.m_bAllAction == false)
			return true;
#endif // __S_SERVER_UNIFY
		Destroy();
	}

	return CWndNeuz::OnChildNotify(message, nID, pLResult);
}
#endif