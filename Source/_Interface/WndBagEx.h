#ifndef __WNDBAGEX__H
#define __WNDBAGEX__H

#if __VER >= 11 // __SYS_POCKET
class CWndBagEx : public CWndNeuz
{
public:

	CWndItemCtrl	m_wndItemCtrl[3];
	CRect			m_RectItemCtrl[3];
	bool			m_bUse[3];
	CTexture* m_pTexNouse;
	CTexture* m_pTexIco;
	CTexture* m_pTexIco_empty;

	CWndBagEx();
	~CWndBagEx();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	virtual bool process();

	void	InitItem(void);
};
#endif
#endif