// WndWebBox.h: interface for the CWndWebBox class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __WNDWEBOX__H
#define __WNDWEBOX__H


void InitWebGlobalVar();

class CWndWebBox : public CWndNeuz
{
public:
	CRect    m_rectOldBackup;

	CWndWebBox();
	~CWndWebBox();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual HRESULT RestoreDeviceObjects();
	virtual bool Process();
	//	virtual void OnDestroy( void );
};

class CWndWebBox2 : public CWndNeuz
{
public:
	CRect    m_rectOldBackup;

	CWndWebBox2();
	~CWndWebBox2();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual HRESULT RestoreDeviceObjects();
	//	virtual void OnDestroy( void );
};
#endif // __WNDWEBOX__H