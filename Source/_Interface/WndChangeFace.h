#ifndef __WNDCHANGEFACE__H
#define __WNDCHANGEFACE__H

class CWndChangeFace : public CWndNeuz
{
public:
	int m_nSelectFace;
	CWndChangeFace();
	~CWndChangeFace();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndChangeSex : public CWndNeuz
{
private:
	unsigned long	m_dwItemId;
	unsigned long	m_dwObjId;
public:
	CModelObject* m_pModel;
	void	SetData(unsigned long dwItemId, unsigned long dwObjId)
	{
		m_dwItemId = dwItemId;	m_dwObjId = dwObjId;
	}

public:
	int m_nSelectFace;
	CWndChangeSex();
	~CWndChangeSex();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndItemTransy : public CWndNeuz
{
public:
	CRect		m_Rect[2];
	CItemElem* m_pItemElem[2];
	CItemElem   m_pItemElemChange;
#ifdef __SYS_ITEMTRANSY
	bool		m_bMenu;
#endif //__SYS_ITEMTRANSY

	CWndItemTransy();
	~CWndItemTransy();

#ifdef __SYS_ITEMTRANSY
	void Init(CItemElem* pItemElem, bool bMenu = false);
#else //__SYS_ITEMTRANSY
	void Init(CItemElem* pItemElem);
#endif // __SYS_ITEMTRANSY

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnDestroy(void);
	virtual	bool OnDropIcon(LPSHORTCUT	pShortcut, CPoint point);
};
#endif	// __WNDCHANGEFACE__H