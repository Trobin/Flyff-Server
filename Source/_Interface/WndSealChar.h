#ifndef __WNDSEALCHAR__H
#define __WNDSEALCHAR__H

class CWndSealChar : public CWndNeuz
{
public:

	CWndSealChar();
	~CWndSealChar();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndSealCharSelect : public CWndNeuz
{
public:
	char	m_szSrc1[MAX_NAME];	// 이름
	OBJID	m_idSrc1;
	char	m_szSrc2[MAX_NAME];	// 이름
	OBJID	m_idSrc2;
	long	m_lPlayerSlot1;
	long	m_lPlayerSlot2;
	short	m_sCount;

	CWndSealCharSelect();
	~CWndSealCharSelect();

	void	SetData(short sCount, long lPlayerSolt1, long lPlayerSolt2, unsigned long uPlayerID1, unsigned long uPlayerID2, char* szName1, char* szName2);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndSealCharSend : public CWndNeuz
{
public:
	char	m_szSrc1[MAX_NAME];	// 이름
	OBJID	m_idSrc1;

	CWndSealCharSend();
	~CWndSealCharSend();

	void	SetData(unsigned long uPlayerID1, char* szName1);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndSealCharSet : public CWndNeuz
{
public:
	char	m_szSrc1[MAX_NAME];	// 이름
	OBJID	m_idSrc1;
	unsigned long	m_dwData;

	CWndSealCharSet();
	~CWndSealCharSet();

	void SetData(unsigned long dwId, unsigned short wReset);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


#endif
