// WndBase.h: interface for the CWndBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDCONTROL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_)
#define AFX_WNDCONTROL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class C2DRender;

// Notification code
#define WNM_ERRSPACE            (-2)
#define WNM_SELCHANGE           1
#define WNM_DBLCLK              2
#define WNM_SELCANCEL           3
#define WNM_SETFOCUS            4
#define WNM_KILLFOCUS           5
#define WNM_CLICKED             6
///////
#define WNM_ITEMCHANGING        10
#define WNM_ITEMCHANGED         11
#define WNM_INSERTITEM          12
#define WNM_DELETEITEM          13
#define WNM_DELETEALLITEMS      14
#define WNM_BEGINLABELEDITA     15
#define WNM_BEGINLABELEDITW     16
#define WNM_ENDLABELEDITA       17
#define WNM_ENDLABELEDITW       18
#define WNM_COLUMNCLICK         19
#define WNM_BEGINDRAG           20
#define WNM_BEGINRDRAG          21
///////
//#define WNM_ITEMCHANGING        30
//#define WNM_ITEMCHANGED         31 
#define WNM_ITEMCLICK           32
#define WNM_ITEMDBLCLICK        33
#define WNM_DIVIDERDBLCLICK     34
#define WNM_BEGINTRACK          35
#define WNM_ENDTRACK            36
#define WNM_TRACK               37

//  WndItemCtrl Norification Message
#define WIN_DBLCLK              40
#define WIN_ITEMDROP            41   


//NM_CLICK
//WNM_BEGINRDRAG
//HDN_TRACK

/**
 윈도 매시지 박스에서 사용하는 버튼 종류
 MB_OK
 MB_OKCANCEL
 MB_ABORTRETRYIGNORE
 MB_YESNOCANCEL
 MB_YESNO
 MB_RETRYCANCEL
**/
// 이것은 추가된 것
#define MB_CANCEL           0x00000006L

//////////////////////////////////////////////////////////////////////////////
// CWndButton 
//////////////////////////////////////////////////////////////////////////////


class CWndMenu;

class CWndStatic : public CWndBase
{
public:
	CWndStatic();// { m_byWndType = WTYPE_STATIC; m_dwColor = 0xff646464; }
	~CWndStatic() { }
	virtual bool IsPickupSpace(CPoint point);
	void OnDraw(C2DRender* p2DRender);
	void SetWndRect(CRect rectWnd, bool bOnSize);
	bool Create(const char* lpszCaption, unsigned long dwStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void PaintFrame(C2DRender* p2DRender);
};

class CWndCustom : public CWndBase
{
public:
	CWndCustom() { m_byWndType = WTYPE_CUSTOM; }
	~CWndCustom() { }

	void OnDraw(C2DRender* p2DRender);
	bool Create(const char* lpszCaption, unsigned long dwStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID) {
		if (lpszCaption && strlen(lpszCaption)) m_strTitle = lpszCaption;
		return CWndBase::Create(dwStyle | WBS_CHILD | WBS_NOFRAME | WBS_NODRAWFRAME, rect, pParentWnd, nID);
	}
};
class CWndButton : public CWndBase
{
protected:
	unsigned long     m_nFontColor;
	unsigned long     m_nPushColor;
	unsigned long     m_nHighlightColor;
	unsigned long     m_nDisableColor;
	bool      m_bCheck;
	int       m_nTimePush;
	bool      m_bHighLight;
	CPoint    m_ptPush;
	CWndBase* m_pWndExecute; // 이 버튼을 누르므로서 실행될 윈도. 
//	CTimer m_timerPush;

	//unsigned long     m_dwButtonStyle;

public:
	char      m_cHotkey;

	CWndMenu* m_pWndMenu;
	int       m_bTopDown;
	SHORTCUT   m_shortcut;
	/*
unsigned long     m_dwShortCutType    ;
unsigned long     m_dwShortCutIndex   ;
unsigned long     m_dwShortCutUserId  ;
unsigned long     m_dwShortCutData    ;
CString   m_strShortCutString ;
//unsigned short     m_dwShortCutIndex;
*/
	CWndButton();
	~CWndButton();

	//bool Create(const char* lpszCaption,unsigned long dwStyle,const RECT& rect,CWndBase* pParentWnd,unsigned int nID,CSprPack* pSprPack,int nSprIdx,int nColorTable = 0);
	bool Create(const char* lpszCaption, unsigned long dwStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);// { return Create(lpszCaption,dwStyle,rect,pParentWnd,nID,NULL,0); }
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void PaintFrame(C2DRender* p2DRender);
	virtual	bool Process();

	void SetMenu(CWndMenu* pWndMenu);

	void FitTextureSize();

	// Attributes
	//bool IsButtonStyle( unsigned long dwStyle ) { return ( m_dwButtonStyle & dwStyle ) ? true : false; }
	void SetPushTime(int nTime); // 버튼을 누르고 nTime이후에 계속 OnCommand를 패어런트에게 보낸다.
	unsigned int GetState() const; // Retrieves the check state, highlight state, and focus state of a button control.
	void SetState(bool bHighlight); // Sets the highlighting state of a button control
	bool  GetCheck() const; // Retrieves the check state of a button control.
	void SetCheck(bool nCheck); // Sets the check state of a button control.
	//unsigned int GetButtonStyle() const; // Retrieves information about the button control style.
	//void SetButtonStyle(unsigned int nStyle, bool bRedraw = true); // Changes the style of a button.
	void SetString(CString strSndEffect);
	bool IsHighLight() { return m_bHighLight; }
	void SetFontColor(unsigned long dwColor) { m_nFontColor = (unsigned long)dwColor; }
	void SetPushColor(unsigned long dwColor) { m_nPushColor = (unsigned long)dwColor; }
	void SetDisableColor(unsigned long dwColor) { m_nDisableColor = (unsigned long)dwColor; }
	void SetHighLightColor(unsigned long dwColor) { m_nHighlightColor = (unsigned long)dwColor; }
	unsigned long GetFontColor() { return m_nFontColor; }
	unsigned long GetPushColor() { return m_nPushColor; }
	unsigned long GetDisableColor() { return m_nDisableColor; }
	unsigned long GetHighLightColor() { return m_nHighlightColor; }
	void SetWndExecute(CWndBase* pWndBase);
	CWndBase* GetWndExecute() { return m_pWndExecute; }
	void SetPushPoint(int x, int y) { m_ptPush = CPoint(x, y); }
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual LRESULT DefWindowProc(unsigned int message, WPARAM wParam, LPARAM lParam);
};

//////////////////////////////////////////////////////////////////////////////
// CWndScrollBar
//////////////////////////////////////////////////////////////////////////////

class CWndScrollBar : public CWndBase
{
protected:
	int        m_nScrollPos;
	int        m_nScrollMinPos;
	int        m_nScrollMaxPos;
	int        m_nScrollBarIdx;
	int        m_nScrollPage;
	int        m_nPos;
	bool       m_bPushPad;
	CRect      m_rectStick;
	CWndButton m_wndArrow1;
	CWndButton m_wndArrow2;

public:
	CTexture* m_pTexButtVScrUp;
	CTexture* m_pTexButtVScrDown;
	CTexture* m_pTexButtVScrBar;
	CTexture* m_pTexButtVScrPUp;
	CTexture* m_pTexButtVScrPDown;
	CTexture* m_pTexButtVScrPBar;

	//static CTexturePack m_texturePack;

	CWndScrollBar();
	~CWndScrollBar();

	bool Create(unsigned long dwScrollStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);//,CSprPack* pSprPack,int nSprIdx);

	int  GetMinScrollPos();
	int  GetMaxScrollPos();

	void SetMinScrollPos();
	void SetMaxScrollPos();
	int  GetScrollPos() const; // Retrieves the current position of a scroll box. 
	int  SetScrollPos(int nPos, bool bRedraw = true); // Sets the current position of a scroll box. 
	void GetScrollRange(LPINT lpMinPos, LPINT lpMaxPos) const; // Retrieves the current minimum and maximum scroll-bar positions for the given scroll bar. 
	int  GetScrollRange() const;
	void SetScrollRange(int nMinPos, int nMaxPos, bool bRedraw = true); // Sets minimum and maximum position values for the given scroll bar. 
	void SetScrollPage(int nPage);
	int  GetScrollPage() const;
	int  GetScrollAbiliableRange();
	int  GetScrollAbiliablePos();
	int  GetScrollAbiliablePage();
	void ShowScrollBar(bool bShow = true); // Shows or hides a scroll bar. 
	bool EnableScrollBar(unsigned int nArrowFlags = ESB_ENABLE_BOTH); // Enables or disables one or both arrows of a scroll bar. 
	bool SetScrollInfo(LPSCROLLINFO lpScrollInfo, bool bRedraw = true); // Sets information about the scroll bar. 
	bool GetScrollInfo(LPSCROLLINFO lpScrollInfo, unsigned int nMask = SIF_ALL); // Retrieves information about the scroll bar. 
	int  GetScrollLimit(); // Retrieves the limit of the scroll bar
	void SetScrollDown();
	/*
		CSprite* GetSprVertArrow1();
		CSprite* GetSprVertArrow2();
		CSprite* GetSprVertPad();
		CSprite* GetSprVertBar();
		CSprite* GetSprHoriArrow1();
		CSprite* GetSprHoriArrow2();
		CSprite* GetSprHoriPad();
		CSprite* GetSprHoriBar();
	*/
	virtual void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
};

//////////////////////////////////////////////////////////////////////////////
// CWndMenu

class CWndMenu : public CWndBase
{
	bool IsOnMenu(CPoint pt);
	int m_nLargeWidth;
public:
	CPtrArray  m_awndMenuItem;
	//	CWndButton m_pWndCommand[18];

	// Constructors
	CWndMenu();
	~CWndMenu();

	CWndButton* GetMenuItem(int nPos);
	void SetVisibleSub(bool bVisible);
	void SetVisibleAllMenu(bool bVisible);

	bool CreateMenu(CWndBase* pWndParent);
	bool CreatePopupMenu();
	bool LoadMenu(const char* lpszResourceName);
	bool LoadMenu(unsigned int nIDResource);
	bool LoadMenuIndirect(const void* lpMenuTemplate);
	bool DestroyMenu();
	virtual void OnKillFocus(CWndBase* pNewWnd);
	virtual void PaintFrame(C2DRender* p2DRender);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual void SetWndRect(CRect rectWnd, bool bOnSize = true);
	// Attributes
	//HMENU m_hMenu;          // must be first data member
	//HMENU GetSafeHmenu() const;
	//operator HMENU() const;

	//static CMenu* PASCAL FromHandle(HMENU hMenu);
	//static void PASCAL DeleteTempMap();
	//bool Attach(HMENU hMenu);
	//HMENU Detach();

	// CMenu Operations
	void DeleteAllMenu();
	bool DeleteMenu(unsigned int nPosition, unsigned int nFlags);
	//bool TrackPopupMenu(unsigned int nFlags, int x, int y,CWnd* pWnd, LPCRECT lpRect = 0);
	//bool operator==(const CMenu& menu) const;
	//bool operator!=(const CMenu& menu) const;

	// CMenuItem Operations
	virtual CWndButton* AppendMenu(unsigned int nFlags, unsigned int nIDNewItem = 0, const char* lpszNewItem = NULL);
	//bool AppendMenu(unsigned int nFlags, unsigned int nIDNewItem, const CBitmap* pBmp);
	unsigned int CheckMenuItem(unsigned int nIDCheckItem, unsigned int nCheck);
	unsigned int EnableMenuItem(unsigned int nIDEnableItem, unsigned int nEnable);
	unsigned int GetMenuItemCount() const;
	unsigned int GetMenuItemID(int nPos) const;
	unsigned int GetMenuState(unsigned int nID, unsigned int nFlags) const;
	int  GetMenuString(unsigned int nIDItem, LPTSTR lpString, int nMaxCount, unsigned int nFlags) const;
	int  GetMenuString(unsigned int nIDItem, CString& rString, unsigned int nFlags) const;
	//bool GetMenuItemInfo(unsigned int nIDItem, LPMENUITEMINFO lpMenuItemInfo,	bool fByPos = false);
	CWndMenu* GetSubMenu(int nPos) const;
	bool InsertMenu(unsigned int nPosition, unsigned int nFlags, unsigned int nIDNewItem = 0, const char* lpszNewItem = NULL);
	//bool InsertMenu(unsigned int nPosition, unsigned int nFlags, unsigned int nIDNewItem,	const CBitmap* pBmp);
	bool ModifyMenu(unsigned int nPosition, unsigned int nFlags, unsigned int nIDNewItem = 0, const char* lpszNewItem = NULL);
	//bool ModifyMenu(unsigned int nPosition, unsigned int nFlags, unsigned int nIDNewItem,	const CBitmap* pBmp);
	bool RemoveMenu(unsigned int nPosition, unsigned int nFlags);
	//bool SetMenuItemBitmaps(unsigned int nPosition, unsigned int nFlags, const CBitmap* pBmpUnchecked, const CBitmap* pBmpChecked);
	bool CheckMenuRadioItem(unsigned int nIDFirst, unsigned int nIDLast, unsigned int nIDItem, unsigned int nFlags);
	bool SetDefaultItem(unsigned int uItem, bool fByPos = false);
	unsigned int GetDefaultItem(unsigned int gmdiFlags, bool fByPos = false);
	void SetLargeWidth(int nLargeWidth) { m_nLargeWidth = nLargeWidth; }

	// Context Help Functions
	//bool SetMenuContextHelpId(unsigned long dwContextHelpId);
	//unsigned long GetMenuContextHelpId() const;

	// Overridables (must override draw and measure for owner-draw menu items)
	//virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
};
//////////////////////////////////////////////////////////////////////////////
// CWndText
//////////////////////////////////////////////////////////////////////////////
#define EBS_LEFT             0x00000000L
#define EBS_CENTER           0x00000001L
#define EBS_RIGHT            0x00000002L
#define EBS_MULTILINE        0x00000004L
#define EBS_UPPERCASE        0x00000008L
#define EBS_LOWERCASE        0x00000010L
#define EBS_PASSWORD         0x00000020L
#define EBS_AUTOVSCROLL      0x00000040L
#define EBS_AUTOHSCROLL      0x00000080L
#define EBS_NOHIDESEL        0x00000100L
#define EBS_OEMCONVERT       0x00000400L
#define EBS_READONLY         0x00000800L
#define EBS_WANTRETURN       0x00001000L
#define EBS_NUMBER           0x00002000L

class CWndText : public CWndBase
{
protected:
	CTimer m_timerCaret;
	bool   m_bCaretVisible;
	CPoint m_ptCaret;
	bool   m_bLButtonDown;

	unsigned long m_dwBlockBegin;
	unsigned long m_dwBlockEnd;
	unsigned long m_dwOffset;

	bool	m_bScr;
	int		m_nLineRefresh;

public:
	void Delete(int nIndex, int nLen);
	unsigned long GetBlockBegine() { return m_dwBlockBegin; }
	unsigned long GetBlockEnd() { return m_dwBlockEnd; }

	void BlockDelete(const char* pstr, const char* pstr2);
	void BlockInsert(const char* pstr, const char* pstr2);
	void Insert(int nIndex, const char* pstr);
	void BlockSetStyle(unsigned long dwStyle);
	void BlockClearStyle(unsigned long dwStyle);
	void BlockSetColor(unsigned long dwColor);
	CWndMenu m_wndMenu;

	TCHAR m_TextComp[3];
	TCHAR m_szCaret[3];
	bool m_bEnableClipboard;
	//CTexture* m_apTexture[ 9 ];
	int m_nLineSpace;
	CWndScrollBar m_wndScrollBar;
	CEditString m_string;

	CWndText();
	~CWndText();
	bool Create(unsigned long dwTextStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	CPoint m_ptDeflateRect;

	virtual	void OnInitialUpdate();
	virtual void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual bool OnChildNotify(unsigned int nCode, unsigned int nID, LRESULT* pLResult);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnSetFocus(CWndBase* pOldWnd);
	virtual void OnKillFocus(CWndBase* pNewWnd);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnChar(unsigned int nChar);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);

	int GetFontHeight() { return m_pFont->GetMaxHeight() + m_nLineSpace; }

	long GetOffset(CPoint point);
	void UpdateScrollBar();
	bool IsEmptyBlock() { return m_dwBlockBegin == m_dwBlockEnd; }
	CPoint OffsetToPoint(unsigned long dwOffset, TCHAR* pszStr);
	CPoint GetCaretPos() { return m_ptCaret; }
	virtual void DrawCaret(C2DRender* p2DRender);
	void SetCaretPos(CPoint ptCaret) { m_ptCaret = ptCaret; m_timerCaret.Reset(); }
	void HideCaret() { m_bCaretVisible = false; }
	void ShowCaret() { m_bCaretVisible = true; }

	void SetString(const char* pszString, unsigned long dwColor = 0xff000000);
	void AddString(const char* pszString, unsigned long dwColor = 0xff000000, unsigned long dwPStyle = 0x00000001);
	void ResetString();
};
//////////////////////////////////////////////////////////////////////////////
// CWndStatic
//////////////////////////////////////////////////////////////////////////////
/*
class CWndStatic : public CWndBase
{
public:

	CWndStatic();
	~CWndStatic();
	bool Create(unsigned long dwTextStyle,const RECT& rect,CWndBase* pParentWnd,unsigned int nID);

	virtual	void OnInitialUpdate();
	virtual void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnSetFocus(CWndBase* pOldWnd);
	virtual void OnKillFocus(CWndBase* pNewWnd);
	virtual void OnSize(unsigned int nType, int cx, int cy);

	long GetOffset(CPoint point);
	void UpdateScrollBar();

	CPoint OffsetToPoint(unsigned long dwOffset);
	CPoint GetCaretPos() { return m_ptCaret; }
	void DrawCaret(C2DRender* p2DRender);
	void SetCaretPos(CPoint ptCaret) { m_ptCaret = ptCaret; }
	void HideCaret() { m_bCaretVisible = false; }
	void ShowCaret() { m_bCaretVisible = true; }

	void SetString(const char* pszString);
	void AddString(const char* pszString);
	void ResetString();
};
*/
//////////////////////////////////////////////////////////////////////////////
// CWndViewCtrl
//////////////////////////////////////////////////////////////////////////////

typedef struct tagScriptElem
{
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	tagScriptElem(void);
	tagScriptElem(CRect* pRect);
	tagScriptElem* m_lpParent;
	unsigned long m_dwColor;
	unsigned long m_dwSelectColor;
	CEditString m_strKeyword;
	unsigned long m_dwData;
	CPtrArray m_ptrArray;
	bool m_bOpen;
	CWndButton* m_pWndCheckBox;
#else // __IMPROVE_QUEST_INTERFACE
	tagScriptElem* m_lpParent;
	unsigned long m_dwColor;
	CString   m_strKeyword;
	unsigned long     m_dwData;
	CPtrArray m_ptrArray;
	bool      m_bOpen;
#endif // __IMPROVE_QUEST_INTERFACE
}
TREEELEM, * LPTREEELEM;

class CWndTreeCtrl : public CWndBase
{
public:							//sun!!
	typedef struct tagITEM
	{
		CRect      m_rect;
		bool       m_bButton;
		LPTREEELEM m_lpTreeElem;
	} TREEITEM, * LPTREEITEM;

private:
	CPtrArray m_treeItemArray;
	void InterpriteScript(CScript& script, CPtrArray& ptrArray);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	virtual void PaintTree(C2DRender* p2DRender, CPoint& pt, CPtrArray& ptrArray);
#else // __IMPROVE_QUEST_INTERFACE
	void PaintTree(C2DRender* p2DRender, CPoint& pt, CPtrArray& ptrArray);
#endif // __IMPROVE_QUEST_INTERFACE
	LPTREEELEM m_pFocusElem;
	int  m_nFontHeight;
	unsigned long m_nWndColor;
	TREEELEM m_treeElem;
	CWndScrollBar m_wndScrollBar;
	void FreeTree(CPtrArray& ptrArray);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
public:
	enum { CHECK_BOX_SIZE_XY = 13 };
private:
	bool m_bMemberCheckingMode;
	int m_nMaxCheckNumber;
	int m_nTreeTabWidth;
	int m_nCategoryTextSpace;
	int m_nTreeItemsMaxWidth;
#endif // __IMPROVE_QUEST_INTERFACE
public:
	int   m_nLineSpace;
	CTexture* m_pTexButtOpen;
	CTexture* m_pTexButtClose;
	unsigned long m_nFontColor;
	unsigned long m_nSelectColor;

	CWndTreeCtrl();
	~CWndTreeCtrl();
	int GetFontHeight() { return m_pFont->GetMaxHeight() + m_nLineSpace; }
	bool DeleteAllItems();
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	void DeleteItemArray(void);
#endif // __IMPROVE_QUEST_INTERFACE
	LPTREEELEM GetCurSel();
	LPTREEELEM GetRootElem();
	LPTREEELEM GetNextElem(LPTREEELEM pElem, int& nPos);
	LPTREEELEM FindTreeElem(unsigned long dwData);
	LPTREEELEM FindTreeElem(CPtrArray& ptrArray, unsigned long dwData);
	LPTREEELEM SetCurSel(unsigned long dwData);
	LPTREEELEM SetCurSel(const char* lpszKeyword);
	LPTREEELEM FindTreeElem(const char* lpszKeyword);
	LPTREEELEM FindTreeElem(CPtrArray& ptrArray, const char* lpszKeyword);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	LPTREEELEM InsertItem(LPTREEELEM lpParent,
		const char* lpString,
		unsigned long dwData,
		bool bForbidChecking = true,
		bool bCheck = false,
		unsigned long dwColor = D3DCOLOR_ARGB(255, 64, 64, 64),
		unsigned long dwSelectColor = D3DCOLOR_ARGB(255, 0, 0, 255));
#else // __IMPROVE_QUEST_INTERFACE
	LPTREEELEM InsertItem(LPTREEELEM lpParent, const char* lpString, unsigned long dwData);
#endif // __IMPROVE_QUEST_INTERFACE
	void LoadTreeScript(const char* lpFileName);
	bool Create(unsigned long dwTextStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	bool CheckParentTreeBeOpened(LPTREEELEM lpTreeElem);
	CPtrArray* GetTreeItemArray(void);
	void SetFocusElem(const LPTREEELEM pFocusElem);
	const LPTREEELEM GetFocusElem(void) const;
	void SetLineSpace(int nLineSpace);
	int GetLineSpace(void) const;
	void SetMemberCheckingMode(bool bMemberCheckingMode);
	bool GetMemberCheckingMode(void) const;
	void SetMaxCheckNumber(int nMaxCheckNumber);
	int GetMaxCheckNumber(void) const;
	void SetTreeTabWidth(int nTreeTabWidth);
	int GetTreeTabWidth(void) const;
	void SetCategoryTextSpace(int nCategoryTextSpace);
	int GetCategoryTextSpace(void) const;
	int GetTreeItemsNumber(void) const;
	void SetTreeItemsMaxWidth(int nTreeItemsMaxWidth);
	int GetTreeItemsMaxWidth(void) const;
	int GetTreeItemsMaxHeight(void);
	void SetTextColor(unsigned long dwCategoryTextColor, unsigned long dwNormalTextColor, unsigned long dwSelectedCategoryTextColor, unsigned long dwSelectedNormalTextColor);

private:
	void CalculateTreeItemsNumber(int& nSumTreeItemsNumber, const CPtrArray& rPtrArray) const;
	void CalculateTreeItemsMaxHeight(int& nSumHeight, const CPtrArray& rPtrArray);
	void CalculateTextColor(unsigned long dwCategoryTextColor, unsigned long dwNormalTextColor, unsigned long dwSelectedCategoryTextColor, unsigned long dwSelectedNormalTextColor, const CPtrArray& rPtrArray);

public:
#endif // __IMPROVE_QUEST_INTERFACE
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);

	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);

	//virtual bool OnChildNotify(unsigned int message,unsigned int nID,LRESULT* pLResult);
/*
	bool Create(unsigned long dwStyle, const RECT& rect, CWnd* pParentWnd, unsigned int nID);

// Attributes
	bool GetItemRect(HTREEITEM hItem, LPRECT lpRect, bool bTextOnly) const;
	unsigned int GetCount() const;
	unsigned int GetIndent() const;
	void SetIndent(unsigned int nIndent);
	CImageList* GetImageList(unsigned int nImageList) const;
	CImageList* SetImageList(CImageList* pImageList, int nImageListType);
	HTREEITEM GetNextItem(HTREEITEM hItem, unsigned int nCode) const;
	HTREEITEM GetChildItem(HTREEITEM hItem) const;
	HTREEITEM GetNextSiblingItem(HTREEITEM hItem) const;
	HTREEITEM GetPrevSiblingItem(HTREEITEM hItem) const;
	HTREEITEM GetParentItem(HTREEITEM hItem) const;
	HTREEITEM GetFirstVisibleItem() const;
	HTREEITEM GetNextVisibleItem(HTREEITEM hItem) const;
	HTREEITEM GetPrevVisibleItem(HTREEITEM hItem) const;
	HTREEITEM GetSelectedItem() const;
	HTREEITEM GetDropHilightItem() const;
	HTREEITEM GetRootItem() const;
	bool GetItem(TVITEM* pItem) const;
	CString GetItemText(HTREEITEM hItem) const;
	bool GetItemImage(HTREEITEM hItem, int& nImage, int& nSelectedImage) const;
	unsigned int GetItemState(HTREEITEM hItem, unsigned int nStateMask) const;
	unsigned long GetItemData(HTREEITEM hItem) const;
	bool SetItem(TVITEM* pItem);
	bool SetItem(HTREEITEM hItem, unsigned int nMask, const char* lpszItem, int nImage,
		int nSelectedImage, unsigned int nState, unsigned int nStateMask, LPARAM lParam);
	bool SetItemText(HTREEITEM hItem, const char* lpszItem);
	bool SetItemImage(HTREEITEM hItem, int nImage, int nSelectedImage);
	bool SetItemState(HTREEITEM hItem, unsigned int nState, unsigned int nStateMask);
	bool SetItemData(HTREEITEM hItem, unsigned long dwData);
	bool ItemHasChildren(HTREEITEM hItem) const;
	CEdit* GetEditControl() const;
	unsigned int GetVisibleCount() const;
	CToolTipCtrl* GetToolTips() const;
	CToolTipCtrl* SetToolTips(CToolTipCtrl* pWndTip);
	COLORREF GetBkColor() const;
	COLORREF SetBkColor(COLORREF clr);
	short GetItemHeight() const;
	short SetItemHeight(short cyHeight);
	COLORREF GetTextColor() const;
	COLORREF SetTextColor(COLORREF clr);
	bool SetInsertMark(HTREEITEM hItem, bool fAfter = true);
	bool GetCheck(HTREEITEM hItem) const;
	bool SetCheck(HTREEITEM hItem, bool fCheck = true);
	COLORREF GetInsertMarkColor() const;
	COLORREF SetInsertMarkColor(COLORREF clrNew);

// Operations
	HTREEITEM InsertItem(LPTVINSERTSTRUCT lpInsertStruct);
	HTREEITEM InsertItem(unsigned int nMask, const char* lpszItem, int nImage,
		int nSelectedImage, unsigned int nState, unsigned int nStateMask, LPARAM lParam,
		HTREEITEM hParent, HTREEITEM hInsertAfter);
	HTREEITEM InsertItem(const char* lpszItem, HTREEITEM hParent = TVI_ROOT,
		HTREEITEM hInsertAfter = TVI_LAST);
	HTREEITEM InsertItem(const char* lpszItem, int nImage, int nSelectedImage,
		HTREEITEM hParent = TVI_ROOT, HTREEITEM hInsertAfter = TVI_LAST);
	bool DeleteItem(HTREEITEM hItem);
	bool DeleteAllItems();
	bool Expand(HTREEITEM hItem, unsigned int nCode);
	bool Select(HTREEITEM hItem, unsigned int nCode);
	bool SelectItem(HTREEITEM hItem);
	bool SelectDropTarget(HTREEITEM hItem);
	bool SelectSetFirstVisible(HTREEITEM hItem);
	CEdit* EditLabel(HTREEITEM hItem);
	HTREEITEM HitTest(CPoint pt, unsigned int* pFlags = NULL) const;
	HTREEITEM HitTest(TVHITTESTINFO* pHitTestInfo) const;
	CImageList* CreateDragImage(HTREEITEM hItem);
	bool SortChildren(HTREEITEM hItem);
	bool EnsureVisible(HTREEITEM hItem);
	bool SortChildrenCB(LPTVSORTCB pSort);
	*/
};
//////////////////////////////////////////////////////////////////////////////
// CWndSliderCtrl
//////////////////////////////////////////////////////////////////////////////

class CWndSliderCtrl : public CWndBase
{
public:
};
//////////////////////////////////////////////////////////////////////////////
// CWndListBox
//////////////////////////////////////////////////////////////////////////////

/*
 * Listbox Styles
 */
#define WLBS_NOTIFY            0x0001L
#define WLBS_SORT              0x0002L
#define WLBS_NOREDRAW          0x0004L
#define WLBS_MULTIPLESEL       0x0008L
#define WLBS_OWNERDRAWFIXED    0x0010L
#define WLBS_OWNERDRAWVARIABLE 0x0020L
#define WLBS_HASSTRINGS        0x0040L
#define WLBS_USETABSTOPS       0x0080L
#define WLBS_NOINTEGRALHEIGHT  0x0100L
#define WLBS_MULTICOLUMN       0x0200L
#define WLBS_WANTKEYBOARDINPUT 0x0400L
#define WLBS_EXTENDEDSEL       0x0800L
#define WLBS_DISABLENOSCROLL   0x1000L
#define WLBS_NODATA            0x2000L
#define WLBS_NOSEL             0x4000L
#define WLBS_STANDARD          (LBS_NOTIFY | LBS_SORT | WS_VSCROLL | WS_BORDER)

class CWndListBox : public CWndBase
{
	void InterpriteScript(CScanner& scanner, CPtrArray& ptrArray);
	void PaintListBox(C2DRender* p2DRender, CPoint& pt, CPtrArray& ptrArray);

public:
	typedef struct tagITEM
	{
		CRect      m_rect;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
		CEditString m_strWord;
		bool		m_bIsValid;
		bool		m_bIsVisible;
#else // __IMPROVE_QUEST_INTERFACE
		bool       m_bButton;
		CString    m_strWord;
#endif // __IMPROVE_QUEST_INTERFACE
		unsigned long      m_dwData;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
		CString    m_strKey;
		unsigned long      m_dwData2;
		tagITEM(void) : m_rect(0, 0, 0, 0), m_strWord(_T("")), m_bIsValid(true), m_bIsVisible(true), m_dwData(0), m_strKey(_T("")), m_dwData2(0) {}
#endif // __IMPROVE_QUEST_INTERFACE
	} LISTITEM, * LPLISTITEM;

protected:
	CPtrArray m_listItemArray;
	LPLISTITEM m_pFocusItem;
	int           m_nCurSelect;
	unsigned long         m_nWndColor;
	LISTITEM      m_listItem;
	CWndScrollBar m_wndScrollBar;
public:
	unsigned long         m_nFontColor;
	unsigned long         m_nSelectColor;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
private:
	unsigned long         m_dwOnMouseColor;
	unsigned long         m_dwInvalidColor;
	unsigned long         m_nLeftMargin;
public:
#endif // __IMPROVE_QUEST_INTERFACE
	int           m_nLineSpace;
	int           m_nFontHeight;

	CWndListBox();
	~CWndListBox();

	void Create(unsigned long dwListBoxStyle, RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	int GetFontHeight() { return m_pFont->GetMaxHeight() + m_nLineSpace; }
	int   GetCount() const;
	unsigned long GetItemData(int nIndex) const;
	void* GetItemDataPtr(int nIndex) const;
	int   SetItemData(int nIndex, unsigned long dwItemData);
	int   SetItemDataPtr(int nIndex, void* pData);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	unsigned long GetItemData2(int nIndex) const;
	void* GetItemData2Ptr(int nIndex) const;
	bool GetItemValidity(int nIndex);
	bool GetItemVisibility(int nIndex);
	int SetItemData2(int nIndex, unsigned long dwItemData);
	int SetItemData2Ptr(int nIndex, void* pData);
	int SetItemValidity(int nIndex, bool bValidity);
	int SetItemVisibility(int nIndex, bool bIsVisible);
#endif // __IMPROVE_QUEST_INTERFACE
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	const CRect& GetItemRect(int nIndex) const;
#else // __IMPROVE_QUEST_INTERFACE
	int   GetItemRect(int nIndex, LPRECT lpRect) const;
#endif // __IMPROVE_QUEST_INTERFACE
	int   GetSel(int nIndex) const;
	int   GetText(int nIndex, LPSTR lpszBuffer) const;
	void  GetText(int nIndex, CString& rString) const;
	int   GetTextLen(int nIndex) const;

	int   GetCurSel() const;
	int   SetCurSel(int nSelect);
	int   SetSel(int nIndex, bool bSelect = true);
	int   GetSelCount() const;
	int   GetSelItems(int nMaxItems, LPINT rgIndex) const;
	int   GetScrollPos() { return m_wndScrollBar.GetScrollPos(); };
	void  SetScrollPos(int nPos, bool bRedraw = true) { m_wndScrollBar.SetScrollPos(nPos, bRedraw); }	//gmpbigsun: added
	int   AddString(const char* lpszItem);
	int   DeleteString(unsigned int nIndex);
	int   InsertString(int nIndex, const char* lpszItem);
	void	SetString(int nIndex, const char* lpszItem);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	const CString& GetString(int nIndex) const;
	void SetListStringAlpha(int nIndex, BYTE byAlpha);
	void SetKeyString(int nIndex, const char* lpszItem);
	const CString& GetKeyString(int nIndex) const;
	void SetOnMouseColor(unsigned long dwOnMouseColor);
	unsigned long GetOnMouseColor(void) const;
	void SetInvalidColor(unsigned long dwInvalidColor);
	unsigned long GetInvalidColor(void) const;
	void SetLeftMargin(int nLeftMargin);
	int GetLeftMargin(void) const;
#endif // __IMPROVE_QUEST_INTERFACE
#ifdef __IMPROVE_MAP_SYSTEM
	int GetItemIndex(const CString& strItem) const;
	int GetItemIndex(unsigned long dwItem) const;
#endif // __IMPROVE_MAP_SYSTEM
	void  ResetContent();
	int   FindString(int nStartAfter, const char* lpszItem) const;
	int   FindStringExact(int nIndexStart, const char* lpszItem) const;
	int   SelectString(int nStartAfter, const char* lpszItem);
	void  SortListBox();

	void LoadListBoxScript(const char* lpFileName);

	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnSetFocus(CWndBase* pOldWnd);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	friend int QSortListBox(const VOID* arg1, const VOID* arg2);
};
// class CListCtrl  CButton

/////////////////////////////////////////////////////////////////////////////
// CListCtrl
// begin_r_commctrl
// List view Control

#define WLVS_ICON                0x0000
#define WLVS_REPORT              0x0001
#define WLVS_SMALLICON           0x0002
#define WLVS_LIST                0x0003
#define WLVS_TYPEMASK            0x0003
#define WLVS_SINGLESEL           0x0004
#define WLVS_SHOWSELALWAYS       0x0008
#define WLVS_SORTASCENDING       0x0010
#define WLVS_SORTDESCENDING      0x0020
#define WLVS_SHAREIMAGELISTS     0x0040
#define WLVS_NOLABELWRAP         0x0080
#define WLVS_AUTOARRANGE         0x0100
#define WLVS_EDITLABELS          0x0200
#define WLVS_NOSCROLL            0x2000
#define WLVS_TYPESTYLEMASK       0xfc00
#define WLVS_ALIGNTOP            0x0000
#define WLVS_ALIGNLEFT           0x0800
#define WLVS_ALIGNMASK           0x0c00
#define WLVS_OWNERDRAWFIXED      0x0400
#define WLVS_NOCOLUMNHEADER      0x4000
#define WLVS_NOSORTHEADER        0x8000

// end_r_commctrl

class CWndEdit;

class CWndListCtrl : public CWndBase
{
	void InterpriteScript(CScanner& scanner, CPtrArray& ptrArray);
	//void PaintListBox(C2DRender* p2DRender,CPoint& pt,CPtrArray& ptrArray);
	LVITEM* m_pFocusItem;
	int           m_nCurSelect;
	int           m_nFontHeight;
	unsigned long         m_nWndColor;
	//	unsigned long         m_nFontColor  ; 
	//	unsigned long         m_nSelectColor;
	CWndScrollBar m_wndScrollBar;
	CPtrArray     m_aItems;
	CPtrArray     m_aColumns;

	// Constructors
public:
	int           m_nLineSpace;
	unsigned long         m_nFontColor;
	unsigned long         m_nSelectColor;
	//	unsigned long         m_dwListCtrlStyle;

	//CWndListCtrl();
	//bool Create(unsigned long dwStyle, const RECT& rect, CWnd* pParentWnd, unsigned int nID);
	CWndListCtrl();
	~CWndListCtrl();

	void Create(unsigned long dwListCtrlStyle, RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void LoadListBoxScript(const char* lpFileName);
	int GetFontHeight() { return m_pFont->GetMaxHeight() + m_nLineSpace; }
	// Attributes
	int   GetCurSel() const;
	int   SetCurSel(int nSelect);
	COLORREF GetBkColor() const;
	bool SetBkColor(COLORREF cr);
	//CImageList* GetImageList(int nImageList) const;
	//CImageList* SetImageList(CImageList* pImageList, int nImageListType);
	int GetItemCount() const;
	bool GetItem(LVITEM* pItem) const;
	bool SetItem(const LVITEM* pItem);
	bool SetItem(int nItem, int nSubItem, unsigned int nMask, const char* lpszItem,
		int nImage, unsigned int nState, unsigned int nStateMask, LPARAM lParam);
	unsigned int GetCallbackMask() const;
	bool SetCallbackMask(unsigned int nMask);
	int GetNextItem(int nItem, int nFlags) const;
	POSITION GetFirstSelectedItemPosition() const;
	int GetNextSelectedItem(POSITION& pos) const;
	bool GetItemRect(int nItem, LPRECT lpRect, unsigned int nCode) const;
	bool SetItemPosition(int nItem, POINT pt);
	bool GetItemPosition(int nItem, LPPOINT lpPoint) const;
	int GetStringWidth(const char* lpsz) const;
	CWndEdit* GetEditControl() const;
	bool GetColumn(int nCol, LVCOLUMN* pColumn) const;
	bool SetColumn(int nCol, const LVCOLUMN* pColumn);
	int GetColumnWidth(int nCol) const;
	bool SetColumnWidth(int nCol, int cx);
	bool GetViewRect(LPRECT lpRect) const;
	COLORREF GetTextColor() const;
	bool SetTextColor(COLORREF cr);
	COLORREF GetTextBkColor() const;
	bool SetTextBkColor(COLORREF cr);
	int GetTopIndex() const;
	int GetCountPerPage() const;
	bool GetOrigin(LPPOINT lpPoint) const;
	bool SetItemState(int nItem, LVITEM* pItem);
	bool SetItemState(int nItem, unsigned int nState, unsigned int nMask);
	unsigned int GetItemState(int nItem, unsigned int nMask) const;
	CString GetItemText(int nItem, int nSubItem) const;
	int GetItemText(int nItem, int nSubItem, LPTSTR lpszText, int nLen) const;
	bool SetItemText(int nItem, int nSubItem, const char* lpszText);
	void SetItemCount(int nItems);
	bool SetItemData(int nItem, unsigned long dwData);
	unsigned long GetItemData(int nItem) const;
	unsigned int GetSelectedCount() const;
	bool SetColumnOrderArray(int iCount, LPINT piArray);
	bool GetColumnOrderArray(LPINT piArray, int iCount = -1);
	CSize SetIconSpacing(CSize size);
	CSize SetIconSpacing(int cx, int cy);
	//CHeaderCtrl* GetHeaderCtrl();
	//HCURSOR GetHotCursor();
	//HCURSOR SetHotCursor(HCURSOR hc);
	bool GetSubItemRect(int iItem, int iSubItem, int nArea, CRect& ref);
	int GetHotItem();
	int SetHotItem(int iIndex);
	int GetSelectionMark();
	int SetSelectionMark(int iIndex);
	unsigned long GetExtendedStyle();
	unsigned long SetExtendedStyle(unsigned long dwNewStyle);
	int SubItemHitTest(LPLVHITTESTINFO pInfo);
	void SetWorkAreas(int nWorkAreas, LPRECT lpRect);
	bool SetItemCountEx(int iCount, unsigned long dwFlags = LVSICF_NOINVALIDATEALL);
	CSize ApproximateViewRect(CSize sz = CSize(-1, -1), int iCount = -1) const;
	bool GetBkImage(LVBKIMAGE* plvbkImage) const;
	unsigned long GetHoverTime() const;
	void GetWorkAreas(int nWorkAreas, LPRECT prc) const;
	bool SetBkImage(HBITMAP hbm, bool fTile = true, int xOffsetPercent = 0, int yOffsetPercent = 0);
	bool SetBkImage(LPTSTR pszUrl, bool fTile = true, int xOffsetPercent = 0, int yOffsetPercent = 0);
	bool SetBkImage(LVBKIMAGE* plvbkImage);
	unsigned long SetHoverTime(unsigned long dwHoverTime = (unsigned long)-1);
	unsigned int GetNumberOfWorkAreas() const;
	bool GetCheck(int nItem) const;
	bool SetCheck(int nItem, bool fCheck = true);

	// Operations
	int InsertItem(const LVITEM* pItem);
	int InsertItem(int nItem, const char* lpszItem);
	int InsertItem(int nItem, const char* lpszItem, int nImage);
	bool DeleteItem(int nItem);
	bool DeleteAllItems();
	//int FindItem(LVFINDINFO* pFindInfo, int nStart = -1) const;
	//int HitTest(LVHITTESTINFO* pHitTestInfo) const;
	int HitTest(CPoint pt, unsigned int* pFlags = NULL) const;
	bool EnsureVisible(int nItem, bool bPartialOK);
	bool Scroll(CSize size);
	bool RedrawItems(int nFirst, int nLast);
	bool Arrange(unsigned int nCode);
	CEdit* EditLabel(int nItem);
	int InsertColumn(int nCol, const LVCOLUMN* pColumn);
	int InsertColumn(int nCol, const char* lpszColumnHeading,
		int nFormat = LVCFMT_LEFT, int nWidth = -1, int nSubItem = -1);
	bool DeleteColumn(int nCol);
	//	CImageList* CreateDragImage(int nItem, LPPOINT lpPoint);
	bool Update(int nItem);
	//bool SortItems(PFNLVCOMPARE pfnCompare, unsigned long dwData);

// Overridables
	//virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual	void PaintFrame(C2DRender* p2DRender);
	// Implementation
public:
	int InsertItem(unsigned int nMask, int nItem, const char* lpszItem, unsigned int nState,
		unsigned int nStateMask, int nImage, LPARAM lParam);
	//virtual ~CWndListCtrl();
protected:
	void RemoveImageList(int nImageList);
	//	virtual bool OnChildNotify(unsigned int, WPARAM, LPARAM, LRESULT*);
protected:
	////{{AFX_MSG(CListCtrl)
	//afx_msg void OnNcDestroy();
	////}}AFX_MSG
	//DECLARE_MESSAGE_MAP()
};


//////////////////////////////////////////////////////////////////////////////
// CWndGroupBox
//////////////////////////////////////////////////////////////////////////////

class CWndGroupBox : public CWndBase
{
protected:

public:
	CWndGroupBox();
	~CWndGroupBox();
	bool Create(unsigned long dwTextStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);

	virtual	void OnInitialUpdate();
	//virtual void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnDraw(C2DRender* p2DRender);
	//virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	//virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	//virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	//virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	//virtual void OnSetFocus(CWndBase* pOldWnd);
	//virtual void OnKillFocus(CWndBase* pNewWnd);aa
	//virtual void OnSize(unsigned int nType, int cx, int cy);
};
/////////////////////////////////////////////////////////////////////////////
// CTabCtrl

typedef struct tagWTCITEM
{
	unsigned int mask;
	unsigned long dwState;
	unsigned long dwStateMask;
	//TCHAR szText[ 32 ];
	const char* pszText;
	int cchTextMax;
	int iImage;
	LPARAM lParam;
	CWndBase* pWndBase;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	tagWTCITEM(void);
#endif // __IMPROVE_QUEST_INTERFACE
} WTCITEM, FAR* LPWTCITEM;


class CWndTabCtrl : public CWndBase
{
	//	DECLARE_DYNAMIC(CTabCtrl)
	vector< LPWTCITEM > m_aTab;
	//	CObjArray::iterator m_itor;
	// Constructors
	int m_nCurSelect;
	//#ifdef __NEWTAB
	CTexture m_aTexture[6];
	//#else
	//	CTexture* m_apTexture[ 10 ];
	//#endif
	int m_nTabButtonLength;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
public:
	enum TabTitleAlign { ALIGN_LEFT, ALIGN_RIGHT, ALIGN_CENTER };

private:
	TabTitleAlign m_eTabTitleAlign;
#endif // __IMPROVE_QUEST_INTERFACE
public:
	CWndTabCtrl();
	bool Create(unsigned long dwStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);

	void SetButtonLength(int nLength) { m_nTabButtonLength = nLength; }
	bool InsertTexture(int nItem, const char* lpszFileName);
	int GetSize() { return m_aTab.size(); }

	virtual HRESULT InitDeviceObjects();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void AdditionalSkinTexture(LPWORD pDest, CSize sizeSurface, D3DFORMAT d3dFormat = D3DFMT_A4R4G4B4);
	virtual	void OnInitialUpdate();

	// Attributes
		//CImageList* GetImageList() const;
		//CImageList* SetImageList(CImageList* pImageList);
		//int GetItemCount() const;
	bool GetItem(int nItem, WTCITEM* pTabCtrlItem) const;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	LPWTCITEM GetTabItem(int nItemNumber) const;
	LPWTCITEM GetSelectedTab(void) const;
	void SetTabTitleAlign(const TabTitleAlign eTabTitleAlign);
	const TabTitleAlign GetTabTitleAlign(void) const;
#endif // __IMPROVE_QUEST_INTERFACE
	//bool SetItem(int nItem, TCITEM* pTabCtrlItem);
	//bool SetItemExtra(int nBytes);
	//bool GetItemRect(int nItem, LPRECT lpRect) const;
	int GetCurSel() const;
	int SetCurSel(int nItem);
	/*
	void SetCurFocus(int nItem);
	CSize SetItemSize(CSize size);
	void SetPadding(CSize size);
	int GetRowCount() const;
	CToolTipCtrl* GetToolTips() const;
	void SetToolTips(CToolTipCtrl* pWndTip);
	int GetCurFocus() const;
	int SetMinTabWidth(int cx);
	unsigned long GetExtendedStyle();
	unsigned long SetExtendedStyle(unsigned long dwNewStyle, unsigned long dwExMask = 0);
	unsigned long GetItemState(int nItem, unsigned long dwMask) const;
	bool SetItemState(int nItem, unsigned long dwMask, unsigned long dwState);
*/
// Operations
	bool InsertItem(int nItem, WTCITEM* pTabCtrlItem);
	bool InsertItem(int nItem, const char* lpszItem);
	/*
	bool InsertItem(int nItem, const char* lpszItem, int nImage);
	bool InsertItem(unsigned int nMask, int nItem, const char* lpszItem,
		int nImage, LPARAM lParam);
	bool InsertItem(unsigned int nMask, int nItem, const char* lpszItem,
		int nImage, LPARAM lParam, unsigned long dwState, unsigned long dwStateMask);
	bool DeleteItem(int nItem);
	bool DeleteAllItems();
	void AdjustRect(bool bLarger, LPRECT lpRect);
	void RemoveImage(int nImage);
	int HitTest(TCHITTESTINFO* pHitTestInfo) const;
	void DeselectAll(bool fExcludeFocus);
	bool HighlightItem(int idItem, bool fHighlight = true);
*/
// Overridables
	//virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

// Implementation
public:
	virtual ~CWndTabCtrl();
	//protected:
		//virtual bool OnChildNotify(unsigned int, WPARAM, LPARAM, LRESULT*);
		////{{AFX_MSG(CTabCtrl)
		//afx_msg void OnDestroy();
		////}}AFX_MSG
		//DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////////////
// CWndComboBox
//////////////////////////////////////////////////////////////////////////////

#define WCBS_SIMPLE            0x0001L
#define WCBS_DROPDOWN          0x0002L
#define WCBS_DROPDOWNLIST      0x0003L
#define WCBS_OWNERDRAWFIXED    0x0010L
#define WCBS_OWNERDRAWVARIABLE 0x0020L
#define WCBS_AUTOHSCROLL       0x0040L
#define WCBS_OEMCONVERT        0x0080L
#define WCBS_SORT              0x0100L
#define WCBS_HASSTRINGS        0x0200L
#define WCBS_NOINTEGRALHEIGHT  0x0400L
#define WCBS_DISABLENOSCROLL   0x0800L
#define WCBS_UPPERCASE         0x2000L
#define WCBS_LOWERCASE         0x4000L

#include "WndEditCtrl.h"

#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
class CWndComboListBox : public CWndListBox
{
public:
#ifdef __IMPROVE_MAP_SYSTEM
	virtual	void PaintFrame(C2DRender* p2DRender);
#endif // __IMPROVE_MAP_SYSTEM
	virtual void OnKillFocus(CWndBase* pNewWnd);
};
#endif // __IMPROVE_QUEST_INTERFACE

class CWndComboBox : public CWndEdit
{
public:
	//	unsigned long m_dwComboBoxStyle; 
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	CWndComboListBox m_wndListBox;
#else // __IMPROVE_QUEST_INTERFACE
	CWndListBox m_wndListBox;
#endif // __IMPROVE_QUEST_INTERFACE
	CWndButton  m_wndButton;

	CWndComboBox();
	~CWndComboBox();

	// Attributes
		// for entire combo box
	int GetCount() const;
	int GetCurSel() const;
	int SetCurSel(int nSelect);
	LCID GetLocale() const;
	LCID SetLocale(LCID nNewLocale);
	// Win4
	int GetTopIndex() const;
	int SetTopIndex(int nIndex);
	int InitStorage(int nItems, unsigned int nBytes);
	void SetHorizontalExtent(unsigned int nExtent);
	unsigned int GetHorizontalExtent() const;
	int SetDroppedWidth(unsigned int nWidth);
	int GetDroppedWidth() const;

	// for edit control
	unsigned long GetEditSel() const;
	bool LimitText(int nMaxChars);
	bool SetEditSel(int nStartChar, int nEndChar);

	// for combobox item
	unsigned long GetItemData(int nIndex) const;
	int SetItemData(int nIndex, unsigned long dwItemData);
	void* GetItemDataPtr(int nIndex) const;
	int SetItemDataPtr(int nIndex, void* pData);
#ifdef __IMPROVE_MAP_SYSTEM
	unsigned long GetSelectedItemData(void) const;
	void GetListBoxText(int nIndex, CString& strString) const;
	int GetListBoxTextLength(int nIndex) const;
	int GetListBoxSize(void) const;
#else // __IMPROVE_MAP_SYSTEM
	int GetLBText(int nIndex, LPTSTR lpszText) const;
	void GetLBText(int nIndex, CString& rString) const;
	int GetLBTextLen(int nIndex) const;
#endif // __IMPROVE_MAP_SYSTEM

	int SetItemHeight(int nIndex, unsigned int cyItemHeight);
	int GetItemHeight(int nIndex) const;
	int FindStringExact(int nIndexStart, const char* lpszFind) const;
	int SetExtendedUI(bool bExtended = true);
	bool GetExtendedUI() const;
	void GetDroppedControlRect(LPRECT lprect) const;
	bool GetDroppedState() const;

	// Operations
		// for drop-down combo boxes
	void ShowDropDown(bool bShowIt = true);

	// manipulating listbox items
	int AddString(const char* lpszString);
	int DeleteString(unsigned int nIndex);
	int InsertString(int nIndex, const char* lpszString);
	void ResetContent();
	int Dir(unsigned int attr, const char* lpszWildCard);

	// selection helpers
	int FindString(int nStartAfter, const char* lpszString) const;
	int SelectString(int nStartAfter, const char* lpszString);

	// Clipboard operations
	void Clear();
	void Copy();
	void Cut();
	void Paste();

#ifdef __IMPROVE_MAP_SYSTEM
	void SelectItem(const CString& strItem);
	void SelectItem(unsigned long dwItem);

private:
	void SetSelectedItemInformation(void);

public:
#endif // __IMPROVE_MAP_SYSTEM
	/*
	// Overridables (must override draw, measure and compare for owner draw)
		virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
		virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
		virtual int CompareItem(LPCOMPAREITEMSTRUCT lpCompareItemStruct);
		virtual void DeleteItem(LPDELETEITEMSTRUCT lpDeleteItemStruct);
	*/


	void Create(unsigned long dwComboBoxStyle, const RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	virtual	void OnInitialUpdate();
	virtual void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
private:
	void OpenListBox(void);
	bool m_bOpen;
#endif // __IMPROVE_QUEST_INTERFACE
};

#endif // !defined(AFX_WNDCONTROL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_)