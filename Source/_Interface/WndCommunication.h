#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H

class CWndCommand;
class CWndChatFilter;
//////////////////////////////////////////////////////////////////////////////////////
// ä�� ���� 
//
class CWndEditChat : public CWndEdit
{
public:
	CWndEditChat();
	~CWndEditChat();
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnSetFocus(CWndBase* pOldWnd);
	virtual void OnKillFocus(CWndBase* pNewWnd);
};
class CWndMacroChat : public CWndButton
{
public:
	CTexture* m_pTexMacro;
	CTexture m_texMacroChat;
	CWndMacroChat();
	~CWndMacroChat();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual	void OnInitialUpdate();
};
class CWndTextChat : public CWndText
{
public:
	CWndTextChat();
	~CWndTextChat();
	virtual bool IsPickupSpace(CPoint point);
};

#if __VER >= 8 //__Y_CHAT_SYSTEM_8
class CWndChatLog : public CWndNeuz
{
	CWndMenu	 m_wndMenuPlace;
	CWndTextChat m_wndText;

public:
	CWndChatLog();
	virtual ~CWndChatLog();
	void  PutString(const char* lpszString, unsigned long dwColor = 0xffffffff, unsigned long dwPStyle = 0x00000001); //CObj* pObj );
//	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	//	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual void OnDestroy();
	virtual void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnDestroyChildWnd(CWndBase* pWndChild);
	virtual void OnSetFocus(CWndBase* pOldWnd);
	virtual void OnKillFocus(CWndBase* pNewWnd);
	virtual void AdditionalSkinTexture(LPWORD pDest, CSize size, D3DFORMAT d3dFormat);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual bool Process();
};
#endif //__Y_CHAT_SYSTEM_8

class CWndChat : public CWndNeuz
{
	IMAGE m_wndChatEdit[3];
	//CSize m_sizeWndChatEdit[3];

	CWndMacroChat m_wndMacroChat;
	CTimer m_timerDobe;
	CTexture m_texEdit;
	CTimer m_timerInsMsg;
#ifdef __CSC_GAME_GRADE
	CTimer m_timerAlertGGrade;
#endif //__CSC_GAME_GRADE
	int m_nInsMsgCnt;
	int				m_nHistoryIndex;
	vector<CString> m_strHistory;
	CTimer m_timerInputTimeOut;

public:
	bool m_bChatLock;
#if __VER >= 8 //__CSC_VER8_1
	bool m_bChatLog;
#endif //CSC_VER8_1
	bool m_bMoveLock;
	static int m_nChatChannel;
	CWndChatFilter* m_pWndChatFilter;
	CString m_strCharName;
	CWndTextChat m_wndText;
	CStringArray m_strArray;
	CWndCommand* m_pWndCommand;
	//	CWndButton m_wndMenu;
		//CWndText m_wndText;
		//CWndEdit m_wndBeginning;
	CWndEditChat m_wndEdit;
	//CWndText m_wndReceiver;

//	CWndButton m_wndShout;
	//CWndButton m_wndParty;

	CWndChat();
	virtual ~CWndChat();
	void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	void  Parsing(CString string);
	void  PutString(const char* lpszString, unsigned long dwColor = 0xffffffff, unsigned long dwPStyle = 0x00000001); //CObj* pObj );
//	virtual CItem* GetFocusItem() { return NULL; }
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	void SetChannel();
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual void OnDestroy();
	virtual void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnDestroyChildWnd(CWndBase* pWndChild);
	virtual void OnSetFocus(CWndBase* pOldWnd);
	virtual void OnKillFocus(CWndBase* pNewWnd);
	virtual void AdditionalSkinTexture(LPWORD pDest, CSize size, D3DFORMAT d3dFormat);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool Process();
};

#endif