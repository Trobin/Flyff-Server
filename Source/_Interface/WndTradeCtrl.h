// WndBase.h: interface for the CWndBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDTRADECTRL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_)
#define AFX_WNDTRADECTRL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class C2DRender;
class CItemElem;

class CWndTradeCtrl : public CWndBase
{
	void InterpriteScript(CScanner& scanner, CPtrArray& ptrArray);

	CItemBase* m_pFocusItem;
	int           m_nCurSelect;
	int           m_nFontHeight;
	unsigned long         m_nWndColor;
	unsigned long         m_nFontColor;
	unsigned long         m_nSelectColor;
	CPtrArray     m_aItems;
	CPtrArray     m_aColumns;

	// Constructors
public:
	static CTextureMng m_textureMng;
	CRect         m_rect;
	bool          m_bDrag;
	unsigned long         m_dwListCtrlStyle;
	CMover* m_pMover;

	void InitItem(CMover* pMover);

	CWndTradeCtrl();
	virtual ~CWndTradeCtrl();

	void Create(unsigned long m_dwListCtrlStyle, RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void LoadListBoxScript(const char* lpFileName);

	// Attributes
	COLORREF GetBkColor() const;
	bool SetBkColor(COLORREF cr);
	int GetItemCount() const;
	bool GetItem(LVITEM* pItem) const;
	bool SetItem(const LVITEM* pItem);
	bool SetItem(int nItem, int nSubItem, unsigned int nMask, const char* lpszItem,
		int nImage, unsigned int nState, unsigned int nStateMask, LPARAM lParam);
	unsigned int GetCallbackMask() const;
	bool SetCallbackMask(unsigned int nMask);
	int GetNextItem(int nItem, int nFlags) const;
	POSITION GetFirstSelectedItemPosition() const;
	int GetNextSelectedItem(POSITION& pos) const;
	bool GetItemRect(int nItem, LPRECT lpRect, unsigned int nCode) const;
	bool SetItemPosition(int nItem, POINT pt);
	bool GetItemPosition(int nItem, LPPOINT lpPoint) const;
	int GetStringWidth(const char* lpsz) const;
	CWndEdit* GetEditControl() const;
	bool GetColumn(int nCol, LVCOLUMN* pColumn) const;
	bool SetColumn(int nCol, const LVCOLUMN* pColumn);
	int GetColumnWidth(int nCol) const;
	bool SetColumnWidth(int nCol, int cx);
	bool GetViewRect(LPRECT lpRect) const;
	COLORREF GetTextColor() const;
	bool SetTextColor(COLORREF cr);
	COLORREF GetTextBkColor() const;
	bool SetTextBkColor(COLORREF cr);
	int GetTopIndex() const;
	int GetCountPerPage() const;
	bool GetOrigin(LPPOINT lpPoint) const;
	bool SetItemState(int nItem, LVITEM* pItem);
	bool SetItemState(int nItem, unsigned int nState, unsigned int nMask);
	unsigned int GetItemState(int nItem, unsigned int nMask) const;
	CString GetItemText(int nItem, int nSubItem) const;
	int GetItemText(int nItem, int nSubItem, LPTSTR lpszText, int nLen) const;
	bool SetItemText(int nItem, int nSubItem, const char* lpszText);
	void SetItemCount(int nItems);
	bool SetItemData(int nItem, unsigned long dwData);
	unsigned long GetItemData(int nItem) const;
	unsigned int GetSelectedCount() const;
	bool SetColumnOrderArray(int iCount, LPINT piArray);
	bool GetColumnOrderArray(LPINT piArray, int iCount = -1);
	CSize SetIconSpacing(CSize size);
	CSize SetIconSpacing(int cx, int cy);
	bool GetSubItemRect(int iItem, int iSubItem, int nArea, CRect& ref);
	int GetHotItem();
	int SetHotItem(int iIndex);
	int GetSelectionMark();
	int SetSelectionMark(int iIndex);
	unsigned long GetExtendedStyle();
	unsigned long SetExtendedStyle(unsigned long dwNewStyle);
	int SubItemHitTest(LPLVHITTESTINFO pInfo);
	void SetWorkAreas(int nWorkAreas, LPRECT lpRect);
	bool SetItemCountEx(int iCount, unsigned long dwFlags = LVSICF_NOINVALIDATEALL);
	CSize ApproximateViewRect(CSize sz = CSize(-1, -1), int iCount = -1) const;
	bool GetBkImage(LVBKIMAGE* plvbkImage) const;
	unsigned long GetHoverTime() const;
	void GetWorkAreas(int nWorkAreas, LPRECT prc) const;
	bool SetBkImage(HBITMAP hbm, bool fTile = true, int xOffsetPercent = 0, int yOffsetPercent = 0);
	bool SetBkImage(LPTSTR pszUrl, bool fTile = true, int xOffsetPercent = 0, int yOffsetPercent = 0);
	bool SetBkImage(LVBKIMAGE* plvbkImage);
	unsigned long SetHoverTime(unsigned long dwHoverTime = (unsigned long)-1);
	unsigned int GetNumberOfWorkAreas() const;
	bool GetCheck(int nItem) const;
	bool SetCheck(int nItem, bool fCheck = true);

	// Operations
	int InsertItem(const LVITEM* pItem);
	int InsertItem(int nItem, const char* lpszItem);
	int InsertItem(int nItem, const char* lpszItem, int nImage);
	bool DeleteItem(int nItem);
	bool DeleteAllItems();
	int HitTest(CPoint pt, unsigned int* pFlags = NULL) const;
	bool EnsureVisible(int nItem, bool bPartialOK);
	bool Scroll(CSize size);
	bool RedrawItems(int nFirst, int nLast);
	bool Arrange(unsigned int nCode);
	CEdit* EditLabel(int nItem);
	int InsertColumn(int nCol, const LVCOLUMN* pColumn);
	int InsertColumn(int nCol, const char* lpszColumnHeading,
		int nFormat = LVCFMT_LEFT, int nWidth = -1, int nSubItem = -1);
	bool DeleteColumn(int nCol);
	bool Update(int nItem);

	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);

	// Implementation
public:
	int InsertItem(unsigned int nMask, int nItem, const char* lpszItem, unsigned int nState,
		unsigned int nStateMask, int nImage, LPARAM lParam);

protected:
	void RemoveImageList(int nImageList);
};

#endif // !defined(AFX_WNDTRADECTRL_H__0B45596D_70D7_48A4_BCB2_3D0F32F58E57__INCLUDED_)