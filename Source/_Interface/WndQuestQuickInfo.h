#ifndef __WND_QUEST_QUICK_INFO_H__
#define __WND_QUEST_QUICK_INFO_H__

#if __VER >= 15 /* __IMPROVE_QUEST_INTERFACE */ && defined( __CLIENT )
//-----------------------------------------------------------------------------
class CWndQITreeCtrl : public CWndTreeCtrl
{
public:
	CWndQITreeCtrl(void);
	virtual ~CWndQITreeCtrl(void);

public:
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void PaintTree(C2DRender* p2DRender, CPoint& pt, CPtrArray& ptrArray);
};
//-----------------------------------------------------------------------------
class CWndQuestQuickInfo : public CWndNeuz
{
public:
	CWndQuestQuickInfo(void);
	virtual ~CWndQuestQuickInfo(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool Process(void);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnSize(unsigned int nType, int cx, int cy);

public:
	void SetUpdateTextSwitch(bool bUpdateText);
	void ExtendAppletHeight(void);

public:
	bool GetActiveGroundClick(void) const;

private:
	void DeleteTree(void);

private:
	int m_nTreeInformationListSize;
	bool m_bOnSurface;
	bool m_bActiveGroundClick;
	unsigned long m_tmOld;
	bool m_bUpdateText;
	CWndQITreeCtrl m_CWndQITreeCtrl;
};
//-----------------------------------------------------------------------------
#endif // defined( __IMPROVE_QUEST_INTERFACE ) && defined( __CLIENT )

#endif // __WND_QUEST_QUICK_INFO_H__