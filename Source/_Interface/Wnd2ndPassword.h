#ifndef __WND_2ND_PASSWORD__
#define __WND_2ND_PASSWORD__

#if __VER >= 15 /* __2ND_PASSWORD_SYSTEM */ && defined( __CLIENT )
class CWnd2ndPassword : public CWndNeuz
{
public:
	CWnd2ndPassword(void);
	~CWnd2ndPassword(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

public:
	void SetInformation(unsigned long idNumberPad, int nSelectCharacter = -1);
	void InsertPassword(int nPasswordNumber);
	void DeletePassword(void);
	void ResetNumberpad(unsigned long idNumberPad);

private:
	enum { TABLE_NUMBER_X = 10, TABLE_NUMBER_Y = 1000 };

private:
	int m_nSelectCharacter;
};
#endif // defined( __IMPROVE_QUEST_INTERFACE ) && defined( __CLIENT )

#endif // __WND_2ND_PASSWORD__



















