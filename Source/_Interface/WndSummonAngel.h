#if __VER >= 8 //__CSC_VER8_5

#ifndef __WNDSUMMONANGEL__H
#define __WNDSUMMONANGEL__H

struct GENMATDIEINFO {
	LPWNDCTRL wndCtrl;
	bool isUse;
	int staticNum;
	CItemElem* pItemElem;
};

struct ItemCountSet {
	OBJID itemid;
	int extracount;
};

#define MAX_MATDIE	20
#define ITEM_VALID			0
#define ITEM_MAX_OVERFLOW	1
#define ITEM_INVALID		2

class CWndSummonAngel : public CWndNeuz
{
public:
	CWndText* m_pText;
	CWndStatic* m_pStatic[3];
	GENMATDIEINFO m_MatDie[MAX_MATDIE];
	ItemCountSet m_ItemInfo[MAX_MATDIE];

	int m_nitemcount;
	int m_nSelecCtrl;
	int m_nOrichalcum;
	int m_nMoonstone;

	float m_WhiteAngelRate;
	float m_RedAngelRate;
	float m_BlueAngelRate;
	float m_GreenAngelRate;

	bool m_nowStarting;
	//	bool m_isCreateSuccess;
	//	CString m_CreateAngel;

	CWndInventory* m_pWndInventory;
public:

	CWndSummonAngel();
	~CWndSummonAngel();

	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual	void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
	virtual void OnDestroy(void);

	void SetQuestText(char* szChar);
	int HitTest(CPoint point);
	void SummonRateRefresh();
	void ReFreshAll(bool extracheck);
	//	void CreateAngelIs(bool isSuccess, char* createAngel);
	//	void SummonAngel();
	void SetDie(CItemElem* pItemElem);
};

#endif

#endif //__CSC_VER8_5
