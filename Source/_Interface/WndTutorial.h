#ifndef __WNDTUTORIAL__H
#define __WNDTUTORIAL__H

#if __VER >= 12 // __MOD_TUTORIAL
struct TUTORIAL_STRING
{
	CString		strTitle;
	CString		strContents;
};

// typedef map<int, TUTORIAL_STRING>::value_type mgValType;
// typedef map<int, TUTORIAL_STRING>::iterator mgMapItor;

class CWndTutorial : public CWndNeuz
{
public:

	CString       m_strKeyword;
	map<int, TUTORIAL_STRING>  m_mapTutorial;

	CWndTutorial();
	~CWndTutorial();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

	bool LoadTutorial(const char* lpszFileName);
	bool AddToList(int nIndex);
};
#endif

#endif