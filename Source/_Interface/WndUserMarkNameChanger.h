#ifndef __WND_USER_MARK_NAME_CHANGER_H__
#define __WND_USER_MARK_NAME_CHANGER_H__

#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT
class CWndUserMarkNameChanger : public CWndNeuz
{
public:
	CWndUserMarkNameChanger(void);
	~CWndUserMarkNameChanger(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual	void OnInitialUpdate(void);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

public:
	void SetInfo(unsigned long dwUserMarkPositionInfoID, const CString& strName);

private:
	enum { USER_MARK_NAME_MAX_LENGTH = 16 };

private:
	unsigned long m_dwUserMarkPositionInfoID;
};
#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM

#endif // __WND_USER_MARK_NAME_CHANGER_H__