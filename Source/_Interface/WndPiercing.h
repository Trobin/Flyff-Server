#ifndef __WNDPIERCING__H
#define __WNDPIERCING__H

class CPiercingMessageBox : public CWndMessageBox
{
public:
	CPiercingMessageBox() {};
	~CPiercingMessageBox() {};
	OBJID m_Objid[3];

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};

class CWndPiercing : public CWndNeuz
{
public:
	CSfx* m_pSfx;
	CItemElem* m_pItemElem[3];
	CRect      m_Rect[3];
	CPiercingMessageBox* m_pPiercingMessageBox;

	CWndPiercing();
	~CWndPiercing();

	virtual bool    OnDropIcon(LPSHORTCUT pShortcut, CPoint point);

	virtual bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void	OnDraw(C2DRender* p2DRender);
	virtual	void	OnInitialUpdate();
	virtual bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void	OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void	OnDestroyChildWnd(CWndBase* pWndChild);
	virtual	void	OnDestroy(void);
	virtual void	OnRButtonUp(unsigned int nFlags, CPoint point);
};

#endif