#ifndef __WNDCHANGENAME__H
#define __WNDCHANGENAME__H


class CWndChangeName : public CWndNeuz
{
private:
	unsigned long	m_dwData;

public:
	CWndChangeName();
	~CWndChangeName();

	void	SetData(unsigned short wId, unsigned short wReset);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#ifdef __PET_1024
class CWndChangePetName : public CWndNeuz
{
private:
	unsigned long	m_dwData;
	unsigned long	m_dwId;

public:
	CWndChangePetName();
	~CWndChangePetName();

	void	SetData(unsigned short wId, unsigned short wReset);
	void	SetItemId(unsigned long wId) { m_dwId = wId; };
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif

#endif	// __WNDCHANGENAME__H