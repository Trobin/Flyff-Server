#if __VER >= 11 // __CSC_VER11_4

#ifndef __WNDMESSENGERCTRL_H
#define __WNDMESSENGERCTRL_H

class C2DRender;

typedef struct __MESSENGER_PLAYER
{
	int		m_nChannel;
	unsigned long	m_dwStatus;
	int		m_nLevel;
	int		m_nJob;
	unsigned long	m_dwPlayerId;
	TCHAR	m_szName[64];
#ifdef __RT_1025
	bool	m_bBlock;
#endif	// __RT_1025
#if __VER >= 13 // __HOUSING
	bool	m_bVisitAllowed;
#endif // __HOUSING
#if __VER >= 15 // __CAMPUS
	__MESSENGER_PLAYER(void);
	void Initialize(void);
#endif // __CAMPUS
} __MESSENGER_PLAYER;

class CWndFriendCtrlEx : public CWndBase
{
public:
	int m_nCurSelect;
	int m_nFontHeight;
	int m_nDrawCount;
	CWndScrollBar m_wndScrollBar;
	CWndMenu m_menu;
	vector < __MESSENGER_PLAYER > m_vPlayerList;

private:
	enum { SORT_BY_CHANNEL, SORT_BY_STATUS, SORT_BY_LEVEL, SORT_BY_JOB, SORT_BY_NAME };
	bool m_bSortbyChannel;
	bool m_bSortbyStatus;
	bool m_bSortbyLevel;
	bool m_bSortbyJob;
	bool m_bSortbyName;
	int m_nCurSort;

public:
	CWndFriendCtrlEx();
	~CWndFriendCtrlEx();

	void Create(RECT& rect, CWndBase* pParentWnd, unsigned int nID);

	// Overridables
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);

	// Sort Func.
	void SortbyChannel(bool bCheckbefore = true);
	void SortbyStatus(bool bCheckbefore = true);
	void SortbyLevel(bool bCheckbefore = true);
	void SortbyJob(bool bCheckbefore = true);
	void SortbyName(bool bCheckbefore = true);
	void UpdatePlayerList();

	// UI Func.
	void SetScrollBar();
#ifdef __RT_1025
	int		GetSelect(CPoint point, unsigned long& idPlayer, Friend** ppFriend);
	void	GetSelectFriend(int SelectCount, unsigned long& idPlayer, Friend** ppFriend);
#else	// __RT_1025
	int GetSelect(CPoint point, LPFRIEND* lppFriend);
	void GetSelectFriend(int SelectCount, LPFRIEND* lppFriend);
#endif	// __RT_1025
	unsigned long GetSelectId(int SelectCount);
	void ScrollBarPos(int nPos);
	int GetDrawCount(void);
};

class CWndGuildCtrlEx : public CWndBase
{
public:
	int m_nCurSelect;
	int m_nFontHeight;
	int m_nDrawCount;
	CWndScrollBar m_wndScrollBar;
	vector < __MESSENGER_PLAYER > m_vPlayerList;

private:
	enum { SORT_BY_CHANNEL, SORT_BY_STATUS, SORT_BY_LEVEL, SORT_BY_JOB, SORT_BY_NAME };
	bool m_bSortbyChannel;
	bool m_bSortbyStatus;
	bool m_bSortbyLevel;
	bool m_bSortbyJob;
	bool m_bSortbyName;
	int m_nCurSort;

public:
	CWndGuildCtrlEx();
	~CWndGuildCtrlEx();

	void Create(RECT& rect, CWndBase* pParentWnd, unsigned int nID);

	// Overridables
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);

	// Sort Func.
	void SortbyChannel(bool bCheckbefore = true);
	void SortbyStatus(bool bCheckbefore = true);
	void SortbyLevel(bool bCheckbefore = true);
	void SortbyJob(bool bCheckbefore = true);
	void SortbyName(bool bCheckbefore = true);
	void UpdatePlayerList();

	// UI Func.
	void SetScrollBar();
#ifdef __RT_1025
	int		GetSelect(CPoint point, unsigned long& idPlayer, CGuildMember** lppGuildMember);
	//	void	GetSelectFriend( int SelectCount, unsigned long & idPlayer, Friend** ppFriend );
#else	// __RT_1025
	int GetSelect(CPoint point, LPFRIEND* lppFriend);
	void GetSelectFriend(int SelectCount, LPFRIEND* lppFriend);
#endif	// __RT_1025
	unsigned long GetSelectId(int SelectCount);
	void ScrollBarPos(int nPos);
	int GetDrawCount(void);
};

#if __VER >= 15 // __CAMPUS
class CWndCampus : public CWndNeuz
{
public:
	CWndCampus(void);
	~CWndCampus(void);

public:
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual	void OnInitialUpdate(void);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);

public:
	void UpdatePlayerList(void);
	int GetDiscipleDrawCount(void) const;
	__MESSENGER_PLAYER* GetSelectedDiscipleID(int nSelectedNumber);
	unsigned long GetSelectedMasterID(CPoint point);
	unsigned long GetSelectedDiscipleID(CPoint point);
	void SortbyChannel(bool bCheckbefore = true);
	void SortbyStatus(bool bCheckbefore = true);
	void SortbyLevel(bool bCheckbefore = true);
	void SortbyJob(bool bCheckbefore = true);
	void SortbyName(bool bCheckbefore = true);

private:
	enum { MASTER_RENDERING_POSITION = 43, DISCIPLE_RENDERING_POSITION = 122 };
	enum { SORT_BY_CHANNEL, SORT_BY_STATUS, SORT_BY_LEVEL, SORT_BY_JOB, SORT_BY_NAME };
	bool m_bSortbyChannel;
	bool m_bSortbyStatus;
	bool m_bSortbyLevel;
	bool m_bSortbyJob;
	bool m_bSortbyName;
	int m_nCurSort;
	bool m_bCurSelectedMaster;
	int m_nCurSelectedDisciple;
	int m_nFontHeight;
	CWndMenu m_Menu;
	__MESSENGER_PLAYER m_MasterPlayer;
	vector < __MESSENGER_PLAYER > m_vDisciplePlayer;
};
#endif // __CAMPUS

#endif //__WNDMESSENGERCTRL_H

#endif //__CSC_VER11_4