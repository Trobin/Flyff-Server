#ifndef __WNDREPAIRITEM__H
#define __WNDREPAIRITEM__H

#include "WndRepairItemCtrl.h"

class CWndRepairItem : public CWndNeuz
{
public:
	//	CWndItemCtrl		m_wndItemCtrl;
	//	CItemContainer< CItemElem > pRepairItem;
	CWndStatic* pWndStaticCost;
	CWndRepairItemCtrl	m_wndItemCtrl;
	unsigned long	m_adwIdRepair[MAX_REPAIRINGITEM];
	unsigned long	m_dwCost;


	void OnInit(void);

public:
	CWndRepairItem();
	~CWndRepairItem();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void OnDestroy(void);
};
#endif	// __WNDREPAIRITEM__H