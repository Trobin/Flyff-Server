// WndArcane.h: interface for the CWndNeuz class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDWORLD_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)
#define AFX_WNDWORLD_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __HELP_BUG_FIX
// 헬프 이중생성 방지
extern vector<CString> g_vecHelpInsKey;
#endif //__HELP_BUG_FIX

#ifdef __BUFF_1107
#include "buff.h"
#endif	// __BUFF_1107

typedef struct tagCAPTION
{
	TCHAR m_szCaption[512];
	CD3DFontAPI* m_pFont;

	CTexture m_texture;
	CSize m_size;
	float m_fXScale;
	float m_fYScale;
	float m_fAddScale;
	int m_nAlpha;

} CAPTION, * LPCAPTION;

class CCaption
{
public:
	bool m_bEnd;
	CTimer m_timer;
	int m_nCount;
	CPtrArray m_aCaption;
	CCaption();
	~CCaption();

	void RemoveAll();
	void Process();

	void Render(CPoint ptBegin, C2DRender* p2DRender);
	void AddCaption(const char* lpszCaption, CD3DFontAPI* pFont, bool bChatLog = true, unsigned long dwColor = D3DCOLOR_ARGB(255, 255, 255, 255));

	// Initializing and destroying device-dependent objects
	HRESULT InitDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT DeleteDeviceObjects();
	HRESULT RestoreDeviceObjects();
	HRESULT InvalidateDeviceObjects();
};

class CCapTime
{
public:
	CD3DFontAPI* m_pFont;
	CTexture m_texture;
	CSize m_size;

	float m_fXScale;
	float m_fYScale;
	float m_fAddScale;
	int m_nAlpha;
	int m_nTime;
	int m_nStep;
	bool m_bRender;
	CCapTime();
	~CCapTime();

	void Process();
	void Render(CPoint ptBegin, C2DRender* p2DRender);
	void SetTime(int nTime, CD3DFontAPI* pFont);
	HRESULT InitDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT DeleteDeviceObjects();
	HRESULT RestoreDeviceObjects();
	HRESULT InvalidateDeviceObjects();
};

extern CCapTime g_CapTime;
extern CCaption g_Caption1;
//////////////////////////////////////////////////////////////////////////////////////
// 월드 
//
#define NEXTSKILL_NONE				0xffffffff	// 다음스킬 사용하지 않음
#define NEXTSKILL_ACTIONSLOT		0x7fffffff	// 다음스킬 액션슬롯으로 사용
// 그외는 스킬인덱스.

typedef struct BUFFSKILL
{
	bool m_bFlsh;
	int   m_nAlpha;
	CTexture* m_pTexture;
} BUFFSKILL;

typedef	struct	_SET_NAVIGATOR
{
	unsigned long dwWorldId;
	D3DXVECTOR3 vPos;
	_SET_NAVIGATOR()
	{
		dwWorldId = 0;
	}
}
SET_NAVIGATOR;


struct BUFFICON_INFO
{
	CPoint	pt;			// render위치 
	int		nCount;		// 그려진 갯수 
	int		nDelta;		// 이동할 좌표 
};

struct BUFFICONRECT_INFO
{
	RECT    rc;
	unsigned long	dwID;
};

// 버튼형식의 도움말( 클릭시 지정된 창 실행 )
#define MAX_ADVBUTTON	10

typedef struct BUTTON_INFO
{
	CWndButton* m_pwndButton;
	unsigned long			m_dwRunWindow;
} BUTTON_INFO, * PBUTTON_INFO;

class CAdvMgr
{
	int						m_nIndex;
	CWndBase* m_pParentWnd;
	vector<BUTTON_INFO>		m_vecButton;

public:
	CAdvMgr();
	~CAdvMgr();

	void RemoveButton();
	void Init(CWndBase* pParentWnd);
	void AddAdvButton(unsigned long dwid);
	bool RunButton(unsigned long dwID);
	void SortButton();
	BUTTON_INFO* FindRunWindowButton(unsigned long dwID);
};

typedef struct __GUILDRATE
{
	unsigned long m_uidPlayer;
	int    nLife;
	bool   bJoinReady;
} __GUILDRATE;

typedef struct __GCWARSTATE
{
	unsigned long m_uidPlayer;
	bool   m_bWar;
} __GCWARSTATE;

typedef struct __PGUEST_TIME_TEXT
{
	bool bFlag;
	int  nState;
	unsigned long dwQuestTime;
} __PGUEST_TIME_TEXT;

#if __VER >= 12 // __SECRET_ROOM
typedef struct __SRGUILDINFO
{
	unsigned long dwGuildId;
	int nWarState;
	int nKillCount;
} __SRGUILDINFO;

typedef struct __KILLCOUNTCIPHERS
{
	bool bDrawMyGuildKillCount;
	char szMyGuildKillCount;
	CPoint ptPos;
	float fScaleX;
	float fScaleY;
	int nAlpha;
} __KILLCOUNTCIPHERS;

#define MAX_KILLCOUNT_CIPHERS 3
#endif //__SECRET_ROOM


class CWndWorld : public CWndNeuz
{
	bool m_bBGM;
	float m_fHigh;
	float m_fLow;
	CPoint m_ptMouseOld;
	CPoint m_ptMouseSpot;
	bool m_bFreeMove;
	bool m_bFlyMove;
	bool m_bSelectTarget;
#ifdef __VRCAMERA
	bool m_bCameraMode;
#endif
	bool		m_bViewMap;

public:
	CAdvMgr		m_AdvMgr;
	CObj* m_pSelectRenderObj;

	CAdvMgr* GetAdvMgr() { return &m_AdvMgr; }

	bool	m_IsMailTexRender;
	bool	m_bCtrlInfo;
	bool	m_bCtrlPushed;
	bool	m_bRenderFPS;

#if __VER >= 8 //__CSC_VER8_5
	bool	m_bShiftPushed; //다이스 옮길 때 한번에 넣기.
	bool	m_bAngelFinish; //Angel 경험치 완료 유무.
#endif //__CSC_VER8_5
#if __VER >= 11 // __CSC_VER11_2
	CObj* m_pNextTargetObj;
	CObj* m_pRenderTargetObj;
#endif //__CSC_VER11_2
	unsigned long m_dwIdBgmMusic;
	//#if __VER >= 9
	//	unsigned long	m_dwIdBgmMusicOld;
	//#endif	//
	CTexturePack m_texTarget;		// 지상에서의 4귀퉁이 타겟그림		//sun!
	CTexturePack m_texTargetFly;	// 비행중에서의 4귀퉁이 타겟그림.
	CTexturePack m_texTargetArrow;	// 타겟이 화면을 벗어났을때 화살표방향표시.
	CTexturePack m_texGauFlight;	// 비행 게이지 인터페이스.

	bool s_bUped, s_bDowned, s_bLefted, s_bRighted;
	bool s_bCombatKeyed, s_bFlyKeyed, s_bAccKeyed, s_bSitKeyed;
	bool s_bTempKeyed, m_bTemp2ed, m_bTemp3ed;
	bool s_bBoarded;

	CTimer m_timerFocusAttack;
	CTimer m_timerLButtonDown;
	CTimer m_timerRButtonDown;
	//CTimer m_timerSpell;
	unsigned long  m_dwPowerTick;				// 마우스를 누른 상태로 힘을 모아 공격하는 패턴에 사용하는 시작시각 
	CTimer m_timerAutoRun;
	CTimer m_timerAutoRunPush;
	CTimer m_timerAutoRunBlock;
	int   m_nDubleUp;
	//	CTexture m_texFlaris;

		//CObj* m_pCaptureObj;

	CWndMenu m_wndMenuMover;

	//#ifdef __QUEST_HELPER
	bool m_bSetQuestNPCDest;
	D3DXVECTOR3 m_vQuestNPCDest;
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	D3DXVECTOR3 m_vDestinationArrow;
#endif // __IMPROVE_QUEST_INTERFACE

#ifdef __BS_PUTNAME_QUESTARROW
	CString m_strDestName;
	bool m_bStartAniArrow;
#endif
	//#endif //__QUEST_HELPER
		//ect m_rectBound;

	CTexture m_texLvUp;
	CTexture m_texLvDn;
	CTexture m_texLvUp2;
	CTexture m_texLvDn2;
	CTexture m_texGauEmptyNormal;
	CTexture m_texGauFillNormal;
	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;

	//CWndTradeGold* m_pWndTradeGold;
	int		m_nMouseMode;		// 비행중일때. 0:우버튼+드래그 빗자루회전   1:걍드래그 빗자루회전.

	int		ControlPlayer(unsigned long dwMessage, CPoint point);
	int		ControlGround(unsigned long dwMessage, CPoint point);
	int		ControlFlying(unsigned long dwMessage, CPoint point);
	int		ControlShip(unsigned long dwMessage, CPoint point);

	//CTexture m_texTargetGauge;
	CModelObject m_meshArrow; // 타겟의 방향을 알려주는 화살표 오브젝트이다.

	CModelObject	m_meshArrowWanted; // 타겟의 방향을 알려주는 화살표 오브젝트이다.
	bool			m_bRenderArrowWanted;
	unsigned long			m_dwRenderArrowTime;
	D3DXVECTOR3		m_v3Dest;
	void			SetRenderArrowWanted(bool b, D3DXVECTOR3 vDest) { m_v3Dest = vDest; m_bRenderArrowWanted = b; m_dwRenderArrowTime = g_tmCurrent; }

	void RenderArrow();
	bool UseSkillToFocusObj(CCtrl* pFocusObj);
	void GetBoundRect(CObj* pObj, CRect* pRect);

	void RenderArrow_Text(LPDIRECT3DDEVICE9 pDevice, const D3DXVECTOR3& vDest, const D3DXMATRIX& mat);	//gmpbigsun : refactoring

	CDWordArray		m_aFlyTarget;

	unsigned long	m_dwNextSkill;		// 치고있는중에 스킬치기 예약.
	vector <BUFFICONRECT_INFO> m_rcCheck;

	CTexture		m_TexGuildWinner;
	CTexture		m_TexGuildBest;
	multimap< int, CString >	m_mmapGuildCombat_GuildPrecedence;
	multimap< int, unsigned long >		m_mmapGuildCombat_PlayerPrecedence;
	vector  < __GUILDRATE >		m_vecGuildCombat_GuildStatus;
	map< unsigned long, vector<__GCWARSTATE> >  m_mapGC_GuildStatus;
#if __VER >= 8 //__CSC_VER8_3
	CWndBase* m_pWndBuffStatus;
#endif //__CSC_VER8_3

public:
	void InitEyeFlash();
	void AddGuildPrecedence(int, CString);
	void AddPlayerPrecedence(int, unsigned long);
	void AddGuildStatus(unsigned long uidPlayer, int nLife, bool bJoinReady);
	void AddGCStatus(unsigned long uidDefender, unsigned long uidPlayer, bool bWar);
	unsigned long GetGCStatusDefender(unsigned long uidDefender);
	int  IsGCStatusPlayerWar(unsigned long uidPlayer);
	void ClearGuildPrecedence() { m_mmapGuildCombat_GuildPrecedence.clear(); }
	void ClearPlayerPrecedence() { m_mmapGuildCombat_PlayerPrecedence.clear(); }
	void ClearGuildStatus() { m_vecGuildCombat_GuildStatus.clear(); }
	void SetViewMap(bool bViewMap) { m_bViewMap = bViewMap; }
	bool IsViewMap() { return m_bViewMap; }
	bool GetBuffIconRect(unsigned long dwID, const CPoint& point);
	int  GetBuffTimeGap();
	void RenderOptBuffTime(C2DRender* p2DRender, CPoint& point, CTimeSpan& ct, unsigned long dwColor);
	void RenderWantedArrow();
	void RenderMoverBuff(CMover* pMover, C2DRender* p2DRender);

	CD3DFontAPI* m_pFontAPICaption;
	CD3DFontAPI* m_pFontAPITitle;
	CD3DFontAPI* m_pFontAPITime;

	CTexturePack	m_texMsgIcon;
	SET_NAVIGATOR	m_stnv;
	CTexturePack	m_texAttrIcon;
#if __VER >= 11 // __CSC_VER11_4
	CTexturePack	m_texPlayerDataIcon;
#endif //__CSC_VER11_4

	CTexture			m_pTextureLogo[CUSTOM_LOGO_MAX];
	unsigned long				m_dwOneSecCount;
	ADDSMMODE			m_AddSMMode;
	CTexture* m_dwSMItemTexture[SM_MAX];
	CTexture* m_dwSMResistItemTexture[10];
	bool				m_bSMFlsh[SM_MAX];
	int					m_nSMAlpha[SM_MAX];
	CWndGuideSystem* m_pWndGuideSystem;

	void UseSkill();
	unsigned long m_dwDropTime;
	CTexturePack m_texFontDigital;	// 디지탈 모양의 폰트(비행속도에 사용)

	static D3DXVECTOR3 m_vTerrainPoint;

	CRect m_rectSelectBegine;
	bool m_bNewSelect;
	//CObj* m_pSelectObj;
	bool m_bAutoAttack;
	OBJID	m_idLastTarget;
	bool	IsLastTarget(OBJID objid) { return(m_idLastTarget == objid); }
	void	SetLastTarget(OBJID objid) { m_idLastTarget = objid; }
	unsigned long	GetNextSkill(void) { return m_dwNextSkill; }
	void	SetNextSkill(unsigned long dwNextSkill)
	{
		m_dwNextSkill = dwNextSkill;
	}

	int		GetGMLogoIndex();

	float	m_fRollAng;		// 롤링 앵글.

#ifdef __YAUTOATTACK
	bool	m_bAttackDbk;
#endif //__YAUTOATTACK

	int		n_nMoverSelectCount;
	unsigned long	m_dwGuildCombatTime;

	__PGUEST_TIME_TEXT m_QuestTime;

	char	m_szGuildCombatStr[64];
	struct __GuildCombatJoin
	{
		unsigned long uidGuild;
		char szJoinGuildName[MAX_NAME];
		int nJoinSize;
		int nOutSize;
	};
#ifdef __S_BUG_GC
	vector< __GuildCombatJoin > m_vecGuildCombatJoin;
#else // __S_BUG_GC
	map< unsigned long, __GuildCombatJoin > m_GuildCombatJoin;
#endif // __S_BUG_GC
	vector< __GuildCombatJoin > m_vGuildCombatSort;

	CWndButton m_wndMenu;
	D3DXVECTOR3		m_vTelePos;
	void SetMouseMode(int nMode)
	{
		m_nMouseMode = nMode;
		if (nMode == 0)
			ClipCursor(NULL); // 커서가 다시 윈도우를 벗어나게 한다.
	}
	int	GetMouseMode(void) { return m_nMouseMode; }

	void Projection(LPDIRECT3DDEVICE9 pd3dDevice);

	CObj* PickObj(POINT point, bool bOnlyNPC = false);
	CObj* SelectObj(POINT point);
	CObj* HighlightObj(POINT point);

	int		m_nSelect;		// 현재 선택된 타겟 인덱스.
	void	ClearFlyTarget(void)
	{
		m_aFlyTarget.RemoveAll();
	}

	void	AddFlyTarget(OBJID idMover)
	{
		m_aFlyTarget.Add(idMover);
	}

	OBJID	m_objidTracking;

	CWndWorld();
	virtual ~CWndWorld();
	void RenderAltimeter();
#if __VER >= 11 // __CSC_VER11_2
	void RenderFocusObj(CObj* pObj, CRect rect, unsigned long dwColor1, unsigned long dwColor2);
	void SetNextTarget();
#else //__CSC_VER11_2
	void RenderFocusObj(CRect rect, unsigned long dwColor1, unsigned long dwColor2);
#endif //__CSC_VER11_2
	void RenderFocusArrow(CPoint pt);
	void RenderGauFlight(C2DRender* p2DRender);		// 비행모드시 게이지 인터페이스 Draw

	bool						m_bFirstFlying;
	int                         m_nLimitBuffCount;
#ifdef __BUFF_1107
	CBuffMgr	m_buffs;
#else	// __BUFF_1107
	CSkillInfluence             m_partySkillState;
#endif	// __BUFF_1107
	vector< multimap<unsigned long, BUFFSKILL> >	m_pBuffTexture;

#ifdef __BUFF_1107
	unsigned long	GetSystemPetTextureKey(IBuff* pBuff);
#else	// __BUFF_1107
	unsigned long	GetSystemPetTextureKey(SKILLINFLUENCE* pSkill);
#endif	// __BUFF_1107
	void RenderBuff(C2DRender* p2DRender);
	void RenderSelectObj(C2DRender* pRender, CObj* pObj);
#ifdef __BUFF_1107
	void RenderExpBuffIcon(C2DRender* p2DRender, IBuff* pBuff, BUFFICON_INFO* pInfo, CPoint ptMouse, unsigned long dwItemID);
	void RenderBuffIcon(C2DRender* p2DRender, IBuff* pBuff, bool bPlayer, BUFFICON_INFO* pInfo, CPoint ptMouse);
#else	// __BUFF_1107
	void RenderExpBuffIcon(C2DRender* p2DRender, SKILLINFLUENCE* pSkill, BUFFICON_INFO* pInfo, CPoint ptMouse, unsigned long dwItemID);
	void RenderBuffIcon(C2DRender* p2DRender, SKILLINFLUENCE* pSkill, bool bPlayer, BUFFICON_INFO* pInfo, CPoint ptMouse);
#endif	// __BUFF_1107
	void RenderSMBuff(C2DRender* p2DRender, BUFFICON_INFO* pInfo, CPoint ptMouse);
	void RenderCasting(C2DRender* p2DRender);
#if __VER >= 12 // __LORD
	void	RenderEventIcon(C2DRender* p2DRender, BUFFICON_INFO* pInfo, CPoint ptMouse);
#endif	// __LORD

#if __VER >= 9 // __CSC_VER9_1
	void PutPetTooltipInfo(CItemElem* pItemElem, CEditString* pEdit);
#endif //__CSC_VER9_1
#if __VER >= 11 // __GUILD_COMBAT_1TO1
	void DrawGuildCombat1to1Info(C2DRender* p2DRender);
	void DrawGuildCombat1to1PlayerInfo(C2DRender* p2DRender);
	void DrawGuildCombat1ot1GuildInfo(C2DRender* p2DRender);
	bool m_bGuildCombat1to1Wait;
#endif //__GUILD_COMBAT_1TO1

#if __VER >= 12 // __SECRET_ROOM
	char m_szSecretRoomStr[256];
	vector<__SRGUILDINFO> m_vecGuildList;
	__KILLCOUNTCIPHERS m_stKillCountCiphers[MAX_KILLCOUNT_CIPHERS];
	bool m_bFlashBackground;

	void DrawSecretRoomGuildInfo(C2DRender* p2DRender, bool bIsMyGuild, int nRank, __SRGUILDINFO stGuildInfo, CPoint ptRank, CPoint ptLogo,
		CPoint ptGName, CPoint ptHypoon, CPoint ptState, float fLogoScaleX, float fLogoScaleY, CRect rectBg);
	void DrawSecretRoomInfo(C2DRender* p2DRender);
	void DrawMyGuildKillCount(C2DRender* p2DRender, __SRGUILDINFO stGuildInfo, CPoint ptState, int nMax);
	void DrawOutLineLamp(C2DRender* p2DRender, CRect rectBg, unsigned long dwColorstart, unsigned long dwColorend, int nState, int nRank, bool bIsMyGuild);
	void DrawOutLineFlash(C2DRender* p2DRender, CRect rectBg, unsigned long dwColorstart, unsigned long dwColorend);
#endif //__SECRET_ROOM

	void InviteParty(unsigned long uidPlayer);
	void InviteCompany(OBJID objId);

	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnSetCursor(CWndBase* pWndBase, unsigned int nHitTest, unsigned int message);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnMButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnMButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool OnMouseWheel(unsigned int nFlags, short zDelta, CPoint pt);
	virtual void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	virtual	bool Process();
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual LRESULT WndMsgProc(unsigned int message, WPARAM wParam, LPARAM lParam);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnMouseWndSurface(CPoint point);

	bool UseFocusObj(CCtrl* pFocusObj);

	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

private:
	void	ShowMoverMenu(CMover* pTarget);
	int		GetGaugePower(int* pnValue);

#if __VER >= 15 // __GUILD_HOUSE
	void	ShowCCtrlMenu(CCtrl* pCCtrl);
#endif 
#if __VER >= 15 // __IMPROVE_SYSTEM_VER15
	bool MenuException(CPoint point);	// 메뉴를 띄우기 전에 처리해 줘야 할 예외 사항들
#endif // __IMPROVE_SYSTEM_VER15
};
#endif // !defined(AFX_WNDFIELD_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)