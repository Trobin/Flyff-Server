#ifndef __WNDGUILDBANK__H
#define __WNDGUILDBANK__H

#include "WndGuildMerit.h"

class CWndGuildBank : public CWndNeuz
{
	CWndGuildMerit* m_pwndGuildMerit;		// ������.
public:
	CWndItemCtrl m_wndItemCtrl;
	CWndGold     m_wndGold;

public:
	CWndGuildBank();
	~CWndGuildBank();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#if __VER >= 11 // __GUILD_BANK_LOG
class CWndAddItemLog : public CWndBase
{
public:
	int m_nCurSelect;
	int m_nFontHeight;
	int m_nDrawCount;

	CWndScrollBar m_wndScrollBar;
	vector < CString > m_vLogList;

public:
	CWndAddItemLog();
	~CWndAddItemLog();

	void Create(RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void UpdateLogList();
	void UpdateScroll();

	// Overridables
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);

	// UI Func.
	int GetDrawCount(void);
};

class CWndRemoveItemLog : public CWndBase
{
public:
	int m_nCurSelect;
	int m_nFontHeight;
	int m_nDrawCount;

	CWndScrollBar m_wndScrollBar;
	vector < CString > m_vLogList;

public:
	CWndRemoveItemLog();
	~CWndRemoveItemLog();

	void Create(RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void UpdateLogList();
	void UpdateScroll();

	// Overridables
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);

	// UI Func.
	int GetDrawCount(void);
};

class CWndReceivePenyaLog : public CWndBase
{
public:
	int m_nCurSelect;
	int m_nFontHeight;
	int m_nDrawCount;

	CWndScrollBar m_wndScrollBar;
	vector < CString > m_vLogList;

public:
	CWndReceivePenyaLog();
	~CWndReceivePenyaLog();

	void Create(RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void UpdateLogList();
	void UpdateScroll();

	// Overridables
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);

	// UI Func.
	int GetDrawCount(void);
};

class CWndInvestPenyaLog : public CWndBase
{
public:
	int m_nCurSelect;
	int m_nFontHeight;
	int m_nDrawCount;

	CWndScrollBar m_wndScrollBar;
	vector < CString > m_vLogList;

public:
	CWndInvestPenyaLog();
	~CWndInvestPenyaLog();

	void Create(RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void UpdateLogList();
	void UpdateScroll();

	// Overridables
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);

	// UI Func.
	int GetDrawCount(void);
};

class CWndGuildBankLog : public CWndNeuz
{
public:
	CWndGuildBankLog();
	~CWndGuildBankLog();

	CWndAddItemLog		m_wndAddItemLog;
	CWndRemoveItemLog	m_wndRemoveItemLog;
	CWndReceivePenyaLog	m_wndReceivePenyaLog;
	CWndInvestPenyaLog	m_wndInvestPenyaLog;

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

	void UpdateScroll();
};
#endif //__GUILD_BANK_LOG

#endif