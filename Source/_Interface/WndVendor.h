#ifndef __WNDVENDOR__H
#define __WNDVENDOR__H

#include "wndvendorctrl.h"
#include "wndregvend.h"
#include "wndvendorbuy.h"

class CWndVendorMessage;

class CWndVendor : public CWndNeuz
{
public:
	CWndVendorCtrl	m_wndctrlVendor;
	CWndVendorBuy* m_pWndVendorBuy;
	CMover* m_pVendor;
	CWndRegVend* m_pWndRegVend;

	CWndVendorMessage* m_pwndVenderMessage;

public:
	CWndVendor();
	~CWndVendor();

	void	SetVendor(CMover* pVendor);

	virtual bool	Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	void			ReloadItemList();
	virtual bool	OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void	OnDraw(C2DRender* p2DRender);
	virtual bool	Process();
	virtual	void	OnInitialUpdate();
	virtual bool	OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void	OnSize(unsigned int nType, int cx, int cy);
	virtual void	OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void	OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void	OnDestroyChildWnd(CWndBase* pWndChild);
	virtual	void	OnDestroy(void);
};

class CWndVendorMessage : public CWndNeuz
{
public:
	void RemoveChattingMemver(const char* lpszName);
	void AddChattingMemver(const char* lpszName);
	CWndVendorMessage();
	~CWndVendorMessage();

	CString m_strPlayer;

#if __VER >= 11 // __MOD_VENDOR
	CWndText		m_wndChat;
	CWndText		m_wndInfo;
	bool			m_nIsOwner;

	void WriteBuyInfo(char* pBuyerName, CString strItem, int nItemNum, int	nTotalCost);
#endif

	void InitSize(void);
	void AddMessage(const char* lpszFrom, const char* lpszMessage);
	void OnInputString();

	virtual void SetWndRect(CRect rectWnd, bool bOnSize);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#if __VER >= 8 // __S8_VENDOR_REVISION
class CWndVendorConfirm : public CWndMessageBox
{
public:
	CString m_strVendorName;
	void	SetVendorName(CString str);
	void	SetValue(CString str);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};
#endif // __VER >= 8 // __S8_VENDOR_REVISION

#endif