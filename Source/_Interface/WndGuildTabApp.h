#ifndef __WNDGUILD_TAB_APP__H
#define __WNDGUILD_TAB_APP__H

#include "guild.h"
extern	CGuildMng	g_GuildMng;

class CWndGuildPayConfirm : public CWndNeuz
{
public:
	unsigned long   m_dwAppellation;
	CWndGuildPayConfirm();
	~CWndGuildPayConfirm();

	virtual bool Initialize(CWndBase* pWndParent);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndGuildTabApp : public CWndNeuz
{
public:
	void UpdateData();
	void EnableButton(bool bEnable);
	CWndGuildTabApp();
	~CWndGuildTabApp();

	unsigned long	m_adwPower[MAX_GM_LEVEL];
	CWndGuildPayConfirm* m_pWndGuildPayConfirm;
	CWndStatic* m_pWndPenya[MAX_GM_LEVEL];

	void SetData(unsigned long dwPower[]);
	void SetPenya(void);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif