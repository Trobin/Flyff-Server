#if __VER >= 13 // __HOUSING
#ifndef __WNDROOMLIST__H
#define __WNDROOMLIST__H

class CWndRoomList : public CWndNeuz
{
public:
	CWndRoomList();
	~CWndRoomList();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	void	Refresh();
};
#endif
#endif // __HOUSING