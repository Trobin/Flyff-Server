
//Author : gmpbigsun
//Date : 2009_11_16
// Tab winodw ( of guild window )

#pragma once 

#if __VER >= 15 // __GUILD_HOUSE

class CWndGuildTabPower : public CWndNeuz
{
	// 길드윈도우에서 추가되는 탭 윈도우 ( 길드 하우스에 관한 권한 설정 )
public:
	CWndGuildTabPower();
	virtual ~CWndGuildTabPower();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);

	void UpdateData();
	void SetData(unsigned long dwPower[]);
	void EnableButton(bool bEnable);

protected:
	unsigned long m_adwPower[MAX_GM_LEVEL];
	bool m_bChanedCheckBox;

};

#endif //__GUILD_HOUSE