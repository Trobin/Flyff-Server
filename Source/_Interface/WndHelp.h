#ifndef __WNDHELP_H
#define __WNDHELP_H

//////////////////////////////////////////////////////////////////////////////////////
// ����
//
class CWndHelp : public CWndNeuz
{
public:
	//CWndText      m_wndText;
	CString       m_strKeyword;
	//CWndTreeCtrl  m_wndViewCtrl;
	//CStringArray  m_strArray;

	CWndHelp();
	virtual ~CWndHelp();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};
class CWndHelpFAQ : public CWndNeuz
{
public:
	CMapStringToString m_mapFAQ;

	//CWndText      m_wndText;
	CString       m_strKeyword;
	//CWndTreeCtrl  m_wndViewCtrl;
	//CStringArray  m_strArray;
	bool LoadFAQ(const char* lpszFileName);

	CWndHelpFAQ();
	virtual ~CWndHelpFAQ();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
};
class CWndHelpTip : public CWndNeuz
{
	CStringArray m_aString;
	int m_nPosString;
public:
	CWndHelpTip();
	~CWndHelpTip();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


class CWndHelpInstant : public CWndNeuz
{
public:
	CString m_strHelpKey;
	CWndHelpInstant();
	~CWndHelpInstant();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif