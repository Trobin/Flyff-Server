#ifndef __WNDPARTY__H
#define __WNDPARTY__H

#include "WndPartyChangeTroup.h"

#if __VER >= 8 //__CSC_VER8_2
#include "WndPartyQuick.h"
#endif //__CSC_VER8_2

class CWndPartyInfo : public CWndNeuz
{
public:
	int m_nSelected; // 현재 파티창에서 선택된 놈 인덱스 (위에서부터 zero base)
	CWndPartyInfo();
	~CWndPartyInfo();

	CTexture m_texGauEmptyNormal;
	CTexture m_texGauFillNormal;
	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;

	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
class CWndPartySkill : public CWndNeuz
{
public:
	CTexture* m_atexSkill[10];
	int m_nSkillSelect;

	CWndPartySkill();
	~CWndPartySkill();

	virtual void OnMouseWndSurface(CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
};
class CWndParty : public CWndNeuz
{
public:
	CWndButton* m_pWndLeave;
	CWndButton* m_pWndChange;
	CWndButton* m_pWndTransfer;
#if __VER >= 8 //__CSC_VER8_2
	CWndButton* m_pBtnPartyQuick;
	CWndPartyQuick* m_pWndPartyQuick;
#else
	CWndButton* m_pBtnPartyQuick;
#endif //__CSC_VER8_2

public:
	CWndPartyInfo  m_wndPartyInfo;
	CWndPartySkill m_wndPartySkill;
	CWndPartyChangeTroup* m_WndPartyChangeTroup;

	CWndParty();
	~CWndParty();

	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif