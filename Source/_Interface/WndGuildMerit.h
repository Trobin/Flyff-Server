#ifndef __WNDGUILDMERIT__H
#define __WNDGUILDMERIT__H


class C2DRender;
class CItemElem;

class CWndGuildMeritCtrl : public CWndBase
{
	void	InterpretScript(CScanner& s, CPtrArray& ptrArray);
	CItemBase* m_pFocusItem;
	int		m_nCurSel;
	int		m_nFontHeight;
	unsigned long	m_nWndColor;
	unsigned long	m_nFontColor;
	unsigned long	m_nSelectColor;
	CWndScrollBar	m_wndScrollBar;

public:
	//	Constructions
	static	CTextureMng		m_textureMng;
	CWndGuildMeritCtrl();
	~CWndGuildMeritCtrl();

	CRect	m_rect;
	bool	m_bDrag;
	unsigned long	m_dwListCtrlStyle;
	vector<CItemBase*>		m_pItemContainer;

	void	Create(unsigned long dwListCtrlStyle, RECT& rect, CWndBase* pParentWnd, unsigned int nID);

	int		HitTest(CPoint point);

	//	Overridables
	int				GetTotalCount();
	void			RestoreItem();
	int				GetItemSize();
	virtual	void	SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual	void	OnInitialUpdate(void);
	virtual	void	OnDraw(C2DRender* p2DRender);
	//	virtual	void	OnLButtonUp( unsigned int nFlags, CPoint point );
	//	virtual	void	OnLButtonDown( unsigned int nFlags, CPoint point );
	virtual	void	OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual	void	OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual	void	OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual	void	OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual	void	OnSize(unsigned int nType, int cx, int cy);
	virtual	bool	OnEraseBkgnd(C2DRender* p2DRender);
	virtual	void	PaintFrame(C2DRender* p2DRender);
	virtual	void	OnMouseMove(unsigned int nFlags, CPoint point);
	virtual	bool	OnDropIcon(LPSHORTCUT pShortcut, CPoint point = 0);
	//	Implementation
public:
	void ClearItem();
	void AddItem(CItemBase* pItemContainer);
	int		InsertItem(unsigned int nMask, int nItem, const char* lpszItem, unsigned int nState,
		unsigned int nStateMask, int nImage, LPARAM lParam);
protected:
protected:
};

class CWndGuildMerit : public CWndNeuz
{
	CWndGuildMeritCtrl	m_wndctrlMerit;

	int		m_nItem;
	int		m_nPenya;	// ��� ���差.
public:
	CWndGuildMerit();
	~CWndGuildMerit();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif