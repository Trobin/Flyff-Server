// GuideSystem.h: interface for the CGuideSystem class.
//
//////////////////////////////////////////////////////////////////////
#ifndef __WNDGUIDESYSTEM__H
#define __WNDGUIDESYSTEM__H

#include "wndTutorial.h"

typedef struct GUIDE_STRUCT
{
	CString m_str;
	int     m_nkey;
	int     m_nEventMsg;
	bool    m_bBeginner;
	bool    m_bFlag;
	bool    m_bIsClear;
	int		m_nShowLevel;
#if __VER >= 12 // __MOD_TUTORIAL
	int		m_nSequence;
	int		m_nVicCondition;
	int		m_nLevel;
	unsigned int	m_nInput;
	CString m_strInput;

	GUIDE_STRUCT()
	{
		init();
	};

	void init()
	{
		m_nLevel = m_nInput = m_nkey = m_nEventMsg = m_bBeginner = m_bFlag = m_nShowLevel = m_nSequence = m_nVicCondition = 0;
		m_strInput.Empty();
		m_str.Empty();
	};
#endif
} GUIDE_STRUCT;

class CWndGuideTextMgr : public CWndNeuz
{

public:
	CString		m_strHelpKey;
	CTexture* m_pTextureBG;
	CRect       m_Rect[4];
	int						  m_nCurrentVector;

	vector<GUIDE_STRUCT>	  m_VecGuideText;

	GUIDE_STRUCT GetGuideText()
	{
		return m_VecGuideText[m_nCurrentVector];
	}


	void AddGuideText(GUIDE_STRUCT guide);
#if __VER >= 12 // __MOD_TUTORIAL
	void _SetGuideText(GUIDE_STRUCT guide, bool bIsNext);
#else
	void _SetGuideText(GUIDE_STRUCT guide);
#endif
	CWndGuideTextMgr();
	~CWndGuideTextMgr();

	void UpDate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual	bool OnEraseBkgnd(C2DRender* p2DRender);
};

class CWndGuideSelection : public CWndNeuz
{
public:
	CWndGuideSelection();
	virtual ~CWndGuideSelection();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
};
#if __VER >= 12 // __MOD_TUTORIAL
typedef map<int, GUIDE_STRUCT>::value_type mgValType;
typedef map<int, GUIDE_STRUCT>::iterator mgMapItor;

#define NO_CONDITION		0
#define CAMERA_ROTATION		1
#define CAMERA_ZOOMED		2
#define INPUT_KEY			3
#define INPUT_STRING		4
#define MOVE_ON_MOUSE		5
#define MOVE_ON_KEY			6
#define	OPEN_WINDOW			7

struct CONDITION
{
	bool		bIsCamMove;
	bool		bIsCamZoomed;
	unsigned int		nInputKey;
	bool		bIsClickOnLand;
	bool		bIsKeyMove;
	int			nOpenedWindowID;
	CString		strInput;

	void Init()
	{
		bIsKeyMove = bIsCamMove = bIsCamZoomed = bIsClickOnLand = false;
		nOpenedWindowID = nInputKey = 0;
		strInput.Empty();
	};
};

class CWndInfoPang : public CWndNeuz
{

public:
	CWndInfoPang() {};
	virtual ~CWndInfoPang();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
};

#endif

class CWndGuideSystem : public CWndNeuz
{
public:
	bool				 m_bIsLoad;
	enum { ANI_INTRO = 0, ANI_IDLE, ANI_BYTE };
	enum { KEY = 0, STR };
	unsigned long				 m_dwGuideLevel;
	unsigned long				 m_dwTime;
	BYTE				 m_bAniState;
#if __VER >= 12 // __MOD_TUTORIAL
	vector<GUIDE_STRUCT>	m_vecEventGuide;
	map<int, GUIDE_STRUCT>  m_mapGuide;
	mgMapItor				m_CurrentIter;
	mgMapItor				m_EmptyIter;
	GUIDE_STRUCT			m_CurrentGuide;
	CONDITION				m_Condition;
	bool CheckCompletion(GUIDE_STRUCT Guide);
	bool PassToNext();
	CWndTutorial* m_pWndTutorialView;
	bool				m_bIsViewVisible;
#else
	list<int>			 m_listGuideMsg;
	list<GUIDE_STRUCT>   m_listGuide;
	list<GUIDE_STRUCT*>  m_listGuideChart;
#endif
	CModelObject* m_pModel;
	CWndGuideTextMgr* m_wndGuideText;
	CWndGuideSelection* m_wndGuideSelection;

	CWndMenu			m_wndMenuPlace;


public:
	void SetAni(int nJob, int nAniKind);
	void ChangeModel(int nJob);
	void CreateGuideSelection();
	void GuideStart(int ischart = 0);
	void PushBack(GUIDE_STRUCT guide);
	bool	m_bIsGuideChart[2];

	CWndGuideSystem();
	virtual ~CWndGuideSystem();

	void		 SendGuideMessage(int nMsg);
	bool		 LoadGuide(const char* lpszFileName);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDown(unsigned int nFlags, CPoint point);
	virtual	void PaintFrame(C2DRender* p2DRender);
	virtual bool Process();
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
};

#endif // __WNDGUIDESYSTEM__H