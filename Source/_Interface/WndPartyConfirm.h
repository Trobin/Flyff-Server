#ifndef __WNDPARTYCONFIRM__H
#define __WNDPARTYCONFIRM__H

class CWndPartyConfirm : public CWndNeuz
{
public:
	unsigned long m_uLeader, m_uMember;
	long m_nLeaderLevel, m_nMemberLevel;
	long m_nLeaderJob, m_nMemberJob;
	unsigned long m_dwLeaderSex, m_dwMemberSex;
	bool bTroup;

	TCHAR m_szLeaderName[MAX_NAME];
	TCHAR m_szMemberName[MAX_NAME];
	void SetMember(unsigned long uLeader, long nLLevel, long nLJob, unsigned long dwLSex, unsigned long uMember, long nMLevel, long nMJob, unsigned long dwMSex, char* szLName, bool bTroupbuf);
	CWndPartyConfirm();
	~CWndPartyConfirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif