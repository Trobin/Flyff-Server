#ifndef __WNDGUILD_TAB_INFO__H
#define __WNDGUILD_TAB_INFO__H

#include "WndGuildName.h"

class CWndGuildDisMiss : public CWndNeuz
{
public:
	CWndGuildDisMiss();
	~CWndGuildDisMiss();

	virtual bool Initialize(CWndBase* pWndParent);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


class CWndGuildNotice : public CWndNeuz
{
public:
	CWndGuildNotice();
	~CWndGuildNotice();

	virtual bool Initialize(CWndBase* pWndParent);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndGuildSetLogo : public CWndNeuz
{
public:
	CRect    m_rect[CUSTOM_LOGO_MAX];
	int      m_nSelectLogo;

	CWndGuildSetLogo();
	~CWndGuildSetLogo();

	virtual bool Initialize(CWndBase* pWndParent);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndGuildTabInfo : public CWndNeuz
{
public:
#ifdef _DEBUG
	int m_nMx, m_nMy;
#endif

	CWndGuildName* m_pwndGuildName;
	CWndGuildNotice* m_pwndGuildNotice;
	CWndGuildSetLogo* m_pwndGuildSetLogo;
	CWndGuildDisMiss* m_pwndGuildDisMiss;

	CWndGuildTabInfo();
	~CWndGuildTabInfo();

	void UpdateData(void);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
};


#endif