#ifndef __WNDGUILDVOTE__H
#define __WNDGUILDVOTE__H



class CWndGuildVoteSeting;

class CWndGuildVote : public CWndNeuz
{
public:
	void SelChange(CGuild* pGuild, int nIndex);
	void SetszString(char* text1, char* text2, char* text3, char* text4);
	void SetCount(int ncount1, int ncount2, int ncount3, int ncount4);

	void SetQuestion(const char* question);
	void SetRadioGroup(int ncount);
	CWndGuildVoteSeting* m_pWndGuildVoteSeting;

	CWndGuildVote();
	~CWndGuildVote();

	void UpdateDataAll();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
};

class CWndGuildVoteSeting : public CWndNeuz
{
public:
	int m_nSelect;
	CWndGuildVoteSeting();
	~CWndGuildVoteSeting();

	virtual bool Initialize(CWndBase* pWndParent);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};


#endif