#ifndef __WNDGUILDWARREQUEST__H
#define __WNDGUILDWARREQUEST__H

class CWndGuildWarRequest : public CWndNeuz
{
	unsigned long m_idEnemyGuild;
	char m_szMaster[MAX_PLAYER];
public:
	CWndGuildWarRequest();
	~CWndGuildWarRequest();

	// Initialize하기전에 적길드아이디랑 적길마 이름을 받아둠.
	void	Set(unsigned long idEnemyGuild, const char* szMaster)
	{
		m_idEnemyGuild = idEnemyGuild;
		strcpy(m_szMaster, szMaster);
	}

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif