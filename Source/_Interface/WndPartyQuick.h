#if __VER >= 8 //__CSC_VER8_2

#ifndef __WNDPARTY_QUICK__H
#define __WNDPARTY_QUICK__H

class CWndParty;

class CWndPartyQuick : public CWndNeuz
{
public:
	CWndButton* m_pBtnParty;
	CWndStatic* m_pWndMemberStatic[8];
	CTexture m_texGauEmptyNormal;
	CTexture m_texGauFillNormal;

	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;

	int m_MemberCount;
	CWndParty* m_pWndParty;
	CMover* m_pFocusMember;
public:

	CWndPartyQuick();
	~CWndPartyQuick();

	virtual void SerializeRegInfo(CAr& ar, unsigned long& dwVersion);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	virtual void OnRButtonDblClk(unsigned int nFlags, CPoint point);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	void SetActiveMember(int MemberNum);
};

#endif //__WNDPARTY_QUICK__H

#endif //__CSC_VER8_2