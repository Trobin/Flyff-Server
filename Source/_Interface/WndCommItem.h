#ifndef __WNDCOMMITEM__H
#define __WNDCOMMITEM__H

class C2DRender;
class CItemElem;

class CWndCommItemCtrl : public CWndBase
{
	void InterpriteScript(CScanner& scanner, CPtrArray& ptrArray);
	//void PaintListBox(C2DRender* p2DRender,CPoint& pt,CPtrArray& ptrArray);
	int           m_nFontHeight;
	CWndScrollBar m_wndScrollBar;

	CWndWorld* pWndWorld;
	int			m_dwDraw[SM_MAX + MAX_SKILLINFLUENCE];
	int			m_nMaxDraw;

	CTexture m_texGauEmptyNormal;
	CTexture m_texGauFillNormal;

	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;

	// Constructors
public:
	CRect         m_rect;
	unsigned long         m_dwListCtrlStyle;


	void	SetScrollBar();
	int		GetMaxBuff();			// 출력해야할 MAX값얻어오기
	void	DrawSM(C2DRender* p2DRender, CPoint* pPoint, int x, int& nScroll);
	void	DrawSkill(C2DRender* p2DRender, CPoint* pPoint, int x, int& nScroll);

	CWndCommItemCtrl();
	~CWndCommItemCtrl();

	void GetFriendName();
	void Create(unsigned long m_dwListCtrlStyle, RECT& rect, CWndBase* pParentWnd, unsigned int nID);
	void LoadListBoxScript(const char* lpFileName);
	LPSKILL GetSkill(int i);
	// Attributes
	COLORREF GetBkColor() const;
	bool SetBkColor(COLORREF cr);
	bool GetItem(LVITEM* pItem) const;
	bool SetItem(const LVITEM* pItem);
	bool SetItem(int nItem, int nSubItem, unsigned int nMask, const char* lpszItem,
		int nImage, unsigned int nState, unsigned int nStateMask, LPARAM lParam);
	// Operations
	int InsertItem(const LVITEM* pItem);

	// Overridables
	virtual void OnMouseWndSurface(CPoint point);
	virtual	void SetWndRect(CRect rectWnd, bool bOnSize = true);
	virtual void OnInitialUpdate();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual	void PaintFrame(C2DRender* p2DRender);

	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	// Implementation
public:
	int InsertItem(unsigned int nMask, int nItem, const char* lpszItem, unsigned int nState,
		unsigned int nStateMask, int nImage, LPARAM lParam);
protected:
	void RemoveImageList(int nImageList);
};

class CWndCommItem : public CWndNeuz
{
public:
	CWndCommItemCtrl	m_wndCommItemCtrl;
public:
	CWndCommItem();
	~CWndCommItem();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndCommercialElem : public CWndNeuz
{
public:
	CItemElem* m_pItemElem[2];
	CRect      m_Rect[2];

	void InitSetting(void);
	bool IsRestrictionItem(CItemElem* pItemElem, bool bMessage = false);
	bool IsUpgradeItem(CItemElem* pItemElem, bool bMessage = false);
	bool IsSMItem(CItemElem* pItemElem, bool bMessage = false);


	CWndCommercialElem();
	~CWndCommercialElem();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnRButtonUp(unsigned int nFlags, CPoint point);
	virtual bool OnDropIcon(LPSHORTCUT pShortcut, CPoint point);
};

class CWndRemoveElem : public CWndNeuz
{
public:
	BYTE		m_nType;
	int			m_nParts;
	unsigned long		m_dwItemId;
	OBJID		m_objid;
	CItemElem* m_pItemElem;
	bool		m_bSetting;

	CWndRemoveElem();
	~CWndRemoveElem();

	void OnSetItem(BYTE nType, unsigned long dwItemId, OBJID objid, int nParts, CItemElem* pItemElem);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

#endif