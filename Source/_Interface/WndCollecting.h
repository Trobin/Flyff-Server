#ifndef __COLLECTINGWND__H
#define __COLLECTINGWND__H

class CCollectingWnd : public CWndNeuz
{
public:
	CCollectingWnd();
	~CCollectingWnd();

	bool		m_bIsCollecting;

	CModelObject* m_pModel; // �ݷ��� ��
	int				m_nDisplay;
	CItemElem* m_pElem;
	D3DXMATRIX		m_matModel;
	float			m_fAngle;
	CSfxModel* m_pSfx;
	CSfxBase* m_pSfxBase;
	CRect			m_BetteryRect;
	CRect			m_LevelRect;
	CTexture* m_pTexLevel, * m_pTexBatt;
	CTexture* m_pTexGauEmptyNormal;
	//CTexture		m_texGauEmptySmall;
	CTexture* m_pTexGauFillNormal;
	//CTexture		m_texGauFillSmall;
	char			m_pbufText[24];
	LPDIRECT3DVERTEXBUFFER9 m_pVBGauge;
	bool 			m_bVBGauge;
	int 			m_nGWidth;

	virtual	bool Process();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual void OnDestroy();
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual bool OnEraseBkgnd(C2DRender* p2DRender);
	virtual void OnMouseWndSurface(CPoint point);
	void	Update();
	bool	SetButtonCaption(bool bIsStart);
	bool	IsChangeState();

	void	AddSfx();
	void	DeleteSfx();

};
#endif