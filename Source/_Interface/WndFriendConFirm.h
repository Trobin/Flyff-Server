#ifndef __WNDFRIENDCONFIRM__H
#define __WNDFRIENDCONFIRM__H

class CWndFriendConFirm : public CWndNeuz
{
public:
	unsigned long m_uLeader, m_uMember;
	BYTE m_nLeaderSex;
	long m_nLeaderJob;
	TCHAR m_szLeaderName[MAX_NAME];

public:
	void SetMember(unsigned long uLeader, unsigned long uMember, long nLeaderJob, BYTE nLeaderSex, char* szLeadName);
public:
	CWndFriendConFirm();
	~CWndFriendConFirm();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};

class CWndAddFriend : public CWndNeuz
{
public:
	CWndAddFriend();
	~CWndAddFriend();

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif