#ifndef __WNDGUILDTABWAR__H
#define __WNDGUILDTABWAR__H

#include "WndGuildWarDecl.h"
#include "WndGuildWarGiveUp.h"
#include "WndGuildWarPeace.h"

class CWndGuildTabWar : public CWndNeuz
{
	CWndGuildWarDecl* m_pWndGuildWarDecl;
	CWndGuildWarGiveUp* m_pWndGuildWarGiveUp;
	CWndGuildWarPeace* m_pWndGuildWarPeace;

public:


	CWndGuildTabWar();
	~CWndGuildTabWar();

	void UpdateData(void);

	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
};
#endif
