#ifndef __WNDMOTION__H
#define __WNDMOTION__H

class CWndMotion1 : public CWndBase
{
	MotionProp* m_pSelectMotion;
	int m_nSelect;

	CPtrArray m_motionArray;

public:
	CWndMotion1();
	virtual ~CWndMotion1();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool Process();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
};

class CWndEmoticon : public CWndBase
{
	CTexture* m_pSelectTexture;

	int m_nSelect;

	CPtrArray m_emoticonArray;

public:
	CWndEmoticon();
	virtual ~CWndEmoticon();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwWndId = 0);
	// message
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase = NULL);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseWndSurface(CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
};

class CWndMotion : public CWndNeuz
{
public:
	CWndMotion1		m_wndMotion1;
	CWndEmoticon    m_wndEmoticon;

	CWndMotion();
	~CWndMotion();
	virtual void OnMouseWndSurface(CPoint point);
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long nType = MB_OK);
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);
	virtual void OnMouseMove(unsigned int nFlags, CPoint point);
};

#endif