// WndArcane.h: interface for the CWndNeuz class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDTITLE_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)
#define AFX_WNDTITLE_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

DECLARE_WNDMESSAGEBOX(CWndConnectingBox)
DECLARE_WNDMESSAGEBOX(CWndDisconnectedBox)
DECLARE_WNDMESSAGEBOX(CWndCharBlockBox)
DECLARE_WNDMESSAGEBOX(CWndAllCharBlockBox)

#ifdef __NPKCRYPT
#include "npkcrypt.h"
#endif	// __NPKCRYPT
#if __VER >= 15 // __2ND_PASSWORD_SYSTEM
#include "Wnd2ndPassword.h"
#endif // __2ND_PASSWORD_SYSTEM

class CWndLogin : public CWndNeuz
{
	//CTexture   m_Texture;
public:
#ifdef __NPKCRYPT
	HKCRYPT		m_hKCrypt;
#endif	// __NPKCRYPT

	bool m_bDisconnect;

	CWndEdit   m_wndAccount;
	CWndEdit   m_wndPassword;
	CWndButton m_wndSaveAccount;
	CWndButton m_wndSavePassword;
	CWndButton m_wndFindAccount;

	CWndButton m_wndLogin;
	CWndButton m_wndRegist;
	CWndButton m_wndQuit;

	void Connected(long lTimeSpan);

	CWndLogin();
	virtual ~CWndLogin();
	virtual void OnDraw(C2DRender* p2DRender);
	virtual	void OnInitialUpdate();
	virtual bool Initialize(CWndBase* pWndParent = NULL, unsigned long dwStyle = 0);
	virtual bool Process();
	// message
	virtual bool OnChildNotify(unsigned int message, unsigned int nID, LRESULT* pLResult);
	virtual bool OnCommand(unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase);
	virtual void OnSize(unsigned int nType, int cx, int cy);
	virtual void OnLButtonUp(unsigned int nFlags, CPoint point);
	virtual void OnLButtonDown(unsigned int nFlags, CPoint point);

#ifdef __CON_AUTO_LOGIN
	void SetAccountAndPassword(const CString& account, const CString& pass);
#endif
};

BEGIN_WNDCLASS(CWndSelectServer)
public:
	vector<CString> m_vecStrBanner;
	unsigned long		 m_dwChangeBannerTime;
	LPIMAGE		 m_atexPannel;

	CWndEdit     m_wndURL;
	CWndButton   m_wndSearch;
	CWndListCtrl	m_wndServerList;
	CWndButton   m_wndBack;
	CWndButton   m_wndAccept;

	virtual bool Process();
	void AfterSkinTexture(LPWORD pDest, CSize size, D3DFORMAT d3dFormat = D3DFMT_A4R4G4B4);
	void Connected();
	virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	END_WNDCLASS


		BEGIN_WNDCLASS(CWndDeleteChar)
public:
	/*
		virtual bool Initialize( CWndBase* pWndParent = NULL, unsigned long nType = MB_OK );
		virtual bool OnChildNotify( unsigned int message, unsigned int nID, LRESULT* pLResult );
		virtual void OnDraw( C2DRender* p2DRender );
		virtual	void OnInitialUpdate();
		virtual bool OnCommand( unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase );
		virtual void OnSize( unsigned int nType, int cx, int cy );
		virtual void OnLButtonUp( unsigned int nFlags, CPoint point );
		virtual void OnLButtonDown( unsigned int nFlags, CPoint point );
	*/
	void DeletePlayer(int nSelect, const char* szNo);
	virtual void AdditionalSkinTexture(LPWORD pDest, CSize sizeSurface, D3DFORMAT d3dFormat = D3DFMT_A4R4G4B4);
	END_WNDCLASS
		/*
		class CWndConfirmSell : public CWndNeuz
		{
		public:
			CMover* m_pMover;
			CItemElem* m_pItemElem;
			CWndConfirmSell();
			~CWndConfirmSell();

			virtual bool Initialize( CWndBase* pWndParent = NULL, unsigned long nType = MB_OK );
			virtual bool OnChildNotify( unsigned int message, unsigned int nID, LRESULT* pLResult );
			virtual void OnDraw( C2DRender* p2DRender );
			virtual	void OnInitialUpdate();
			virtual bool OnCommand( unsigned int nID, unsigned long dwMessage, CWndBase* pWndBase );
			virtual void OnSize( unsigned int nType, int cx, int cy );
			virtual void OnLButtonUp( unsigned int nFlags, CPoint point );
			virtual void OnLButtonDown( unsigned int nFlags, CPoint point );
		};
		*/
		BEGIN_WNDCLASS(CWndSelectChar)
public:
	CWndText m_wndText1;
	CWndText m_wndText2;
	CWndText m_wndText3;

	CWndDeleteChar* m_pWndDeleteChar;
#if __VER >= 15 // __2ND_PASSWORD_SYSTEM
	CWnd2ndPassword* m_pWnd2ndPassword;
#endif // __2ND_PASSWORD_SYSTEM
	static int m_nSelectCharacter;
	CRect m_aRect[MAX_CHARACTER_LIST];
	unsigned long m_dwMotion[MAX_CHARACTER_LIST];
	CModelObject* m_pBipedMesh[MAX_CHARACTER_LIST];
	bool m_bDisconnect;

private:
	bool m_CreateApply; //서버통합 관련 특정 기간 캐릭터 생성 금지.

public:
	void DeleteCharacter();
	void UpdateCharacter();
	void Connected();
	void OnDestroyChildWnd(CWndBase* pWndChild);

	bool SetMotion(CModelObject* pModel, unsigned long dwIndex, unsigned long dwMotion, int nLoop, unsigned long dwOption);

	void SelectCharacter(int i);

	virtual bool    Process();
	virtual HRESULT InitDeviceObjects();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();
	virtual void	OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);

	END_WNDCLASS

		BEGIN_WNDCLASS(CWndCreateChar)
		CWndButton   m_wndAccept;

	/*
		CWndComboBox m_wndComboGender;
		CWndComboBox m_wndComboHair;
		CWndComboBox m_wndComboSkin;
		CWndComboBox m_wndComboHairColor;
		CWndComboBox m_wndComboCostume;
		CWndComboBox m_wndComboJob ;
		CWndEdit     m_wndName     ;
		CWndButton   m_wndCancel   ;
		CWndButton   m_wndCreate   ;

		CWndButton   m_wndMale;
		CWndButton   m_wndFemale;
		CWndButton   m_wndLHair;
		CWndButton   m_wndRHair;
		CWndButton   m_wndLHairColor;
		CWndButton   m_wndRHairColor;
		CWndButton   m_wndLFace;
		CWndButton   m_wndRFace;
		*/
		//CWndButton   m_wndLCostume;
		//CWndButton   m_wndRCostume;
		//CWndButton   m_wndNude;

	CModelObject* m_pModel;

	PLAYER m_Player;

	void SetSex(int nSex);
	void Connected();
	virtual HRESULT InitDeviceObjects();
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

#if __VER >= 15 // __2ND_PASSWORD_SYSTEM
	enum { MAX_2ND_PASSWORD_NUMBER = 4 };
#endif // __2ND_PASSWORD_SYSTEM
	END_WNDCLASS
#endif // !defined(AFX_WNDTITLE_H__A93F3186_63D6_43C1_956F_EC8691E0C7D9__INCLUDED_)