#ifndef __DPBILLING_H__
#define	__DPBILLING_H__

#include "dpmng.h"
#include "msghdr.h"
#include "misc.h"
#include <map>

using	namespace	std;

#undef	theClass
#define theClass	CDPBilling
#undef theParameters
#define theParameters	CAr & ar, unsigned long

// 일본 빌링통신용 
class CDPBilling : public CDPMng<CBuffer2>
{
private:
	HWND	m_hWnd;
public:
	bool	m_bConnected;

public:
	//	Constructions
	CDPBilling();
	virtual	~CDPBilling();
	//	Overrides
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long idFrom);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long idFrom);
	// Operations
	void	Init(HWND hWnd) { m_hWnd = hWnd; }
	void	SendGameStart(const char* lpszUserID, const char* lpszUserIP);
	void	SendKeepAlive();
	void	SendGSCSStart();
	//	Handlers
	USES_PFNENTRIES;
	void	OnKeepAliveReceive(CAr& ar, unsigned long dpid);
	void	OnGameStartReceive(CAr& ar, unsigned long dpid);
};

#endif	// __DPBILLING_H__