// AccountServer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"

#include "dpsrvr.h"
#include "dpdbsrvr.h"
#include "account.h"
#include "BillingMgr.h"
#include "dpadbill.h"

#include "dbmanager.h"

#include <iostream>
#include "MyTimer.h"

extern	CDbManager	g_DbManager;
extern	CDPSrvr		g_dpSrvr;
extern	CDPDBSrvr	g_dpDbSrvr;
extern	CAccountMng	g_AccountMng;

#ifdef __SECURITY_0628
void	LoadResAuth(const char* lpszFileName);
#endif	// __SECURITY_0628

bool Script(const char* lpszFileName);

bool init()
{
#ifdef __BILLING0712
	if (::CreateBillingMgr() == false)		// CreateWindow전에 호출되어야 한다.
	{
		return false;
	}
#endif

	g_dpSrvr.m_bCheckAddr = true;

	std::srand(time(NULL));

	// (menu -> script -> createdbworker)
	if (Script("AccountServer.ini") == false)
	{
		return false;
	}

#ifdef __SECURITY_0628
	LoadResAuth("Flyff.b");
#endif	// __SECURITY_0628

	//	if( CQuery::EnableConnectionPooling() )
	//		OutputDebugString( "EnableConnectionPooling\n" );

	g_DbManager.CreateDbWorkers();

	if (g_dpSrvr.LoadAddrPmttd("pmttd.ini") == false)
	{
		std::cout << "LoadAddrPmttd false\n";
	}

	if (g_dpSrvr.LoadIPCut("IPCut.ini") == false)
	{
		std::cout << "LoadIPCut false\n";
	}

	if (false == g_dpSrvr.StartServer(PN_ACCOUNTSRVR_0)
		|| false == g_dpDbSrvr.StartServer(PN_ACCOUNTSRVR_1)
		|| false == CDPAdbill::GetInstance()->StartServer(PN_ADBILL))
	{
		std::cerr << "Unable to start server";
		return false;
	}

#ifdef __BILLING0712
	if (GetBillingMgr()->Init(hWnd) == false)
		return false;
#endif

	if (false == g_DbManager.AllOff())
	{
		return false;
	}
	return true;
}

void exit()
{
#ifdef __BILLING0712
	GetBillingMgr()->Release();
#endif
	g_dpSrvr.DeleteDPObject();
	g_dpDbSrvr.DeleteDPObject();
	CDPAdbill::GetInstance()->DeleteDPObject();

	g_AccountMng.Clear();
	SAFE_DELETE(CAccount::m_pPool);

	g_DbManager.CloseDbWorkers();

	//	UninitializeNetLib();
	SAFE_DELETE(CAr::m_pHeapMng);
	SAFE_DELETE(CBuffer::m_pPool);
	SAFE_DELETE(CBuffer2::m_pPool2);
	SAFE_DELETE(CBuffer3::m_pPool2);
	SAFE_DELETE(CBuffer::m_pHeapMng);

	UNLOAD_WS2_32_DLL;
}

void callbackSendPlayerCount()
{
	std::cout << "Thread send player count\n";
	g_dpDbSrvr.SendPlayerCount();
}

void callbackTimeCheckAddr()
{
	std::cout << "Thread time check addr\n";
	if (g_dpSrvr.m_bCheckAddr && g_AccountMng.IsTimeCheckAddr())
	{
		g_dpSrvr.m_bCheckAddr = false;
	}
}

void callbackBuyingInfoMngProcess()
{
	CBuyingInfoMng::GetInstance()->Process();
}

#ifdef __SECURITY_0628
void	LoadResAuth(const char* lpszFileName)
{
	CScanner s;
	if (s.Load(lpszFileName))
	{
		s.GetToken();
		lstrcpy(g_dpSrvr.m_szResVer, s.Token);
	}
}
#endif	// __SECURITY_0628

bool Script(const char* lpszFileName)
{
#ifdef __INTERNALSERVER	
	strcpy(g_DbManager.m_szLoginPWD, "#^#^account");
	strcpy(g_DbManager.m_szLogPWD, "#^#^log");
#endif

	CScanner s;
	bool bSkipTracking = false;

	if (s.Load(lpszFileName))
	{
		s.GetToken();
		while (s.tok != FINISHED)
		{
			if (s.Token == ";")
			{
				s.GetToken();
				continue;
			}
			else if (s.Token == "TEST")
			{
				g_dpSrvr.m_bCheckAddr = false;
			}
			else if (s.Token == "AddTail")
			{
				LPSERVER_DESC pServer
					= g_dpSrvr.m_aServerset + g_dpSrvr.m_dwSizeofServerset++;
				s.GetToken();	// (
				pServer->dwParent = s.GetNumber();
				s.GetToken();	// ,
				pServer->dwID = s.GetNumber();
				s.GetToken();	// ,
				s.GetToken();
				strcpy(pServer->lpName, s.Token);
				s.GetToken();	// ,
				s.GetToken();
				strcpy(pServer->lpAddr, s.Token);
				s.GetToken();	// ,
				pServer->b18 = (bool)s.GetNumber();
				s.GetToken();	// ,
				pServer->lEnable = (long)s.GetNumber();
#ifdef __SERVERLIST0911
				pServer->lEnable = 0L;
#endif	// __SERVERLIST0911
				s.GetToken();	// ,
				pServer->lMax = (long)s.GetNumber();
				s.GetToken();	// )

//				if( pServer->dwParent != NULL_ID )
				{
					unsigned long uId = pServer->dwParent * 100 + pServer->dwID;
					g_dpSrvr.m_2ServersetPtr.insert(map<unsigned long, LPSERVER_DESC>::value_type(uId, pServer));
				}
			}
			else if (s.Token == "MAX")
			{
				g_dpSrvr.m_nMaxConn = s.GetNumber();
			}
			else if (s.Token == "BillingIP")
			{
				s.GetToken();
				GetBillingMgr()->SetConfig(BID_IP, (unsigned long)(const char*)s.Token);
			}
			else if (s.Token == "BillingPort")
			{
				GetBillingMgr()->SetConfig(BID_PORT, s.GetNumber());
			}
			else if (s.Token == "BillingFreePass")
			{
				GetBillingMgr()->SetConfig(BID_FREEPASS, s.GetNumber());
			}
			else if (s.Token == "BillingPWD")
			{
				s.GetToken();
				char* szPWD = GetBillingPWD();
				::GetPWDFromToken(s.Token, szPWD); // from query.cpp
			}
			else if (s.Token == "MSG_VER")
			{
				s.GetToken();
				lstrcpy(g_dpSrvr.m_szVer, s.Token);
			}
			else if (s.Token == "SKIP_TRACKING")		// login logout
			{
				bSkipTracking = true;
				g_DbManager.SetTracking(false);
			}
			else if (s.Token == "DSN_NAME_LOGIN")
			{
				s.GetToken();
				strcpy(DSN_NAME_LOGIN, s.Token);
				if (bSkipTracking == false)
				{
					g_DbManager.SetTracking(true);
				}
			}
			else if (s.Token == "DB_ADMIN_ID_LOGIN")
			{
				s.GetToken();
				strcpy(DB_ADMIN_ID_LOGIN, s.Token);
			}
			else if (s.Token == "DSN_NAME_LOG")
			{
				s.GetToken();
				strcpy(DSN_NAME_LOG, s.Token);
			}
			else if (s.Token == "DB_ADMIN_ID_LOG")
			{
				s.GetToken();
				strcpy(DB_ADMIN_ID_LOG, s.Token);
			}
			else if (s.Token == "DSN_NAME_BILLING")
			{
				s.GetToken();
				strcpy(DSN_NAME_BILLING, s.Token);
			}
			else if (s.Token == "DB_ADMIN_ID_BILLING")
			{
				s.GetToken();
				strcpy(DB_ADMIN_ID_BILLING, s.Token);
			}
			else if (s.Token == "NOT_RELOADPRO")
			{
				g_dpSrvr.m_bReloadPro = false;
			}
			else if (s.Token == "NOLOG")
			{
				g_DbManager.SetLogging(false);
			}
			else if (s.Token == "DB_PWD_LOGIN")
			{
				s.GetToken();
				::GetPWDFromToken(s.Token, g_DbManager.m_szLoginPWD);
			}
			else if (s.Token == "DB_PWD_LOG")
			{
				s.GetToken();
				::GetPWDFromToken(s.Token, g_DbManager.m_szLogPWD);
			}
#ifdef __LOG_PLAYERCOUNT_CHANNEL
			else if (s.Token == "AddChannel")
			{
				s.GetToken();
				g_dpDbSrvr.m_vecstrChannelAccount.push_back(s.Token);
			}
#endif // __LOG_PLAYERCOUNT_CHANNEL

			s.GetToken();
		}
		return true;
	}

	std::cerr << "Can't open file " << lpszFileName << "\n";

	return false;
}

int main(int argc, char** argv)
{
	bool resInit = init();
	std::cout << "Initialisation : " << resInit << "\n";
	Timer t;
	t.setInterval(&callbackSendPlayerCount, SEC(60));
	t.setInterval(&callbackTimeCheckAddr, SEC(30));
	t.setInterval(&callbackBuyingInfoMngProcess, 500);

	while (getchar() != 'q');

	t.stop();

	exit();

	return 0;
}