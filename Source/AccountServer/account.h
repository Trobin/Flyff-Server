#ifndef __ACCOUNT_H__
#define	__ACCOUNT_H__

#include "mempooler.h"
#include <map>
#include <string>
using namespace std;

#define	MAX_CERTIFIER			32
const int LEFTIME_NOTIFIED = 1;

//빌링의 결과값 
enum BILLING_RETURN
{
	SUCCESS = 0,
	BILLING_INFO_FAILED = 1,
	DATABASE_ERROR = 2,
	OTHER_ERROR = 3,
	TIME_OVER = 11
};

struct BILLING_INFO
{
	const char* szAccount;
	unsigned long dwKey;
	long lResult;				// 빌링의 결과값 
	const CTime* pTimeOverDays;
	char cbLastOneLogon;			// 대만에서 사용하는 플래그 'Y'일 경우만 30분통지를 한다. 

	BILLING_INFO() :
		szAccount(NULL),
		dwKey(0),
		lResult(OTHER_ERROR),
		cbLastOneLogon('N')
	{
	}
};

enum ACCOUNT_STATUS
{
	ACCOUNT_STATUS_INITIAL,			// 초기상태 
	ACCOUNT_STATUS_NOTIFIED,		// 남은 시간을 알려준 상태 
	ACCOUNT_STATUS_SECONDQUERY,		// 2번째 쿼리결과를 기다리는 상태   
};

class CAccount
{
public:
	//	Constructions
	CAccount();
	CAccount(const char* lpszAccount, unsigned long dpid1, unsigned long dpid2, unsigned char b18, int fCheck);
	virtual	~CAccount();

	bool IsAdult();
	void SetTimeOverDays(const CTime* pOverDays);
	void SendBillingResult(BILLING_INFO* pResult);

	//	Attributes
public:
	unsigned long	m_dpid1;
	unsigned long	m_dpid2;
	unsigned long	m_dwIdofServer;
	unsigned char			m_cbAccountFlag;
	unsigned long	m_dwBillingClass;
	CTime			m_TimeOverDays;
	TCHAR			m_lpszAccount[MAX_ACCOUNT];
	bool			m_fRoute;
	time_t			m_dwPing;
	unsigned long	m_dwAuthKey;
	int				m_cbRef;
	int				m_fCheck;
	ACCOUNT_STATUS	m_nStatus;				// ACCOUNT_STATUS_INITIAL, ...
	char			m_cbLastOneLogon;		// 대만에서 사용하는 플래그 'Y'일 경우만 30분통지를 한다. 
	unsigned long			m_uIdofMulti;

public:
#ifndef __VM_0820
#ifndef __MEM_TRACE
	static MemPooler<CAccount>* m_pPool;
	void* operator new(size_t nSize) { return CAccount::m_pPool->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CAccount::m_pPool->Alloc(); }
	void	operator delete(void* lpMem) { CAccount::m_pPool->Free((CAccount*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CAccount::m_pPool->Free((CAccount*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820
};

class CAccountMng
{
private:
	map<string, CAccount*> m_stringToPAccount;
	map<unsigned long, int> m_dpidToIndex;
	map<unsigned long, CAccount*> m_adpidToPAccount[MAX_CERTIFIER];
	int		m_nSizeof;
	int		m_nOldHour;
#ifndef __EUROPE_0514
	unsigned long	m_dwSerial;
#endif	// __EUROPE_0514
public:
	int						m_nCount;
	map<unsigned long, unsigned long>		m_2IdofServer;

	int		m_nYear;
	int		m_nMonth;
	int		m_nDay;
	int		m_nHour;
	int		m_nMinute;
public:
	//	Constructions
	CAccountMng();
	virtual	~CAccountMng();
public:
	//	Attributes
	int			GetIndex(unsigned long dpid1);
	int			GetIdofServer(unsigned long dpid);
	CAccount* GetAccount(const char* lpszAccount);
	CAccount* GetAccount(unsigned long dpid1, unsigned long dpid2);
	CAccount* GetAccount(const char* lpszAccount, unsigned long dwSerial);
#ifdef __LOG_PLAYERCOUNT_CHANNEL
	map<string, CAccount*> GetMapAccount();
#endif // __LOG_PLAYERCOUNT_CHANNEL

	//	Operations
	void	Clear(void);
	void	AddConnection(unsigned long dpid);
	void	RemoveConnection(unsigned long dpid);
	void	AddIdofServer(unsigned long dpid, unsigned long dwIdofServer);
#ifdef __SERVERLIST0911
	unsigned long	RemoveIdofServer(unsigned long dpid, bool bRemoveConnection = true);
#else	// __SERVERLIST0911
	void	RemoveIdofServer(unsigned long dpid, bool bRemoveConnection = true);
#endif	// __SERVERLIST0911

	unsigned char	AddAccount(const char* lpszAccount, unsigned long dpid1, unsigned long dpid2, unsigned long* pdwAuthKey, unsigned char b18, int fCheck);
	void	RemoveAccount(const char* lpszAccount);
	void	RemoveAccount(unsigned long dpid1, unsigned long dpid2);
	void	PreventExcess();
	void	KickOutCheck();
	bool	IsTimeCheckAddr();
	void	SendBillingResult(BILLING_INFO* pResult);

public:
	CMclCritSec		m_AddRemoveLock;

#if __VER >= 14 // __PCBANG
	void PushPCBangPlayer(unsigned long dwAuthKey, unsigned long dwClass) { m_mapPCBang.insert(map<unsigned long, unsigned long>::value_type(dwAuthKey, dwClass)); }
	unsigned long PopPCBangPlayer(unsigned long dwAuthKey);

private:
	map<unsigned long, unsigned long>	m_mapPCBang;
#endif // __PCBANG
};

#endif	// __ACCOUNT_H__