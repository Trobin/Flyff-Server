#ifndef __DPSRVR_H__
#define	__DPSRVR_H__

#include "dpmng.h"
#include "msghdr.h"
#include "misc.h"
#include <map>

using	namespace	std;

#undef	theClass
#define theClass	CDPSrvr
#undef theParameters
#define theParameters	CAr & ar, unsigned long, unsigned long

#define MAX_IP 10240

typedef map<string, int>	STRING2INT;

class CDPSrvr : public CDPMng<CBuffer>
{
public:
	bool	m_bCheckAddr;		// 접속하는 account의 address를 검사해야 하는가?
	int		m_nMaxConn;
	bool	m_bReloadPro;
	CMclCritSec		m_csAddrPmttd;
	char	m_sAddrPmttd[128][16];
	int		m_nSizeofAddrPmttd;


	CMclCritSec		m_csIPCut;
	STRING2INT m_sIPCut;
	int		m_nIPCut[MAX_IP][3];
	int		m_nSizeofIPCut;

	unsigned long	m_dwSizeofServerset;
	SERVER_DESC		m_aServerset[128];
	map<unsigned long, LPSERVER_DESC>	m_2ServersetPtr;
	char	m_szVer[32];
#ifdef __SECURITY_0628
	char	m_szResVer[100];
#endif	// __SECURITY_0628

public:
	//	Constructions
	CDPSrvr();
	virtual	~CDPSrvr();
	//	Overrides
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long idFrom);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long idFrom);
	// Operations
	void	CloseExistingConnection(const char* lpszAccount, long lError);
	bool	LoadAddrPmttd(const char* lpszFileName);
	bool	LoadIPCut(LPCSTR lpszFileName);
	void	DestroyPlayer(unsigned long dpid1, unsigned long dpid2);

	void	OnAddConnection(unsigned long dpid);
	void	OnRemoveConnection(unsigned long dpid);

	void	SendServersetList(unsigned long dpid);
	void	SendPlayerCount(unsigned long uId, long lCount);

	bool	EnableServer(unsigned long dwParent, unsigned long dwID, long lEnable);
	void	SendEnableServer(unsigned long uId, long lEnable);
#ifdef __GPAUTH_02
#ifdef __EUROPE_0514
	void	OnAfterChecking(unsigned char f, const char* lpszAccount, unsigned long dpid1, unsigned long dpid2, unsigned long dwAuthKey, unsigned char cbAccountFlag, long lTimeSpan, const char* szCheck, const char* szBak);
#else	// __EUROPE_0514
	void	OnAfterChecking(unsigned char f, const char* lpszAccount, unsigned long dpid1, unsigned long dpid2, unsigned long dwAuthKey, unsigned char cbAccountFlag, long lTimeSpan, const char* szCheck);
#endif	// __EUROPE_0514
#else	// __GPAUTH_02
	void	OnAfterChecking(unsigned char f, const char* lpszAccount, unsigned long dpid1, unsigned long dpid2, unsigned long dwAuthKey, unsigned char cbAccountFlag, long lTimeSpan);
#endif	// __GPAUTH_02

	bool	IsABClass(LPCSTR lpszIP);
	void	GetABCClasstoString(LPCSTR lpszIP, char* lpABClass, int& nCClass);
	void	InitIPCut(void);
	//	Handlers
	USES_PFNENTRIES;
	void	OnAddAccount(CAr& ar, unsigned long dpid1, unsigned long dpid2);
	void	OnRemoveAccount(CAr& ar, unsigned long dpid1, unsigned long dpid2);
	void	OnPing(CAr& ar, unsigned long dpid1, unsigned long dpid2);
	void	OnCloseExistingConnection(CAr& ar, unsigned long dpid1, unsigned long dpid2);
};

#endif	// __DPCRTFRSRVR_H__