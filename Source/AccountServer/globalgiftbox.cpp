#include "stdafx.h"
#include "globalgiftbox.h"
#include "dpmng.h"
#include "msghdr.h"

#include <iostream>

CGlobalGiftbox::CGlobalGiftbox()
{
	m_bLoad = false;
}

CGlobalGiftbox::~CGlobalGiftbox()
{
	m_map.clear();
}

CGlobalGiftbox* CGlobalGiftbox::GetInstance(void)
{
	static CGlobalGiftbox sGlobalGiftbox;
	return	&sGlobalGiftbox;
}

void CGlobalGiftbox::OnUpload(CAr& ar)
{
	if (m_bLoad)
	{
		return;
	}

	// recv
	int nSize;
	ar >> nSize;
	for (int i = 0; i < nSize; i++)
	{
		unsigned long dwGiftbox, dwItem;
		int cbItem, nNum;
		ar >> dwGiftbox;
		ar >> cbItem;
		for (int j = 0; j < cbItem; j++)
		{
			ar >> dwItem >> nNum;
			unsigned long dwKey = MAKELONG((unsigned short)dwGiftbox, (unsigned short)dwItem);
			bool bResult = m_map.insert(map<unsigned long, int>::value_type(dwKey, nNum)).second;
			if (!bResult)
			{
				Error("global giftbox error: %d, %d, %d", dwGiftbox, dwItem, nNum);
			}
		}
	}
	// load script
	CScanner s;
	unsigned long dwGiftbox, dwItem;
	int nNum;
	if (s.Load("../globalgiftbox.txt"))
	{
		dwGiftbox = (unsigned long)s.GetNumber();
		while (s.tok != FINISHED)
		{
			dwItem = (unsigned long)s.GetNumber();
			nNum = s.GetNumber();

			map<unsigned long, int>::iterator it = m_map.find(MAKELONG((unsigned short)dwGiftbox, (unsigned short)dwItem));
			if (it == m_map.end())
			{
				char szString[100];
				sprintf(szString, "globalgiftbox load error: key not found, g: %d, i: %d", dwGiftbox, dwItem);
				std::cerr << szString;
			}
			// it->second -= nNum;
			it->second = nNum;
			if (it->second < 0)
			{
				Error("global giftbox error: number is negative, %d, %d", dwGiftbox, dwItem);
				it->second = 0;
			}
			dwGiftbox = (unsigned long)s.GetNumber();
		}
	}

	m_bLoad = true;
}

void CGlobalGiftbox::OnQuery(CDPMng<CBuffer>* pdp, CAr& ar, unsigned long dpid)	// recv & check & save
{
	unsigned long idPlayer;
	unsigned long dwGiftbox, dwItem, dwObjId;
	int nNum, nQueryGiftbox;
	ar >> idPlayer >> dwGiftbox >> dwItem >> nNum >> dwObjId >> nQueryGiftbox;

	map<unsigned long, int>::iterator i = m_map.find(MAKELONG((unsigned short)dwGiftbox, (unsigned short)dwItem));
	if (i == m_map.end())
	{
		Error("CGlobalGiftbox::OnQuery: key not found, g: %d, i: %d", dwGiftbox, dwItem);
		return;
	}
	bool bResult = false;
	if (i->second > 0)
	{
		i->second--;
		bResult = true;
	}

	CAr arResult;
	arResult << PACKETTYPE_QUERYGLOBALGIFTBOX;
	arResult << idPlayer << dwGiftbox << dwItem << nNum << dwObjId << nQueryGiftbox << bResult;
	int nBufSize;
	LPBYTE lpBuf = arResult.GetBuffer(&nBufSize);
	pdp->Send(lpBuf, nBufSize, dpid);

	Save();
}

void CGlobalGiftbox::OnRestore(CAr& ar)
{
	unsigned long dwGiftbox, dwItem;
	ar >> dwGiftbox >> dwItem;
	map<unsigned long, int>::iterator i = m_map.find(MAKELONG((unsigned short)dwGiftbox, (unsigned short)dwItem));
	if (i == m_map.end())
	{
		Error("CGlobalGiftbox::OnRestore: key not found, g: %d, i: %d", dwGiftbox, dwItem);
		return;
	}
	i->second++;

	Save();
}

bool CGlobalGiftbox::Save(void)
{
	FILE* f = fopen("../globalgiftbox.txt", "w");
	if (f)
	{
		for (map<unsigned long, int>::iterator i = m_map.begin(); i != m_map.end(); ++i)
		{
			unsigned long dwGiftbox, dwItem;
			dwGiftbox = (unsigned long)LOWORD(i->first);
			dwItem = (unsigned long)HIWORD(i->first);
			int nNum = i->second;
			fprintf(f, "%d\t%d\t%d\n", dwGiftbox, dwItem, nNum);
		}
		fclose(f);
		return true;
	}
	return false;
}
