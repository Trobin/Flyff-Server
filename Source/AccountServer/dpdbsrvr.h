#ifndef __DPDBSRVR_H__
#define	__DPDBSRVR_H__

#include "dpmng.h"
#include "msghdr.h"

#undef	theClass
#define theClass	CDPDBSrvr
#undef theParameters
#define theParameters	CAr & ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize

class CAccount;

class CDPDBSrvr : public CDPMng<CBuffer>
{
public:
	//	Constructions
	CDPDBSrvr();
	virtual	~CDPDBSrvr();

	//	Overrides
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long dpId);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long dpId);

	// Operations
	void	SendOneHourNotify(CAccount* pAccount);
	void	SendPlayerCount(void);
	void	OnRemoveConnection(unsigned long dpid);
	void	SendCloseExistingConnection(const char* lpszAccount, long lError);
	void	SendFail(long lError, const char* lpszAccount, unsigned long dpid);
	void	SendBuyingInfo(PBUYING_INFO2 pbi2);
	void	SendLogSMItem();

#ifdef __LOG_PLAYERCOUNT_CHANNEL
	vector<CString>	m_vecstrChannelAccount;
#endif // __LOG_PLAYERCOUNT_CHANNEL
	/*
	#ifdef __S0114_RELOADPRO
		void	SendReloadAccount();
	#endif // __S0114_RELOADPRO
	*/
	//	Handlers
	USES_PFNENTRIES;
	void	OnAddConnection(CAr& ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRemoveAccount(CAr& ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnGetPlayerList(CAr& ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize);
#ifdef __REMOVE_PLAYER_0221
	void	OnRemovePlayer(CAr& ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize);
#endif	// __REMOVE_PLAYER_0221
	void	OnJoin(CAr& ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRemoveAllAccounts(CAr& ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnBuyingInfo(CAr& ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize);
	//	void	OnOpenBattleServer( CAr & ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize );
	//	void	OnCloseBattleServer( CAr & ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize );
	void	OnServerEnable(CAr& ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize);
	/*
	#ifdef __S0114_RELOADPRO
		void	OnCompleteReloadProject( CAr & ar, unsigned long dpid, LPBYTE lpBuf, unsigned long uBufSize );
	#endif // __S0114_RELOADPRO
	*/
};

#endif	// __DPDBSRVR_H__