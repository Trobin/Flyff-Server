#ifndef __BILLINGMGRTH_H__
#define	__BILLINGMGRTH_H__

#include "BillingMgr.h"


class CDPBillingTH;

class CBillingMgrTH : public CBillingMgr
{
public:
	CBillingMgrTH();
	virtual ~CBillingMgrTH();

	virtual bool		Init(HWND hWnd);
	virtual void		Release();
	virtual unsigned char		CheckAccount(int nType, unsigned long dwKey, const char* szAccount, const char* szAddr);
	virtual bool		PreTranslateMessage(HWND hWnd, unsigned int message, WPARAM wParam, LPARAM lParam);
	virtual bool		SetConfig(BILLING_ENUM id, unsigned long data);
	virtual void		OnTimer(CAccount* pAccount);
	virtual void		OnDBQuery(CQuery& query, tagDB_OVERLAPPED_PLUS* pOV) {}
	virtual void		NotifyLogout(const char* lpszAccount, unsigned long dwSession);

private:
	CDPBillingTH* m_pDPBillings;
	int					m_nMaxConnect;
	vector< string >	m_strIPs;
	vector< unsigned short >	m_nPorts;

	bool				Connect();
	bool				RequestBillingInfo(const char* lpszAccount, const char* lpAddr, unsigned long dwSession);
	void				SendKeepAlive();
	CDPBillingTH* GetBilling(unsigned long dwSession);

};

#endif	// __BILLINGMGR_H__