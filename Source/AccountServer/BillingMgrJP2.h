#ifndef __BILLINGMGRJP2_H__
#define	__BILLINGMGRJP2_H__

#include "BillingMgr.h"


class CBillingMgrJP2 : public CBillingMgr
{
public:
	CBillingMgrJP2();
	virtual ~CBillingMgrJP2();

	virtual bool		Init(HWND hWnd);
	virtual void		Release();
	virtual unsigned char		CheckAccount(int nType, unsigned long dwKey, const char* szAccount, const char* szAddr);
	virtual bool		PreTranslateMessage(HWND hWnd, unsigned int message, WPARAM wParam, LPARAM lParam);
	virtual bool		SetConfig(BILLING_ENUM id, unsigned long data);
	virtual void		OnTimer(CAccount* pAccount);
	virtual void		OnDBQuery(CQuery& query, tagDB_OVERLAPPED_PLUS* pOV);
	virtual void		NotifyLogout(const char* lpszAccount, unsigned long dwSession) {}

private:
	int					m_iBillingFreePass;
};

#endif	// __BILLINGMGR_H__