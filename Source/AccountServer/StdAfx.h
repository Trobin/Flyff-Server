#pragma once

#define WIN32_LEAN_AND_MEAN		
#define POINTER_64 __ptr64
#define _USE_32BIT_TIME_T
#define __ACCOUNT

#include <afxwin.h>

#include <stdlib.h>
//#include <malloc.h>
//#include <memory.h>
//#include <tchar.h>
#include <timeapi.h>
//#include <dplay.h>
//#include <d3dx9tex.h>

#include <assert.h>

#pragma warning(disable:4786)
#include <map>
#include <set>
#include <list>
#include <vector>
using namespace std;

#include "VersionCommon.h"

#include "memtrace.h"

#include "DefineCommon.h"
#include "cmnhdr.h"

#include "dxutil.h"
#include "vutil.h"
#include "data.h"
#include "file.h"
#include "d3dfont.h"
#include "scanner.h"
#include "timer.h"
#include "debug.h"
#include "xutil.h"
#include "buyinginfo.h"

const unsigned int	IDT_SENDPLAYERCOUNT = 0;
const unsigned int	IDT_RECONNECT = 1;
const unsigned int  IDT_KEEPALIVE = 2;
const unsigned int  IDT_KICKOUT = 3;
const unsigned int  IDT_PREVENT_EXCESS = 4;
const unsigned int	IDT_BUYING_INFO = 5;
const unsigned int  IDT_RELOAD_PROJECT = 6;
const unsigned int  IDT_TIME_CHECKADDR = 7;


const unsigned int		WM_CONNENT_BILLING = WM_USER + 100;

extern char		DSN_NAME_LOG[];
extern char		DB_ADMIN_ID_LOG[];

extern char		DSN_NAME_LOGIN[];
extern char		DB_ADMIN_ID_LOGIN[];

extern char		DSN_NAME_BILLING[];
extern char		DB_ADMIN_ID_BILLING[];

