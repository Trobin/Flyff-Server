#ifndef __DPBillingTH_H__
#define	__DPBillingTH_H__

#include "dpmng.h"
#include "msghdr.h"
#include "misc.h"
#include <map>

using namespace std;

#undef	theClass
#define theClass	CDPBillingTH
#undef theParameters
#define theParameters	CAr & ar, unsigned long


class CDPBillingTH : public CDPMng<CBuffer2>
{
public:
	//	Constructions
	CDPBillingTH();
	virtual	~CDPBillingTH();

private:
	HWND	m_hWnd;
	int		m_nKeepAlive;

public:
	bool	m_bConnected;

public:
	//	Overrides
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long idFrom);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long idFrom);

	// Operations
	void	Init(HWND hWnd) { m_hWnd = hWnd; }
	void	ReqBillingInfo(const char* lpszUserID, const char* lpszUserIP, unsigned long dwSession);
	void	SendKeepAlive();
	void	NotifyLogout(const char* lpszAccount, unsigned long dwSession);

	//	Handlers
	USES_PFNENTRIES;
	void	AckKeepAlive(CAr& ar, unsigned long dpid);
	void	AckBillingInfo(CAr& ar, unsigned long dpid);
	void	ReqIsAlive(CAr& ar, unsigned long dpid);
};

#endif	// __DPBillingTH_H__