#ifndef __DPWLDSRVR_H__
#define	__DPWLDSRVR_H__

#include "dpmng.h"
#include "msghdr.h"

#undef	theClass
#define theClass	CDPWldSrvr
#undef theParameters
#define theParameters	CAr & ar, unsigned long dpid

class CDPWldSrvr : public CDPMng<CBuffer>
{
public:
	//	Constructions
	CDPWldSrvr();
	virtual	~CDPWldSrvr();

	//	Overrides
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long dpId);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, unsigned long dpId);

	static	CDPWldSrvr* GetInstance(void);
	//	Handlers
	USES_PFNENTRIES;
	void	OnUpload(CAr& ar, unsigned long dpid);
	void	OnQuery(CAr& ar, unsigned long dpid);
	void	OnRestore(CAr& ar, unsigned long dpid);
};

#endif	// __DPWLDSRVR_H__