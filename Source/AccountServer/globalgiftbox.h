#ifndef __GLOBAL_GIFT_BOX_H__
#define	__GLOBAL_GIFT_BOX_H__

#include "ar.h"
#include "buffer.h"

template <class>
class CDPMng;
class CGlobalGiftbox
{
private:
	map<unsigned long, int>	m_map;
	/* first: MAKELOG( dwGiftBox, dwItem )
		second: nNum;
	*/
	bool	m_bLoad;
public:
	CGlobalGiftbox();
	virtual	~CGlobalGiftbox();

	static	CGlobalGiftbox* GetInstance(void);

	void	OnUpload(CAr& ar);	// recv & load script
	void	OnQuery(CDPMng<CBuffer>* pdp, CAr& ar, unsigned long dpid);	// recv & check & save
	void	OnRestore(CAr& ar);
private:
	bool	Save(void);
};

#endif	// __GLOBAL_GIFT_BOX_H__