#ifndef __BILLINGMGRJP_H__
#define	__BILLINGMGRJP_H__

#include "BillingMgr.h"


class CDPBilling;

class CBillingMgrJP : public CBillingMgr
{
public:
	CBillingMgrJP();
	virtual ~CBillingMgrJP();

	virtual bool			Init(HWND hWnd);
	virtual void			Release();
	virtual unsigned char	CheckAccount(int nType, unsigned long dwKey, const char* szAccount, const char* szAddr);
	virtual bool			PreTranslateMessage(HWND hWnd, unsigned int message, WPARAM wParam, LPARAM lParam);
	virtual bool			SetConfig(BILLING_ENUM id, unsigned long data);
	virtual void			OnTimer(CAccount* pAccount);
	virtual void			OnDBQuery(CQuery& query, tagDB_OVERLAPPED_PLUS* pOV) {}
	virtual void			NotifyLogout(const char* lpszAccount, unsigned long dwSession) {}

private:
	int					m_iBillingFreePass;
	CDPBilling* m_pDPBillings;
	int					m_nMaxConnect;
	vector< string >	m_strIPs;
	vector< unsigned short >	m_nPorts;

	bool				Connect();
	bool				RequestBillingInfo(const char* lpszAccount, const char* lpAddr);
	void				SendKeepAlive();
};

#endif	// __BILLINGMGR_H__