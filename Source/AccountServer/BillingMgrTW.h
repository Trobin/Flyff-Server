#ifndef __BILLINGMGRTW_H__
#define	__BILLINGMGRTW_H__

#include "BillingMgr.h"


class CBillingMgrTW : public CBillingMgr
{
public:
	CBillingMgrTW();
	virtual ~CBillingMgrTW();

	virtual bool		Init(HWND hWnd);
	virtual void		Release();
	virtual unsigned char		CheckAccount(int nType, unsigned long dwKey, const char* szAccount, const char* szAddr);
	virtual bool		PreTranslateMessage(HWND hWnd, unsigned int message, WPARAM wParam, LPARAM lParam);
	virtual bool		SetConfig(BILLING_ENUM id, unsigned long data);
	virtual void		OnTimer(CAccount* pAccount);
	virtual void		OnDBQuery(CQuery& query, tagDB_OVERLAPPED_PLUS* pOV);
	virtual void		NotifyLogout(const char* lpszAccount, unsigned long dwSession) {}

private:
	int					m_iBillingFreePass;
};

#endif	// __BILLINGMGRTW_H__