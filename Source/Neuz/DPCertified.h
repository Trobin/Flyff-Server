#ifndef __DPCERTIFIED_H__
#define	__DPCERTIFIED_H__

#include "dpmng.h"
#include "msghdr.h"
#include "misc.h"

#undef	theClass
#define theClass	CDPCertified
#undef theParameters
#define theParameters	CAr & ar, DPID

class CDPCertified : public CDPMng
{
private:
	bool	m_fConn;
	CTimer	m_timer;
	long	m_lError;					// protocol error code 

public:
	unsigned long		m_dwSizeofServerset;
	SERVER_DESC	m_aServerset[128];

public:
	//	Constructions
	CDPCertified();
	virtual	~CDPCertified();

	//	Overrides
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID dpId);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID dpId);

	//	Operations
	long	GetNetError();
	long	GetErrorCode() { return m_lError; }
	//	void	Destroy( bool bDestroy );
	bool	IsDestroyed(void);
	bool	IsConnected(void);
	void	Ping(void);
	void	SendNewAccount(LPCSTR lpszAccount, LPCSTR lpszpw);
	const char* GetServerName(int nServerIndex);
	void	SendHdr(unsigned long dwHdr);
	void	SendCloseExistingConnection(const char* lpszAccount, const char* lpszpw);
	bool	CheckNofityDisconnected();
	void	SendCertify();

	USES_PFNENTRIES;

	// Handlers
	void	OnSrvrList(CAr& ar, DPID);
	void	OnError(CAr& ar, DPID dpid);
#ifdef __GPAUTH
	void	OnErrorString(CAr& ar, DPID dpid);
#endif	// __GPAUTH
	void	OnKeepAlive(CAr& ar, DPID);
};

inline void CDPCertified::SendHdr(unsigned long dwHdr)
{
	BEFORESEND(ar, dwHdr);
	SEND(ar, this, DPID_SERVERPLAYER);
}
inline void CDPCertified::Ping(void)
{
	if (m_timer.IsTimeOut())
	{
		m_timer.Reset();
		SendHdr(PACKETTYPE_PING);
	}
}

inline bool CDPCertified::IsConnected(void)
{
	return m_fConn;
}

#endif	// __DPCERTIFIED_H__