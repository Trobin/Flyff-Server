#ifndef __USER_H__
#define __USER_H__

#pragma once

#include <DPlay.h>
#include "MsgHdr.h"
#include "Snapshot.h"
#include "UserTaskBar.h"
#include "Party.h"
#ifdef __RT_1025
#include "rtmessenger.h"
#else	// __RT_1025
#include "messenger.h"
#endif	// __RT_1025
#include "misc.h"
#include "guild.h"
#include "eveschool.h"
#include "Chatting.h"
#include "CooltimeMgr.h"

#if __VER >= 11 // __SYS_PLAYER_DATA
#include "playerdata.h"
#endif	// __SYS_PLAYER_DATA

#if __VER >= 12 // __SECRET_ROOM
#include "SecretRoom.h"
#endif // __SECRET_ROOM
#if __VER >= 13 // __RAINBOW_RACE
#include "RainbowRace.h"
#endif // __RAINBOW_RACE
#if __VER >= 13 // __HOUSING
#include "Housing.h"
#endif // __HOUSING
#if __VER >= 14 // __PCBANG
#include "PCBang.h"
class CPCBangInfo;
#endif // __PCBANG

#if __VER >= 15 // __GUILD_HOUSE
#include "GuildHouse.h"
#endif // __GUILD_HOUSE

#if __VER >= 15 // __CAMPUS
#include "CampusHelper.h"
#endif // __CAMPUS

#define GETID( pCtrl )		( (pCtrl) ? (pCtrl)->GetId() : NULL_ID )
typedef	MemPooler<CUser>	CUserPool;

#ifdef __AZRIA_1023
#define	MAX_INPUT_LEN	100
#endif	// __AZRIA_1023

class	CShip;
class CMail;
class CMailBox;

//////////////////////////////////////////////////////////////////////////
#define CHECK_TICK_FROM_CLIENT	SEC(3)
//////////////////////////////////////////////////////////////////////////

#if __VER >= 12 // __LORD
class CLordSkill;
#endif	// __LORD
/// 플레이어를 나타내는 클래스 
class CUser : public CMover
{
public:
	CUser();
	CUser(DPID dpidCache, DPID dpidUser);
	virtual	~CUser();

	//////////////////////////////////////////////////////////////////////////

private:
	bool			m_bCheckTransMailBox;
	int				m_nCountFromClient;
	unsigned long			m_dwTickFromClient;

	//////////////////////////////////////////////////////////////////////////

public:
	unsigned long			m_dwSerial;							/// 캐쉬서버가 발급한 유저의 순차적 번호
	bool			m_bValid;							/// db server에서 join packet받고, 맵에 Add된후 true
	CSnapshot		m_Snapshot;							/// SNAPSHOTTYPE_류 송신 누적 버퍼( Notify()에서 보내진다. )
	map<unsigned long, CCtrl*>	m_2npc;							/// 주변 NPC를 담고 있는 container
	unsigned long			m_dwTickSFS;						/// SubSMMode, Finite, SavePlyer()용 타이머 
	unsigned long			m_dwTickNotify;						/// Notify()용 타이머 
	unsigned long			m_dwTimeout4Save;					/// db 저장용 타이머 
	unsigned long			m_dwLeavePenatyTime;				/// 종료요청 시각 	
	unsigned long			m_dwDestroyTime;					/// Destory예약 시각 

	unsigned long			m_dwAuthKey;						/// 인증용 key
	PLAY_ACCOUNT	m_playAccount;						/// Account정보 
	CUserTaskBar	m_playTaskBar;						/// player의 taskbar class(큇슬롯)
	OBJID			m_idSetTarget;						/// 이 유저가 잡고있는 타겟
#ifdef __RT_1025
	CRTMessenger	m_RTMessenger;
#else	// __RT_1025
	CMessenger		m_Messenger;						/// 메신져 
#endif	// __RT_1025
	char			m_szPartyName[32];					/// 파티 이름 
	char			m_szBankPass[5];					/// 은행 비빌번호 

	int				m_nOverHeal;						/// 오버힐 한 어시에게는 이 카운트가 붙는다.	
	CObj* m_pWall[2];						/// 사이킥 월을 생성하면 여기에 저장
	bool			m_bPosting;							/// 메일 사용중?

	unsigned long			m_idChatting;						/// 채팅방에 들어갔을 때의 채팅방 id
	unsigned long			m_idSnoop;							/// 나를 감청하는자 id 
	bool			m_bInstantBank;						/// 즉시 사용 뱅크 사용중? 
	CCooltimeMgr	m_cooltimeMgr;						/// 아이템 쿨타임 관리자 
	unsigned long			m_tmEscape;							/// 탈출 인터페이스 시작시각
	D3DXVECTOR3		m_vReturnPos;						/// '귀환 두루마리' - 목표지점  
	unsigned long			m_dwReturnWorldID;					/// '귀환 두루마리' - 월드 id
	const char*			m_lpszVillage;						/// '귀환 두루마리' - 마을 이름 

	unsigned long			m_dwPlayerTime;						/// DB저장용 총 플레이 시간 
	unsigned long			m_dwSavePlayerTime;					/// 마지막 저장 플레이 시간의 총합 

#ifndef __JEFF_9_20
	unsigned long			m_dwFlyTime;						/// DB저장용 총 비행 시간 
	unsigned long			m_dwSaveFlyTime;					/// 비행시간 계산용 
	unsigned long			m_dwSendFlyTime;					/// 비행시간 계산용 
#endif	// __JEFF_9_20

//	OBJID			m_idTargetCollect;					/// 자원몬스터 채집대상 - 채집중에 타겟을 바꿀 수도 있기때문
//	int				m_nCollect;							/// 현재 자원 채집량
//	unsigned long			m_tmCollect;						/// 자원채집 타이머 - 1초에 한번씩

#ifdef __EVENT_1101
#ifdef __EVENT_1101_2
	__int64	m_nEventFlag;
#else // __EVENT_1101_2
	unsigned long	m_dwEventFlag;	//	경과 일 번째 비트
#endif // __EVENT_1101_2
	unsigned long	m_dwEventTime;	// 분 단위 계수
	unsigned long	m_dwEventElapsed;	// 경과 일
#ifdef __EVENT_1101_2
	__int64	GetEventFlagBit(int nBit);
#else // __EVENT_1101_2
	int		GetEventFlagBit(int nBit);
#endif // __EVENT_1101_2
	int		SetEventFlagBit(int nBit);
	void	AddCallTheRoll(void);
#endif	// __EVENT_1101

#ifdef __HACK_1130
	unsigned long	m_tmDuelRequest;
#endif	// __HACK_1130

#ifdef __QUEST_1208
	unsigned long	m_tickScript;
#endif	// __QUEST_1208

#ifdef __S_SERVER_UNIFY
	bool	m_bAllAction;
#endif // __S_SERVER_UNIFY

#ifdef __EVE_MINIGAME
	int		m_nKawibawiboWin;
	int		m_nKawibawiboState;
	int		m_nBetFiveSystem[6];
#endif // __EVE_MINIGAME

#ifdef __EXP_ANGELEXP_LOG
	int		m_nExpLog;
	int		m_nAngelExpLog;
#endif // __EXP_ANGELEXP_LOG

#ifdef __EVENTLUA_COUPON
	int		m_nCoupon;
	unsigned long	m_dwTickCoupon;
#endif // __EVENTLUA_COUPON

#ifdef __EVENTLUA_KEEPCONNECT
	unsigned long	m_dwTickKeepConnect;
#endif // __EVENTLUA_KEEPCONNECT

#ifdef __PERIN_BUY_BUG
	unsigned long	m_dwLastTryBuyItem;
	unsigned long	m_dwLastBuyItemTick;
#endif // __PERIN_BUY_BUG

public:
	virtual void	Process();
	void			DoSMItemEquip(CItemElem* pItemElem, unsigned long dwParts);
	void			DoSMItemUnEquip(CItemElem* pItemElem, unsigned long dwParts);
	void			CheckFiniteItem();
	void			LevelUpSetting(void);
	void			ExpUpSetting(void);
	void			SetValid(bool bValid) { m_bValid = bValid; }
	bool			IsValid();
	void			SetReturnPos(const D3DXVECTOR3& vPos);
	D3DXVECTOR3& GetReturnPos(unsigned long* pdwWorldID);
	void			SetPlayerTime(unsigned long dwSaveTime) { m_dwPlayerTime = timeGetTime(); m_dwSavePlayerTime = dwSaveTime; };
	unsigned long			GetPlayerTime() { return (((timeGetTime() - m_dwPlayerTime) / 1000) + m_dwSavePlayerTime); };
#ifndef __JEFF_9_20
	void			SetSaveFlyTime(unsigned long dwFlyTime) { m_dwFlyTime = m_dwSendFlyTime = 0; m_dwSaveFlyTime = dwFlyTime; };
	void			SetFlyTimeOn() { m_dwFlyTime = timeGetTime(); };
	void			SetFlyTimeOff() { m_dwSendFlyTime += (timeGetTime() - m_dwFlyTime) / 1000; m_dwFlyTime = 0; };
	unsigned long			GetFlyTime() { return m_dwSendFlyTime; };
#endif	// __JEFF_9_20
	void			NPCSetAt(OBJID objid, CCtrl* pCtrl) { m_2npc[objid] = pCtrl; }
	bool			NPCRemoveKey(OBJID objid) { return m_2npc.erase(objid) > 0; }
	void			RemoveItFromView2(bool bRemoveall = false);
#ifdef __MAP_SECURITY
	void			Open(unsigned long dwWorldId);
#else // __MAP_SECURITY
	void			Open();
#endif // __MAP_SECURITY
	//	void			Notify();
	int		Notify(void);
	//	void			Flush()	{	Notify();	m_Snapshot.Flush();		}
#ifdef __S_SERVER_UNIFY
	void			SetAllAction();
#endif // __S_SERVER_UNIFY

	void			AddPostMail(CMail* pMail);
	void			AddRemoveMail(unsigned long nMail, int nType);
	void			AddMailBox(CMailBox* pMailBox);




	//////////////////////////////////////////////////////////////////////////
	void			SendCheckMailBoxReq(bool bCheckTransMailBox);
	void			CheckTransMailBox(bool bCheckTransMailBox);
	bool			GetCheckTransMailBox();
	bool			CheckClientReq();
	void			ResetCheckClientReq();
	int				GetCountClientReq();
	//////////////////////////////////////////////////////////////////////////




	void			SetPosting(bool bPosting) { m_bPosting = bPosting; }
	bool			IsPosting(void) { return m_bPosting; }
	bool			IsUsableState(unsigned long dwId);
	void			OnDoUseItem(unsigned long dwData, OBJID objid, int nPart);
	void			DoUsePackItem(CItemElem* pItemElem, PPACKITEMELEM pPackItemElem);
	bool			DoUseGiftbox(CItemElem* pItemElem, unsigned long dwItemId);
	void			DoUseEveFBOX(CItemElem* pItemElem);
	void			OnMsgArrival(unsigned long dwParam);
	void			ADDGameJoin();
	void			AddHdr(OBJID objid, unsigned short wHdr);
	void			AddBlock(LPBYTE lpBlock, unsigned long uBlockSize);
	void			AddAddObj(CCtrl* pCtrl);
	void			AddRemoveObj(OBJID objid) { AddHdr(objid, SNAPSHOTTYPE_DEL_OBJ); }
	void			AddDiagText(LPCSTR lpszText);
	void			AddText(LPCSTR lpszText);
	void			AddReplace(unsigned long dwWorldID, D3DXVECTOR3& vPos);
	void			AddCreateItem(CItemBase* pItemBase, BYTE* pnId, short* pnNum, BYTE nCount);
	void			AddMoveItem(BYTE nItemType, BYTE nSrcIndex, BYTE nDestIndex);
	void			AddTrade(CUser* pTrader, unsigned long uidPlayer);
	void			AddComfirmTrade(OBJID objid);
	void			AddComfirmTradeCancel(OBJID objid);
	void			AddTradePut(OBJID objid, BYTE i, BYTE nItemType, BYTE nId, short nItemNum = 1);
	void			AddTradePutError();
	void			AddTradePull(OBJID objid, BYTE i);
	void			AddTradePutGold(OBJID objid, unsigned long dwGold);
	//raiders.2006.11.28
	//	void			AddTradeClearGold( OBJID objid );
	void			AddTradeCancel(OBJID objid, unsigned long uidPlayer, int nMode = 0);
	void			AddTradeOk(OBJID objid) { AddHdr(objid, SNAPSHOTTYPE_TRADEOK); }
	void			AddTradeConsent() { AddHdr(NULL_ID, SNAPSHOTTYPE_TRADECONSENT); }
	void			AddTradelastConfirm() { AddHdr(NULL_ID, SNAPSHOTTYPE_TRADELASTCONFIRM); }
	void			AddTradelastConfirmOk(OBJID objid) { AddHdr(objid, SNAPSHOTTYPE_TRADELASTCONFIRMOK); }
	void			AddOpenShopWnd(CMover* pVendor);
#if __VER >= 15 // __PETVIS
	void			AddUpdateItem(char cType, BYTE nId, char cParam, unsigned long dwValue, unsigned long dwTime);
#else // __PETVIS
	void			AddUpdateItem(char cType, BYTE nId, char cParam, unsigned long dwValue);
#endif // __PETVIS
#if __VER >= 11 // __SYS_IDENTIFY
	void			AddUpdateItemEx(unsigned char id, char cParam, __int64 iValue);
#endif	// __SYS_IDENTIFY
#ifdef __JEFF_11
	void			AddQuePetResurrectionResult(bool bResult);
#endif	// __JEFF_11
#if __VER >= 11 // __SYS_POCKET
	void			AddPocketAttribute(int nAttribute, int nPocket, int nData);
	void			AddPocketView(void);
	void			AddPocketAddItem(int nPocket, CItemElem* pItem);
	void			AddPocketRemoveItem(int nPocket, int nItem, short nNum);
#endif	// __SYS_POCKET
	void			AddSetFxp(int nFxp, int nFlightLv);
	void			AddSetGrowthLearningPoint(long nRemainGP);
	void			AddSetStatLevel(char chID, long nValue, long nRemainGP);
	void			AddDefinedText(int dwText, LPCSTR lpszFormat, ...);
	void			AddChatText(int dwText, LPCSTR lpszFormat, ...);
	void			AddDefinedText(int dwText);
	void			AddDefinedCaption(bool bSmall, int dwText, LPCSTR lpszFormat, ...);
	void			AddGoldText(int nPlus);
	void			AddExpBoxInfo(OBJID objid, unsigned long dwSet, unsigned long dwTime, unsigned long idPlayer);
	void			AddSetQuest(LPQUEST lpQuest);
	void			AddSetGuildQuest(int nQuestId, int nState);
	void			AddRemoveGuildQuest(int nQuestId);
	void			AddSetChangeJob(int nJob);
	void			AddReturnSay(int ReturnFlag, const char* lpszPlayer);
	void			AddGameTimer(double dCurrentTime);
	void			AddDoEquip(BYTE nId, unsigned long dwItemId, BYTE fEquip);
	void			AddPartyChangeLeader(unsigned long uidChangeLeader);
	void			AddCancelQuest(unsigned long dwQuestCancelID);
	void			AddRemoveQuest(unsigned long dwQuestCancelID);
	void			AddRemoveAllQuest();
	void			AddRemoveCompleteQuest();
	void			AddGetItemBank(CItemElem* pItemElem);
	void			AddMoveBankItem(BYTE nSrcIndex, BYTE nDestIndex);
	void			AddBankIsFull();
	void			AddRemoveGuildBankItem(unsigned long idGuild, unsigned long dwId, unsigned long dwItemNum);
	void			AddChangeBankPass(int nMode, unsigned long dwId, unsigned long dwItemId);
	void			AddBankWindow(int nMode, unsigned long dwId, unsigned long dwItemId);
	void			AddconfirmBankPass(int nMode, unsigned long dwId, unsigned long dwItemId);
	void			AddTaskBar();
	void			AddSendErrorParty(unsigned long dw, unsigned long dwSkill = 0);
#if __VER >= 11 // __SYS_PLAYER_DATA
	void			AddPartyMember(CParty* pParty, unsigned long idPlayer, const char* pszLeader, const char* pszMember);
#else	// __SYS_PLAYER_DATA
	void			AddPartyMember(CParty* pParty, unsigned long uPlayerId = 0);
#endif	// __SYS_PLAYER_DATA
	void			AddPartyRequest(CUser* pLeader, CUser* pMember, bool bTroup);
	void			AddPartyRequestCancel(unsigned long uLeaderid, unsigned long uMemberid, int nMode = 0);
	void			AddPartyExpLevel(int Exp, int Level, int nPoint);
#if __VER < 11 // __SYS_PLAYER_DATA
	void			AddPartyMemberJob(int nMemberIndex, int nJob);
	void			AddPartyMemberLevel(int nMemberIndex, int nLevel);
#endif	// __SYS_PLAYER_DATA
	void			AddPartyMemberFlightLevel(int nMemberIndex, int nLevel);
	void			AddPartyChangeTroup(const char* szPartyName);
	void			AddPartyChangeName(const char* szPartyName);
#if __VER >= 12 // __JHMA_VER12_1	//12차 극단유료아이템
	void			AddSetPartyMode(int nMode, bool bOnOff, long nPoint, unsigned long dwSkillTime);
#else // //12차 극단유료아이템
	void			AddSetPartyMode(int nMode, bool bOnOff, long nPoint);
#endif // //12차 극단유료아이템
	void			AddPartyChangeItemMode(int nItemMode);
	void			AddPartyChangeExpMode(int nExpMode);
	void			AddSetPartyMemberParam(unsigned long idPlayer, BYTE nParam, int nVal);
	void			AddPartyName();
	void			AddPartySkillCall(const D3DXVECTOR3& vLeader);
	void			AddPartySkillBlitz(OBJID idTarget);
#if __VER < 11 // __SYS_PLAYER_DATA
	void			AddFriendChangeJob(unsigned long uidPlayer, int nJob);
#endif	// __SYS_PLAYER_DATA
	void			AddFriendGameJoin();
	void			AddFriendReqest(unsigned long uLeader, long nJob, BYTE nSex, const char* szName);
	void			AddFriendCancel();
	void			AddFriendError(BYTE nError, const char* szName);
#if __VER < 11 // __SYS_PLAYER_DATA
	void			AddGetFriendName(int nCount, FRIEND* aFriend);
#endif	// __SYS_PLAYER_DATA

#ifdef __ENVIRONMENT_EFFECT
	void			AddEnvironmentSetting();
#endif // __ENVIRONMENT_EFFECT
	void			AddEnvironment();
	void			AddPartyChat(const char* lpName, const char* lpString, OBJID objid);
#ifdef __RT_1025
	void			AddAddFriend(unsigned long idPlayer, const char* lpszPlayer);
#else	// __RT_1025
	void			AddAddFriend(unsigned long idPlayer, long nJob, unsigned long dwSex, const char* lpszPlayer);
#endif	// __RT_1025
	void			AddRemoveFriend(unsigned long uidSender);
	void			AddDuelCount(int nCount);
	void			AddDuelRequest(unsigned long uidSrc, unsigned long uidDst);
	void			AddDuelStart(unsigned long uidTarget, int bStart = 0);
	void			AddDuelNo(OBJID idTarget);
	void			AddDuelCancel(OBJID idTarget);	// 듀얼중 취소시킴
	void			AddDuelPartyRequest(unsigned long uidSrc, unsigned long uidDst);
	void			AddDuelPartyStart(const char* szPartyName, int nMax, OBJID* pMembers, unsigned long idParty, int bFlag = 0);
	void			AddDuelPartyNo(OBJID idTarget);
	void			AddDuelPartyCancel(CParty* pDuelOther);	// 듀얼중 취소시킴
	void			AddDuelPartyResult(CParty* pDuelOther, bool bWin);		// 듀얼결과 통보.
#if __VER >= 11 // __SYS_PLAYER_DATA
	void			AddQueryPlayerData(unsigned long idPlayer, PlayerData* pPlayerData);
	void			AddPlayerData(void);
#else	// __SYS_PLAYER_DATA
	void			AddQueryPlayerString(unsigned long idPlayer, LPCSTR lpszPlayer, BYTE nQuery);
	void			AddQueryPlayerListString(BYTE nQuery);
#endif	// __SYS_PLAYER_DATA
	void			AddSetState(long nStr, long nSta, long nDex, long nInt, long nGP);
	void			AddSetSkill(unsigned long dwSkill, unsigned long dwSkillLevel);
	void			AddQueryGetPos(OBJID objid);
	void			AddGetPos(OBJID objid, const D3DXVECTOR3& vPos, float fAngle);
	void			AddResurrectionMessage();
	void			AddCorrReq(CMover* pMover);
	void			AddSetFuel(int nFuel, unsigned long tmAccFuel);
	void			AddTag(short nTagCount, const TAG_ENTRY* tags);
	void			AddSetSkillState(CMover* pMover, unsigned long dwSkill, unsigned long dwLevel, unsigned long dwTime);
	void			AddPlaySound(unsigned long idSound);
	void			AddMoverFocus(CMover* pMover);
	void			AddPlayMusic(unsigned long idMusic);
	void			AddRegisterPVendorItem(BYTE iIndex, BYTE nType, BYTE nId, short nNum, int nCost);
	void			AddUnregisterPVendorItem(BYTE iIndex);
	void			AddPVendorItem(CUser* pUser, bool bState);	// locked
	void			AddPVendorClose(OBJID objidVendor);
	void			AddMotionError();
	void			AddPutItemBank(BYTE nSlot, CItemElem* pItemElem);
	void			AddPutGoldBank(BYTE nSlot, unsigned long dwGold, unsigned long dwGoldBank);
	void			AddUpdateBankItem(BYTE nSlot, BYTE nId, char cParam, unsigned long dwValue);
	void			AddGuildBankWindow(int nMode);
	void			AddPutItemGuildBank(CItemElem* pItemElem);
	void			AddGetItemGuildBank(CItemElem* pItemElem);
	void			AddGetGoldGuildBank(unsigned long p_Gold, BYTE p_Mode, unsigned long playerID, BYTE cbCloak);
	void			AddContribution(CONTRIBUTION_CHANGED_INFO& info);
	void			AddSetNotice(unsigned long idGuild, const char* szNotice);
	void			AddInsertedVote(VOTE_INSERTED_INFO& info);
	void			AddModifyVote(unsigned long idVote, BYTE cbOperation, BYTE cbExtra);
	void			AddSetGuildAuthority(unsigned long dwAuthority[]);
	void			AddSetGuildPenya(unsigned long dwType, unsigned long dwPenya);
	void			AddGuildRealPenya(int nGoldGuild, int nType);
#if __VER < 11 // __SYS_PLAYER_DATA
	void			AddChangeGuildJobLevel(unsigned long uidPlayer, long nJob, long nLevel);
#endif	// __SYS_PLAYER_DATA
	void			AddAllGuilds();
	void			AddMyGuild();
	void			AddGuildInvite(unsigned long idGuild, unsigned long idMaster);
	void			AddMyGuildWar();
	void			SendGuildRank();
	void			AddSMMode(int nType, unsigned long dwTime);
	void			AddSMModeAll();
	void			AddResistSMMode(BYTE nAttackResistLeft, BYTE nAttackResistRight, BYTE nDefenceResist);
	void			AddCommercialElem(unsigned long dwItemId, int nResistSMItemId);
	void			AddFlyffEvent();
#if __VER >= 9 // __EVENTLUA
	void			AddEventLuaDesc(int nState, string strDesc);
#endif // __EVENTLUA
	void			AddQueryGetDestObj(OBJID objid);
	void			AddGetDestObj(OBJID objid, OBJID objidDest, float fRange);
	//	bool			DoCollect( CMover *pTarget );		// 채집시작.
	//	int				OnActCollecting();					// 채집중 계속 호출.
	void			AddCommonSkill(unsigned long dwSkill, unsigned long dwLevel);
	void			AddExpBoxCoolTime(OBJID objid, unsigned long dwTime);
	void			AddPlaySound2(const char* szSound);
	void			AddRunScriptFunc(const RunScriptFunc& runScriptFunc);
	void			AddRemoveSkillInfluence(unsigned short wType, unsigned short wID);
	void			AddGCWindow(__int64 dwPrizePenya, unsigned long dwRequstPenya, unsigned long dwMinRequestPenya);
	void			AddGCRequestStatus(__int64 nPrizePenya, vector<CGuildCombat::__REQUESTGUILD> vecRequestStatus);
	void			AddGCSelectPlayerWindow(vector<CGuildCombat::__JOINPLAYER>& vecSelectPlayer, unsigned long uidDefender, bool bWindow, bool bRequestWar);
	void			AddGCJoinWarWindow(int nMap, int nTelTime);
	void			AddGuildCombatNextTime(unsigned long dwTime, unsigned long dwState);
	void			AddGuildCombatEnterTime(unsigned long dwTime);
	void			AddGCWinGuild(void);
	void			AddGCBestPlayer(void);
	void			AddGCIsRequest(bool bRequest);
	void			AddGCGetPenyaGuild(int nGetResult, __int64 nGetPenya);
	void			AddGCGetPenyaPlayer(int nGetResult, __int64 nGetPenya);
	void			AddGCDiagMessage(const char* str);
	void			AddGCTele(const char* str);
	void			AddGCWarPlayerlist(void);
	void			AddCtrlCoolTimeCancel(void);
	void			AddGuildCombatState(void);
	void			AddQuestTextTime(bool bFlag, int nState, unsigned long dwTime);
	void			AddGCLog(void);
	void			AddGCPlayerPoint(void);
	void			AddFocusObj(OBJID objid);
	void			AddEnterChatting(CUser* pUser);
	void			AddNewChatting(CChatting* pChatting);
	void			AddRemoveChatting(unsigned long uidPlayer);
	void			AddDeleteChatting(void);
	void			AddChatting(unsigned long uidPlayer, char* pszChat);
	void			AddChttingRoomState(bool bState);
	void			AddGameRate(float fRate, BYTE nFlag);
	void			AddGameSetting();
	void			AddInitSkill();
	void			AddSetNaviPoint(const NaviPoint& nv, OBJID objid, const char* Name);
	void			AddEscape(unsigned long tmMaxEscape);
	void			AddSetActionPoint(int nAP);
	void			AddSetTarget(OBJID idTarget);
	void			AddSetDuel(CMover* pMover);
#if __VER >= 8 // __S8_PK
	void			AddPKValue();
#else // __VER >= 8 // __S8_PK
	void			UpdatePlayerEnemy(BYTE byType, OBJID objid);
#endif // __VER >= 8 // __S8_PK
	void			ScheduleDestory(unsigned long dwDestoryTime);
	void			AddWantedInfo(const D3DXVECTOR3& vPos, BYTE byOnline, unsigned long dwWorldID, const char* lpszWorld);
	void			AddReturnScroll();
	void			AddSnoop(const char* lpString);
	void			AddSetCheerParam(int nCheerPoint, unsigned long dwRest, bool bAdd = false);
	void			AddQueryEquip(CUser* pUser);
	void			AddSummonFriendUse(CItemElem* pItemElem);
	void			AddSummonFriendConfirm(OBJID objid, unsigned long dwData, const char* szName, const char* szWorldName);
	void			AddSummonPartyConfirm(OBJID objid, unsigned long dwData, const char* szWorldName);

	void			AddDoUseSkillPoint(SKILL aJobSkill[], int nSkillPoint);
	void			AddPartyMapInfo(int nIndex, D3DXVECTOR3 vPos);	// __S1005_PARTY_MAP
	void			AddSetExperience(EXPINTEGER nExp1, unsigned short wLevel, int nSkillPoint, int nSkillLevel, EXPINTEGER nDeathExp = (EXPINTEGER)-1, unsigned short wDeathLevel = 0xffff);
#ifdef __S_SERVER_UNIFY
	void			AddAllAction(bool bCharacter);
#endif // __S_SERVER_UNIFY

#ifdef __S1108_BACK_END_SYSTEM
	void			AddMonsterProp();
#endif // __S1108_BACK_END_SYSTEM
#if __VER >= 8 //__CSC_VER8_5
	//	void			AddCreateAngel(bool isSuccess, char* createAngel);
	void			AddAngelInfo(bool bComplete = false);
#endif //__CSC_VER8_5

#ifdef __EVE_MINIGAME
	void			AddKawibawiboResult(int nResult, int nWinCount, unsigned long dwItemId = 0, int nItemCount = 0, unsigned long dwNextItemId = 0, int nNextItemCount = 0);
	void			AddReassembleResult(unsigned long dwItemId, int nItemCount);
	void			AddReassembleOpenWnd(vector<unsigned long> vecItemId);
	void			AddAlphabetOpenWnd(int nQuestionID, int nPublicAlphabet, char chPublicChar, CString strQuestion);
	void			AddAlphabetResult(int nResult, unsigned long dwItemId = 0, int nItemCount = 0);
	void			AddFiveSystemOpenWnd(int nBetMinPenya, int nBetMaxPenya, int nMultiple);
	void			AddFiveSystemResult(int nResult, int nGetPenya = 0, int nBetNum = 0);
#endif // __EVE_MINIGAME

#if __VER >= 9 // __ULTIMATE
	void			AddUltimateMakeItem(int bResult);
	void			AddUltimateMakeGem(int nResult, int nNum);
	void			AddUltimateWeapon(BYTE state, int nResult);
#endif // __ULTIMATE

#ifdef __TRADESYS
	void			AddExchangeResult(BYTE state, int nResult);
#endif // __TRADESYS

#if __VER >= 9	// __PET_0410
	void	AddPet(CPet* pPet, BYTE nPetLevelup = PF_PET);
	void	AddPetState(unsigned long dwPetId, unsigned short wLife, unsigned short wEnergy, unsigned long dwExp);
	void	AddPetSetExp(unsigned long dwExp);
	void	AddPetFoodMill(int nResult, int nCount);
#endif	// __PET_0410

#if __VER >= 10 // __LEGEND	//	9차 전승시스템	Neuz, World, Trans
	void			AddLegendSkillResult(int nResult);
#endif	//__LEGEND	//	9차 전승시스템	Neuz, World, Trans

#if __VER >= 10 // __REMOVE_ATTRIBUTE
	void			AddRemoveAttribute(bool bSuccess);
#endif // __REMOVE_ATTRIBUTE

#if __VER >= 11 // __SYS_COLLECTING
	virtual	void	ProcessCollecting(void);
	virtual	void	StartCollecting(void);
	virtual	void	StopCollecting(void);
	bool	DoUseItemBattery(void);
#if __VER < 14 // __SMELT_SAFETY
	bool	PreRefine(unsigned long dwTarget, unsigned long dwMaterial);
	void	RefineCollector(CItemElem* pTarget, CItemElem* pMaterial);
	void	RefineAccessory(CItemElem* pTarget, CItemElem* pMaterial);
#endif // __SMELT_SAFETY
	void	AddRestartCollecting(unsigned long dwItemId);
	int		m_nCollecting;
#endif	// __SYS_COLLECTING

#ifdef __AZRIA_1023
	bool	HasInput() { return m_szInput[0] != '\0'; }
	void	ResetInput() { m_szInput[0] = '\0'; }
	void	SetInput(char* szInput) { strncpy(m_szInput, szInput, MAX_INPUT_LEN);	m_szInput[MAX_INPUT_LEN - 1] = '\0'; }
	char* GetInput() { return m_szInput; }
	int		DoUseItemInput(ItemProp* pProp, CItemElem* pItem);
#endif	// __AZRIA_1023

#ifdef __PET_1024
	int		DoUseItemPetNaming();
#endif	// __PET_1024

#ifdef __SYS_TICKET
private:
	CItemElem* FindActiveTicket(unsigned long dwItemId);
public:
	void	DoUseItemTicket(CItemElem* pItemElem);
#endif	// __SYS_TICKET

#if __VER >= 11 // __GUILD_COMBAT_1TO1
	void	AddGC1to1TenderOpenWnd(int nPenya);
	void	AddGC1to1TenderGuildView(int nPenya, int nRanking, time_t t, vector<CGuildCombat1to1Mng::__GC1TO1TENDER>& vecTenderGuild);
	void	AddGC1to1MemberLineUpOpenWnd(vector<unsigned long>& vecMemberId);
	void	AddGC1to1NowState(int nState, int nTime, int nProgCount = 0);
	void	AddGC1to1WarResult(int m_nState, unsigned long uIdPlayer, int nWinCount0, int nWinCount1);
#endif // __GUILD_COMBAT_1TO1
#if __VER >= 8 // __CSC_VER8_5
	void	RemoveAngel(void);
#endif	// __CSC_VER8_5

#if __VER >= 11 // __MA_VER11_04	// 길드 창고 로그 기능 world,database,neuz
	void	AddGuildBankLogView(BYTE byListType, short nLogCount, const __GUILDBANKLOG_ENTRY* logs);
#endif //__MA_VER11_04	// 길드 창고 로그 기능 world,database,neuz
#if __VER >= 11 // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz
	void	AddSealChar(short nLogCount, const __SEALCHAR_ENTRY* seals);
	void	AddSealCharSet();
#endif // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz

#if __VER >= 13 // __HONORABLE_TITLE			// 달인
	void	AddHonorListAck();
#endif	// __HONORABLE_TITLE			// 달인

#ifdef __EVENTLUA_COUPON
	void	AddCoupon(int nCoupon);
#endif // __EVENTLUA_COUPON

#if __VER >= 12 // __SECRET_ROOM
	void	AddSecretRoomMngState(int nState, unsigned long dwRemainTime);
	void	AddSecretRoomContInfo(BYTE nContinent, BYTE nType, vector<__SECRETROOM_TENDER>& vecSecreetRoomTender, int nIndex);
	void	AddSecretRoomTenderOpenWnd(int nTenderPenya);
	void	AddSecretRoomLineUpOpenWnd(vector<unsigned long>& vecLineUpMember);
	void	AddSecretRoomTenderView(int nTenderPenya, int nRanking, time_t t, vector<__SECRETROOM_TENDER>& vecSRTender);
#endif // __SECRET_ROOM

#if __VER >= 12 // __LORD
	void	AddLord(void);
	void	AddLordSkillTick(CLordSkill* pSkills);
	int		GetMuteText(void);
	void	SetElection(unsigned long idElection) { m_idElection = idElection; }
	unsigned long	GetElection(void) { return m_idElection; }
	bool	IsTeleportable(void);
	bool	IsQuerying(void) { return m_bQuerying; }
	void	SetQuerying(bool bQuerying) { m_bQuerying = bQuerying; }
#endif	// __LORD

#if __VER >= 12 // __MOD_TUTORIAL
	void	AddSetTutorialState(void);
#endif	// __MOD_TUTORIAL

#if __VER >= 12 // __TAX
	void	AddTaxInfo(void);
	void	AddTaxSetTaxRateOpenWnd(BYTE nCont);
#endif // __TAX

	bool	IsGuildMaster();

	void	ValidateItem(void);
	void	AdjustGuildQuest(unsigned long dwWorldId);
	void	AdjustMailboxState(void);
	void	AddEventLuaDesc(void);
	void	AdjustPartyQuest(unsigned long dwWorldId);

public:
#ifndef __VM_0820
#ifndef __MEM_TRACE
	static CUserPool* m_pPool;
	void* operator new(size_t nSize) { return CUser::m_pPool->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CUser::m_pPool->Alloc(); }
	void	operator delete(void* lpMem) { CUser::m_pPool->Free((CUser*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CUser::m_pPool->Free((CUser*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820

#if __VER >= 13 // __RAINBOW_RACE
	void AddRainbowRacePrevRankingOpenWnd();
	void AddRainbowRaceApplicationOpenWnd();
	void AddRainbowRaceState(CRainbowRace* pRainbowRace);
	void AddMiniGameState(__MINIGAME_PACKET MiniGamePacket);
	void AddMiniGameExtState(__MINIGAME_EXT_PACKET MiniGameExtPacket);
#endif // __RAINBOW_RACE

#if __VER >= 13 // __HOUSING
	void AddHousingAllInfo(CHousing* pHousing);
	void AddHousingSetFurnitureList(HOUSINGINFO& housingInfo, bool bAdd);
	void AddHousingSetupFurniture(HOUSINGINFO* pHousingInfo);
	void AddHousingPaperingInfo(unsigned long dwItemId, bool bSetup);
	void AddHousingSetVisitAllow(unsigned long dwTargetId, bool bAllow);
	void AddHousingVisitableList(vector<unsigned long>& vecVisitable);
#endif // __HOUSING

#if __VER >= 13 // __QUEST_HELPER
	void AddNPCPos(const D3DXVECTOR3& vPos);
#endif // __QUEST_HELPER

	void AddClearTarget();

private:
	void			Init(DPID dpidCache = DPID_UNKNOWN, DPID dpidUser = DPID_UNKNOWN);

#if __VER >= 12 // __LORD
	unsigned long	m_idElection;
	bool	m_bQuerying;
#endif	// __LORD
#ifdef __AZRIA_1023
	char	m_szInput[MAX_INPUT_LEN];
#endif	// __AZRIA_1023

#if __VER >= 13 // __COUPLE_1117
public:
	void	SetProposer(unsigned long idProposer) { m_idProposer = idProposer; }
	unsigned long	GetProposer() { return m_idProposer; }
	void	AddCouple();
	void	AddProposeResult(unsigned long idProposer, const char* pszProposer);
	void	AddCoupleResult(unsigned long idPartner, const char* pszPartner);
	void	AddDecoupleResult();
#if __VER >= 13 // __COUPLE_1202
	void	ProcessCouple();
	void	ActiveCoupleBuff(int nLevel);
	void	AddAddCoupleExperience(int nExperience);
#endif	// __COUPLE_1202
#if __VER >= 14 // __PCBANG
	void	AddPCBangInfo(CPCBangInfo* pPI);
#endif // __PCBANG
#ifdef __VTN_TIMELIMIT
	void	AddAccountPlayTime();
#endif // __VTN_TIMELIMIT
#if __VER >= 14 // __SMELT_SAFETY
	void AddSmeltSafety(BYTE nResult);
#endif // __SMELT_SAFETY
#ifdef __MAP_SECURITY
	void AddWorldReadInfo(unsigned long dwWorldId, D3DXVECTOR3 vPos);
#endif // __MAP_SECURITY
#ifdef __QUIZ
	void AddQuizEventState(int nType, int nState);
	void AddQuizEventMessage(const char* lpszMessage, int nState = 0);
	void AddQuizQuestion(int nType, const char* lpszQuestion, int nCount);
#endif // __QUIZ

#if __VER >= 15 // __PETVIS
	void AddActivateVisPet(OBJID objIdVisPetItem, OBJID objIdVisPetId);
#endif // PETVIS

#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	void AddCheckedQuest();
#endif // __IMPROVE_QUEST_INTERFACE

#if __VER >= 15 // __GUILD_HOUSE
public:
	void	AddGuildHousePakcet(int nPacketType, GH_Fntr_Info& gfi, int nIndex);
	void	AddGuildHouseAllInfo(CGuildHouseBase* pGuildHouse);
	void	AddGuildHouseRemvoe();
	int		GetRestPoint() { return m_nRestPoint; }
	int		SetIncRestPoint(int nRestPoint) { return SetAddRestPoint((int)(nRestPoint * static_cast<float>(GetParam(DST_RESTPOINT_RATE, 0) / 100.0f))); }
	int		SetDecRestPoint(int nRestPoint) { return SetAddRestPoint(-nRestPoint); }
	void	SetRestPoint(int nRestPoint) { m_nRestPoint = nRestPoint; }
	void	SetRestPointTick() { m_tRestPointTick = time_null() + REST_POINT_TICK; }
	EXPINTEGER	GetAddExpAfterApplyRestPoint(EXPINTEGER nExp);

private:
	int		SetAddRestPoint(int nRestPoint);
	void	AddRestPoint();

	int		m_nRestPoint;
	int		m_nPrevRestPoint;
	time_t	m_tRestPointTick;
#endif // __GUILD_HOUSE

#if __VER >= 15 // __CAMPUS
public:
	void	AddInviteCampusMember(CUser* pRequest);
	void	AddUpdateCampus(CCampus* pCampus);
	void	AddRemoveCampus(unsigned long idCampus);
	void	AddUpdateCampusPoint(int nCampusPoint);
	void	ProcessCampus();
	void	ActiveCampusBuff(int nLevel);
	unsigned long	GetCampusTick() { return m_dwCampusTick; }
	void	SetCampusTick(unsigned long dwTick) { m_dwCampusTick = dwTick; }

	int		GetPointByType(int nType);	// 100122 define.h 에 정의되어 있는 포인트 타입에 따른 유저의 포인트 리턴

private:
	unsigned long	m_dwCampusTick;
#endif // __CAMPUS

#ifdef __GUILD_HOUSE_MIDDLE
public:
	void	AddGuildHouseTenderMainWnd(map<OBJID, char*>& mapGHList);
	void	AddGuildHouseTenderInfoWnd(OBJID objGHId, int nTenderMinPenya, __int64 nTenderPenya, vector<unsigned long>& vecGuildId);
	void	AddGuildHouseTenderResult(OBJID objGHId, bool bResult);
#endif // __GUILD_HOUSE_MIDDLE

private:
	unsigned long	m_idProposer;
#if __VER >= 13 // __COUPLE_1202
	int		m_cbProcessCouple;
#endif	// __COUPLE_1202
#endif	// __COUPLE_1117


	//	mulcom	BEGIN100405	각성 보호의 두루마리
public:
	void	SelectAwakeningValue(unsigned long dwItemObjID, unsigned long dwSerialNumber, BYTE bySelectFlag);
	void	SendNewRandomOption(unsigned char id, unsigned long dwSerialNumber, __int64 n64NewRandomOption);
	//	mulcom	END100405	각성 보호의 두루마리

};

#define	SEC_SAVEPLAYER	900

class CDPSrvr;
#if __VER >= 12 // __LORD
class CLEComponent;
class ILordEvent;
#endif	// __LORD

class CUserMng
{
public:
	CUserMng();
	virtual	~CUserMng();

private:
	long				m_lCount;
	map<unsigned long, CUser*>	m_users;

private:
	void			RemoveUserFromCacheMsg(CUser* pUser);

public:
	void			DestroyPlayer(CUser* pUser);
	long			GetCount() { return m_lCount; }
	void			RemoveAllUsers();
	CUser* AddUser(DPID dpidCache, DPID dpidUser, DPID dpidSocket);
	void			RemoveUser(unsigned long dwSerial);
	CUser* GetUser(DPID dpidCache, DPID dpidUser);
	CUser* GetUserByPlayerID(unsigned long idPlayer);
	//	void			Notify();
#ifdef __LAYER_1015
	bool			AddPlayer(CUser* pUser, unsigned long dwWorldID, int nLayer);
#else	// __LAYER_1015
	bool			AddPlayer(CUser* pUser, unsigned long dwWorldID);
#endif	// __LAYER_1015
	void			AddDisguise(CMover* pMover, unsigned long dwMoverIdx);
	void			AddNoDisguise(CMover* pMover);
	void			AddSetStateLevel(CMover* pMover, char chID, long nValue);
	void			AddHdr(CCtrl* pCtrl, unsigned short wHdr);
	void			AddChat(CCtrl* pCtrl, const TCHAR* szChat);
#if __VER >= 11 // __SYS_COLLECTING
	void			AddStartCollecting(CUser* pUser);
	void			AddStopCollecting(CUser* pUser);
#endif	// __SYS_COLLECTING
	void			AddDlgEmoticon(CCtrl* pCtrl, int nIdx);
	void			AddDamage(CMover* pMover, OBJID objidAttacker, unsigned long dwHit, unsigned long dwAtkFlags);
	void			AddDoEquip(CMover* pMover, int nPart, BYTE nId, const EQUIP_INFO& rEquipInfo, BYTE fEquip);
	void			AddDoEquip(CMover* pMover, int nPart, CItemElem* pItemElem, BYTE fEquip);	// 간단 파라메터 버전.
	void			AddVendor(CMover* pVendor);
	void			AddUpdateVendor(CMover* pVendor, char cTab, BYTE nId, short nNum);
	void			AddSetDestParam(CMover* pMover, int nDstParameter, int nAdjParameterValue, int nChgParameterValue);
	void			AddResetDestParam(CMover* pMover, int nDstParamter, int nAdjParameterValue);
#ifdef __SPEED_SYNC_0108		// ResetDestParam speed 수정
	void			AddResetDestParamSync(CMover* pMover, int nDstParameter, int nAdjParameterValue, int nParameterValue);
#endif // __SPEED_SYNC_0108		// ResetDestParam speed 수정
	void			AddSetPointParam(CMover* pMover, int nDstParameter, int nValue);
	void			AddSetPos(CCtrl* pCtrl, D3DXVECTOR3& vPos);
	void			AddSetPosAngle(CCtrl* pCtrl, const D3DXVECTOR3& vPos, float fAngle);
	void			AddSetLevel(CMover* pMover, unsigned short wLevel);
	void			AddSetFlightLevel(CMover* pMover, int nFlightLv);
	void			AddSetSkillLevel(CMover* pMover, unsigned long dwSkill, unsigned long dwLevel);
	void			AddSetScale(CMover* pMover, float fScalePercent);
	void			AddWorldShout(const TCHAR* szName, const TCHAR* szShout, D3DXVECTOR3 vPos, CWorld* pWorld);
	void			AddSetSkillState(CMover* pCenter, CMover* pTarget, unsigned short wType, unsigned short wID, unsigned long dwLevel, unsigned long dwTime);
	void			AddShipActMsg(CMover* pMover, CShip* pShip, unsigned long dwMsg, int nParam1, int nParam2);
	void			AddMeleeAttack(CMover* pMover, unsigned long dwAtkMsg, OBJID objid, int nParam2, int nParam3);
	void			AddMeleeAttack2(CMover* pMover, unsigned long dwAtkMsg, OBJID objid, int nParam2, int nParam3);
	void			AddMagicAttack(CMover* pMover, unsigned long dwAtkMsg, OBJID objid, int nParam2, int nParam3, int nMagicPower, int idSfxHit);
	void			AddRangeAttack(CMover* pMover, unsigned long dwAtkMsg, OBJID objid, int nParam2, int nParam3, int idSfxHit);
	void			AddAttackSP(CMover* pMover, unsigned long dwAtkMsg, OBJID objid, int nParam2, int nParam3);
	void			AddSetDestAngle(CMover* pMover, float fAngle, BYTE fLeft);
	void			AddMoverSetDestObj(CMover* pMover, OBJID objid, float fRange = 0.0f, bool fTransferToMe = false);
	void			AddMoverDeath(CMover* pMover, CMover* pAttacker, unsigned long dwMsg);
	void			AddSetMovePattern(CMover* pMover, int nPattern, const D3DXVECTOR3& vPos, float fAngle, float fAngleX);

	void			AddMoverBehavior2(CMover* pMover, D3DXVECTOR3& v, D3DXVECTOR3& vd, float f,
		float fAngleX, float fAccPower, float fTurnAngle,
		unsigned long dwState, unsigned long dwStateFlag, unsigned long dwMotion, int nMotionEx,
		int nLoop, unsigned long dwMotionOption, __int64 nTickCount, bool fTransferToMe = false);

	void			AddCreateSfxAllow(CMover* pMover, unsigned long dwSfxObjArrow, unsigned long dwSfxObjHit, D3DXVECTOR3 vPosDest, int idTarget);
	void			AddCreateSfxObj(CCtrl* pCtrl, unsigned long dwSfxObj, float x = 0, float y = 0, float z = 0, bool bFlag = false);
#if __VER >= 11 // __MA_VER11_06				// 확율스킬 효과수정 world,neuz
	void			AddRemoveSfxObj(CCtrl* pCtrl, unsigned long dwSfxObj, float x = 0, float y = 0, float z = 0, bool bFlag = false);
#endif // __MA_VER11_06				// 확율스킬 효과수정 world,neuz
	void			AddNearSetChangeJob(CMover* pMover, int nJob, LPSKILL lpSkill);
	void			AddModifyMode(CUser* pUser);
	void			AddStateMode(CUser* pUser, BYTE nFlag);
#if __VER < 8 // __S8_PK
	void			AddSetSlaughterPoint(CMover* pMover, int nVal, int nNumKill);
#endif // __VER < 8 // __S8_PK
	void			AddSetFame(CMover* pMover, int nFame);
	void			AddDefinedText(CMover* pMover, int dwText, LPCSTR lpszFormat, ...);
	void			AddBlock(LPBYTE lpBlock, unsigned long uBlockSize);
	void			AddBlock(LPBYTE lpBlock, unsigned long uBlockSize, CWorld* pWorld);
	void			AddBlock(CWorld* pWorld, const D3DXVECTOR3& vPos, int nRange, LPBYTE lpBlock, unsigned long uBlockSize);
	void			AddPVendorOpen(CUser* pUser);
	void			AddPVendorClose(CUser* pUser);
#if __VER >= 11 // __MOD_VENDOR
	void			AddPVendorItemNum(CUser* pUser, BYTE nItem, short nVend, const char* sBuyer);
#else	// __MOD_VENDOR
	void			AddPVendorItemNum(CUser* pUser, BYTE nItem, short nVend);
#endif	// __MOD_VENDOR
	void			AddSetHair(CUser* pUser, BYTE nHair, BYTE r, BYTE g, BYTE b);

	//	void			AddDoCollect( CUser* pUser, OBJID idTarget );
	void			AddCreateSkillEffect(CMover* pAttacker, OBJID idTarget, unsigned long dwSkill, unsigned long dwLevel);
	void			AddSetStun(CMover* pMover, int nMaxTime);
	void			AddSendActMsg(CMover* pMover, OBJMSG dwMsg, int nParam1 = 0, int nParam2 = 0, int nParam3 = 0);
	void			AddPushPower(CMover* pMover, D3DXVECTOR3 vPos, float fAngle, float fPushAngle, float fPower);
	void			AddRemoveSkillInfluence(CMover* pMover, unsigned short wType, unsigned short wID);
	void			AddDoApplySkill(CCtrl* pCtrl, OBJID idTarget, unsigned long dwSkill, unsigned long dwLevel);
	void			AddWorldCreateSfxObj(unsigned long dwSfxObj, float x, float y, float z, bool bFlag, unsigned long dwWorldId);
	void			AddMoverMoved(CMover* pMover, D3DXVECTOR3& v, D3DXVECTOR3& vd, float f,
		unsigned long dwState, unsigned long dwStateFlag, unsigned long dwMotion, int nMotionEx,
		int nLoop, unsigned long dwMotionOption, __int64 nTickCount);
	void			AddMoverBehavior(CMover* pMover, D3DXVECTOR3& v, D3DXVECTOR3& vd, float f,
		unsigned long dwState, unsigned long dwStateFlag, unsigned long dwMotion, int nMotionEx,
		int nLoop, unsigned long dwMotionOption, __int64 nTickCount, bool fTransferToMe = false);
	void			AddMoverMoved2(CMover* pMover, D3DXVECTOR3& v, D3DXVECTOR3& vd, float f,
		float fAngleX, float fAccPower, float fTurnAngle,
		unsigned long dwState, unsigned long dwStateFlag, unsigned long dwMotion, int nMotionEx,
		int nLoop, unsigned long dwMotionOption, __int64 nTickCount, BYTE nFrame = MAX_CORR_SIZE_150);
	void			AddUseSkill(CMover* pMover, unsigned long dwSkill, unsigned long dwLevel, OBJID objid, int nUseType, int nCastingTime);
	void			AddEventMessage(CCtrl* pCtrl, const TCHAR* szChat, int nFlag, unsigned long dwItemId = 0);
	void			AddMotion(CMover* pMover, unsigned long dwMsg);
	void			AddRemoveAllSkillInfluence(CMover* pMover);
	void			AddMoverAngle(CMover* pMover, D3DXVECTOR3& v, D3DXVECTOR3& vd,
		float f, float fAngleX, float fAccPower, float fTurnAngle, __int64 nTickCount);
	void			AddMoverCorr(CMover* pMover, D3DXVECTOR3& v, D3DXVECTOR3& vd, float f,
		unsigned long dwState, unsigned long dwStateFlag, unsigned long dwMotion, int nMotionEx,
		int nLoop, unsigned long dwMotionOption, __int64 nTickCount);
	void			AddMoverCorr2(CMover* pMover, D3DXVECTOR3& v, D3DXVECTOR3& vd, float f,
		float fAngleX, float fAccPower, float fTurnAngle,
		unsigned long dwState, unsigned long dwStateFlag, unsigned long dwMotion, int nMotionEx,
		int nLoop, unsigned long dwMotionOption, __int64 nTickCount);
	void			AddCreateGuild(unsigned long idPlayer, const char* lpszPlayer, unsigned long idGuild, const char* szGuild);
	void			AddDestroyGuild(const char* lpszPlayer, unsigned long idGuild);
	void			AddSetGuild(CUser* pUser, unsigned long idGuild);
	void			AddSetLogo(unsigned long idGuild, unsigned long dwLogo);
	void			AddGetItemElem(CUser* pUser, CItemElem* pItemElem);
	void			AddPutItemElem(CUser* pUser, CItemElem* pItemElem);
#if __VER >= 8 // __GUILDCOMBAT_85
	void			AddPutItemElem(unsigned long uidGuild, CItemElem* pItemElem);
#endif // __VER >= 8
	void			AddSetWar(CUser* pUser, unsigned long idWar);
	void			ModifyMode(unsigned long dwMode);
	void			AddSchoolReport(PSCHOOL_ENTRY pSchool, short nElapse = 0);
	void			AddSexChange(CMover* pMover);
	void			AddSetLocalEvent(short id, BYTE nState);
	void			AddGameRate(float fRate, BYTE nFlag);
	void			AddChangeFace(unsigned long uidPlayer, unsigned long dwFace);
#if __VER >= 13
	void			AddShout(CUser* pUserSrc, int nRange, LPBYTE lpBlock, unsigned long uBlockSize);
#else // __VER >= 13
	void			AddShout(const D3DXVECTOR3& vPos, int nRange, LPBYTE lpBlock, unsigned long uBlockSize);
#endif // __VER >= 13
	void			AddWorldMsg(const CRect* pRect, const char* lpszString);
	void			AddGameSetting();
	void			AddCommonPlace(CCtrl* pCtrl, BYTE nType);
	void			OutputStatistics(void);
	void			AddGuildCombatNextTimeWorld(unsigned long dwTime, unsigned long dwState);
	void			AddGuildCombatState(void);
	void			AddGuildCombatUserState(CMover* pMover);
#ifdef __LAYER_1015
	void			ReplaceWorld(unsigned long dwWorldId, unsigned long dwReplaceWorldId, float fReplaceX, float fReplaceZ, int nLayer);
	void			ReplaceWorldArea(unsigned long idParty, unsigned long dwWorldId, unsigned long dwReplaceWorldId, float fReplaceX, float fReplaceZ, float fArea, bool bDieFlag, int nLayer);
#else	// __LAYER_1015
	void			ReplaceWorld(unsigned long dwWorldId, unsigned long dwReplaceWorldId, float fReplaceX, float fReplaceZ);
	void			ReplaceWorldArea(unsigned long idParty, unsigned long dwWorldId, unsigned long dwReplaceWorldId, float fReplaceX, float fReplaceZ, float fArea, bool bDieFlag);
#endif	// __LAYER_1015
	void			AddWorldMsg(unsigned long dwWorldId, const char* lpszString);
	void			AddGCGuildStatus(unsigned long uidGuild, CUser* pSendUser = NULL);
	void			AddGCGuildPrecedence(CUser* pSendUser = NULL);
	void			AddGCPlayerPrecedence(CUser* pSendUser = NULL);
	void			AddGCWinGuild(void);
	void			AddGCBestPlayer(void);
	void			AddGCWarPlayerlist(unsigned long uidGuild = 0, CUser* pSendUser = NULL);
	void			AddGuildCombatEnterTime(unsigned long dwTime);
	void			AddGCLogWorld(void);
	void			AddGCLogRealTimeWorld(CGuildCombat::__GCGETPOINT GCGetPoint);
	void			AddGCIsRequest(unsigned long uidGuild, bool bRequest);
	void			AddBlockNoLock(LPBYTE lpBlock, unsigned long uBlockSize);
	void			AddBlockNoLock(LPBYTE lpBlock, unsigned long uBlockSize, CWorld* pWorld);
	void			AddGuildMsg(unsigned long idGuild, LPCSTR lpsz);
	void			AddGuildMsg(CGuild* pGuild, LPCSTR lpsz);

#ifdef __S1108_BACK_END_SYSTEM
	void			AddMonsterProp();
	void			AddGMChat(int nSize);
#endif // __S1108_BACK_END_SYSTEM

	void			AddMotionArrive(CMover* pMover, OBJMSG objmsg);

#if __VER >= 8 // __S8_PK
	void			AddPKPink(CMover* pMover, BYTE byPink);
	void			AddPKPropensity(CMover* pMover);
#endif // __VER >= 8 // __S8_PK

#ifdef __IAOBJ0622
	void			AddSetDestPos(CMover* pMover, CONST D3DXVECTOR3& vPos, BYTE fForward, OBJID objidIAObj = NULL_ID);
#else	// __IAOBJ0622
	void			AddSetDestPos(CMover* pMover, CONST D3DXVECTOR3& vPos, BYTE fForward);
#endif	// __IAOBJ0622
	void	AddAddRegion(unsigned long dwWorldId, REGIONELEM& re);
#ifdef __EVENT_1101
	void	CallTheRoll(int nBit);
#endif	// __EVENT_1101
#if __VER >= 9	// __PET_0410
#ifdef __PET_1024
	void	AddPetCall(CMover* pMover, unsigned long dwPetId, unsigned long dwIndex, BYTE nPetLevel, const char* szPetName);
	void	AddSetPetName(CUser* pUser, const char* szPetName);
#else	// __PET_1024
	void	AddPetCall(CMover* pMover, unsigned long dwPetId, unsigned long dwIndex, BYTE nPetLevel);
#endif	// __PET_1024
	void	AddPetRelease(CMover* pMover);
	void	AddPetLevelup(CMover* pMover, unsigned long dwPetId);
	void	AddPetFeed(CMover* pMover, unsigned short wEnergy);
#endif	// __PET_0410
#if __VER >= 13 // __HONORABLE_TITLE			// 달인
	void	AddHonorTitleChange(CMover* pMover, int nChange);
#endif	// __HONORABLE_TITLE			// 달인
#if __VER >= 9	//__AI_0509
	void	AddSetSpeedFactor(CMover* pMover, float fSpeedFactor);
#endif	// __AI_0509

#if __VER >= 9 // __EVENTLUA
	void	AddEventLua(BYTE nId, bool bState);
#endif // __EVENTLUA

#if __VER >= 12 // __LORD
	void	AddElectionBeginCandidacy(void);
	void	AddElectionBeginVote(int nRequirement);
	void	AddElectionEndVote(unsigned long idPlayer);
	void	AddElectionAddDeposit(unsigned long idPlayer, __int64 iDeposit, time_t tCreate);
	void	AddElectionSetPledge(unsigned long idPlayer, const char* szPledge);
	void	AddElectionIncVote(unsigned long idPlayer, unsigned long idElector);
	void	AddLEventCreate(CLEComponent* pComponent);
	void	AddLEventInitialize(void);
	void	AddLEventTick(ILordEvent* pEvent);
	void	AddLordSkillUse(CUser* pUser, unsigned long idTarget, int nSkill);
	void	AddQueryPlayerData(unsigned long idPlayer);
#endif	// __LORD
#if __VER >= 12 // __TAX
	void	AddTaxInfo(void);
#endif // __TAX
#if __VER >= 13 // __HOUSING
	void	AddHousingPaperingInfo(unsigned long dwItemId, bool bSetup, unsigned long dwMasterId);
#endif // __HOUSING
#ifdef __VTN_TIMELIMIT
	void	ResetAccountPlayTime();
#endif // __VTN_TIMELIMIT
#if __VER >= 15 // __PETVIS
	void	AddChangeMoverSfxId(CMover* pMover);
#endif // __PETVIS
#if __VER >= 15 // __GUILD_HOUSE
	bool	HasUserSameWorldnLayer(CUser* pUserSrc);
#endif // __GUILD_HOUSE

};

#endif	// __USER_H__