#ifndef __DPDATABASECLIENT_H__
#define __DPDATABASECLIENT_H__

#include "DPMng.h"
#include "MsgHdr.h"
#include "guild.h"

#include "eveschool.h"
#if __VER >= 12 // __SECRET_ROOM
#include "SecretRoom.h"
#endif // __SECRET_ROOM
#if __VER >= 15 // __GUILD_HOUSE
#include "GuildHouse.h"
#endif // __GUILD_HOUSE

#undef	theClass
#define theClass	CDPDatabaseClient
#undef theParameters
#define theParameters CAr & ar, DPID, DPID

class CUser;
class CDPDatabaseClient : public CDPMng
{
private:
	int		m_cbPing;
	bool	m_bAlive;

public:
	//	Constructions
	CDPDatabaseClient();
	virtual	~CDPDatabaseClient();

	//	Operations
	virtual	void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	virtual	void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);

	void	SendITEM_TBL_Update(void);
#ifdef __LAYER_1015
	void	SavePlayer(CUser* pUser, unsigned long dwWorldId, D3DXVECTOR3& vPos, int nLayer, bool bLogout = false);
#else	// __LAYER_1015
	void	SavePlayer(CUser* pUser, unsigned long dwWorldId, const D3DXVECTOR3& vPos, bool bLogout = false);
#endif	// __LAYER_1015

#ifdef __S_NEW_SKILL_2
	void	SaveSkill(CUser* pUser);
#endif // __S_NEW_SKILL_2

#ifdef __S_RECOMMEND_EVE
	void	SendRecommend(CUser* pUser, int nValue = 0);
#endif // __S_RECOMMEND_EVE
	void	SendLogConnect(CUser* pUser);
	void	SendLogPlayDeath(CMover* pMover, CMover* pSender);
	void	SendLogLevelUp(CMover* pSender, int Action);
	void	SendLogSkillPoint(int nAction, int nPoint, CMover* pMover, LPSKILL pSkill);
	void	SendLogItem(CUser* pUser, CMover* pSender);
	void	SendLogSMItemUse(const char* Action, CUser* pUser, CItemElem* pItemElem, ItemProp* pItemProp, const char* szName1 = "");
	void	SendLogGamemaChat(CUser* pUser, const char* lpszString);
	void	SendLogUniqueItem(CMover* pMover, CItem* pItem, int nOption);
	void	SendLogUniqueItem2(CMover* pMover, ItemProp* pItemProp, int nOption);
	void	SendChangeBankPass(const char* szName, const char* szNewPass, unsigned long uidPlayer);
	void	SendLogPkPvp(CMover* pUser, CMover* pLose, int nPoint, char chState);
	void	SendLogSchool(unsigned long idPlayer, const char* szName);
	void	SendPing(void);
	void	SendInGuildCombat(unsigned long idGuild, unsigned long dwPenya, unsigned long dwExistingPenya);
	void	SendOutGuildCombat(unsigned long idGuild);
	void	SendGuildCombatResult(void);
	void	SendGuildCombatStart(void);
	void	SendGCGetPenyaGuild(unsigned long uidPlayer, int nGuildCombatID, unsigned long uidGuild);
	void	SendGCGetPenyaPlayer(unsigned long uidPlayer, int nGuildCombatID, unsigned long uidGuild);
	void	SendGuildcombatContinue(int nGuildCombatID, unsigned long uidGuild, int nWinCount);
#ifdef __INVALID_LOGIN_0320
	void	CalluspXXXMultiServer(unsigned long uKey, CUser* pUser);
#else	// __INVALID_LOGIN_0320
	void	CalluspXXXMultiServer(unsigned long uKey, unsigned long idPlayer);
#endif	// __INVALID_LOGIN_0320
	void	SendLogExpBox(unsigned long idPlayer, OBJID objid, EXPINTEGER iExp, bool bGet = false);
	void	UpdateGuildRanking(); // TRANS서버에게 길드랭킹을 업데잇하라는 패킷을 보내는 함수
	void	UpdateGuildRankingUpdate();
	void	SendQueryGuildQuest(void);
	void	SendInsertGuildQuest(unsigned long idGuild, int nId);
	void	SendUpdateGuildQuest(unsigned long idGuild, int nId, int nState);
	void	SendQueryGuildBank(); // TRANS서버에게 길드창고를 쿼리하라는 패킷을 보내는 함수
	void	SendGuildContribution(CONTRIBUTION_CHANGED_INFO& info, BYTE nLevelUp, long nMemberLevel);
	void	SendGuildGetPay(unsigned long uGuildId, unsigned long nGoldGuild, unsigned long dwPay);
	void	SendQuerySetPlayerName(unsigned long idPlayer, const char* lpszPlayer, unsigned long dwData);
	void	SendQueryMailBox(unsigned long idReceiver);


	//////////////////////////////////////////////////////////////////////////
	void	SendQueryMailBoxReq(unsigned long idReceiver);
	void	SendQueryMailBoxCount(unsigned long idReceiver, int nCount);
	void	OnMailBoxReq(CAr& ar, DPID, DPID);
	//////////////////////////////////////////////////////////////////////////


	void	OnMailBox(CAr& ar, DPID, DPID);
	void	SendQueryPostMail(unsigned long idReceiver, unsigned long idSender, CItemElem& itemElem, int nGold, char* lpszTitle, char* lpszText);
	void	SendQueryRemoveMail(unsigned long idReceiver, unsigned long nMail);
	void	SendQueryGetMailItem(unsigned long idReceiver, unsigned long nMail);
	void	SendQueryGetMailGold(unsigned long idReceiver, unsigned long nMail);
	void	SendQueryReadMail(unsigned long idReceiver, unsigned long nMail);
	void	OnPostMail(CAr& ar, DPID, DPID);
	void	OnRemoveMail(CAr& ar, DPID, DPID);
	void	OnGetMailItem(CAr& ar, DPID, DPID);
	void	OnGetMailGold(CAr& ar, DPID, DPID);
	void	OnReadMail(CAr& ar, DPID, DPID);
	void	OnAllMail(CAr& ar, DPID, DPID);
	void	CalluspLoggingQuest(unsigned long idPlayer, int nQuest, int nState, const char* pszComment = "");
	void	SendPreventLogin(const char* szAccount, unsigned long dwPreventTime);
	void	SendSchoolReport(PSCHOOL_ENTRY pSchool);
	void	SendBuyingInfo(PBUYING_INFO2 pbi2, SERIALNUMBER iSerialNumber);

#if __VER >= 11 // __MA_VER11_04	// 길드 창고 로그 기능 world,database,neuz
	void	SendQueryGetGuildBankLogList(unsigned long idReceiver, unsigned long	idGuild, BYTE byListType);
#endif //__MA_VER11_04	// 길드 창고 로그 기능 world,database,neuz
#if __VER >= 11 // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz
	void	SendQueryGetSealChar(unsigned long idReceiver, const char* szAccount);
	void	SendQueryGetSealCharConm(unsigned long idReceiver);
	void	SendQueryGetSealCharGet(unsigned long idReceiver, const char* szAccount, unsigned long dwGetId);
	void	SendQueryGetSealCharSet(unsigned long idReceiver, const char* szAccount, long lSetPlayerSlot, unsigned long dwSetPlayerId);
#endif // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz

#if __VER >= 9	// __PET_0410
	void	CalluspPetLog(unsigned long idPlayer, SERIALNUMBER iSerial, unsigned long dwData, int nType, CPet* pPet);
#endif	// __PET_0410

#if __VER >= 9 // __EVENTLUA
	void	SendEventLuaChanged(void);
#endif // __EVENTLUA

#if __VER >= 11 // __GUILD_COMBAT_1TO1
	void	SendGC1to1StateToDBSrvr(void);
	void	SendGC1to1Tender(char cGU, unsigned long uGuildId, int nPenya, char cState);
	void	SendGC1to1LineUp(unsigned long uGuildId, vector<unsigned long>& vecMemberId);
	void	SendGC1to1WarPerson(unsigned long uGuildId, unsigned long uIdPlayer, char cState);
	void	SendGC1to1WarGuild(unsigned long dwWorldId, unsigned long uGuildId_0, unsigned long uGuildId_1, char cState);
#endif // __GUILD_COMBAT_1TO1

#if __VER >= 11 // __SYS_PLAYER_DATA
	void	SendUpdatePlayerData(CUser* pUser);
#endif	// __SYS_PLAYER_DATA

private:
	USES_PFNENTRIES;

	//	Handlers
	void	OnJoin(CAr& ar, DPID dpidCache, DPID dpidUser);
#if __VER >= 11 // __SYS_PLAYER_DATA
	void	OnAllPlayerData(CAr& ar, DPID, DPID);
	void	OnAddPlayerData(CAr& ar, DPID, DPID);
	void	OnDeletePlayerData(CAr& ar, DPID, DPID);
	void	OnUpdatePlayerData(CAr& ar, DPID, DPID);
#else	// __SYS_PLAYER_DATA
	void	OnAllPlayerID(CAr& ar, DPID, DPID);
	void	OnPlayerID(CAr& ar, DPID, DPID);
	void	OnRemovePlayerID(CAr& ar, DPID, DPID);
#endif	// __SYS_PLAYER_DATA
	void	OnGCPlayerPoint(CAr& ar);
	void	OnGCResultValue(CAr& ar);
	void	OnAllGuildCombat(CAr& ar, DPID, DPID);
	void	OnInGuildCombat(CAr& ar, DPID, DPID);
	void	OnOutGuildCombat(CAr& ar, DPID, DPID);
	void	OnResultGuildCombat(CAr& ar, DPID, DPID);
	void	OnPlayerPointGuildCombat(CAr& ar, DPID, DPID);
	void	OnGetPenyaGuildGC(CAr& ar, DPID, DPID);
	void	OnGetPenyaPlayerGC(CAr& ar, DPID, DPID);
	void	OnContinueGC(CAr& ar, DPID, DPID);
	void	OnBaseGameSetting(CAr& ar, DPID, DPID);
	void	OnMonsterRespawnSetting(CAr& ar, DPID, DPID);
	void	OnMonsterPropSetting(CAr& ar, DPID, DPID);
	void	OnGMChat(CAr& ar, DPID, DPID);
	void	OnGuildBank(CAr& ar, DPID, DPID);
	void	OnUpdateGuildRankingFinish(CAr& ar, DPID, DPID);
	void	OnQueryGuildQuest(CAr& ar, DPID, DPID);
	void	OnPing(CAr& ar, DPID, DPID);
	void	OnQueryRemoveGuildBankTbl(CAr& ar, DPID, DPID);
	void	SendQueryRemoveGuildBankTbl(int nNo, unsigned long dwRemoved);
	void	OnEventGeneric(CAr& ar, DPID, DPID);
	void	OnEventFlag(CAr& ar, DPID, DPID);

#if __VER >= 9 // __EVENTLUA
	void	OnEventStateLua(CAr& ar, DPID, DPID);
	void	OnEventLuaChanged(CAr& ar, DPID, DPID);
#endif // __EVENTLUA

#if __VER >= 11 // __GUILD_COMBAT_1TO1
	void	OnGC1to1Open(CAr& ar, DPID, DPID);
	void	OnGC1to1TenderGuildFromDB(CAr& ar, DPID, DPID);
#endif // __GUILD_COMBAT_1TO1

#if __VER >= 11 // __MA_VER11_04	// 길드 창고 로그 기능 world,database,neuz
	void	OnGuildBankLogViewFromDB(CAr& ar, DPID, DPID);
#endif //__MA_VER11_04	// 길드 창고 로그 기능 world,database,neuz
#if __VER >= 11 // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz
	void	OnSealCharFromDB(CAr& ar, DPID, DPID);
	void	OnSealCharGetFromDB(CAr& ar, DPID, DPID);
#endif // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz

	/*
	#ifdef __S0114_RELOADPRO
		void	OnReloadProject( CAr & ar, DPID dpidCache, DPID dpidUser );
	#endif // __S0114_RELOADPRO
	*/

#if __VER >= 12 // __LORD
private:
	void	OnElectionAddDeposit(CAr& ar, DPID, DPID);
	void	OnElectionSetPledge(CAr& ar, DPID, DPID);
	void	OnElectionIncVote(CAr& ar, DPID, DPID);
	void	OnElectionBeginCandidacy(CAr& ar, DPID, DPID);
	void	OnElectionBeginVote(CAr& ar, DPID, DPID);
	void	OnElectionEndVote(CAr& ar, DPID, DPID);
	void	OnLord(CAr& ar, DPID, DPID);
	void	OnLEventCreate(CAr& ar, DPID, DPID);
	void	OnLEventInitialize(CAr& ar, DPID, DPID);
	void	OnLordSkillUse(CAr& ar, DPID, DPID);
	void	OnLordSkillTick(CAr& ar, DPID, DPID);
	void	OnLEventTick(CAr& ar, DPID, DPID);
public:
	void	SendElectionAddDeposit(unsigned long idPlayer, __int64 iDeposit);
	void	SendElectionSetPledge(unsigned long idPlayer, const char* szPledge);
	void	SendElectionIncVote(unsigned long idPlayer, unsigned long idElector);
	void	SendLEventCreate(unsigned long idPlayer, int iEEvent, int iIEvent);
	void	SendLordSkillUse(unsigned long idPlayer, unsigned long idTarget, int nSkill);
	// operator commands
	void	SendElectionProcess(bool bRun);
	void	SendElectionBeginCandidacy(void);
	void	SendElectionBeginVote(void);
	void	SendElectionBeginEndVote(void);
	void	SendLEventInitialize(void);
#endif	// __LORD

#if __VER >= 12 // __TAX
private:
	void	OnTaxInfo(CAr& ar, DPID, DPID);
public:
	void	SendSecretRoomWinGuild(BYTE nCont, unsigned long dwGuildId);
	void	SendLord(unsigned long dwIdPlayer);
	void	SendTaxRate(BYTE nCont, int nSalesTaxRate, int nPurchaseTaxRate);
	void	SendAddTax(BYTE nCont, int nTax, BYTE nTaxKind);
	void	SendApplyTaxRateNow();
#endif // __TAX

#if __VER >= 12 // __SECRET_ROOM
private:
	void	OnSecretRoomInfoClear(CAr& ar, DPID, DPID);
	void	OnSecretRoomTenderInfo(CAr& ar, DPID, DPID);
public:
	void	SendSecretRoomInsertToDB(BYTE nContinent, __SECRETROOM_TENDER& srTender);
	void	SendSecretRoomUpdateToDB(BYTE nContinent, __SECRETROOM_TENDER& srTender, char chState);
	void	SendSecretRoomInsertLineUpToDB(BYTE nContinent, __SECRETROOM_TENDER& srTender);
	void	SendSecretRoomClosed();
#endif // __SECRET_ROOM

#if __VER >= 13 // __RAINBOW_RACE
private:
	void	OnRainbowRaceInfo(CAr& ar, DPID, DPID);

public:
	void	SendRainbowRaceReqLoad();
	void	SendRainbowRaceApplication(unsigned long dwPlayerId);
	void	SendRainbowRaceFailedUser(unsigned long dwPlayerId);
	void	SendRainbowRaceRanking(unsigned long dwPlayerId, int nRanking);
#endif // __RAINBOW_RACE

#if __VER >= 13 // __HOUSING
	void	OnHousingLoadInfo(CAr& ar, DPID, DPID);
	void	OnHousingSetFunitureList(CAr& ar, DPID, DPID);
	void	OnHousingSetupFuniture(CAr& ar, DPID, DPID);
	void	OnHousingSetVisitAllow(CAr& ar, DPID, DPID);
	void	OnHousingDBFailed(CAr& ar, DPID, DPID);
#endif // __HOUSING

#if __VER >= 13 // __COUPLE_1117
	void	SendPropose(unsigned long idProposer, unsigned long idTarget);
	void	SendCouple(unsigned long idProposer, unsigned long idTarget);
	void	SendDecouple(unsigned long idPlayer);
	void	SendClearPropose();
	void	OnProposeResult(CAr& ar, DPID, DPID);
	void	OnCoupleResult(CAr& ar, DPID, DPID);
	void	OnDecoupleResult(CAr& ar, DPID, DPID);
	void	OnCouple(CAr& ar, DPID, DPID);
#if __VER >= 13 // __COUPLE_1202
	void	SendQueryAddCoupleExperience(unsigned long idPlayer, int nExperience);
	void	OnAddCoupleExperience(CAr& ar, DPID, DPID);
#endif	// __COUPLE_1202
#endif	// __COUPLE_1117
#if __VER >= 13 // __HONORABLE_TITLE			// 달인
	void	SendLogGetHonorTime(CMover* pMover, int nGetHonor);
#endif	// __HONORABLE_TITLE			// 달인

#ifdef __FUNNY_COIN
	void	SendFunnyCoinReqUse(unsigned long dwPlayerId, CItemElem* pItemElem);	// 퍼니코인을 사용했음을 TransServer에 알린다.
	void	OnFunnyCoinAckUse(CAr& ar, DPID, DPID);		// 퍼니코인 사용에 대한 응답을 TranServer로 부터 받았다.
#endif // __FUNNY_COIN
#if __VER >= 14 // __PCBANG
	void	OnPCBangToggle(CAr& ar, DPID, DPID);
#endif // __PCBANG
#ifdef __VTN_TIMELIMIT
	void	SendTimeLimitInfoReq(unsigned long dwPlayerId, char* szAccount);
	void	OnTimeLimitInfoAck(CAr& ar, DPID, DPID);
	void	SendTimeLimitUpdateReq(char* szAccount, int nPlayTime);
	void	OnTimeLimitReset(CAr& ar, DPID, DPID);
#endif // __VTN_TIMELIMIT
#if __VER >= 14 // __INSTANCE_DUNGEON
	void	SendLogInstanceDungeon(unsigned long dwDungeonId, unsigned long dwWorldId, unsigned long uChannel, int nDungeonType, char chState);
#endif // __INSTANCE_DUNGEON
#ifdef __QUIZ
	void	OnQuizEventOpen(CAr& ar, DPID, DPID);
	void	OnQuizList(CAr& ar, DPID, DPID);
	void	OnQuizEventNotice(CAr& ar, DPID, DPID);
	void	OnQuizEventChanged(CAr& ar, DPID, DPID);
	void	SendQuizEventOpen(int nType);
	void	SendQuizEventState(int nState, int nChannel, int nWinnerCount = 0, int nQuizCount = 0);
	void	SendQuizEventEntrance(unsigned long idPlayer, int nChannel);
	void	SendQuizEventSelect(unsigned long idPlayer, int nChannel, int nQuizId, int nSelect, int nAnswer);
	void	SendPostPrizeItem(unsigned long idPlayer, unsigned long dwItemId, int nItemNum);
#endif // __QUIZ

#ifdef __ERROR_LOG_TO_DB
	void	SendErrorLogToDB(CUser* pUser, char chType, const char* szError);
#endif // __ERROR_LOG_TO_DB
#if __VER >= 15 // __GUILD_HOUSE
	void	OnLoadGuildHouse(CAr& ar, DPID, DPID);
	void	OnBuyGuildHouse(CAr& ar, DPID, DPID);
	void	OnRemoveGuildHouse(CAr& ar, DPID, DPID);
	void	OnGuildHousePacket(CAr& ar, DPID, DPID);
	void	SendLogGuildFurniture(unsigned long dwGuildId, GH_Fntr_Info& gfi, char chState);
#ifdef __GUILD_HOUSE_MIDDLE
private:
	void	OnGuildHouseTenderInfo(CAr& ar, DPID, DPID);
	void	OnGuildHouseTenderJoin(CAr& ar, DPID, DPID);
	void	OnGuildHouseTenderState(CAr& ar, DPID, DPID);
	void	OnGuildHouseTenderResult(CAr& ar, DPID, DPID);
	void	OnGuildHouseGradeUpdate(CAr& ar, DPID, DPID);
	void	OnGuildHouseLevelUpdate(CAr& ar, DPID, DPID);
	void	OnGuildHouseExpired(CAr& ar, DPID, DPID);
public:
#endif // __GUILD_HOUSE_MIDDLE
#endif // __GUILD_HOUSE

#if __VER >= 15 // __CAMPUS
private:
	void	OnAllCampus(CAr& ar, DPID, DPID);
	void	OnAddCampusMember(CAr& ar, DPID, DPID);
	void	OnRemoveCampusMember(CAr& ar, DPID, DPID);
	void	OnUpdateCampusPoint(CAr& ar, DPID, DPID);
public:
	void	SendAddCampusMember(unsigned long idMaster, int nMasterPoint, unsigned long idPupil, int nPupilPoint);
	void	SendRemoveCampusMember(unsigned long idCampus, unsigned long idPlayer);
	void	SendUpdateCampusPoint(unsigned long idPlayer, int nCampusPoint, bool bAdd, char chState);
#endif // __CAMPUS
};

#endif	// __DPDATABASECLIENT_H__