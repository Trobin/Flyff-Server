#ifndef __LORD_SKILL_EXECUTABLE_H__
#define	__LORD_SKILL_EXECUTABLE_H__

#include "User.h"
#include "lordskill.h"

class CLordSkillComponentItemBuf :
	public CLordSkillComponentExecutable
{
public:
	CLordSkillComponentItemBuf() : CLordSkillComponentExecutable() {}
	virtual	~CLordSkillComponentItemBuf() {}
	virtual	void	Execute(unsigned long idPlayer, unsigned long idTarget, VOID* pParam);
};

class CLordSkillComponentSummon :
	public CLordSkillComponentExecutable
{
public:
	CLordSkillComponentSummon() : CLordSkillComponentExecutable() {}
	virtual	~CLordSkillComponentSummon() {}
	virtual	void	Execute(unsigned long idPlayer, unsigned long idTarget, VOID* pParam);
	virtual	int		IsExecutable(CUser* pUser, const char* szTarget, unsigned long& idTarget);
};

class CLordSkillComponentTeleport :
	public CLordSkillComponentExecutable
{
public:
	CLordSkillComponentTeleport() : CLordSkillComponentExecutable() {}
	virtual	~CLordSkillComponentTeleport() {}
	virtual	void	Execute(unsigned long idPlayer, unsigned long idTarget, VOID* pParam);
	virtual	int		IsExecutable(CUser* pUser, const char* szTarget, unsigned long& idTarget);
};

namespace	lordskill
{
	int		UseRequirements(CUser* pUser, const char* szTarget, int nSkill, unsigned long& idTarget);
};

#endif	// __LORD_SKILL_EXECUTABLE_H__