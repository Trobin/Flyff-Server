#ifndef __SLORD_H__
#define	__SLORD_H__

#if __VER >= 12 // __LORD

#include "lord.h"

////////////////////////////////////////////////////////////////////////////////
class CSLord
	: public CLord
{
public:
	CSLord();
	virtual	~CSLord();
	static	CSLord* Instance(void);
	virtual	void	CreateColleagues(void);
	virtual void	DestroyColleagues(void);
};

////////////////////////////////////////////////////////////////////////////////
class CSElection :
	public IElection
{
public:
	CSElection(CLord* pLord);
	virtual	~CSElection();

protected:
	virtual	bool	DoTestBeginCandidacy(void);
	virtual	bool	DoTestBeginVote(int& nRequirement);
	virtual	bool	DoTestEndVote(unsigned long idPlayer);
	virtual	void	DoAddDepositComplete(unsigned long idPlayer, __int64 iDeposit, time_t tCreate);
	virtual	bool	DoTestSetPledge(unsigned long idPlayer, const char* szPledge);
	virtual	bool	DoTestIncVote(unsigned long idPlayer, unsigned long idElector);
};

////////////////////////////////////////////////////////////////////////////////
class CLEvent :
	public ILordEvent
{
public:
	CLEvent(CLord* pLord);
	virtual	~CLEvent();
protected:
	virtual	bool	DoTestAddComponent(CLEComponent* pComponent);
	virtual	bool	DoTestInitialize(void);
};

////////////////////////////////////////////////////////////////////////////////
class CSLordSkill
	: public CLordSkill
{
public:
	CSLordSkill(CLord* pLord);
	virtual	~CSLordSkill();
	virtual	CLordSkillComponentExecutable* CreateSkillComponent(int nType);	// template method
};

////////////////////////////////////////////////////////////////////////////////
class CUser;
namespace	election
{
	int	AddDepositRequirements(CUser* pUser, __int64 iTotal, __int64& iDeposit);
	int	SetPledgeRequirements(CUser* pUser, int& nCost);
	int	IncVoteRequirements(CUser* pUser, unsigned long idPlayer);
};

namespace	lordevent
{
	int	CreateRequirements(CUser* pUser, int iEEvent, int iIEvent);
};

////////////////////////////////////////////////////////////////////////////////

#endif	// __LORD

#endif	// __SLORD_H__