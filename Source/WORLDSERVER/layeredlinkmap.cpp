#include "stdafx.h"
#include "linkmap.h"
#include "layeredlinkmap.h"

#ifdef __LAYER_1015

CLayeredLinkMap::CLayeredLinkMap()
	:
	m_nLandWidth(0),
	m_nLandHeight(0),
	m_nView(0)
{
}

CLayeredLinkMap::~CLayeredLinkMap()
{
	Release();
}

void CLayeredLinkMap::Init(int nLandWidth, int nLandHeight, int nView, bool bBaseLayer, int nMPU)
{
	m_nLandWidth = nLandWidth;
	m_nLandHeight = nLandHeight;
	m_nView = nView;
	m_iMPU = nMPU;

	if (bBaseLayer)
		CreateLinkMap(nDefaultLayer);

}

void CLayeredLinkMap::Release()
{
	for (MLM::iterator i = m_mapLinkMap.begin(); i != m_mapLinkMap.end(); ++i)
	{
		CLinkMap* pLinkMap = i->second;
		pLinkMap->Release();
		SAFE_DELETE(pLinkMap);
	}
	m_mapLinkMap.clear();
}

int CLayeredLinkMap::GetLinkWidth(unsigned long dwLinkType, int nLinkLevel, int nLayer)
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	return pLinkMap->GetLinkWidth(dwLinkType, nLinkLevel);
}

CObj** CLayeredLinkMap::GetObj(unsigned long dwLinkType, unsigned long dwLinkLevel, int nLayer)
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	return pLinkMap->GetObj(dwLinkType, dwLinkLevel);
}

unsigned long CLayeredLinkMap::CalcLinkLevel(CObj* pObj, float fObjWidth, int nLayer)
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	return pLinkMap->CalcLinkLevel(pObj, fObjWidth);
}

int	CLayeredLinkMap::GetMaxLinkLevel(unsigned long dwLinkType, int nLayer)
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	return pLinkMap->GetMaxLinkLevel(dwLinkType);
}

void CLayeredLinkMap::SetMaxLinkLevel(unsigned long dwLinkType, int nLevel, int nLayer)
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	pLinkMap->SetMaxLinkLevel(dwLinkType, nLevel);
}

bool CLayeredLinkMap::InsertObjLink(CObj* pObj)
{
	CLinkMap* pLinkMap = GetLinkMap(pObj->GetLayer());
	return pLinkMap->InsertObjLink(pObj);
}

bool CLayeredLinkMap::RemoveObjLink2(CObj* pObj)
{
	CLinkMap* pLinkMap = GetLinkMap(pObj->GetLayer());
	return pLinkMap->RemoveObjLink2(pObj);
}

bool CLayeredLinkMap::RemoveObjLink(CObj* pObj)
{
	CLinkMap* pLinkMap = GetLinkMap(pObj->GetLayer());
	return pLinkMap->RemoveObjLink(pObj);
}

CObj* CLayeredLinkMap::GetObjInLinkMap(const D3DXVECTOR3& vPos, unsigned long dwLinkType, int nLinkLevel, int nLayer)
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	return pLinkMap->GetObjInLinkMap(vPos, dwLinkType, nLinkLevel);
}

bool CLayeredLinkMap::SetObjInLinkMap(const D3DXVECTOR3& vPos, unsigned long dwLinkType, int nLinkLevel, CObj* pObj, int nLayer)
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	return pLinkMap->SetObjInLinkMap(vPos, dwLinkType, nLinkLevel, pObj);
}

void CLayeredLinkMap::AddItToView(CCtrl* pCtrl)
{
	CLinkMap* pLinkMap = GetLinkMap(pCtrl->GetLayer());
	pLinkMap->AddItToView(pCtrl);
}

void CLayeredLinkMap::ModifyView(CCtrl* pCtrl)
{
	CLinkMap* pLinkMap = GetLinkMap(pCtrl->GetLayer());
	pLinkMap->ModifyView(pCtrl);
}

bool CLayeredLinkMap::CreateLinkMap(int nLayer)
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	if (pLinkMap)
		return false;
	pLinkMap = new CLinkMap;
	pLinkMap->Init(m_nLandWidth, m_nLandHeight, m_nView, m_iMPU);
	bool bResult = m_mapLinkMap.insert(MLM::value_type(nLayer, pLinkMap)).second;
	return true;
}

CLinkMap* CLayeredLinkMap::GetLinkMap(int nLayer)
{
	MLM::iterator i = m_mapLinkMap.find(nLayer);
	if (i != m_mapLinkMap.end())
		return i->second;
	return NULL;
}

void CLayeredLinkMap::DeleteLinkMap(int nLayer)
{
	MLM::iterator i = m_mapLinkMap.find(nLayer);
	if (i != m_mapLinkMap.end())
	{
		CLinkMap* pLinkMap = i->second;
		pLinkMap->Release();
		SAFE_DELETE(pLinkMap);
		m_mapLinkMap.erase(i);
	}
}

void CLayeredLinkMap::Process(CWorld* pWorld)
{
	for (MLM::iterator i = m_mapLinkMap.begin(); i != m_mapLinkMap.end(); )
	{
		int nLayer = i->first;
		CLinkMap* pLinkMap = i->second;
		++i;
		if (nLayer <= 0)
			continue;
		if (pLinkMap->IsInvalid())
		{
			if (pWorld->HasNobody(nLayer))
			{
				pWorld->ReleaseLayer(nLayer);
				TRACE("CWorld.ReleaseLayer( %d, %d )\n", pWorld->GetID(), nLayer);
			}
			else
				pWorld->DriveOut(nLayer);
		}
	}
}

#if __VER >= 15 // __GUILD_HOUSE
void CLayeredLinkMap::Invalidate(int nLayer, bool bInvalid)
#else // __GUILD_HOUSE
void CLayeredLinkMap::Invalidate(int nLayer)
#endif // __GUILD_HOUSE
{
	CLinkMap* pLinkMap = GetLinkMap(nLayer);
	if (pLinkMap)
		pLinkMap->Invalidate(bInvalid);
}

#endif	// __LAYER_1015