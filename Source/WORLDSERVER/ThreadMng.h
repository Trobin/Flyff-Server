#ifndef __THREADMNG_H__
#define __THREADMNG_H__

class	CTimeout
{
protected:
	unsigned long	m_curTime;
	unsigned long	m_endTime;
	unsigned long	m_dwInterval;
public:
	CTimeout(unsigned long dwInterval, unsigned long dwFirstTimeout);
	virtual	~CTimeout() {}
	unsigned long Over(unsigned long dwCurr);		// if > 0, over
	unsigned long Over(void);		// if > 0, over
	void	Reset(unsigned long dwCurr, unsigned long dwTimeout);
	bool TimeoutReset(void);
	bool TimeoutReset(unsigned long dwCurr);
};

class CRunObject
{
private:
	HANDLE	m_hRunObject;
	HANDLE	m_hClose;
public:
	//
	CRunObject();
	virtual	~CRunObject();
	//	Op
	bool	Init(void);
	void	Close(void);
	//
	static	unsigned int	_Run(LPVOID pParam);
	static	CRunObject* GetInstance(void);
	void	Run(void);
	void	ToggleProfiling();
};

#endif	// __THREADMNG_H__