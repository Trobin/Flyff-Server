#ifndef __USERMACRO_H__
#define __USERMACRO_H__

#define ALLOCBLOCK( ar )	\
							CAr ar;

#define	GETBLOCK( ar, lpBuf, nBufSize )	\
										int nBufSize;	\
										LPBYTE lpBuf	= ar.GetBuffer( &nBufSize );

#define	USERPTR							( _it->second )

#define FOR_VISIBILITYRANGE( pCtrl )	\
										map< unsigned long, CUser* >::iterator _it  = pCtrl->m_2pc.begin(); \
										map< unsigned long, CUser* >::iterator _end = pCtrl->m_2pc.end(); \
										while( _it != _end )	\
										{

#define	NEXT_VISIBILITYRANGE( pCtrl )	\
											++_it;	\
										}
#endif // USERMACRO_H