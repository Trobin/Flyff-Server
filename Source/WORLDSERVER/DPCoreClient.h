#ifndef __DPCORECLIENT_H__
#define __DPCORECLIENT_H__

#include "DPMng.h"
#include "MsgHdr.h"
#include "WorldServer.h"
#include "misc.h"

#include "guild.h"

#if __VER >= 14 // __INSTANCE_DUNGEON
#include "InstanceDungeonBase.h"
#endif // __INSTANCE_DUNGEON

#define BEFOREPASS( ar, dw, wWorldSrvr, wCacheSrvr, dpid, objid )	\
	BEFORESENDDUAL( ar, PACKETTYPE_PASSAGE, MAKELONG( wWorldSrvr, wCacheSrvr ), dpid );	\
	ar << objid << dw;

#define PASS( ar )	\
	SEND( ar, this, DPID_SERVERPLAYER );

#define	BEFOREBROADCAST( ar, dw )	\
	BEFORESENDDUAL( ar, PACKETTYPE_BROADCAST, DPID_UNKNOWN, DPID_UNKNOWN );	\
	ar << dw;

#define	BROADCAST( ar )	\
	SEND( ar, this, DPID_SERVERPLAYER );


#undef	theClass
#define theClass	CDPCoreClient
#undef theParameters
#define theParameters CAr & ar, DPID, DPID, OBJID objid

class CDPCoreClient : public CDPMng
{
private:
	WSAEVENT	m_hWait;
	unsigned long		m_uRecharge;
	bool		m_bAlive;
private:
	void	MyRegister(unsigned long uKey);
	void	Recharge(unsigned long uBlockSize);
	bool	Contribute(CUser* pUser, unsigned long dwPxpCount, unsigned long dwPenya);

public:

public:
	//	Constructions
	CDPCoreClient();
	virtual	~CDPCoreClient();
	//	Override
	virtual	void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	virtual	void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	//	Operations
	bool	Run(LPSTR lpszAddr, USHORT uPort, unsigned long uKey);
	bool	CheckIdStack(void);

	void	SendJoin(unsigned long idPlayer, const char* szPlayer, bool bOperator);
	void	SendSystemMessage(const char* lpsz);
	//	Operator commands
	void	SendWhisper(unsigned long idFrom, unsigned long idTo, const char* lpString);

	void	SendSay(unsigned long idFrom, unsigned long idTo, const char* lpString);
	void	SendModifyMode(unsigned long dwMode, BYTE fAdd, unsigned long idFrom, unsigned long idTo);
	void	SendShout(CUser* pUser, const char* lpString);
	void	SendPartyChat(CUser* pUser, const char* lpString);

#if __VER >= 12 // __JHMA_VER12_1	//12차 극단유료아이템
	void	SendUserPartySkill(unsigned long uidPlayer, int nMode, unsigned long dwSkillTime, int nRemovePoint, int nCachMode);
#else	//12차 극단유료아이템
	void	SendUserPartySkill(unsigned long uidPlayer, int nMode, unsigned long dwSkillTime, int nRemovePoint);
#endif // //12차 극단유료아이템

	void	SendGMSay(unsigned long idPlayer, unsigned long dwWorldID, const char* lpString);
	void	SendPlayMusic(unsigned long dwWorldID, unsigned long idmusic);
	void	SendPlaySound(unsigned long dwWorldID, unsigned long idsound);
#ifdef __LAYER_1015
	void	SendSummonPlayer(unsigned long idOperator, unsigned long dwWorldID, const D3DXVECTOR3& vPos, unsigned long idPlayer, int nLayer);
#else	// __LAYER_1015
	void	SendSummonPlayer(unsigned long idOperator, unsigned long dwWorldID, const D3DXVECTOR3& vPos, unsigned long idPlayer);
#endif	// __LAYER_1015
	void	SendTeleportPlayer(unsigned long idOperator, unsigned long idPlayer);
	void	SendKillPlayer(unsigned long idOperator, unsigned long idPlayer);
	void	SendGetPlayerAddr(unsigned long idOperator, unsigned long idPlayer);
	void	SendGetPlayerCount(unsigned long idOperator);
	void	SendGetCorePlayer(unsigned long idOperator);
	void	SendSystem(const char* lpString);
	void	SendToServer(LPBYTE lpBuffer, unsigned long uBufSize, DPID dpidCache, DPID dpidUser);

	void	SendCaption(const char* lpString, unsigned long dwWorldId = 0, bool bSmall = false);

	void	SendEventRealItem(unsigned long uIdPlayer, int nRealItemIndex, int nRealItemCount);
#if __VER < 11 // __SYS_PLAYER_DATA
	void	SendPartyMemberLevel(CUser* pUser);
	void	SendPartyMemberJob(CUser* pUser);
	void	SendFriendChangeJob(CUser* pUser);
	void	SendGuildChangeJobLevel(CUser* pUser);
#endif	// __SYS_PLAYER_DATA
	void	SendPartyLevel(CUser* pUser, unsigned long dwLevel, unsigned long dwPoint, unsigned long dwExp);
	void	SendPartyMemberFlightLevel(CUser* pUser);
#if __VER >= 12 // __JHMA_VER12_1	//12차 극단유료아이템
	void	SendAddPartyExp(unsigned long uPartyId, int nMonLv, bool bSuperLeader, bool bLeaderSMExpUp);
#else // //12차 극단유료아이템
	void	SendAddPartyExp(unsigned long uPartyId, int nMonLv, bool bSuperLeader);
#endif // //12차 극단유료아이템

	void	SendRemovePartyPoint(unsigned long uPartyId, int nRemovePoint);
	void	SendGameRate(float fRate, BYTE nFlag);
	void	SendLoadConstant();
	void	SendSetMonsterRespawn(unsigned long uidPlayer, unsigned long dwMonsterID, unsigned long dwRespawnNum, unsigned long dwAttackNum, unsigned long dwRect, unsigned long dwRespawnTime, bool bFlying);
	void	SendFallSnow();
	void	SendFallRain();
	void	SendStopSnow();
	void	SendStopRain();
	//	void	SendSetPointParam( CMover* pMover, int nParam, int nVal );
	void	SendGuildCombatState(int nState);
	void	SendGCRemoveParty(unsigned long uidPartyid, unsigned long uidPlayer);
	void	SendGCAddParty(unsigned long idLeader, long nLeaderLevel, long nLeaderJob, unsigned long dwLSex,
		unsigned long idMember, long nMemberLevel, long nMemberJob, unsigned long dwMSex);
	void	OnCWWantedList(CAr& ar, DPID, DPID, DPID);
	void	OnCWWantedReward(CAr& ar, DPID, DPID, DPID);
	void	SendSetPartyDuel(unsigned long idParty1, unsigned long idParty2, bool bDuel);
	void	SendCreateGuild(GUILD_MEMBER_INFO* info, int nSize, const char* szGuild);
	void	SendGuildChat(CUser* pUser, const char* sChat);
	bool	SendGuildStat(CUser* pUser, GUILD_STAT stat, unsigned long data);
	void	SendGuildGetPay(unsigned long uGuildId, unsigned long nGoldGuild);
	void	SendGuildMsgControl_Bank_Item(CUser* pUser, CItemElem* pItemElem, BYTE p_Mode);
	void	SendGuildMsgControl_Bank_Penya(CUser* pUser, unsigned long p_Penya, BYTE p_Mode, BYTE cbCloak);
	void	SendWarDead(unsigned long idPlayer);
	void	SendWarMasterAbsent(unsigned long idWar, bool bDecl);
	void	SendWarTimeout(unsigned long idWar);
	void	OnWarDead(CAr& ar, DPID, DPID, OBJID);
	void	OnWarEnd(CAr& ar, DPID, DPID, OBJID);
	void	SendAddFriendNameReqest(unsigned long uLeaderid, long nLeaderJob, BYTE nLeaderSex, unsigned long uMember, const char* szLeaderName, const char* szMemberName);
	void	SendBlock(BYTE nGu, unsigned long uidPlayerTo, char* szNameTo, unsigned long uidPlayerFrom);
	void	SendWCWantedGold(const char* szPlayer, unsigned long idPlayer, int nGold, const char* szMsg);
#ifdef __LAYER_1015
	void	SendWCWantedReward(unsigned long idPlayer, unsigned long idAttacker, const char* szFormat, unsigned long dwWorldID, const D3DXVECTOR3& vPos, int nLayer);
#else	// __LAYER_1015
	void	SendWCWantedReward(unsigned long idPlayer, unsigned long idAttacker, const char* szFormat, unsigned long dwWorldID, const D3DXVECTOR3& vPos);
#endif	// __LAYER_1015
	void	SendQuerySetGuildName(unsigned long idPlayer, unsigned long idGuild, const char* lpszGuild, BYTE nId);
	void	SendSetSnoop(unsigned long idPlayer, unsigned long idSnoop, bool bRelease);
	void	OnSetSnoop(CAr& ar, DPID, DPID, OBJID);
	void	SendSetSnoopGuild(unsigned long idGuild, bool bRelease);
	void	SendChat(unsigned long idPlayer1, unsigned long idPlayer2, const char* lpszChat);
	void	SendPing(void);
	void	OnDestroyPlayer(CAr& ar, DPID, DPID, OBJID);
#if __VER >= 14 // __INSTANCE_DUNGEON
private:
	void	OnInstanceDungeonAllInfo(CAr& ar, DPID, DPID, OBJID);
	void	OnInstanceDungeonCreate(CAr& ar, DPID, DPID, OBJID);
	void	OnInstanceDungeonDestroy(CAr& ar, DPID, DPID, OBJID);
	void	OnInstanceDungeonSetCoolTimeInfo(CAr& ar, DPID, DPID, OBJID);
	void	OnInstanceDungeonDeleteCoolTimeInfo(CAr& ar, DPID, DPID, OBJID);
public:
	void	SendInstanceDungeonCreate(int nType, unsigned long dwDungeonId, ID_INFO& ID_Info);
	void	SendInstanceDungeonDestroy(int nType, unsigned long dwDungeonId, ID_INFO& ID_Info);
	void	SendInstanceDungeonSetCoolTimeInfo(int nType, unsigned long dwPlayerId, COOLTIME_INFO& CT_Info);
#endif // __INSTANCE_DUNGEON
#ifdef __QUIZ
public:
	void	SendQuizSystemMessage(int nDefinedTextId, bool bAll = false, int nChannel = 0, int nTime = 0);
#endif // __QUIZ

protected:
	// Handlers
	USES_PFNENTRIES;

	void	OnLoadWorld(CAr& ar, DPID, DPID, OBJID);
	void	OnQueryTickCount(CAr& ar, DPID, DPID, OBJID);
	void	OnRecharge(CAr& ar, DPID, DPID, OBJID);
	void	OnModifyMode(CAr& ar, DPID, DPID, OBJID objid);

	void	OnSetPartyExp(CAr& ar, DPID, DPID, OBJID objid);
	void	OnRemovePartyPoint(CAr& ar, DPID, DPID, OBJID objid);
	void	OnPartyChangeTroup(CAr& ar, DPID, DPID, OBJID);
	void	OnPartyChangeName(CAr& ar, DPID, DPID, OBJID);
	void	OnAddFriend(CAr& ar, DPID, DPID, OBJID);
	void	OnRemovefriend(CAr& ar, DPID, DPID, OBJID);
	void	OnShout(CAr& ar, DPID, DPID, OBJID);
	void	OnPlayMusic(CAr& ar, DPID, DPID, OBJID);
	void	OnPlaySound(CAr& ar, DPID, DPID, OBJID);
	void	OnErrorParty(CAr& ar, DPID, DPID, OBJID);
	void	OnAddPartyMember(CAr& ar, DPID, DPID, OBJID);
	void	OnRemovePartyMember(CAr& ar, DPID, DPID, OBJID);
	void	OnAddPlayerParty(CAr& ar, DPID, DPID, OBJID);
	void	OnRemovePlayerParty(CAr& ar, DPID, DPID, OBJID);
#if __VER < 11 // __SYS_PLAYER_DATA
	void	OnPartyMemberLevel(CAr& ar, DPID, DPID, OBJID);
	void	OnPartyMemberJob(CAr& ar, DPID, DPID, OBJID);
	void	OnChangeGuildJobLevel(CAr& ar, DPID, DPID, OBJID);
	void	OnFriendChangeJob(CAr& ar, DPID, DPID, OBJID);
#endif	// __SYS_PLAYER_DATA
	void	OnGuildMemberLv(CAr& ar, DPID, DPID, OBJID);
	void	OnSetPartyMode(CAr& ar, DPID, DPID, OBJID);
	void	OnPartyChangeItemMode(CAr& ar, DPID, DPID, OBJID);
	void	OnPartyChangeExpMode(CAr& ar, DPID, DPID, OBJID);

#ifdef __ENVIRONMENT_EFFECT
	void	OnEnvironmentEffect(CAr& ar, DPID, DPID, OBJID);
#else // __ENVIRONMENT_EFFECT
	void	OnEnvironmentSnow(CAr& ar, DPID, DPID, OBJID);
	void	OnEnvironmentRain(CAr& ar, DPID, DPID, OBJID);
	void	OnEnvironmentAll(CAr& ar, DPID, DPID, OBJID);
#endif // __ENVIRONMENT_EFFECT

	void	OnPartyChat(CAr& ar, DPID, DPID, OBJID);
	void	OnCreateGuild(CAr& ar, DPID, DPID, OBJID);
	void	OnDestroyGuild(CAr& ar, DPID, DPID, OBJID);
	void	OnAddGuildMember(CAr& ar, DPID, DPID, OBJID);
	void	OnRemoveGuildMember(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildClass(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildNickName(CAr& ar, DPID, DPID, OBJID);
	void	OnChgMaster(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildMemberLogOut(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildLogoACK(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildContributionACK(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildNoticeACk(CAr& ar, DPID, DPID, OBJID);
	void	OnAddVoteResultACk(CAr& ar, DPID, DPID, OBJID);
	void	OnModifyVote(CAr& ar, DPID, DPID, OBJID);

	void	OnGuildAuthority(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildPenya(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildRealPenya(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildSetName(CAr& ar, DPID, DPID, OBJID);
	void	OnGuildMsgControl(CAr& ar, DPID, DPID, OBJID);
	void	OnAcptWar(CAr& ar, DPID, DPID, OBJID);
	void	OnSurrender(CAr& ar, DPID, DPID, OBJID);

	void	OnSetPointParam(CAr& ar, DPID, DPID, OBJID);
	void	OnFriendInterceptState(CAr& ar, DPID, DPID, OBJID);
	void	OnSetFriendState(CAr& ar, DPID, DPID, OBJID);
	void	OnPartyChangeLeader(CAr& ar, DPID, DPID, OBJID);

	void	OnLoadConstant(CAr& ar, DPID, DPID, OBJID);
	void	OnGameRate(CAr& ar, DPID, DPID, OBJID);
	void	OnSetMonsterRespawn(CAr& ar, DPID, DPID, OBJID);
	void	OnSetPlayerName(CAr& ar, DPID, DPID, OBJID);
#ifdef __EVENT0913
	void	OnEvent0913(CAr& ar, DPID, DPID, DPID);
#endif	// __EVENT0913
#ifdef __EVENT1206
	void	OnEvent1206(CAr& ar, DPID, DPID, DPID);
#endif	// __EVENT1206
	void	OnEvent(CAr& ar, DPID, DPID, DPID);
	void	OnGuildCombatState(CAr& ar, DPID, DPID, DPID);
	void	OnRemoveUserFromCORE(CAr& ar, DPID, DPID, DPID);
	void	OnPing(CAr& ar, DPID, DPID, DPID);
};

#endif	// __DPCORECLIENT_H__