#ifndef __MYSPLASHWND_H__
#define __MYSPLASHWND_H__

#pragma once

class CMySplashWnd : public CWnd
{
	unsigned int	m_nBitmapID;
	unsigned int	m_nDuration;
	unsigned int	m_nTimerID;
	CBitmap	m_bitmap;
	CPalette	m_pal;
	CWnd	m_wndInvisible;

public:
	// Construction
	CMySplashWnd(unsigned int nBitmapID, unsigned int nDuration = 2500);

	// Attributes
public:
	bool	Create(void);

	// Operations
public:
	virtual	int	PreTranslateMessage(MSG* pMsg);

	// Implementation
public:
	afx_msg	void	OnPaint(void);
	afx_msg	void	OnTimer(unsigned int nIDEvent);

	DECLARE_MESSAGE_MAP()

protected:
	bool	GetBitmapAndPalette(unsigned int nIDResource, CBitmap& bitmap, CPalette& pal);

};

#endif	// __MYSPLASHWND_H__