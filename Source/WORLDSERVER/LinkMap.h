#ifndef __LINKMAP_H__20041210
#define __LINKMAP_H__20041210

class CObj;

struct LINKMAP_INFO
{
	int			nMaxLevel;
	int			nWidth[MAX_LINKLEVEL];
	CObj** apObj[MAX_LINKLEVEL];
};

class CLinkMap
{
public:
	CLinkMap();

private:
	LINKMAP_INFO	m_infos[MAX_LINKTYPE];
	int			m_nLandWidth, m_nLandHeight;
	bool* m_apfMask[MAX_LINKLEVEL];
	int			m_nVisibilityRange[MAX_LINKLEVEL];

	int			m_iMPU;		//MPU of the world

public:
	void		Init(int nLandWidth, int nLandHeight, int nView, int nMPU);
	void		Release();

	int			GetLinkWidth(unsigned long dwLinkType, int dwLinkLevel);
	CObj** GetObj(unsigned long dwLinkType, unsigned long dwLinkLevel);
	unsigned long		CalcLinkLevel(CObj* pObj, float fObjWidth);

	int			GetMaxLinkLevel(unsigned long dwLinkType);
	void		SetMaxLinkLevel(unsigned long dwLinkType, int nLevel);

	bool		InsertObjLink(CObj* pObj);
	bool		RemoveObjLink2(CObj* pObj);
	bool		RemoveObjLink(CObj* pObj);
	CObj* GetObjInLinkMap(const D3DXVECTOR3& vPos, unsigned long dwLinkType, int nLinkLevel);
	bool		SetObjInLinkMap(const D3DXVECTOR3& vPos, unsigned long dwLinkType, int nLinkLevel, CObj* pObj);

	void		AddItToView(CCtrl* pCtrl);
	void		ModifyView(CCtrl* pCtrl);

#ifdef __LAYER_1015
#if __VER >= 15 // __GUILD_HOUSE
	void	Invalidate(bool bInvalid) { m_bInvalid = bInvalid; }
#else // __GUILD_HOUSE
	void	Invalidate() { m_bInvalid = true; }
#endif // __GUILD_HOUSE
	bool	IsInvalid() { return m_bInvalid; }
#endif	// __LAYER_1015

private:
	int			IsOverlapped(int c, int p, int r, int w);
	CObj** GetObjPtr(const D3DXVECTOR3& vPos, unsigned long dwLinkType, int nLinkLevel);

#ifdef __LAYER_1015
private:
	bool	m_bInvalid;
#endif	// __LAYER_1015
};

inline int CLinkMap::GetMaxLinkLevel(unsigned long dwLinkType)
{
	//assert( 0 < m_infos[dwLinkType].nMaxLevel );
	return m_infos[dwLinkType].nMaxLevel;
}

inline int	CLinkMap::GetLinkWidth(unsigned long dwLinkType, int dwLinkLevel)
{
	return m_infos[dwLinkType].nWidth[dwLinkLevel];
}

inline CObj** CLinkMap::GetObj(unsigned long dwLinkType, unsigned long dwLinkLevel)
{
	return m_infos[dwLinkType].apObj[dwLinkLevel];
}

#endif  //__LINKMAP_H__20041210 