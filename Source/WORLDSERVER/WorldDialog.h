#ifndef __WORLDDIALOG_H__
#define __WORLDDIALOG_H__

#include "FunctionsInfo.h"

class CNpcDialogInfo : public NPCDIALOG_INFO
{
public:
	int			m_nValue;
	int			m_nSrcId;
	int			m_nDstId;
	int			m_nQuestId;
	int* m_pnGlobal;
	const char*		m_pszName;
	Functions* m_pFuctions;

	virtual int			GetReturn() { return m_nValue; }
	virtual int			GetDstId() { return m_nDstId; }
	virtual int			GetSrcId() { return m_nSrcId; }
	virtual int			GetCaller() { return WES_DIALOG; }	// WES_DIALOG - 5
	virtual int			GetQuest() { return m_nQuestId; }
	virtual int			GetNpcId() { return GetDstId(); }
	virtual int			GetPcId() { return GetSrcId(); }
	virtual int* GetGlobal() { return m_pnGlobal; }
	virtual const char*		GetName() { return m_pszName; }
	virtual Functions* GetFunctions() { return m_pFuctions; }
};

typedef int(WINAPI* PFRUNDIALOG)(const char* szNpc, const char* szKey, NPCDIALOG_INFO* pInfo);
typedef int(WINAPI* PFFINDSCIRPTKEY)(const char* szNpc, const char* szKey);
typedef void (WINAPI* PFSETLANGUAGE)(int nLanguage);
typedef bool(WINAPI* PFINITWORLDDIALOG)();

class CWorldDialog
{
private:
	CWorldDialog();
public:
	~CWorldDialog();

private:
	HMODULE			m_hDLL;
	PFRUNDIALOG		m_pfnRunDialog;
	PFFINDSCIRPTKEY	m_pfnFindScriptKey;
	PFINITWORLDDIALOG	m_pfnInitWorldDialog;

private:
	bool			Load();
	void			Free();


public:
	bool			Init();
	int				Run(const char* szName, const char* szKey, NPCDIALOG_INFO* pInfo);
	bool			Find(const char* szName, const char* szKey);
	bool			Reload();

	static CWorldDialog& GetInstance();
};

#endif // __WORLDDIALOG_H__