#ifndef __DPSOCK_H__
#define __DPSOCK_H__

#pragma once

#include "serversock.h"
#include "clientsock.h"
#include "serversocke.h"
#include "clientsocke.h"

#define	NEWSOCKETMSG	(unsigned long)-3
#define	CLOSEIOWORKERMSG	(unsigned long)-2
#define	INVALIDSOCKMSG	(unsigned long)-4
#define NEWSOCKETMSG2	(unsigned long)-5

class CDPSock
{
private:
	CSock* m_pSock;
	unsigned short	m_uPort;
	char	m_lpAddr[32];
	unsigned long	m_uIoWorker;
	list<HANDLE>	m_listthread;
	CMclCritSec		m_locklistthread;
	HANDLE* m_phCompletionPort;
	WSAEVENT	m_hClose;
	HANDLE	m_hRecv;
	CSock* m_pSockThreaded;
public:
	CBufferQueue	m_lspRecvBuffer;
	bool	m_fServer;
#ifdef __CRC
	unsigned long	m_dwReadHeaderSize;
	unsigned long	m_dwDataSizeOffset;
#endif	// __CRC
#ifdef __PROTOCOL0910
	HANDLE	m_hProtocolId;
#endif	// __PROTOCOL0910
	long	m_lActiveIoWorker;

private:
	//	Operations
	bool	CreateIoWorker(unsigned long uIoWorker);
	bool	CloseIoWorker(void);
public:
	//	Constructions
	CDPSock();
	virtual	~CDPSock();
	//	Operations
	bool	Close(void);
	void	CloseConnection(DPID dpid);
	bool	Shutdown(DPID dpid);
	HANDLE	GetCompletionPort(int iIoWorker);
#ifdef __INFO_SOCKLIB0516
	unsigned long	GetDebugInfo(DPID dpid);
#endif	// __INFO_SOCKLIB0516
	HANDLE	GetCloseHandle(void);
	HANDLE	GetRecvHandle(void);
	CSock* GetSockThreaded(void);
	int		GetIoWorkerCount(void);
	bool	CreateEventWorker(CSock* pSock);
	void	RemoveThread(HANDLE hThread);
	CClientSock* Get(SOCKET hSocket);

	bool	InitializeConnection(LPVOID lpConnection, unsigned long dwFlags);
#ifdef __CRC
	bool	CreateServer(unsigned long dwcrc);
	bool	JoinToServer(unsigned long dwcrc, unsigned long uWaitingTime = 10000);
	bool	CreateServerE(unsigned long dwcrc);
	bool	JoinToServerE(unsigned long dwcrc, unsigned long uWaitingTime = 10000);
#else	// __CRC
	bool	CreateServer(BUFFER_TYPE nBufferType);
	bool	JoinToServer(BUFFER_TYPE nBufferType);
	bool	CreateServerE(BUFFER_TYPE nBufferType);
	bool	JoinToServerE(BUFFER_TYPE nBufferType);
#endif	// __CRC
	bool	Send(char* lpData, unsigned long dwDataSize, DPID dpidTo);

	HRESULT	GetPlayerAddr(DPID dpid, LPVOID lpAddr, LPDWORD lpdwSize);
	unsigned long	GetPlayerAddr(DPID dpid);

	HRESULT	GetHostAddr(LPVOID lpAddr, LPDWORD lpdwSize);
	CBuffer* Receive();

#ifdef __PROTOCOL0910
	void	SetProtocolId(unsigned long dwProtocolId);
#endif	// __PROTOCOL0910

	void	AddCreatePlayerOrGroupMsg(DPID dpid, CBuffer* pBuffer);
	void	AddDestroyPlayerOrGroupMsg(DPID dpid, CBuffer* pBuffer);

	CSock* GetServerSock(void) { return	(m_fServer ? m_pSock : NULL); }
};

inline HANDLE CDPSock::GetCompletionPort(int iIoWorker)
{
	return m_phCompletionPort[iIoWorker];
}
inline HANDLE CDPSock::GetCloseHandle(void)
{
	return m_hClose;
}
inline HANDLE CDPSock::GetRecvHandle(void)
{
	return m_hRecv;
}
inline int	CDPSock::GetIoWorkerCount(void)
{
	return (int)m_uIoWorker;
}
inline CSock* CDPSock::GetSockThreaded(void)
{
	return m_pSockThreaded;
}
inline CClientSock* CDPSock::Get(SOCKET hSocket)
{
	return (m_pSock ? (CClientSock*)m_pSock->Get(hSocket) : NULL);
}

inline HRESULT	CDPSock::GetHostAddr(LPVOID lpAddr, LPDWORD lpdwSize)
{
	return(m_pSock ? m_pSock->GetHostAddr(lpAddr, lpdwSize) : DPERR_NOSESSIONS);
}
inline HRESULT CDPSock::GetPlayerAddr(DPID dpid, LPVOID lpAddr, LPDWORD lpdwSize)
{
	return m_pSock->GetPeerAddr(dpid, lpAddr, lpdwSize);
}
inline unsigned long CDPSock::GetPlayerAddr(DPID dpid)
{
	return m_pSock->GetPeerAddr(dpid);
}

#endif //__DPSOCK_H__