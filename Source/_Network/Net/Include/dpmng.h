#ifndef __DPMNG_H__
#define __DPMNG_H__

#pragma once

#include "dpsock.h"
#include "ar.h"
#include "mymap.h"

extern void UninitializeNetLib();
extern bool InitializeNetLib();
extern	void	TestNetLib(const char* lpAddr, unsigned short uPort);

#define LOAD_WS2_32_DLL	\
	WSADATA wsaData;	\
	int err;	\
	err	= WSAStartup( 0x0202, &wsaData );	\
	if( err == SOCKET_ERROR )	\
	{	\
		TRACE( "WSAStartup() failed with error %ld\n", WSAGetLastError() );	\
		return false;	\
	}

#define UNLOAD_WS2_32_DLL	\
	WSACleanup();

#define BEFORESEND( ar, dw )	\
	CAr ar;	\
	int nBufSize;	\
	ar << dw;

#define	BEFORESENDSOLE( ar, dw, dpid_ )	\
	CAr ar;		\
	int nBufSize;	\
	ar << dpid_ << dw;

#define BEFORESENDDUAL( ar, dw, dpid_, dpid__ )	\
	CAr ar;		\
	int nBufSize;	\
	ar << dpid_ << dpid__ << dw;

#define	SEND( ar, pDPMng, idTo ) \
	LPBYTE lpBuf	= ar.GetBuffer( &nBufSize );	\
	(pDPMng)->Send( (LPVOID)lpBuf, nBufSize, idTo );

#define GETTYPE(ar)		\
	unsigned long dw;	\
	ar >> dw;

#ifdef __STL_0402
#define	USES_PFNENTRIES	\
		private:	\
		map<unsigned long, void (theClass::*)( theParameters )>	m_pfnEntries;	\
		void ( theClass::*GetHandler( unsigned long dwType ) )( theParameters )	\
			{	\
				map<unsigned long, void (theClass::*)( theParameters )>::iterator i = m_pfnEntries.find( dwType );	\
				if( i != m_pfnEntries.end() )	\
					return i->second;	\
				return NULL;	\
			}

#define BEGIN_MSG	\
		void ( theClass::*pfn )( theParameters );

#define	ON_MSG( dwKey, hndlr )	\
		pfn		= hndlr;	\
		m_pfnEntries.insert( map<unsigned long, void (theClass::*)( theParameters )>::value_type( dwKey, pfn ) );

#else	// __STL_0402
#define	USES_PFNENTRIES	\
		private:	\
		CMyMap<void (theClass::*)( theParameters )>	m_pfnEntries;	\
		void ( theClass::*GetHandler( unsigned long dwType ) )( theParameters )	\
			{ void ( theClass::*pfn )( theParameters );	return ( m_pfnEntries.Lookup( dwType, pfn ) ? pfn : NULL ); }

#define BEGIN_MSG	\
		m_pfnEntries.SetSize( 64, 128, 64 );	\
		void ( theClass::*pfn )( theParameters );

#define	ON_MSG( dwKey, hndlr )	\
		pfn		= hndlr;	\
		m_pfnEntries.SetAt( dwKey, pfn );
#endif	// __STL_0402

class CDPMng
{
private:
	CDPSock* m_pDPSock;
	HANDLE	m_hRecvThread;
	HANDLE	m_hClose;
public:
	//	Constructions
	CDPMng();
	virtual	~CDPMng();
	//	Operations
	HANDLE	GetRecvHandle(void);
	HANDLE	GetCloseHandle(void);
	bool	CreateDPObject(bool bSingleThreaded = false);
	bool	DeleteDPObject(void);
	bool	InitConnection(LPVOID lpConnection, unsigned short uPort = 0);
	bool	ReceiveMessage(void);

#ifdef __CRC
	bool	CreateSession(LPCSTR lpSession, unsigned long dwcrc = 0);
	bool	JoinSession(LPCSTR lpSession, unsigned long dwcrc = 0, unsigned long uWaitingTime = 10000);
	bool	StartServer(unsigned short uPort, bool bSingleThreaded = false, unsigned long dwcrc = 0);
	bool	ConnectToServer(LPCSTR lpConnection, unsigned short uPort, bool bSingleThreaded = false, unsigned long dwcrc = 0, unsigned long uWaitingTime = 10000);
#else	// __CRC
	bool	CreateSession(LPCSTR lpSession, BUFFER_TYPE nBufferType = BUFFER_TYPE_5BYTE);
	bool	JoinSession(LPCSTR lpSession, BUFFER_TYPE nBufferType = BUFFER_TYPE_5BYTE);
	bool	StartServer(unsigned short uPort, bool bSingleThreaded = false, BUFFER_TYPE nBufferType = BUFFER_TYPE_5BYTE);
	bool	ConnectToServer(LPCSTR lpConnection, unsigned short uPort, bool bSingleThreaded = false, BUFFER_TYPE nBufferType = BUFFER_TYPE_5BYTE);
#endif	// __CRC
	bool	Send(LPVOID lpData, unsigned long dwDataSize, DPID dpidTo);
	bool	DestroyPlayer(DPID dpid);

#ifdef __INFO_SOCKLIB0516
	unsigned long	GetDebugInfo(DPID dpid);
#endif	// __INFO_SOCKLIB0516

	void	GetHostAddr(LPSTR lpAddr);
	void	GetPlayerAddr(DPID dpid, LPSTR lpAddr);
	unsigned long	GetPlayerAddr(DPID dpid);


	//	Override
	virtual	void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom) = 0;
	virtual	void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom) = 0;
};

inline HANDLE CDPMng::GetRecvHandle(void)
{
	return	m_pDPSock->GetRecvHandle();
}

inline HANDLE CDPMng::GetCloseHandle(void)
{
	return m_hClose;
}

#ifdef __CRC
inline bool CDPMng::StartServer(unsigned short uPort, bool bSingleThreaded, unsigned long dwcrc)
{
	return(CreateDPObject(bSingleThreaded) &&
		InitConnection((LPVOID)NULL, uPort) &&
		CreateSession(NULL, dwcrc));
}
inline bool CDPMng::ConnectToServer(LPCSTR lpConnection, unsigned short uPort, bool bSingleThreaded, unsigned long dwcrc, unsigned long uWaitingTime)
{
	return(CreateDPObject(bSingleThreaded) &&
		InitConnection((LPVOID)lpConnection, uPort) &&
		JoinSession(NULL, dwcrc, uWaitingTime));
}
#else	
inline bool CDPMng::StartServer(unsigned short uPort, bool bSingleThreaded, BUFFER_TYPE nBufferType)
{
	return(CreateDPObject(bSingleThreaded) &&
		InitConnection((LPVOID)NULL, uPort) &&
		CreateSession(NULL, nBufferType));
}
inline bool CDPMng::ConnectToServer(LPCSTR lpConnection, unsigned short uPort, bool bSingleThreaded, BUFFER_TYPE nBufferType)
{
	return(CreateDPObject(bSingleThreaded) &&
		InitConnection((LPVOID)lpConnection, uPort) &&
		JoinSession(NULL, nBufferType));
}
#endif	// __CRC

inline bool CDPMng::Send(LPVOID lpData, unsigned long dwDataSize, DPID dpidTo)
{
	if (!m_pDPSock)
		return false;
	return m_pDPSock->Send((char*)lpData, dwDataSize, dpidTo);
}

#endif //__DPMNG_H__