#ifndef __BUFFER_H__
#define	__BUFFER_H__

#pragma once

#include "mempooler.h"
#include "heapmng.h"
#include <dplay.h>

#ifdef __CRC
#include "crc.h"
using	namespace	std;
#define	HEADERSIZE5		( sizeof(unsigned long) + 1 )
#if defined( __PACKET_REPLAY_R ) || defined( __PACKET_REPLAY_W )
#define	HEADERSIZE13	( sizeof(char) + sizeof(int) + sizeof(unsigned long) + sizeof(unsigned long) + sizeof(unsigned long) )
#else	// __PACKET_REPLAY_R || __PACKET_REPLAY_W
#define	HEADERSIZE13	( sizeof(char) + sizeof(unsigned long) + sizeof(unsigned long) + sizeof(unsigned long) )
#endif	// __PACKET_REPLAY_R || __PACKET_REPLAY_W
#endif	// __CRC

#define	HEADERMARK		'^'		// Normal message header
#define	SYSHEADERMARK	'%'		// System message header
#define	MAX_BUFFER		8192
#define	MAX_WSABUF		128

enum BUFFER_TYPE
{
	BUFFER_TYPE_5BYTE,		// 기존 1 + 4바이트 헤더 방식 
	BUFFER_TYPE_2BYTE,		// 2바이트 헤더 방식 
};



///////////////////////////////////////////////////////////////////////
// CBuffer
///////////////////////////////////////////////////////////////////////

class CBuffer
{
public:
	DPID		dpid;
	unsigned long		cb;
	BYTE		m_lpBuf[MAX_BUFFER];
	LPBYTE		m_lpBufStart;
	LPBYTE		m_lpBufMax;
	LPBYTE		m_pHead;
	LPBYTE		m_pTail;
	CBuffer* pNext;
	CBuffer* pPrevious;
	unsigned long		m_dwHeaderSize;

public:
	//	Constructions
	CBuffer(unsigned long uBufSize);
	virtual	~CBuffer();

	//	Operations
	int		GetSize(void);
	LPBYTE	GetWritableBuffer(int* pnBufSize);
	int		GetWritableBufferSize(void);
	LPBYTE	GetReadableBuffer(int* pnBufSize);
	int		GetReadableBufferSize(void);

	unsigned long	GetHeaderLength()
	{
		return m_dwHeaderSize;
	}

	virtual unsigned long	GetPacketSize(BYTE* ptr)
	{
		return *(UNALIGNED LPDWORD)(ptr + 1);
	}

	virtual void SetHeader(unsigned long uDataSize)
	{
		*m_pTail = HEADERMARK;
		m_pTail++;

		*(UNALIGNED unsigned long*)m_pTail = (unsigned long)uDataSize;
		m_pTail += sizeof(unsigned long);
	}

public:
	static	CHeapMng* m_pHeapMng;
#ifndef __VM_0820
#ifndef __MEM_TRACE
	static	MemPooler<CBuffer>* m_pPool;
	void* operator new(size_t nSize) { return CBuffer::m_pPool->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CBuffer::m_pPool->Alloc(); }
	void	operator delete(void* lpMem) { CBuffer::m_pPool->Free((CBuffer*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CBuffer::m_pPool->Free((CBuffer*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820
};


inline int CBuffer::GetSize(void)
{
	return (int)(m_lpBufMax - m_lpBufStart);
}

inline LPBYTE CBuffer::GetWritableBuffer(int* pnBufSize)
{
	*pnBufSize = (int)(m_lpBufMax - m_pTail);
	return m_pTail;
}

inline int CBuffer::GetWritableBufferSize(void)
{
	return (int)(m_lpBufMax - m_pTail);
}

inline LPBYTE CBuffer::GetReadableBuffer(int* pnBufSize)
{
	*pnBufSize = (int)(m_pTail - m_pHead);
	return m_pHead;
}

inline int CBuffer::GetReadableBufferSize(void)
{
	return (int)(m_pTail - m_pHead);
}

///////////////////////////////////////////////////////////////////////
// CBuffer2
///////////////////////////////////////////////////////////////////////

class CBuffer2 : public CBuffer
{
public:
	CBuffer2(unsigned long uBufSize) : CBuffer(uBufSize)
	{
		m_dwHeaderSize = 2;
	}

	virtual unsigned long	GetPacketSize(BYTE* ptr)
	{
		//		unsigned long dwSize = *(unsigned short *)ptr;
		//		dwSize -= 2;
		//		return dwSize;
		unsigned long dwSize = ntohs(*(unsigned short*)ptr);
		dwSize -= 2;
		return dwSize;
	}

	virtual void SetHeader(unsigned long uDataSize)
	{
		//		*(UNALIGNED unsigned short*)m_pTail	= (unsigned short)(uDataSize + 2);
		//		m_pTail += sizeof(unsigned short);
		*(UNALIGNED unsigned short*)m_pTail = htons((unsigned short)(uDataSize + 2));
		m_pTail += sizeof(unsigned short);
	}

#ifndef __VM_0820
#ifndef __MEM_TRACE
	static	MemPooler<CBuffer2>* m_pPool2;
	void* operator new(size_t nSize) { return CBuffer2::m_pPool2->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CBuffer2::m_pPool2->Alloc(); }
	void	operator delete(void* lpMem) { CBuffer2::m_pPool2->Free((CBuffer2*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CBuffer2::m_pPool2->Free((CBuffer2*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820
};

///////////////////////////////////////////////////////////////////////
// CBufferQueue
///////////////////////////////////////////////////////////////////////

class CBufferQueue
{
private:
	CBuffer* m_pHead;
	CBuffer* m_pTail;
	unsigned long	m_uCount;
public:
	CMclCritSec		m_cs;
#ifdef __CRC
	CRC32* m_pcrc;
	unsigned long	m_dwWriteHeaderSize;
#endif	// __CRC

#ifdef __PROTOCOL0910
	unsigned long* m_pdwProtocolId;
#endif	// __PROTOCOL0910

public:
	//	Constructions
	CBufferQueue();
	virtual	~CBufferQueue();
	//	Operations
	void	AddTail(CBuffer* pBuffer);
	void	RemoveHead(void);
	CBuffer* GetHead(void);
	CBuffer* GetTail(void);
	void	Clear(bool bDelete);
	bool	IsEmpty(void);
	unsigned long	GetCount(void);
	void	AddData(LPBYTE lpData, unsigned long uDataSize, BUFFER_TYPE type);
	void	GetData(LPWSABUF lpBuffers, LPDWORD lpdwBufferCount);
	void	RemoveData(unsigned long uDataSize);
#ifdef __CRC
#ifdef __PROTOCOL0910
	void	crc(CRC32* pcrc, LPDWORD pdwProtocolId) { m_pcrc = pcrc;		m_pdwProtocolId = pdwProtocolId;	m_dwWriteHeaderSize = HEADERSIZE13; }
#else	// __PROTOCOL0910
	void	crc(CRC32* pcrc) { m_pcrc = pcrc;		m_dwWriteHeaderSize = HEADERSIZE13; }
#endif	// __PROTOCOL0910
#endif	// __CRC

#ifdef __PACKET_REPLAY_W
private:
	int		m_nPacket;
#endif	// __PACKET_REPLAY_W
};

inline CBuffer* CBufferQueue::GetHead(void)
{
	return m_pHead;
}
inline CBuffer* CBufferQueue::GetTail(void)
{
	return m_pTail;
}
inline unsigned long CBufferQueue::GetCount(void)
{
	return m_uCount;
}
inline bool CBufferQueue::IsEmpty(void)
{
	return m_uCount == 0;
}
inline void CBufferQueue::AddTail(CBuffer* pBuffer)
{
	CMclAutoLock	Lock(m_cs);
	//	Add it to the active list	
	if (!m_pHead)
		m_pHead = pBuffer;
	if (m_pTail)
		m_pTail->pNext = pBuffer;
	pBuffer->pPrevious = m_pTail;
	pBuffer->pNext = NULL;
	m_pTail = pBuffer;
	m_uCount++;
}
inline void CBufferQueue::RemoveHead(void)
{
	//	CMclAutoLock	Lock( m_cs );
	//	Remove it from active list
	if (!IsEmpty())
	{
		if (m_pTail == m_pHead)
			m_pTail = NULL;
		if (m_pHead->pNext)
			m_pHead->pNext->pPrevious = NULL;
		m_pHead = m_pHead->pNext;
	}
	m_uCount--;
}


///////////////////////////////////////////////////////////////////////
// CBufferFactory
///////////////////////////////////////////////////////////////////////

class CBufferFactory
{
public:
	CBuffer* CreateBuffer(BUFFER_TYPE type, unsigned long uBufSize = MAX_BUFFER);
	static CBufferFactory& GetInstance();

private:
	CBufferFactory();
};

#endif	// __BUFFER_H__