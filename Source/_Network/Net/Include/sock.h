#ifndef __SOCK_H__
#define __SOCK_H__

#pragma once

#include <dplay.h>
#include <list>
using namespace std;

class CSock
{
public:
	enum { crcRead = 1, crcWrite = 2, };
protected:
	SOCKET	m_hSocket;
private:
	DPID	m_dpid;
	DPID	m_dpidpeer;

public:
	//	Constructions
	CSock();
	virtual	~CSock();
	//	Operations
	SOCKET	GetHandle(void);
	void	Clear(void);
	void	SetID(DPID dpid);
	DPID	GetID(void);
	void	SetPeerID(DPID dpid);
	DPID	GetPeerID(void);

	virtual	bool	Create(unsigned short uPort = 0, int type = SOCK_STREAM);
	virtual	void	Attach(SOCKET hSocket);
	virtual	void	Detach(void);
	virtual	HRESULT	GetHostAddr(LPVOID lpAddr, LPDWORD lpdwSize);
	virtual	HRESULT	GetPeerAddr(DPID dpid, LPVOID lpAddr, LPDWORD lpdwSize) = 0;
	virtual	unsigned long	GetPeerAddr(DPID dpid) { return 0; }
	virtual	void	Close(void) = 0;
	virtual	bool	CloseConnection(SOCKET hSocket) = 0;
	virtual	bool	Shutdown(SOCKET hSocket) = 0;
	virtual	void	Send(char* lpData, unsigned long dwDataSize, DPID dpidTo) = 0;
	virtual	CSock* Get(SOCKET hSocket);
#ifdef __PROTOCOL0910
	virtual	void	SetProtocolId(unsigned long dwProtocolId) {}
#endif	// __PROTOCOL0910
#ifdef __INFO_SOCKLIB0516
	virtual	unsigned long	GetDebugInfo(SOCKET hSocket) { return 0; }
#endif	// __INFO_SOCKLIB0516
};
inline SOCKET CSock::GetHandle(void) { return m_hSocket; }
inline CSock* CSock::Get(SOCKET hSocket) { return(m_hSocket == hSocket ? this : NULL); }

inline void	CSock::SetID(DPID dpid) { m_dpid = dpid; }
inline DPID CSock::GetID(void) { return m_dpid; }
inline void CSock::SetPeerID(DPID dpid) { m_dpidpeer = dpid; }
inline DPID CSock::GetPeerID(void) { return m_dpidpeer; }

#endif //__SOCK_H__