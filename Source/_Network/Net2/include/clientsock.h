#ifndef __CLIENTSOCK_H__
#define __CLIENTSOCK_H__

#pragma once

#include "sock.h"
#include "buffer.h"

#include "crc.h"
//using	namespace	CryptoPP;
//using	namespace	std;

template <class T>
class CClientSock : public CSock
{
public:
	T* m_pRecvBuffer;
	WSAOVERLAPPED	m_ovRecv;
	WSAOVERLAPPED	m_ovSend;
	long	m_l;
private:
	CBufferQueue<T>		m_lspSendBuffer;
	WSABUF	m_lpBuffers[MAX_WSABUF];
	CRC32	m_crcRead;
	CRC32	m_crcWrite;
	unsigned long	m_dwReadHeaderSize;
public:
	//	Constructions
	CClientSock(unsigned long dwcrc);
	virtual	~CClientSock();
	//	Operations
	bool	Connect(char* lpAddr, unsigned short uPort);
	int		Recv(void);
	int		SendRemnant(unsigned long dwBytesSent);
	T* Fetch(unsigned long dwBytes);

	virtual	void	Close(void);
	virtual	bool	CloseConnection(SOCKET hSocket);
	virtual	bool	Shutdown(SOCKET hSocket);
	virtual	void	Send(char* lpData, unsigned long dwDataSize, unsigned long dpidTo);
	virtual	HRESULT		GetPeerAddr(unsigned long dpid, LPVOID lpAddr, LPDWORD lpdwSize);
};

template <class T>
CClientSock<T>::CClientSock(unsigned long dwcrc)
{
	m_dwcrc = dwcrc;
	m_l = 0;
	m_pRecvBuffer = new T;

	if (IsCrcWrite())
		m_lspSendBuffer.crc(&m_crcWrite);
}

template <class T>
CClientSock<T>::~CClientSock()
{
	Close();
}

template <class T>
void CClientSock<T>::Close(void)
{
	CLOSE_SOCKET(m_hSocket);
	SAFE_DELETE(m_pRecvBuffer);
}

template <class T>
bool CClientSock<T>::CloseConnection(SOCKET hSocket)
{
	assert(hSocket == m_hSocket);
	Close();
	return true;
}

template <class T>
bool CClientSock<T>::Shutdown(SOCKET hSocket)
{
	return false;
}

template <class T>
bool CClientSock<T>::Connect(char* lpAddr, unsigned short uPort)
{
	int	err;
	SOCKADDR_IN sin;

	if (m_hSocket == INVALID_SOCKET)
	{
		TRACE("Creation needed\n");
		return false;
	}
	sin.sin_family = PF_INET;
	sin.sin_addr.s_addr = inet_addr(lpAddr);

	if (sin.sin_addr.s_addr == INADDR_NONE)
	{
		LPHOSTENT lphost;
		lphost = gethostbyname(lpAddr);
		if (lphost)
			sin.sin_addr.s_addr = ((LPIN_ADDR)lphost->h_addr)->s_addr;
		else
		{
			WSASetLastError(WSAEINVAL);
			return false;
		}
	}

	sin.sin_port = htons(uPort);

	if (connect(m_hSocket, (LPSOCKADDR)&sin, sizeof(sin)) == SOCKET_ERROR)
	{
		if ((err = WSAGetLastError()) == WSAEWOULDBLOCK)
		{
			return true;
		}
		TRACE("Can't connect with error %d\n", err);
		return false;
	}
	return true;
}

template <class T>
void CClientSock<T>::Send(char* lpData, unsigned long dwDataSize, unsigned long dpidTo)
{
	//	assert( m_hSocket != INVALID_SOCKET );
	if (m_hSocket == INVALID_SOCKET)
		return;

	unsigned long dwBufferCount, dwBytes;
	CMclAutoLock	Lock(m_lspSendBuffer.m_cs);
	if (m_lspSendBuffer.IsEmpty())
	{
		m_lspSendBuffer.AddData((LPBYTE)lpData, dwDataSize);
		m_lspSendBuffer.GetData(m_lpBuffers, &dwBufferCount);

		memset((void*)&m_ovSend, 0, sizeof(WSAOVERLAPPED));
		int err;
		InterlockedIncrement(&m_l);
		if (WSASend(GetHandle(), m_lpBuffers, dwBufferCount, &dwBytes, 0, &m_ovSend, NULL) != 0 && (err = WSAGetLastError()) != WSA_IO_PENDING)
		{
			InterlockedDecrement(&m_l);
			//			assert( 0 );
		}
	}
	else
	{
		m_lspSendBuffer.AddData((LPBYTE)lpData, dwDataSize);
	}
}

template <class T>
int CClientSock<T>::Recv(void)
{
	assert(m_hSocket != INVALID_SOCKET);
	unsigned long dwBytesRecvd = 0, dwFlags = 0;

	WSABUF buffer;
	buffer.buf = (char*)m_pRecvBuffer->GetWritableBuffer((int*)&buffer.len);

	memset(&m_ovRecv, 0, sizeof(WSAOVERLAPPED));

	int err;
	if (WSARecv(GetHandle(), &buffer, (unsigned long)1, &dwBytesRecvd, &dwFlags, &m_ovRecv, NULL) != 0 && (err = WSAGetLastError()) != WSA_IO_PENDING)
	{
		TRACE("I/o error, close socket %d, %x //REF:%d\n", GetHandle(), this, m_l - 1);
		InterlockedDecrement(&m_l);
		return err;
	}
	// TOM!
	//for(unsigned long i = 0; i < buffer.len; i++)
	//	buffer.buf[i]--;
	// \TOM!
	return 0;
}

template <class T>
int CClientSock<T>::SendRemnant(unsigned long dwBytes)
{
	assert(m_hSocket != INVALID_SOCKET);
	unsigned long dwBufferCount;

	CMclAutoLock	Lock(m_lspSendBuffer.m_cs);

	m_lspSendBuffer.RemoveData(dwBytes);
	if (m_lspSendBuffer.IsEmpty())
	{
		InterlockedDecrement(&m_l);
		return 0;
	}
	m_lspSendBuffer.GetData(m_lpBuffers, &dwBufferCount);

	memset((void*)&m_ovSend, 0, sizeof(WSAOVERLAPPED));
	int err;
	if (WSASend(GetHandle(), m_lpBuffers, dwBufferCount, &dwBytes, 0, &m_ovSend, NULL) != 0 && (err = WSAGetLastError()) != WSA_IO_PENDING)
	{
		TRACE("i/O error, close socket %d, %x //REF:%d\n", GetHandle(), this, m_l - 1);
		InterlockedDecrement(&m_l);
		return err;
	}
	return 0;
}

template <class T>
T* CClientSock<T>::Fetch(unsigned long dwBytes)
{
	return m_pRecvBuffer->Fetch(&m_pRecvBuffer, dwBytes, (IsCrcRead() ? &m_crcRead : NULL));
	/*
	m_pRecvBuffer->m_pTail	+=	dwBytes;
	assert( m_pRecvBuffer->m_pTail <= m_pRecvBuffer->m_lpBufMax );

	int nRemnant;
	LPBYTE ptr	= m_pRecvBuffer->GetReadableBuffer( &nRemnant );

	CBuffer* pOld	= NULL;
	unsigned long uPacketSize;

	unsigned long dwCrc, dwDataSize;
	unsigned char digest[4];

	while( 1 )
	{
#ifdef _DEBUG
		while( !CBuffer::IsHeader( *ptr ) && nRemnant > 0 )
		{
//			ptr++;
//			nRemnant--;
			WSASetLastError( ERROR_BAD_NET_NAME );
			return NULL;
//			assert( 0 );
		}
#endif

		if( nRemnant < m_dwReadHeaderSize )
		{
			if( m_pRecvBuffer->cb > 0 ) {
				pOld	= m_pRecvBuffer;
				pOld->m_pTail	-= nRemnant;	// remove remnant from old buffer
				m_pRecvBuffer	= new CBuffer;
				assert( m_pRecvBuffer->m_pTail + nRemnant <= m_pRecvBuffer->m_lpBufMax );
				memcpy( m_pRecvBuffer->m_pTail, ptr, nRemnant );
//				if( nRemnant > 0 )
//				{
//					assert( CBuffer::IsHeader( *ptr ) );
//				}
				m_pRecvBuffer->m_pTail	+=	nRemnant;
			}
			return pOld;
		}
		else
		{
			if( m_dwReadHeaderSize == HEADERSIZE13 )
			{
				dwCrc	= *(UNALIGNED LPDWORD)( ptr + sizeof(char) );
				dwDataSize	= *(UNALIGNED LPDWORD)( ptr + sizeof(char) + sizeof(unsigned long) );
				m_crcRead.Restart();
				m_crcRead.Update( (const unsigned char*)( &dwDataSize ), sizeof(unsigned long) );
				m_crcRead.Final( digest );
				if( *(UNALIGNED LPDWORD)digest != dwCrc )
				{
					WSASetLastError( ERROR_BAD_NET_NAME );
					return NULL;
				}
				dwCrc	= *(UNALIGNED LPDWORD)( ptr + sizeof(char) + sizeof(unsigned long) + sizeof(unsigned long) );
				uPacketSize		= m_dwReadHeaderSize + dwDataSize;
			}
			else
			{
				uPacketSize	= m_dwReadHeaderSize + *(UNALIGNED LPDWORD)&ptr[1];
			}
			if( nRemnant < uPacketSize )
			{
				if( uPacketSize > m_pRecvBuffer->GetSize() )
				{
					pOld	= m_pRecvBuffer;
					m_pRecvBuffer	= new CBuffer( uPacketSize );
				}
				else
				{
					if( m_pRecvBuffer->cb > 0 )
					{
						pOld	= m_pRecvBuffer;
						m_pRecvBuffer	= new CBuffer;
					}
				}

				if( pOld )
				{
					assert( m_pRecvBuffer->m_pTail + nRemnant <= m_pRecvBuffer->m_lpBufMax );
					memcpy( m_pRecvBuffer->m_pTail, ptr, nRemnant );
//					if( nRemnant > 0 )
//					{
//						assert( CBuffer::IsHeader( *ptr ) );
//					}
					m_pRecvBuffer->m_pTail	+=	nRemnant;
				}

				return pOld;
			}
			else	// completion
			{
				if( m_dwReadHeaderSize == HEADERSIZE13 )
				{
					m_crcRead.Restart();
					m_crcRead.Update( (const unsigned char*)( ptr + m_dwReadHeaderSize ), dwDataSize );
					m_crcRead.Final( digest );
					if( *(UNALIGNED LPDWORD)digest != dwCrc )
					{
						WSASetLastError( ERROR_BAD_NET_NAME );
						return NULL;
					}
				}
				m_pRecvBuffer->cb++;
				nRemnant	-= ( uPacketSize );
				ptr		+= ( uPacketSize );
			}
		}
	}

	return NULL;
	*/
}

template <class T>
HRESULT CClientSock<T>::GetPeerAddr(unsigned long dpidPlayer, LPVOID lpAddr, LPDWORD lpdwSize)
{
	assert(lpAddr);

	SOCKADDR_IN sin;
	ZeroMemory(&sin, sizeof(SOCKADDR_IN));

	int namelen = sizeof(sin);
	if (getpeername(m_hSocket, (SOCKADDR*)&sin, &namelen) == SOCKET_ERROR)
		return DPERR_GENERIC;

	sprintf((char*)lpAddr, "%d.%d.%d.%d", sin.sin_addr.s_net
		, sin.sin_addr.s_host
		, sin.sin_addr.s_lh
		, sin.sin_addr.s_impno);
	return DP_OK;
}

#endif //__CLIENTSOCK_H__