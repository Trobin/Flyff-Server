#ifndef __DPSOCK_H__
#define __DPSOCK_H__

#pragma once

#include "serversock.h"
#include "clientsock.h"
#include "serversocke.h"
#include "clientsocke.h"

template <class T>
class CDPSock
{
private:
	CSock* m_pSock;
	unsigned short	m_uPort;
	char	m_lpAddr[32];
	unsigned long	m_uIoWorker;
	list<HANDLE>	m_listthread;
	CMclCritSec		m_locklistthread;
	HANDLE	m_hCompletionPort;
	WSAEVENT	m_hClose;
	HANDLE	m_hRecv;
	CSock* m_pSockThreaded;
public:
	CBufferQueue<T>	m_lspRecvBuffer;
	bool	m_fServer;

	//	unsigned long	m_dwReadHeaderSize;
	//	unsigned long	m_dwDataSizeOffset;
private:
	//	Operations
	bool	CreateIoWorker(unsigned long uIoWorker);
	bool	CloseIoWorker(void);
public:
	//	Constructions
	CDPSock();
	virtual	~CDPSock();
	//	Operations
	bool	Close(void);
	void	CloseConnection(unsigned long dpid);
	bool	Shutdown(unsigned long dpid);
	HANDLE	GetCompletionPort(void);
	HANDLE	GetCloseHandle(void);
	HANDLE	GetRecvHandle(void);
	CSock* GetSockThreaded(void);
	int		GetIoWorkerCount(void);
	bool	CreateEventWorker(CSock* pSock);
	void	RemoveThread(HANDLE hThread);
	CClientSock<T>* Get(SOCKET hSocket);

	bool	InitializeConnection(LPVOID lpConnection, unsigned long dwFlags);

	bool	CreateServer(unsigned long dwcrc);
	bool	JoinToServer(unsigned long dwcrc, unsigned long uWaitingTime = 10000);
	bool	CreateServerE(unsigned long dwcrc);
	bool	JoinToServerE(unsigned long dwcrc, unsigned long uWaitingTime = 10000);

	bool	Send(char* lpData, unsigned long dwDataSize, unsigned long dpidTo);

	HRESULT	GetPlayerAddr(unsigned long dpid, LPVOID lpAddr, LPDWORD lpdwSize);
	HRESULT	GetHostAddr(LPVOID lpAddr, LPDWORD lpdwSize);
	HRESULT	Receive(LPDPID lpdpidFrom, LPDPID lpdpidTo, unsigned long dwFlags, T*& pBuffer);

	void	AddCreatePlayerOrGroupMsg(unsigned long dpid);
	void	AddDestroyPlayerOrGroupMsg(unsigned long dpid);

	static	unsigned int	_IoWorkerThread(LPVOID pParam)
	{
		CDPSock<T>* pDPSock = (CDPSock<T>*)pParam;
		return pDPSock->IoWorkerThread();
	}
	static	unsigned int	_EventWorkerThread(LPVOID pParam)
	{
		CDPSock<T>* pDPSock = (CDPSock<T>*)pParam;
		return pDPSock->EventWorkerThread();
	}
	int		IoWorkerThread(void);
	int		EventWorkerThread(void);
};

template <class T>
inline HANDLE CDPSock<T>::GetCompletionPort(void)
{
	return m_hCompletionPort;
}

template <class T>
inline HANDLE CDPSock<T>::GetCloseHandle(void)
{
	return m_hClose;
}

template <class T>
inline HANDLE CDPSock<T>::GetRecvHandle(void)
{
	return m_hRecv;
}

template <class T>
inline int	CDPSock<T>::GetIoWorkerCount(void)
{
	return (int)m_uIoWorker;
}

template <class T>
inline CSock* CDPSock<T>::GetSockThreaded(void)
{
	return m_pSockThreaded;
}

template <class T>
inline CClientSock<T>* CDPSock<T>::Get(SOCKET hSocket)
{
	return (m_pSock ? (CClientSock<T>*)m_pSock->Get(hSocket) : NULL);
}

template <class T>
inline HRESULT	CDPSock<T>::GetHostAddr(LPVOID lpAddr, LPDWORD lpdwSize)
{
	return(m_pSock ? m_pSock->GetHostAddr(lpAddr, lpdwSize) : DPERR_NOSESSIONS);
}

template <class T>
inline HRESULT CDPSock<T>::GetPlayerAddr(unsigned long dpid, LPVOID lpAddr, LPDWORD lpdwSize)
{
	return m_pSock->GetPeerAddr(dpid, lpAddr, lpdwSize);
}

template <class T>
CDPSock<T>::CDPSock()
{
	m_fServer = false;
	m_pSock = NULL;
	m_hCompletionPort = (HANDLE)NULL;
	m_hRecv = CreateEvent(NULL, false, false, NULL);
	assert(m_hRecv);
	m_hClose = WSACreateEvent();
	assert(m_hClose);
	m_uIoWorker = 0;
	memset(m_lpAddr, 0, sizeof(char) * 32);
	m_pSockThreaded = NULL;
	//	m_dwReadHeaderSize	= 0;
	//	m_dwDataSizeOffset	= 0;
}

template <class T>
CDPSock<T>::~CDPSock()
{
	CLOSE_HANDLE(m_hRecv);
	CLOSE_WSAEVENT(m_hClose);
	Close();
}

template <class T>
bool CDPSock<T>::Close(void)
{
	CloseIoWorker();
	if (m_pSock)
	{
		m_pSock->Close();
		SAFE_DELETE(m_pSock);
	}
	m_lspRecvBuffer.Clear(true);	// delete memory
	return true;
}

template <class T>
bool CDPSock<T>::InitializeConnection(LPVOID lpConnection, unsigned long dwFlags)
{
	if (lpConnection)
		strcpy(m_lpAddr, (char*)lpConnection);
	m_uPort = (unsigned short)dwFlags;
	return true;
}

template <class T>
bool CDPSock<T>::CreateIoWorker(unsigned long uIoWorker)
{
	HANDLE hThread;

	// 康
	// 이 부분이 원본 부분 수정 때 누락. 현재 문제 있으므로 임시로 uIoWorker = 1로 설정하였다.
	m_uIoWorker = uIoWorker;
	if ((m_hCompletionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0)) == (HANDLE)NULL)
	{
		return false;
	}

	unsigned long dwThreadId;
	for (unsigned long i = 0; i < m_uIoWorker; i++)
	{
		hThread = chBEGINTHREADEX(NULL, 0, _IoWorkerThread, this, 0, &dwThreadId);
		assert(hThread);

		SetThreadPriority(hThread, THREAD_PRIORITY_NORMAL);

		m_listthread.push_back(hThread);
	}
	return true;
}

template <class T>
bool CDPSock<T>::CloseIoWorker(void)
{
	unsigned long cbThread;
	LPHANDLE lphThread;

	m_locklistthread.Enter();
	cbThread = m_listthread.size();
	if (cbThread > 0)
	{
		lphThread = new HANDLE[cbThread];
		for (unsigned long i = 0; i < cbThread; i++)
		{
			lphThread[i] = m_listthread.front();
			m_listthread.pop_front();
			if (!chWindows9x())
			{
				PostQueuedCompletionStatus(m_hCompletionPort, CLOSEIOWORKERMSG, true, NULL);
			}
		}
		if (chWindows9x())
		{
			WSASetEvent(m_hClose);
		}
		WaitForMultipleObjects(cbThread, lphThread, true, INFINITE);
		for (unsigned long i = 0; i < cbThread; i++) {
			CloseHandle((HANDLE)lphThread[i]);
		}
		SAFE_DELETE_ARRAY(lphThread);
	}
	m_uIoWorker = 0;
	CLOSE_HANDLE(m_hCompletionPort);
	m_locklistthread.Leave();
	return true;
}

template <class T>
bool CDPSock<T>::CreateEventWorker(CSock* pSock)
{
	HANDLE hThread;
	m_uIoWorker++;
	m_pSockThreaded = pSock;
	AddCreatePlayerOrGroupMsg((unsigned long)pSock->GetHandle());
	//	hThread		= (HANDLE)_beginthread( EventWorkerThread, 0, this );
	unsigned long dwThreadId;
	hThread = chBEGINTHREADEX(NULL, 0, _EventWorkerThread, this, 0, &dwThreadId);
	assert(hThread);
	SetThreadPriority(hThread, THREAD_PRIORITY_NORMAL);
	((CClientSockE<T>*)pSock)->m_hWorker = hThread;
	m_locklistthread.Enter();
	m_listthread.push_back(hThread);
	m_locklistthread.Leave();
	return true;
}

template <class T>
void CDPSock<T>::RemoveThread(HANDLE hThread)
{
	HANDLE h;
	m_locklistthread.Enter();
	if (m_listthread.size() > 0)
	{
		for (list<HANDLE>::iterator i = m_listthread.begin(); i != m_listthread.end(); ++i)
		{
			h = *i;
			if (h == hThread)
			{
				m_listthread.erase(i);
				m_uIoWorker--;
				break;
			}
		}
	}
	m_locklistthread.Leave();
}

template <class T>
bool CDPSock<T>::CreateServer(unsigned long dwcrc)
{
	Close();

	CServerSock<T>* pSock = new CServerSock<T>(dwcrc);

	if (!pSock->Create(m_uPort))
	{
		SAFE_DELETE(pSock);
		return false;
	}
	CSystemInfo si;
	//		CreateIoWorker( si.dwNumberOfProcessors*2 );
	CreateIoWorker((unsigned long)0x01);
	pSock->StartServer(m_hCompletionPort, m_uIoWorker);

	if (!pSock->Listen())
	{
		SAFE_DELETE(pSock);
		return false;
	}
	pSock->SetID(DPID_SERVERPLAYER);
	m_pSock = pSock;
	m_fServer = true;

	return true;
}

template <class T>
bool CDPSock<T>::CreateServerE(unsigned long dwcrc)
{
	Close();

	CServerSockE<T>* pSock = new CServerSockE<T>(dwcrc);

	if (!pSock->Create(m_uPort))
	{
		SAFE_DELETE(pSock);
		return false;
	}
	pSock->StartServer(this);
	if (!pSock->Listen())
	{
		SAFE_DELETE(pSock);
		return false;
	}
	pSock->SetID(DPID_SERVERPLAYER);
	m_pSock = pSock;
	m_fServer = true;

	return true;
}

template <class T>
bool CDPSock<T>::JoinToServer(unsigned long dwcrc, unsigned long uWaitingTime)
{
	Close();

	CClientSock<T>* pSock = new CClientSock<T>(dwcrc);

	if (!pSock->Create())
	{
		SAFE_DELETE(pSock);
		return false;
	}
	CreateIoWorker((unsigned long)0x01);
	if (!CreateIoCompletionPort((HANDLE)pSock->GetHandle(), m_hCompletionPort, (unsigned long)(pSock->GetHandle()), 0))
	{
		TRACE("Can't create completion port with error %d\n", WSAGetLastError());
		SAFE_DELETE(pSock);
		return false;
	}
	if (!pSock->Connect(m_lpAddr, m_uPort))
	{
		SAFE_DELETE(pSock);
		return false;
	}

	pSock->SetID(pSock->GetHandle());
	pSock->SetPeerID(DPID_SERVERPLAYER);
	m_pSock = pSock;

	int zero = 0;
	setsockopt(pSock->GetHandle(), SOL_SOCKET, SO_SNDBUF, (char*)&zero, sizeof(zero));
	setsockopt(pSock->GetHandle(), SOL_SOCKET, SO_RCVBUF, (char*)&zero, sizeof(zero));

	PostQueuedCompletionStatus(m_hCompletionPort, NEWSOCKETMSG, (unsigned long)pSock->GetHandle(), NULL);
	m_fServer = false;
	return true;
}

template <class T>
bool CDPSock<T>::JoinToServerE(unsigned long dwcrc, unsigned long uWaitingTime)
{
	Close();

	CClientSockE<T>* pSock = new CClientSockE<T>(dwcrc);

	if (!pSock->Create())
	{
		SAFE_DELETE(pSock);
		return false;
	}

	CreateEventWorker((CClientSockE<T>*)pSock);
	if (!pSock->Connect(m_lpAddr, m_uPort))
	{
		SAFE_DELETE(m_pSock);
		return false;
	}

	pSock->SetID(pSock->GetHandle());
	pSock->SetPeerID(DPID_SERVERPLAYER);
	m_pSock = pSock;

	int zero = 0;
	setsockopt(pSock->GetHandle(), SOL_SOCKET, SO_SNDBUF, (char*)&zero, sizeof(zero));
	setsockopt(pSock->GetHandle(), SOL_SOCKET, SO_RCVBUF, (char*)&zero, sizeof(zero));

	InterlockedIncrement(&pSock->m_l);
	if (pSock->Recv() != 0)
	{
		TRACE("I/0 error %d\n", WSAGetLastError());
		//		if( pSock->m_l == 0 ) {
		RemoveThread(pSock->m_hWorker);
		CloseConnection(pSock->GetID());
		return false;
		//		}
	}
	m_fServer = false;

	return true;
}

template <class T>
bool CDPSock<T>::Send(char* lpData, unsigned long dwDataSize, unsigned long dpidTo)
{
	if (m_pSock)
	{
		m_pSock->Send(lpData, dwDataSize, dpidTo);
		return true;
	}
	return false;
}

template <class T>
HRESULT CDPSock<T>::Receive(LPDPID lpdpidFrom, LPDPID lpdpidTo, unsigned long dwFlags, T*& pBuffer)
{
	CMclAutoLock	Lock(m_lspRecvBuffer.m_cs);

	if (!(pBuffer = m_lspRecvBuffer.GetHead()))
		return DPERR_NOMESSAGES;

	*lpdpidFrom = pBuffer->dpid;
	m_lspRecvBuffer.RemoveHead();
	return DP_OK;
}

template <class T>
void CDPSock<T>::CloseConnection(unsigned long dpid)
{
	if (m_pSock->CloseConnection(dpid))
		AddDestroyPlayerOrGroupMsg(dpid);
	if (!m_fServer) {
		SAFE_DELETE(m_pSock);
	}
}

template <class T>
bool CDPSock<T>::Shutdown(unsigned long dpid)
{
	return m_pSock->Shutdown(dpid);
}

template <class T>
void CDPSock<T>::AddCreatePlayerOrGroupMsg(unsigned long dpid)
{
	DPMSG_CREATEPLAYERORGROUP createPlayer;
	T* pBuffer = new T;
	pBuffer->crc((CRC32*)m_pSock->IsCrcRead());

	ZeroMemory((void*)&createPlayer, sizeof(DPMSG_CREATEPLAYERORGROUP));

	createPlayer.dwType = DPSYS_CREATEPLAYERORGROUP;	// Is it a player or group
	createPlayer.dwPlayerType = DPPLAYERTYPE_PLAYER;	// Is it a player or group
	createPlayer.dpId = dpid;	// ID of the player or group
//	createPlayer.dwCurrentPlayers	= 0;	// current # players & groups in session
//	createPlayer.lpData	= NULL;		// pointer to remote data
//	createPlayer.dwDataSize	= 0;	// size of remote data
	createPlayer.dpIdParent = DPID_SERVERPLAYER;
	//	createPlayer.dwFlags	= 0;	// player or group flags
	/*
		pBuffer->dpid	= DPID_SYSMSG;
		pBuffer->cb++;
		*pBuffer->m_pTail	= SYSHEADERMARK;
		pBuffer->m_pTail++;

		if( m_dwDataSizeOffset != sizeof(char) )
			pBuffer->m_pTail	+= sizeof(unsigned long);	// + 4

		*(UNALIGNED unsigned long*)pBuffer->m_pTail	= sizeof(DPMSG_CREATEPLAYERORGROUP);
		pBuffer->m_pTail	+= sizeof(unsigned long);

		if( m_dwDataSizeOffset != sizeof(char) )
			pBuffer->m_pTail	+= sizeof(unsigned long);	// + 4

		memcpy( pBuffer->m_pTail, (LPBYTE)&createPlayer, sizeof(DPMSG_CREATEPLAYERORGROUP) );
		pBuffer->m_pTail	+= sizeof(DPMSG_CREATEPLAYERORGROUP);
	*/
	pBuffer->AddSysMsg((void*)&createPlayer, sizeof(DPMSG_CREATEPLAYERORGROUP));

	m_lspRecvBuffer.AddTail(pBuffer);

	SetEvent(m_hRecv);
}

template <class T>
void CDPSock<T>::AddDestroyPlayerOrGroupMsg(unsigned long dpid)
{
	DPMSG_DESTROYPLAYERORGROUP destroyPlayer;
	T* pBuffer = new T;
	pBuffer->crc((CRC32*)m_pSock->IsCrcRead());

	ZeroMemory((void*)&destroyPlayer, sizeof(DPMSG_DESTROYPLAYERORGROUP));
	destroyPlayer.dwType = DPSYS_DESTROYPLAYERORGROUP;	// Message type
	destroyPlayer.dwPlayerType = DPPLAYERTYPE_PLAYER;	// Is it a player or group
	destroyPlayer.dpId = dpid;		// player ID being deleted
//	destroyPlayer.lpLocalData	= NULL;	// copy of players local data
//	destroyPlayer.dwLocalDataSize	= 0;	// sizeof local data
//	destroyPlayer.lpRemoteData	= NULL;	// copy of players remote data
//	destroyPlayer.dwRemoteDataSize	= 0;	// sizeof remote data
	destroyPlayer.dpIdParent = DPID_SERVERPLAYER;	// id of parent group
//	destroyPlayer.dwFlags	= 0;	// player or group flags
/*
	pBuffer->dpid	= DPID_SYSMSG;
	pBuffer->cb++;
	*pBuffer->m_pTail	= SYSHEADERMARK;
	pBuffer->m_pTail++;

	if( m_dwDataSizeOffset != sizeof(char) )
		pBuffer->m_pTail	+= sizeof(unsigned long);	// + 4

	*(UNALIGNED unsigned long*)pBuffer->m_pTail	= sizeof(DPMSG_DESTROYPLAYERORGROUP);
	pBuffer->m_pTail	+= sizeof(unsigned long);

	if( m_dwDataSizeOffset != sizeof(char) )
		pBuffer->m_pTail	+= sizeof(unsigned long);	// + 4

	memcpy( pBuffer->m_pTail, (LPBYTE)&destroyPlayer, sizeof(DPMSG_DESTROYPLAYERORGROUP) );
	pBuffer->m_pTail	+= sizeof(DPMSG_DESTROYPLAYERORGROUP);
*/
	pBuffer->AddSysMsg((void*)&destroyPlayer, sizeof(DPMSG_DESTROYPLAYERORGROUP));

	m_lspRecvBuffer.AddTail(pBuffer);

	SetEvent(m_hRecv);
}

template <class T>
int CDPSock<T>::IoWorkerThread(void)
{
	SOCKET hSocket;
	unsigned long dwBytes;
	CClientSock<T>* pClientSock;
	LPOVERLAPPED lpov;
	bool fOk;
	while (1)
	{
		fOk = GetQueuedCompletionStatus(GetCompletionPort(), &dwBytes, (LPDWORD)&hSocket, &lpov, INFINITE);

		if (dwBytes == CLOSEIOWORKERMSG)
			return(0);

		if (dwBytes == NEWSOCKETMSG)
		{
			AddCreatePlayerOrGroupMsg(hSocket);
			pClientSock = Get(hSocket);
			InterlockedIncrement(&pClientSock->m_l);
			if (pClientSock->Recv() != 0)
			{
				TRACE("I/0 error %d\n", WSAGetLastError());
				//				if( pClientSock->m_l == 0 )
				CloseConnection(hSocket);
			}
			continue;
		}

		if (!(pClientSock = Get(hSocket)))
			continue;

		if (!fOk || dwBytes == 0)	// When Connection Closed by peer gracefully or becoz of error
		{
			int err = WSAGetLastError();
			switch (err)
			{
			case ERROR_NETNAME_DELETED:		//64
			case ERROR_OPERATION_ABORTED:
			case 0:
			case WSAENOTSOCK:	//10038
			default:
			{
				if (false == fOk && NULL == lpov) {}
				TRACE("Completion status failed with error %d\n", err);
				TRACE("Close socket %d, %x //REF:%d\n", hSocket, pClientSock, pClientSock->m_l - 1);
				InterlockedDecrement(&pClientSock->m_l);
				//						if( pClientSock->m_l == 0 )
				CloseConnection(hSocket);
				break;
			}
			}
			continue;
		}

		if (&pClientSock->m_ovRecv == lpov)	// receive i/o completed
		{
			T* pBuffer = pClientSock->Fetch(dwBytes);
			if (pBuffer)
			{
				if (pBuffer->cb > 0) {
					pBuffer->dpid = hSocket;
					m_lspRecvBuffer.AddTail(pBuffer);
					SetEvent(GetRecvHandle());
				}
				else {
					SAFE_DELETE(pBuffer);
				}
			}
			else if (WSAGetLastError()
				== ERROR_BAD_NET_NAME)
			{
				CloseConnection(hSocket);
				continue;
			}

			if (pClientSock->Recv() != 0)	// i/o error
			{
				//				if( pClientSock->m_l == 0 )
				CloseConnection(hSocket);
			}
		}
		else if (&pClientSock->m_ovSend == lpov)	// send i/o completed
		{
			if (pClientSock->SendRemnant(dwBytes) != 0)
			{
				if (pClientSock->m_l == 0)
					CloseConnection(hSocket);
			}
		}
		//		Sleep( 0 );
	}
	return(0);
}

template <class T>
int CDPSock<T>::EventWorkerThread(void)
{
	CClientSockE<T>* pClientSock = (CClientSockE<T>*)GetSockThreaded();
	SOCKET hSocket = pClientSock->GetHandle();

	WSAEVENT aEvent[4];
	aEvent[0] = pClientSock->GetRecvEvent();
	aEvent[1] = pClientSock->GetSendEvent();
	aEvent[2] = GetCloseHandle();
	aEvent[3] = NULL;

	unsigned long dwBytes = 0, dw;
	bool fOk;

	while (1)
	{
		dw = WSAWaitForMultipleEvents(3, aEvent, false, INFINITE, false);
		switch (dw)
		{
		case WSA_WAIT_EVENT_0 + 2:
			return 0;
		case WSA_WAIT_FAILED:
			TRACE("WSA_WAIT_FAILED with error %d\n", WSAGetLastError());
			continue;
			break;
		case WSA_WAIT_EVENT_0:
		{
			fOk = GetOverlappedResult((HANDLE)hSocket, &pClientSock->m_ovRecv, &dwBytes, true);
			if (!fOk || dwBytes == 0)	// When Connection Closed by peer gracefully or because of error
			{
				int err = WSAGetLastError();
				switch (err)
				{
				case ERROR_NETNAME_DELETED:
				case ERROR_OPERATION_ABORTED:
				case 0:
				case WSAENOTSOCK:
				default:
				{
					TRACE("Close socket %d, %x //REF:%d\n", hSocket, pClientSock, pClientSock->m_l - 1);
					InterlockedDecrement(&pClientSock->m_l);
					//									if( pClientSock->m_l == 0 ) {
					//										RemoveThread( pClientSock->m_hWorker );
					CloseConnection(hSocket);
					return 0;
					//									}
					break;
				}
				}
			}

			T* pBuffer = pClientSock->Fetch(dwBytes);
			if (pBuffer)
			{
				if (pBuffer->cb > 0) {
					pBuffer->dpid = hSocket;
					m_lspRecvBuffer.AddTail(pBuffer);
					SetEvent(GetRecvHandle());
				}
				else {
					SAFE_DELETE(pBuffer);
				}
			}
			else if (WSAGetLastError()
				== ERROR_BAD_NET_NAME)
			{
				CloseConnection(hSocket);
				return 0;
			}

			if (pClientSock->Recv() != 0)	// i/o error
			{
				//						if( pClientSock->m_l == 0 ) {
				RemoveThread(pClientSock->m_hWorker);
				CloseConnection(hSocket);
				return 0;
				//						}
			}
			break;
		}
		case WSA_WAIT_EVENT_0 + 1:
		{
			fOk = GetOverlappedResult((HANDLE)hSocket, &pClientSock->m_ovSend, &dwBytes, true);
			WSAResetEvent(aEvent[1]);
			if (!fOk || dwBytes == 0)	// When Connection Closed by peer gracefully or because of error
			{
				int err = WSAGetLastError();
				switch (err)
				{
				case ERROR_NETNAME_DELETED:
				case ERROR_OPERATION_ABORTED:
				case 0:
				case WSAENOTSOCK:
				default:
				{
					TRACE("Close socket %d, %x //REF:%d\n", hSocket, pClientSock, pClientSock->m_l - 1);
					InterlockedDecrement(&pClientSock->m_l);
					//									if( pClientSock->m_l == 0 ) {
					RemoveThread(pClientSock->m_hWorker);
					CloseConnection(hSocket);
					return 0;
					//									}
					break;
				}
				}
			}

			if (pClientSock->SendRemnant(dwBytes) != 0)
			{
				//						if( pClientSock->m_l == 0 ) {
				RemoveThread(pClientSock->m_hWorker);
				CloseConnection(hSocket);
				return 0;
				//						}
			}
			break;
		}
		}
	}
	return 0;
}

#endif //__DPSOCK_H__