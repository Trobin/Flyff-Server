#ifndef __SOCK_H__
#define __SOCK_H__

#pragma once

#include <dplay.h>
#include <list>
using namespace std;

class CSock
{
public:
	enum { crcRead = 1, crcWrite = 2, };
public:
	unsigned long	m_dwcrc;
protected:
	SOCKET	m_hSocket;
private:
	unsigned long	m_dpid;
	unsigned long	m_dpidpeer;

public:
	//	Constructions
	CSock();
	virtual	~CSock();
	//	Operations
	SOCKET	GetHandle(void);
	void	Clear(void);
	void	SetID(unsigned long dpid);
	unsigned long	GetID(void);
	void	SetPeerID(unsigned long dpid);
	unsigned long	GetPeerID(void);
	bool	IsCrcRead(void) { return(m_dwcrc & crcRead); }
	bool	IsCrcWrite(void) { return(m_dwcrc & crcWrite); }

	virtual	bool	Create(unsigned short uPort = 0, int type = SOCK_STREAM);
	virtual	void	Attach(SOCKET hSocket);
	virtual	HRESULT	GetHostAddr(LPVOID lpAddr, LPDWORD lpdwSize);
	virtual	HRESULT	GetPeerAddr(unsigned long dpid, LPVOID lpAddr, LPDWORD lpdwSize) = 0;
	virtual	void	Close(void) = 0;
	virtual	bool	CloseConnection(SOCKET hSocket) = 0;
	virtual	bool	Shutdown(SOCKET hSocket) = 0;
	virtual	void	Send(char* lpData, unsigned long dwDataSize, unsigned long dpidTo) = 0;
	virtual	CSock* Get(SOCKET hSocket);
	virtual	void CSock::Detach(void)
	{
		m_hSocket = INVALID_SOCKET;
		m_dpid = m_dpidpeer = DPID_UNKNOWN;
	}

};
inline SOCKET CSock::GetHandle(void) { return m_hSocket; }
inline CSock* CSock::Get(SOCKET hSocket) { return(m_hSocket == hSocket ? this : NULL); }

inline void	CSock::SetID(unsigned long dpid) { m_dpid = dpid; }
inline unsigned long CSock::GetID(void) { return m_dpid; }
inline void CSock::SetPeerID(unsigned long dpid) { m_dpidpeer = dpid; }
inline unsigned long CSock::GetPeerID(void) { return m_dpidpeer; }

#endif //__SOCK_H__