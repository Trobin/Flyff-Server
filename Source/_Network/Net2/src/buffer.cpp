#include "stdafx.h"
#include "buffer.h"
#include "dpmng.h"

#include "buyinginfo.h"

#ifndef __VM_0820
MemPooler<CBuffer>* CBuffer::m_pPool = new MemPooler<CBuffer>(512);
#endif	// __VM_0820
CHeapMng* CBuffer::m_pHeapMng = new CHeapMng;

#ifndef __VM_0820
MemPooler<CBuffer2>* CBuffer2::m_pPool2 = new MemPooler<CBuffer2>(128);
MemPooler<CBuffer3>* CBuffer3::m_pPool2 = new MemPooler<CBuffer3>(128);
#endif	// __VM_0820

CBuffer::CBuffer(unsigned long uBufSize)
{
	if (uBufSize > MAX_BUFFER) {
		m_lpBufStart = (LPBYTE)CBuffer::m_pHeapMng->Malloc(uBufSize);
	}
	else {
		m_lpBufStart = m_lpBuf;
		uBufSize = MAX_BUFFER;
	}

	assert(m_lpBufStart);

	m_pHead = m_pTail = m_lpBufStart;
	m_lpBufMax = m_lpBufStart + uBufSize;
	cb = 0;
	dpid = DPID_UNKNOWN;
	pPrevious = pNext = NULL;

	m_pcrc = NULL;
}

CBuffer::~CBuffer()
{
	if (m_lpBufMax - m_lpBufStart > MAX_BUFFER)
		CBuffer::m_pHeapMng->Free(m_lpBufStart);
}

void CBuffer::AddSysMsg(void* pMsg, unsigned long uMsgSize)
{
	dpid = DPID_SYSMSG;
	cb++;
	*m_pTail = SYSHEADERMARK;
	m_pTail++;
	if (m_pcrc)
		m_pTail += sizeof(unsigned long);	// + 4
	*(UNALIGNED unsigned long*)m_pTail = uMsgSize;
	m_pTail += sizeof(unsigned long);
	if (m_pcrc)
		m_pTail += sizeof(unsigned long);	// + 4

	memcpy(m_pTail, pMsg, uMsgSize);
	m_pTail += uMsgSize;
}

void CBuffer::AddHdr(LPBYTE lpData, unsigned long uDataSize)
{
	int cb;
	LPBYTE ptr = GetWritableBuffer(&cb);
	//	header
	*ptr = HEADERMARK;

	if (m_pcrc)
	{
		unsigned char digest[sizeof(unsigned long)];
		m_pcrc->Restart();
		m_pcrc->Update((const unsigned char*)(&uDataSize), sizeof(unsigned long));
		m_pcrc->Final(digest);
		*(UNALIGNED unsigned long*)(ptr + sizeof(char)) = *(UNALIGNED unsigned long*)digest;
		*(UNALIGNED unsigned long*)(ptr + sizeof(char) + sizeof(unsigned long)) = (unsigned long)uDataSize;
		m_pcrc->Restart();
		m_pcrc->Update((const unsigned char*)(lpData), (unsigned int)uDataSize);
		m_pcrc->Final(digest);
		*(UNALIGNED unsigned long*)(ptr + sizeof(char) + sizeof(unsigned long) + sizeof(unsigned long)) = *(UNALIGNED unsigned long*)digest;
	}
	else
	{
		*(UNALIGNED unsigned long*)& ptr[1] = (unsigned long)uDataSize;
	}
	m_pTail = ptr + CBuffer::GetHdrSize((bool)m_pcrc);
}

void CBuffer::AddData(LPBYTE lpData, unsigned long uDataSize, unsigned long& uRemnant)
{
	unsigned long cb;
	LPBYTE ptr = GetWritableBuffer((int*)&cb);

	cb = (cb < uRemnant ? cb : uRemnant);
	assert(ptr + cb <= m_lpBufMax);
	memcpy((void*)ptr, &lpData[uDataSize - uRemnant], cb);
	m_pTail = ptr + cb;
	uRemnant -= cb;
}

void CBuffer::MessageHandler(CDPMng<CBuffer>* pDPMng, unsigned long dpidFrom)
{
	unsigned char* ptr = m_lpBufStart;
	unsigned long uHdrSize = CBuffer::GetHdrSize((bool)m_pcrc);
	unsigned long uDataSizeOffset = (m_pcrc ? 5 : 1);
	while (cb > 0)
	{
		unsigned long uDataSize = *(UNALIGNED LPDWORD)(ptr + uDataSizeOffset);
		if (dpidFrom == DPID_SYSMSG)
			pDPMng->SysMessageHandler((LPDPMSG_GENERIC)&ptr[uHdrSize], uDataSize, dpidFrom);
		else
			pDPMng->UserMessageHandler((LPDPMSG_GENERIC)&ptr[uHdrSize], uDataSize, dpidFrom);
		ptr += uHdrSize + uDataSize;
		cb--;
	}
}

CBuffer* CBuffer::Fetch(CBuffer** ppRecvBuffer, unsigned long dwBytes, CRC32* pcrc)
{
	crc(pcrc);	// m_pcrc

	m_pTail += dwBytes;
	assert(m_pTail <= m_lpBufMax);

	int nRemnant;
	LPBYTE ptr = GetReadableBuffer(&nRemnant);

	CBuffer* pOld = NULL, * pBuffer;
	unsigned long uPacketSize;

	unsigned long dwCrc, dwDataSize;
	unsigned char digest[4];
	int	uHdrSize = (int)(CBuffer::GetHdrSize((bool)pcrc));

	while (1)
	{
		if (nRemnant < uHdrSize)
		{
			if (cb > 0) {
				pOld = *ppRecvBuffer;
				pOld->m_pTail -= nRemnant;	// remove remnant from old buffer
				pBuffer = new CBuffer;

				assert(pBuffer->m_pTail + nRemnant <= pBuffer->m_lpBufMax);
				memcpy(pBuffer->m_pTail, ptr, nRemnant);
				pBuffer->m_pTail += nRemnant;
				*ppRecvBuffer = pBuffer;
			}
			return pOld;
		}
		else
		{
			if (pcrc)
			{
				dwCrc = *(UNALIGNED LPDWORD)(ptr + sizeof(char));
				dwDataSize = *(UNALIGNED LPDWORD)(ptr + sizeof(char) + sizeof(unsigned long));
				pcrc->Restart();
				pcrc->Update((const unsigned char*)(&dwDataSize), sizeof(unsigned long));
				pcrc->Final(digest);
				if (*(UNALIGNED LPDWORD)digest != dwCrc)
				{
					WSASetLastError(ERROR_BAD_NET_NAME);
					return NULL;
				}
				dwCrc = *(UNALIGNED LPDWORD)(ptr + sizeof(char) + sizeof(unsigned long) + sizeof(unsigned long));
				uPacketSize = uHdrSize + dwDataSize;
			}
			else
			{
				uPacketSize = uHdrSize + *(UNALIGNED LPDWORD) & ptr[1];
			}
			if (nRemnant < (int)(uPacketSize))
			{
				if ((int)(uPacketSize) > (*ppRecvBuffer)->GetSize())
				{
					pOld = *ppRecvBuffer;
					pBuffer = new CBuffer(uPacketSize);
				}
				else
				{
					if ((*ppRecvBuffer)->cb > 0)
					{
						pOld = *ppRecvBuffer;
						pBuffer = new CBuffer;
					}
				}

				if (pOld)
				{
					assert(pBuffer->m_pTail + nRemnant <= pBuffer->m_lpBufMax);
					memcpy(pBuffer->m_pTail, ptr, nRemnant);
					//					if( nRemnant > 0 )
					//					{
					//						assert( CBuffer::IsHeader( *ptr ) );
					//					}
					pBuffer->m_pTail += nRemnant;
					*ppRecvBuffer = pBuffer;
				}

				return pOld;
			}
			else	// completion
			{
				if (pcrc)
				{
					pcrc->Restart();
					pcrc->Update((const unsigned char*)(ptr + uHdrSize), dwDataSize);
					pcrc->Final(digest);
					if (*(UNALIGNED LPDWORD)digest != dwCrc)
					{
						WSASetLastError(ERROR_BAD_NET_NAME);
						return NULL;
					}
				}
				(*ppRecvBuffer)->cb++;
				nRemnant -= (uPacketSize);
				ptr += (uPacketSize);
			}
		}
	}
	return NULL;
}
/*--------------------------------------------------------------------------------*/
void CBuffer2::AddSysMsg(void* pMsg, unsigned long uMsgSize)
{
	dpid = DPID_SYSMSG;
	cb++;

	if (m_pcrc)
		m_pTail += sizeof(unsigned long);	// + 4
	*(UNALIGNED unsigned short*)m_pTail = htons((unsigned short)(uMsgSize + 2));
	m_pTail += sizeof(unsigned short);
	if (m_pcrc)
		m_pTail += sizeof(unsigned long);	// + 4

	memcpy(m_pTail, pMsg, uMsgSize);
	m_pTail += uMsgSize;
}

void CBuffer2::AddHdr(LPBYTE lpData, unsigned long uDataSize)
{
	int cb;
	LPBYTE ptr = GetWritableBuffer(&cb);

	if (m_pcrc)
	{
		unsigned char digest[sizeof(unsigned long)];
		m_pcrc->Restart();
		m_pcrc->Update((const unsigned char*)(&uDataSize), sizeof(unsigned long));
		m_pcrc->Final(digest);
		*(UNALIGNED unsigned long*)ptr = *(UNALIGNED unsigned long*)digest;
		//		*(UNALIGNED unsigned short*)( ptr + sizeof(unsigned long) )	= (unsigned short)uDataSize;
		*(UNALIGNED unsigned short*)(ptr + sizeof(unsigned long)) = htons((unsigned short)(uDataSize + 2));
		m_pcrc->Restart();
		m_pcrc->Update((const unsigned char*)(lpData), (unsigned int)uDataSize);
		m_pcrc->Final(digest);
		*(UNALIGNED unsigned long*)(ptr + sizeof(unsigned long) + sizeof(unsigned short)) = *(UNALIGNED unsigned long*)digest;
	}
	else
	{
		//		*(UNALIGNED unsigned short*)ptr	= (unsigned short)uDataSize;
		*(UNALIGNED unsigned short*)ptr = htons((unsigned short)(uDataSize + 2));
	}
	m_pTail = ptr + CBuffer2::GetHdrSize((bool)m_pcrc);
}

void CBuffer2::MessageHandler(CDPMng<CBuffer2>* pDPMng, unsigned long dpidFrom)
{
	unsigned char* ptr = m_lpBufStart;
	unsigned long uHdrSize = CBuffer2::GetHdrSize((bool)m_pcrc);
	unsigned long uDataSizeOffset = (m_pcrc ? 4 : 0);
	while (cb > 0)
	{
		unsigned long uDataSize = ntohs(*(UNALIGNED LPWORD)(ptr + uDataSizeOffset)) - 2;
		if (dpidFrom == DPID_SYSMSG)
			pDPMng->SysMessageHandler((LPDPMSG_GENERIC)&ptr[uHdrSize], uDataSize, dpidFrom);
		else
			pDPMng->UserMessageHandler((LPDPMSG_GENERIC)&ptr[uHdrSize], uDataSize, dpidFrom);
		ptr += uHdrSize + uDataSize;
		cb--;
	}
}

CBuffer2* CBuffer2::Fetch(CBuffer2** ppRecvBuffer, unsigned long dwBytes, CRC32* pcrc)
{
	crc(pcrc);	// m_pcrc

	m_pTail += dwBytes;
	assert(m_pTail <= m_lpBufMax);

	int nRemnant;
	LPBYTE ptr = GetReadableBuffer(&nRemnant);

	CBuffer2* pOld = NULL, * pBuffer;
	unsigned long uPacketSize;

	unsigned long dwCrc, dwDataSize;
	unsigned char digest[4];
	unsigned long uHdrSize = CBuffer2::GetHdrSize((bool)pcrc);

	while (1)
	{
		if (nRemnant < (int)(uHdrSize))
		{
			if (cb > 0) {
				pOld = *ppRecvBuffer;
				pOld->m_pTail -= nRemnant;	// remove remnant from old buffer
				pBuffer = new CBuffer2;

				assert(pBuffer->m_pTail + nRemnant <= pBuffer->m_lpBufMax);
				memcpy(pBuffer->m_pTail, ptr, nRemnant);
				pBuffer->m_pTail += nRemnant;
				*ppRecvBuffer = pBuffer;
			}
			return pOld;
		}
		else
		{
			if (pcrc)
			{
				dwCrc = *(UNALIGNED LPDWORD)ptr;
				dwDataSize = ntohs(*(UNALIGNED LPWORD)(ptr + sizeof(unsigned long))) - 2;
				pcrc->Restart();
				pcrc->Update((const unsigned char*)(&dwDataSize), sizeof(unsigned long));
				pcrc->Final(digest);
				if (*(UNALIGNED LPDWORD)digest != dwCrc)
				{
					WSASetLastError(ERROR_BAD_NET_NAME);
					return NULL;
				}
				dwCrc = *(UNALIGNED LPDWORD)(ptr + sizeof(unsigned long) + sizeof(unsigned short));
				uPacketSize = uHdrSize + dwDataSize;
			}
			else
			{
				uPacketSize = uHdrSize + ntohs(*(UNALIGNED LPWORD)ptr) - 2;
			}
			if (nRemnant < (int)(uPacketSize))
			{
				if ((int)(uPacketSize) > (*ppRecvBuffer)->GetSize())
				{
					pOld = *ppRecvBuffer;
					pBuffer = new CBuffer2(uPacketSize);
				}
				else
				{
					if ((*ppRecvBuffer)->cb > 0)
					{
						pOld = *ppRecvBuffer;
						pBuffer = new CBuffer2;
					}
				}

				if (pOld)
				{
					assert(pBuffer->m_pTail + nRemnant <= pBuffer->m_lpBufMax);
					memcpy(pBuffer->m_pTail, ptr, nRemnant);
					//					if( nRemnant > 0 )
					//					{
					//						assert( CBuffer::IsHeader( *ptr ) );
					//					}
					pBuffer->m_pTail += nRemnant;
					*ppRecvBuffer = pBuffer;
				}

				return pOld;
			}
			else	// completion
			{
				if (pcrc)
				{
					pcrc->Restart();
					pcrc->Update((const unsigned char*)(ptr + uHdrSize), dwDataSize);
					pcrc->Final(digest);
					if (*(UNALIGNED LPDWORD)digest != dwCrc)
					{
						WSASetLastError(ERROR_BAD_NET_NAME);
						return NULL;
					}
				}
				(*ppRecvBuffer)->cb++;
				nRemnant -= (uPacketSize);
				ptr += (uPacketSize);
			}
		}
	}
	return NULL;
}
/*--------------------------------------------------------------------------------*/
void CBuffer3::AddSysMsg(void* pMsg, unsigned long uMsgSize)
{
	dpid = DPID_SYSMSG;
	cb++;

	//	if( m_pcrc )
	//		m_pTail	+= sizeof(unsigned long);	// + 4
	*(UNALIGNED unsigned short*)m_pTail = (unsigned short)uMsgSize;
	m_pTail += sizeof(unsigned short);
	//	if( m_pcrc )
	//		m_pTail	+= sizeof(unsigned long);	// + 4
	memcpy(m_pTail, pMsg, uMsgSize);
	m_pTail += uMsgSize;
}

void CBuffer3::AddHdr(LPBYTE lpData, unsigned long uDataSize)
{
	/*
		int cb;
		LPBYTE ptr	= GetWritableBuffer( &cb );

		if( m_pcrc )
		{
			unsigned char digest[sizeof(unsigned long)];
			m_pcrc->Restart();
			m_pcrc->Update( (const unsigned char*)( &uDataSize ), sizeof(unsigned long) );
			m_pcrc->Final( digest );
			*(UNALIGNED unsigned long*)ptr	= *(UNALIGNED unsigned long*)digest;
	//		*(UNALIGNED unsigned short*)( ptr + sizeof(unsigned long) )	= (unsigned short)uDataSize;
			*(UNALIGNED unsigned short*)( ptr + sizeof(unsigned long) )	= htons( (unsigned short)( uDataSize + 2 ) );
			m_pcrc->Restart();
			m_pcrc->Update( (const unsigned char*)( lpData ), (unsigned int)uDataSize );
			m_pcrc->Final( digest );
			*(UNALIGNED unsigned long*)( ptr + sizeof(unsigned long) + sizeof(unsigned short) )	= *(UNALIGNED unsigned long*)digest;
		}
		else
		{
	//		*(UNALIGNED unsigned short*)ptr	= (unsigned short)uDataSize;
			*(UNALIGNED unsigned short*)ptr	= htons( (unsigned short)( uDataSize + 2 ) );
		}
		m_pTail		= ptr + CBuffer3::GetHdrSize( (bool)m_pcrc );
	*/
}

void CBuffer3::MessageHandler(CDPMng<CBuffer3>* pDPMng, unsigned long dpidFrom)
{
	unsigned char* ptr = m_lpBufStart;
	unsigned long uPacketSize;
	while (cb > 0)
	{
		uPacketSize = 0;
		if (dpidFrom == DPID_SYSMSG)
		{
			uPacketSize = sizeof(unsigned short) + *(UNALIGNED LPWORD)ptr;
			pDPMng->SysMessageHandler((LPDPMSG_GENERIC)&ptr[sizeof(unsigned short)], *(UNALIGNED LPWORD)ptr, dpidFrom);
		}
		else
		{
			uPacketSize = sizeof(BUYING_INFO);
			pDPMng->UserMessageHandler((LPDPMSG_GENERIC)ptr, sizeof(BUYING_INFO), dpidFrom);
		}
		ptr += uPacketSize;
		cb--;
	}
}

CBuffer3* CBuffer3::Fetch(CBuffer3** ppRecvBuffer, unsigned long dwBytes, CRC32* pcrc)
{
	crc(pcrc);	// m_pcrc

	m_pTail += dwBytes;
	assert(m_pTail <= m_lpBufMax);

	int nRemnant;
	LPBYTE ptr = GetReadableBuffer(&nRemnant);

	CBuffer3* pOld = NULL, * pBuffer;

	while (1)
	{
		if (nRemnant < sizeof(BUYING_INFO))
		{
			if ((*ppRecvBuffer)->cb > 0)
			{
				pOld = *ppRecvBuffer;
				pBuffer = new CBuffer3;
			}
			if (pOld)
			{
				assert(pBuffer->m_pTail + nRemnant <= pBuffer->m_lpBufMax);
				memcpy(pBuffer->m_pTail, ptr, nRemnant);

				pBuffer->m_pTail += nRemnant;
				*ppRecvBuffer = pBuffer;
			}
			return pOld;
		}
		else	// completion
		{
			(*ppRecvBuffer)->cb++;
			nRemnant -= sizeof(BUYING_INFO);
			ptr += sizeof(BUYING_INFO);
		}
	}
	return NULL;
}