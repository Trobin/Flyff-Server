#ifndef __DOS2_H__
#define	__DOS2_H__

#pragma once

#include <map>
#include "mempooler.h"

using	namespace	std;

typedef struct	_CONNECTIONREQUEST2
{
	unsigned long	ip;
	unsigned long	dwHit;
	int	nHit;
	bool	b;
}
CONNECTIONREQUEST2, * PCONNECTIONREQUEST2;

#define	MAX_ADDR	1024
#undef MAX_HIT
#define	MAX_HIT		50

class CDos2 : public map<unsigned long, PCONNECTIONREQUEST2>
{
public:
	CDos2();
	virtual	~CDos2();
	int		IsOver(unsigned long addr);
private:
	CTimer	m_timer;
	unsigned long	m_adwAddr[MAX_ADDR];
	int		m_nAddr;
	MemPooler<CONNECTIONREQUEST2>* m_pPool;
	void	Add(unsigned long addr);
	PCONNECTIONREQUEST2		Get(unsigned long addr);
	void	Release(void);
};

#endif	// __DOS2_H__
