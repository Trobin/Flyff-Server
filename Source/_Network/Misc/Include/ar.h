#ifndef __AR_H__
#define __AR_H__

#pragma once

#include "HeapMng.h"

class CAr
{
public:
	CAr(void* lpBuf = NULL, unsigned int nBufSize = 0);
	~CAr();

	// Flag values
	enum { store = 0, load = 1 };
	enum { nGrowSize = 16384 };

	static CHeapMng* m_pHeapMng;


	// Attributes
	bool	IsLoading()	const;
	bool	IsStoring()	const;

	// Operations
	void	Read(void* lpBuf, unsigned int nSize);
	void	Write(const void* lpBuf, unsigned int nSize);
	void	CheckBuf(unsigned int nSize);
	void	Reserve(unsigned int nSize);
	void	Flush(void);
	void	ReelIn(unsigned int uOffset);

	// reading and writing strings
	void WriteString(const char* lpsz);
	LPTSTR ReadString(LPTSTR lpsz);
	LPTSTR ReadString(LPTSTR lpsz, int nBufSize);

	LPBYTE	GetBuffer(int* pnBufSize);
	unsigned long	GetOffset(void);
	/*
		void	Copy( CAr & ar );
		CAr& operator = ( CAr & ar );
	*/
	// insertion operations
	CAr& operator<<(unsigned char by);
	CAr& operator<<(unsigned short w);
	CAr& operator<<(long l);
	CAr& operator<<(unsigned long dw);
	CAr& operator<<(float f);
	CAr& operator<<(double d);

	CAr& operator<<(int i);
	CAr& operator<<(short w);
	CAr& operator<<(char ch);
	CAr& operator<<(unsigned u);

	CAr& operator<<(bool b);

	// extraction operations
	CAr& operator>>(unsigned char& by);
	CAr& operator>>(unsigned short& w);
	CAr& operator>>(unsigned long& dw);
	CAr& operator>>(long& l);
	CAr& operator>>(float& f);
	CAr& operator>>(double& d);

	CAr& operator>>(int& i);
	CAr& operator>>(short& w);
	CAr& operator>>(char& ch);
	CAr& operator>>(unsigned& u);

	CAr& operator>>(bool& b);

#ifdef __CLIENT
#ifdef _DEBUG
	static	unsigned long	s_dwHdrPrev;
	static	unsigned long	s_dwHdrCur;
#endif	// _DEBUG
#endif	// __CLIENT

protected:
	unsigned char	m_nMode;	// read or write
	unsigned int	m_nBufSize;
	LPBYTE	m_lpBufCur;
	LPBYTE	m_lpBufMax;
	LPBYTE	m_lpBufStart;
	unsigned char	m_lpBuf[nGrowSize];
};
/*
inline void CAr::Copy( CAr & ar )
	{	assert( IsLoading() );	assert( ar.IsStoring() );	ar.Write( (void*)m_lpBufStart, (unsigned int)( m_lpBufMax - m_lpBufStart ) );	}
inline CAr& CAr::operator = ( CAr & ar )
	{	ar.Copy( *this );	return *this;	}
*/
inline bool CAr::IsLoading() const
{
	return (m_nMode & CAr::load) != 0;
}
inline bool CAr::IsStoring() const
{
	return (m_nMode & CAr::load) == 0;
}

inline CAr& CAr::operator<<(int i)
{
	return CAr::operator<<((long)i);
}
inline CAr& CAr::operator<<(unsigned u)
{
	return CAr::operator<<((long)u);
}
inline CAr& CAr::operator<<(short w)
{
	return CAr::operator<<((unsigned short)w);
}
inline CAr& CAr::operator<<(char ch)
{
	return CAr::operator<<((unsigned char)ch);
}
inline CAr& CAr::operator<<(unsigned char by)
{
	CheckBuf(sizeof(unsigned char));
	*(UNALIGNED unsigned char*)m_lpBufCur = by; m_lpBufCur += sizeof(unsigned char); return *this;
}
inline CAr& CAr::operator<<(unsigned short w)
{
	CheckBuf(sizeof(unsigned short));
	*(UNALIGNED unsigned short*)m_lpBufCur = w; m_lpBufCur += sizeof(unsigned short); return *this;
}
inline CAr& CAr::operator<<(long l)
{
	CheckBuf(sizeof(long));
	*(UNALIGNED long*)m_lpBufCur = l; m_lpBufCur += sizeof(long); return *this;
}
inline CAr& CAr::operator<<(unsigned long dw)
{
	CheckBuf(sizeof(unsigned long));
	*(UNALIGNED unsigned long*)m_lpBufCur = dw; m_lpBufCur += sizeof(unsigned long); return *this;
}
inline CAr& CAr::operator<<(float f)
{
	CheckBuf(sizeof(float));
	*(UNALIGNED float*)m_lpBufCur = *(float*)&f; m_lpBufCur += sizeof(float); return *this;
}
inline CAr& CAr::operator<<(double d)
{
	CheckBuf(sizeof(double));
	*(UNALIGNED double*)m_lpBufCur = *(double*)&d; m_lpBufCur += sizeof(double); return *this;
}
inline CAr& CAr::operator<<(bool b)
{
	CheckBuf(sizeof(bool));
	*(UNALIGNED bool*)m_lpBufCur = *(bool*)&b; m_lpBufCur += sizeof(bool); return *this;
}

inline CAr& CAr::operator>>(int& i)
{
	return CAr::operator>>((long&)i);
}
inline CAr& CAr::operator>>(unsigned& u)
{
	return CAr::operator>>((long&)u);
}
inline CAr& CAr::operator>>(short& w)
{
	return CAr::operator>>((unsigned short&)w);
}
inline CAr& CAr::operator>>(char& ch)
{
	return CAr::operator>>((unsigned char&)ch);
}

#define	CAR_SAFE_READ( type, value )	\
	if( m_lpBufCur + sizeof(type) <= m_lpBufMax )	\
		{	value	= *(UNALIGNED type*)m_lpBufCur;	m_lpBufCur += sizeof(type);	}	\
	else	\
		{	value	= (type)0;	m_lpBufCur	= m_lpBufMax;	}	\
	return *this

inline CAr& CAr::operator>>(unsigned char& by)
{
	CAR_SAFE_READ(unsigned char, by);
}
inline CAr& CAr::operator>>(unsigned short& w)
{
	CAR_SAFE_READ(unsigned short, w);
}
inline CAr& CAr::operator>>(unsigned long& dw)
{
	CAR_SAFE_READ(unsigned long, dw);
}
inline CAr& CAr::operator>>(float& f)
{
	CAR_SAFE_READ(float, f);
}
inline CAr& CAr::operator>>(double& d)
{
	CAR_SAFE_READ(double, d);
}
inline CAr& CAr::operator>>(long& l)
{
	CAR_SAFE_READ(long, l);
}

inline CAr& CAr::operator>>(bool& b)
{
	CAR_SAFE_READ(bool, b);
}

#include <D3DX9Math.h>

inline CAr& operator<<(CAr& ar, D3DXVECTOR3 v)
{
	ar.Write(&v, sizeof(D3DXVECTOR3));	return ar;
}

inline CAr& operator>>(CAr& ar, D3DXVECTOR3& v)
{
	ar.Read(&v, sizeof(D3DXVECTOR3));		return ar;
}

inline CAr& operator<<(CAr& ar, __int64 i)
{
	ar.Write(&i, sizeof(__int64));	return ar;
}

inline CAr& operator>>(CAr& ar, __int64& i)
{
	ar.Read(&i, sizeof(__int64));	return ar;
}

/*
inline CAr& operator<<(CAr & ar, CRect rect)
	{	ar.Write( &rect, sizeof(CRect) );	return ar;	}

inline CAr& operator>>(CAr & ar, CRect & rect)
	{	ar.Read( &rect, sizeof(CRect) );	return ar;	}
*/

inline CAr& operator<<(CAr& ar, RECT rect)
{
	ar.Write(&rect, sizeof(RECT));	return ar;
}

inline CAr& operator>>(CAr& ar, RECT& rect)
{
	ar.Read(&rect, sizeof(RECT));	return ar;
}

inline CAr& operator<<(CAr& ar, PLAY_ACCOUNT pa)
{
	ar.Write(&pa, sizeof(PLAY_ACCOUNT));	return ar;
}

inline CAr& operator>>(CAr& ar, PLAY_ACCOUNT& pa)
{
	ar.Read(&pa, sizeof(PLAY_ACCOUNT));	return ar;
}

inline unsigned long CAr::GetOffset(void)
{
	assert(IsStoring());
	return(m_lpBufCur - m_lpBufStart);
}

#endif //__AR_H__