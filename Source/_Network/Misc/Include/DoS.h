#ifndef __DOS_H__
#define	__DOS_H__

#pragma once

#include <map>
#include <deque>
#include <mmsystem.h>
#include "mempooler.h"

using	namespace	std;

typedef	struct	_CONNECTIONREQUEST
{
	unsigned long	ip;
	unsigned long	dwHit;
	int	nHit;
	bool	bBlock;
}	CONNECTIONREQUEST, * PCONNECTIONREQUEST;

typedef	map<unsigned long, PCONNECTIONREQUEST>	ULONGMAP;
typedef deque<unsigned long, allocator<unsigned long> >	ULONGDEQUE;

#define	MAX_QUEUEITEM	8196
#define	MAX_HIT		4

class CDoS : public ULONGMAP
{
public:
	//	Constructions
	CDoS();
	virtual	~CDoS();
	//	Operations
	bool	IsDoS(unsigned long addr);
private:
	PCONNECTIONREQUEST	Get(unsigned long addr);
	void	Add(unsigned long addr);
private:
	MemPooler<CONNECTIONREQUEST>* m_pPool;
	ULONGDEQUE	m_q;
};

#endif	// __DOS_H__