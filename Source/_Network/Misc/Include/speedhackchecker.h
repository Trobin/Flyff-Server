#ifndef __SPEEDHACKCHECKER_H__
#define	__SPEEDHACKCHECKER_H__

#include "dpmng.h"
#include "msghdr.h"

class CSpeedHackChecker
{
private:
	//	time_t	m_tBaseOfServer;
	//	time_t	m_tBaseOfClient;
	unsigned long m_tBaseOfServer;
	unsigned long m_tBaseOfClient;
	bool	m_fKill;
public:
	//	Constructions
	CSpeedHackChecker();
	~CSpeedHackChecker();
	//	Operations
	void	sETbaseOfServer(CDPMng* pDPMng, DPID dpidPlayer);
	//	void	sETbaseOfClient( time_t tBaseOfClient );
	void	sETbaseOfClient(unsigned long tBaseOfClient);
	void	rEQoffsetOfClient(CDPMng* pDPMng, DPID dpidPlayer);
	//	bool	CheckClock( time_t tCurClient, CDPMng* pDPMng, DPID dpidPlayer );
	bool	CheckClock(unsigned long tCurClient, CDPMng* pDPMng, DPID dpidPlayer);
	bool	fsETbaseOfClient(void) { return(bool)m_tBaseOfClient; }
	void	Kill(void) { m_fKill = true; }
};

#endif	// __SPEEDHACKCHECKER_H__