#ifndef __MSGHDR_H__
#define __MSGHDR_H__

#pragma once

//	(unsigned long)0x00000000	// reserved
#define PACKETTYPE_MYREG						(unsigned long)0x00000001
#define PACKETTYPE_PROCSERVER_LIST				(unsigned long)0x00000002
#define PACKETTYPE_LOAD_WORLD					(unsigned long)0x00000003
#define PACKETTYPE_RECHARGE_IDSTACK				(unsigned long)0x00000004
#define	PACKETTYPE_PASSAGE						(unsigned long)0x00000005
#define PACKETTYPE_DUPLICATE					(unsigned long)0x00000006
#define PACKETTYPE_UNIFY						(unsigned long)0x00000007
#define PACKETTYPE_ID							(unsigned long)0x00000008
#define PACKETTYPE_ADDID						(unsigned long)0x00000009
#define	PACKETTYPE_POLL							(unsigned long)0x0000000a
#define	PACKETTYPE_QUERYTICKCOUNT				(unsigned long)0x0000000b
#define	PACKETTYPE_ERROR_TEXT					(unsigned long)0x0000000d
#define	PACKETTYPE_PLAYERCOUNT					(unsigned long)0x0000000f
#define	PACKETTYPE_QUERYSETGUILDNAME			(unsigned long)0x00000010
#define	PACKETTYPE_AUTHQUERY					(unsigned long)0x00000011
#define	PACKETTYPE_QUERYSETPLAYERNAME			(unsigned long)0x00000012
#define	PACKETTYPE_CONN							(unsigned long)0x00000013
#define	PACKETTYPE_PING							(unsigned long)0x00000014
#define	PACKETTYPE_GET_CLOCK					(unsigned long)0x00000015
#define	PACKETTYPE_CLOSE_EXISTING_CONNECTION	(unsigned long)0x00000016
#define PACKETTYPE_ONE_HOUR_NOTIFY				(unsigned long)0x00000017
#define	PACKETTYPE_KEEP_ALIVE					(unsigned long)0x00000018
#define	PACKETTYPE_RUNEQ						(unsigned long)0x00000019

#define	PACKETTYPE_QUERYPOSTMAIL				(unsigned long)0x0000001a
#define	PACKETTYPE_QUERYREMOVEMAIL				(unsigned long)0x0000001b
#define	PACKETTYPE_QUERYGETMAILITEM				(unsigned long)0x0000001c
#define	PACKETTYPE_QUERYMAILBOX					(unsigned long)0x0000001d
#define	PACKETTYPE_ALLMAIL						(unsigned long)0x0000001e
#define	PACKETTYPE_QUERYGETMAILGOLD				(unsigned long)0x0000001f
#define	PACKETTYPE_READMAIL						(unsigned long)0x00000024

#define	PACKETTYPE_DO_ESCAPE					(unsigned long)0x00000020
#define	PACKETTYPE_SETSNOOP						(unsigned long)0x00000021
#define	PACKETTYPE_SETSNOOPGUILD				(unsigned long)0x00000022
#define	PACKETTYPE_SNOOP						(unsigned long)0x00000023
//0x00000024	reserved
#if __VER >= 12 // __ITEMCREATEMON_S0602
#define PACKETTYPE_CREATEMONSTER				(unsigned long)0x0000002a
#endif // __ITEMCREATEMON_S0602
#define	PACKETTYPE_QUERY_DESTROY_PLAYER			(unsigned long)0x00000030

#define	PACKETTYPE_NEW_ACCOUNT					(unsigned long)0x000000f0
#define	PACKETTYPE_DEL_ACCOUNT					(unsigned long)0x000000f1
#define	PACKETTYPE_CACHE_ADDR	(unsigned long)0x000000f2
#define PACKETTYPE_PLAYER_LIST	(unsigned long)0x000000f3
#define	PACKETTYPE_CREATE_PLAYER	(unsigned long)0x000000f4
#define	PACKETTYPE_DEL_PLAYER	(unsigned long)0x000000f5
#define	PACKETTYPE_GETPLAYERLIST	(unsigned long)0x000000f6
#define	PACKETTYPE_SEL_PLAYER	(unsigned long)0x000000f7
#define	PACKETTYPE_SAVE_PLAYER	(unsigned long)0x000000f8
#define PACKETTYPE_GT	(unsigned long)0x000000f9
#define	PACKETTYPE_ALLPLAYERID	(unsigned long)0x000000fa
#define	PACKETTYPE_BUSY		(unsigned long)0x000000fb
#define	PACKETTYPE_CERTIFY	(unsigned long)0x000000fc
#define	PACKETTYPE_SRVR_LIST	(unsigned long)0x000000fd
#define PACKETTYPE_ERROR	(unsigned long)0x000000fe
#define	PACKETTYPE_PLAYERID	(unsigned long)0x000000ff

//#define	PACKETTYPE_RETRY_CERT	(unsigned long)0x00000ff0
//#define	PACKETTYPE_PLAYER_JOINED	(unsigned long)0x00000ff1
#define	PACKETTYPE_REMOVEPLAYERID	(unsigned long)0x00000ff2
#define	PACKETTYPE_PLAYER_COUNT	(unsigned long)0x00000ff3
#define	PACKETTYPE_ENABLE_SERVER	(unsigned long)0x00000ff4
#define	PACKETTYPE_FAIL	(unsigned long)0x00000ff5
#define	PACKETTYPE_NEWYEAR	(unsigned long)0x00000ff6

/* patch
0x00000f00~0x00000fff
*/
#define PACKETTYPE_EXPBOXINFO				    (unsigned long)0x00000f00
#define PACKETTYPE_SEND_TO_CLIENT_NOTICE		(unsigned long)0x00000f01
#define PACKETTYPE_SEND_TO_CLIENT_FILE_LIST		(unsigned long)0x00000f02
#define PACKETTYPE_SEND_TO_SERVER_PATCH			(unsigned long)0x00000f03
#define PACKETTYPE_SEND_TO_CLIENT_FILE			(unsigned long)0x00000f04
#define PACKETTYPE_SEND_TO_CLIENT_PATCH			(unsigned long)0x00000f05
#define PACKETTYPE_SEND_TO_CLIENT_PATCH_END		(unsigned long)0x00000f06
#define PACKETTYPE_SEND_TO_CLIENT_PATCHCLIENTINFO (unsigned long)0x00000f07
#define PACKETTYPE_SEND_TO_SERVER_PATCHCLIENT_CORRECT (unsigned long)0x00000f08
#define PACKETTYPE_SEND_TO_SERVER_PATCHCLIENT_INCORRECT (unsigned long)0x00000f09
#define PACKETTYPE_SEND_TO_CLIENT_PATCHCLIENT (unsigned long)0x00000f0a
#define PACKETTYPE_SEND_TO_SERVER_REQUIRE_FILE (unsigned long)0x00000f0b
#define PACKETTYPE_SEND_TO_SERVER_REQUIRE_FILE_LIST (unsigned long)0x00000f0c
#define PACKETTYPE_SEND_TO_CLIENT_CDN			(unsigned long)0x00000f0d

#define PACKETTYPE_SEND_TO_SERVER_EXP			(unsigned long)0x00000f31  // seghope 서버에 경험치를 보냄(속성으로 렙업이 가능하게하는 패킷)
#define PACKETTYPE_SEND_TO_SERVER_CHANGEJOB		(unsigned long)0x00000f32  // 전직( 속성으로 전직 번호를 줌 )
#define PACKETTYPE_SEND_TO_SERVER_AP			(unsigned long)0x00000f33  // 액션 포인트 보냄.
#define PACKETTYPE_CHANGEJOB					(unsigned long)0x00000f34  // 액션 포인트 보냄.
#define PACKETTYPE_ITEM_TBL_UPDATE				(unsigned long)0x00000f35  // 아이템 업데이트

#define PACKETTYPE_JOIN		(unsigned long)0x0000ff00
#define PACKETTYPE_LEAVE	(unsigned long)0x0000ff01
#define	PACKETTYPE_DESTROY_ALLPLAYERS	(unsigned long)0x0000ff02
#define	PACKETTYPE_REMOVE_ALLACCOUNTS	(unsigned long)0x0000ff03
#define	PACKETTYPE_CLOSE_ERROR	(unsigned long)0x0000ff04
#define	PACKETTYPE_PRE_JOIN	(unsigned long)0x0000ff05

#define PACKETTYPE_GAMERATE				(unsigned long)0x0000ff06
#define PACKETTYPE_SETMONSTERRESPAWN	(unsigned long)0x0000ff07
#define PACKETTYPE_SETITEMEVENT			(unsigned long)0x0000ff08
#define PACKETTYPE_LOADCONSTANT			(unsigned long)0x0000ff09


#define PACKETTYPE_CHAT			(unsigned long)0x00ff0000
#define	PACKETTYPE_ACTMSG		(unsigned long)0x00ff0001
#define	PACKETTYPE_ADDOBJ		(unsigned long)0x00ff0002
#define	PACKETTYPE_REMOVEOBJ	(unsigned long)0x00ff0003
#define PACKETTYPE_CONTROL		(unsigned long)0x00ff0004

#define	PACKETTYPE_BROADCAST	(unsigned long)0x000f0001
#define	PACKETTYPE_SAVEALLPLAYERS	(unsigned long)0x000f0002
#define PACKETTYPE_DOUSESKILLPOINT	(unsigned long)0x000f0003
#define PACKETTYPE_SKILLPOINTLOG	(unsigned long)0x000f0004

#define PACKETTYPE_CREATEITEM	(unsigned long)0x00ff0005
#define	PACKETTYPE_MOVEITEM		(unsigned long)0x00ff0006
#define	PACKETTYPE_DROPITEM		(unsigned long)0x00ff0007
#define	PACKETTYPE_DROPGOLD	(unsigned long)0x00ff0008
#define PACKETTYPE_REMOVEITEM	(unsigned long)0x00ff0009
#define	PACKETTYPE_SYNCITEM		(unsigned long)0x00ff000a
#define	PACKETTYPE_DOEQUIP	(unsigned long)0x00ff000b
#define	PACKETTYPE_DAMAGE	(unsigned long)0x00ff000c
#define	PACKETTYPE_SETEXPERIENCE	(unsigned long)0x00ff000d
#define	PACKETTYPE_REMOVEVENDORITEM	(unsigned long)0x00ff000e
#define	PACKETTYPE_REMOVEALLITEM	(unsigned long)0x00ff000f

#define	PACKETTYPE_MELEE_ATTACK		(unsigned long)0x00ff0010
#define	PACKETTYPE_MAGIC_ATTACK		(unsigned long)0x00ff0011
#define	PACKETTYPE_RANGE_ATTACK		(unsigned long)0x00ff0012
#define	PACKETTYPE_MOVERDEATH		(unsigned long)0x00ff0013
#define	PACKETTYPE_MELEE_ATTACK2	(unsigned long)0x00ff0014

#define PACKETTYPE_SHIP_ACTMSG		(unsigned long)0x00ff0015
#define	PACKETTYPE_MOTION			(unsigned long)0x00ff0016
#define	PACKETTYPE_SETFXP			(unsigned long)0x00ff0017

#define	PACKETTYPE_SETNAVIPOINT		(unsigned long)0x00ff0018
#define	PACKETTYPE_REMOVEINVENITEM	(unsigned long)0x00ff0019

#define	PACKETTYPE_USESKILL	(unsigned long)0x00ff0020
#define PACKETTYPE_DOUSEITEM	(unsigned long)0x00ff0021
#define PACKETTYPE_SFX_ID	(unsigned long)0x00ff0022
#define	PACKETTYPE_SETTARGET	(unsigned long)0x00ff0023
#define PACKETTYPE_SFX_CLEAR	(unsigned long)0x00ff0024
#define PACKETTYPE_TELESKILL	(unsigned long)0x00ff0025
#define PACKETTYPE_REMOVEQUEST	(unsigned long)0x00ff0026
#define PACKETTYPE_TRADECONFIRM		(unsigned long)0x00ff002f
#define	PACKETTYPE_TRADE				(unsigned long)0x00ff00a0
#define	PACKETTYPE_TRADEPUT				(unsigned long)0x00ff00a1
#define	PACKETTYPE_TRADEPULL			(unsigned long)0x00ff00a2
#define	PACKETTYPE_TRADEOK				(unsigned long)0x00ff00a3
#define	PACKETTYPE_TRADECANCEL			(unsigned long)0x00ff00a4
#define	PACKETTYPE_TRADEPUTGOLD			(unsigned long)0x00ff00a5
#define	PACKETTYPE_TRADECLEARGOLD		(unsigned long)0x00ff00a6
#define PACKETTYPE_CONFIRMTRADE			(unsigned long)0x00ff00a7
#define PACKETTYPE_CONFIRMTRADECANCEL	(unsigned long)0x00ff00a8

#define	PACKETTYPE_PVENDOR_OPEN	(unsigned long)0x00ff00a9
#define	PACKETTYPE_PVENDOR_CLOSE		(unsigned long)0x00ff00aa
#define	PACKETTYPE_REGISTER_PVENDOR_ITEM		(unsigned long)0x00ff00ab
#define	PACKETTYPE_QUERY_PVENDOR_ITEM	(unsigned long)0x00ff00ac
#define	PACKETTYPE_BUY_PVENDOR_ITEM		(unsigned long)0x00ff00ad
#define	PACKETTYPE_UNREGISTER_PVENDOR_ITEM	(unsigned long)0x00ff00ae
#define	PACKETTYPE_SET_HAIR		(unsigned long)0x00ff00af

#define	PACKETTYPE_SCRIPTDLG	(unsigned long)0x00ff00b0
#define	PACKETTYPE_OPENSHOPWND	(unsigned long)0x00ff00b1
#define	PACKETTYPE_CLOSESHOPWND	(unsigned long)0x00ff00b2
#define	PACKETTYPE_BUYITEM	(unsigned long)0x00ff00b3
#define PACKETTYPE_SELLITEM		(unsigned long)0x00ff00b4

#define	PACKETTYPE_REPAIRITEM	(unsigned long)0x00ff00b5
#define PACKETTYPE_BUYCHIPITEM	(unsigned long)0x00ff00b6

#define	PACKETTYPE_REVIVAL						(unsigned long)0x00ff00c0
#define	PACKETTYPE_REVIVAL_TO_LODESTAR			(unsigned long)0x00ff00c1
#define	PACKETTYPE_REVIVAL_TO_LODELIGHT			(unsigned long)0x00ff00c2
#define	PACKETTYPE_SETLODELIGHT					(unsigned long)0x00ff00c3
#define	PACKETTYPE_INC_STAT_LEVEL				(unsigned long)0x00ff00c4
#define	PACKETTYPE_INC_JOB_LEVEL				(unsigned long)0x00ff00c5

#define	PACKETTYPE_ADDEXPERIENCE	(unsigned long)0x00ff00d0
#define PACKETTYPE_SET_GROWTH_LEARNING_POINT	(unsigned long)0x00ff00d1
#define	PACKETTYPE_SFX_HIT	(unsigned long)0x00ff00d2
#define	PACKETTYPE_SETPOINTPARAM	(unsigned long)0x00ff00d3
#define	PACKETTYPE_WHISPER	(unsigned long)0x00ff00d4
#define	PACKETTYPE_ENDSKILLQUEUE				(unsigned long)0x00ff00d5


#define	PACKETTYPE_CAPTION	(unsigned long)0x00ff00d6

#define	PACKETTYPE_SAY	(unsigned long)0x00ff00e0
#define	PACKETTYPE_SHOUT	(unsigned long)0x00ff00e1
#define	PACKETTYPE_PLAYMUSIC	(unsigned long)0x00ff00e2
#define	PACKETTYPE_PLAYSOUND	(unsigned long)0x00ff00e3
#define	PACKETTYPE_SUMMONPLAYER		(unsigned long)0x00ff00e4
#define	PACKETTYPE_TELEPORTPLAYER	(unsigned long)0x00ff00e5
#define	PACKETTYPE_KILLPLAYER	(unsigned long)0x00ff00e6
#define	PACKETTYPE_GETPLAYERADDR	(unsigned long)0x00ff00e7
#define	PACKETTYPE_GETPLAYERCOUNT	(unsigned long)0x00ff00e8
#define	PACKETTYPE_GETCOREPLAYER	(unsigned long)0x00ff00e9
#define	PACKETTYPE_SYSTEM	(unsigned long)0x00ff00ea
#define	PACKETTYPE_MODIFYMODE		(unsigned long)0x00ff00eb
#define	PACKETTYPE_DEFINEDTEXT	(unsigned long)0x00ff00ec
#define PACKETTYPE_GMSAY	(unsigned long)0x00ff00ed               // 겜마에게 귓속말


#define PACKETTYPE_CHANGEFACE				(unsigned long)0x00ff00ee	// 얼굴바꾸기
#define PACKETTYPE_NW_WANTED_GOLD			(unsigned long)0x00ff00ef	// 현상금 걸기 패킷 
#define PACKETTYPE_NW_WANTED_LIST			(unsigned long)0x00ff00f0   // 현상금 리스트 요청 패킷
#define PACKETTYPE_WN_WANTED_LIST			(unsigned long)0x00ff00f1   // 현상금 리스트 응답 패킷
#define PACKETTYPE_NW_WANTED_INFO			(unsigned long)0x00ff00f2   // 현상범 자세한정보 요청 패킷 
#define PACKETTYPE_WN_WANTED_INFO			(unsigned long)0x00ff00f3   // 현상범 자세한정보 패킷 

#define PACKETTYPE_WC_WANTED_GOLD			(unsigned long)0x00ff00f4	// 현상금을 설정 
#define PACKETTYPE_WC_WANTED_REWARD			(unsigned long)0x00ff00f5	// 특정 플레이어의 현상금액 구하기 
#define PACKETTYPE_CW_WANTED_LIST			(unsigned long)0x00ff00f6	// 현상금 리스트 보내기 
#define PACKETTYPE_CW_WANTED_REWARD			(unsigned long)0x00ff00f7	// 현상금을 얻는다.

#define PACKETTYPE_CT_WANTED				(unsigned long)0x00ff00f8	// 현상금을 처리요청 
#define PACKETTYPE_TC_LIST					(unsigned long)0x00ff00f9	// 현상금 리스트 얻기 
#define PACKETTYPE_REQ_LEAVE				(unsigned long)0x00ff00fa	// 접속 종료시도 패킷 
#define PACKETTYPE_WT_REQ_LEAVE				(unsigned long)0x00ff00fb	// World->Trans 접속 종료시도 패킷 
#define PACKETTYPE_NW_WANTED_NAME			(unsigned long)0x00ff00fc	// 현상범 이름 요청 
#define PACKETTYPE_WN_WANTED_NAME			(unsigned long)0x00ff00fd	// 현상범 이름 응답 

#define PACKETTYPE_REPLACE					(unsigned long)0x00ff0f00
#define	PACKETTYPE_CORR_REQ					(unsigned long)0x00ff0ff0
#define	PACKETTYPE_SCRIPT_REMOVE_ALL_ITEM	(unsigned long)0x00ff0ff1
#define	PACKETTYPE_SCRIPT_EQUIP_ITEM		(unsigned long)0x00ff0ff2
#define	PACKETTYPE_SETQUEST					(unsigned long)0x00ff0ff3
#define	PACKETTYPE_SCRIPT_CREATE_ITEM		(unsigned long)0x00ff0ff4
#define	PACKETTYPE_SCRIPT_ADD_GOLD			(unsigned long)0x00ff0ff5
#define	PACKETTYPE_SCRIPT_REMOVE_QUEST		(unsigned long)0x00ff0ff6
#define	PACKETTYPE_SCRIPT_REPLACE			(unsigned long)0x00ff0ff7
#define	PACKETTYPE_SCRIPT_REPLACE_KEY		(unsigned long)0x00ff0ff8
#define	PACKETTYPE_SCRIPT_ADD_EXP      		(unsigned long)0x00ff0ff9
#define	PACKETTYPE_SCRIPT_REMOVE_GOLD	(unsigned long)0x00ff0ffa
#define	PACKETTYPE_SCRIPT_TRANS_PXP		(unsigned long)0x00ff0ffb
#define	PACKETTYPE_SCRIPT_TRANS_PENYA	(unsigned long)0x00ff0ffc
#define	PACKETTYPE_CREATE_GUILDCLOAK	(unsigned long)0x00ff0ffd
//#define PACKETTYPE_DO_COLLECT			(unsigned long)0x00ff0ffe
#define PACKETTYPE_LOCALPOSFROMIA		(unsigned long)0x00ff0fff

#if __VER >= 8 //__CSC_VER8_5
#define PACKETTYPE_CREATEANGEL			(unsigned long)0x00fff000
#define PACKETTYPE_SUMMONITEMUSE		(unsigned long)0x00fff001
#define PACKETTYPE_ANGELBUFF			(unsigned long)0x00fff002
#endif //__CSC_VER8_5

#define	PACKETTYPE_SNAPSHOT	(unsigned long)0xffffff00
#define	PACKETTYPE_PLAYERMOVED	(unsigned long)0xffffff01
#define	PACKETTYPE_PLAYERBEHAVIOR	(unsigned long)0xffffff02
#define	PACKETTYPE_PLAYERMOVED2	(unsigned long)0xffffff03
#define	PACKETTYPE_PLAYERBEHAVIOR2	(unsigned long)0xffffff04
#define	PACKETTYPE_PLAYERCORR	(unsigned long)0xffffff05
#define	PACKETTYPE_PLAYERCORR2	(unsigned long)0xffffff06
#define	PACKETTYPE_PLAYERSETDESTOBJ	(unsigned long)0xffffff07
#define	PACKETTYPE_QUERYGETPOS	(unsigned long)0xffffff08
#define	PACKETTYPE_GETPOS	(unsigned long)0xffffff09

#define	PACKETTYPE_REMOVEAPPLETTASKBAR	(unsigned long)0xffffff0a
#define	PACKETTYPE_ADDAPPLETTASKBAR	(unsigned long)0xffffff0b
#define	PACKETTYPE_ADDITEMTASKBAR	(unsigned long)0xffffff0c
#define	PACKETTYPE_REMOVEITEMTASKBAR	(unsigned long)0xffffff0d
#define	PACKETTYPE_SKILLTASKBAR	(unsigned long)0xffffff0e

#define	PACKETTYPE_MOVERDESTPOS	(unsigned long)0xffffff0f

#define PACKETTYPE_ERRORPARTY			(unsigned long)0xffffff10
#define PACKETTYPE_ADDPARTYMEMBER		(unsigned long)0xffffff11
#define PACKETTYPE_REMOVEPARTYMEMBER	(unsigned long)0xffffff12
#define PACKETTYPE_ADDPLAYERPARTY		(unsigned long)0xffffff13
#define PACKETTYPE_REMOVEPLAYERPARTY	(unsigned long)0xffffff14
#define PACKETTYPE_MEMBERLEVEL			(unsigned long)0xffffff15
#define PACKETTYPE_MEMBERJOB			(unsigned long)0xffffff16
#define PACKETTYPE_MEMBERREQUEST		(unsigned long)0xffffff17
#define PACKETTYPE_MEMBERREQUESTCANCLE  (unsigned long)0xffffff18
#define PACKETTYPE_CHANGETROUP			(unsigned long)0xffffff19
#define PACKETTYPE_CHANPARTYNAME		(unsigned long)0xffffff1a
#define PACKETTYPE_PARTYSKILLUSE		(unsigned long)0xffffff1b
#define PACKETTYPE_SETPARTYMODE			(unsigned long)0xffffff1c
//(unsigned long)0xffffff1d
#define PACKETTYPE_ADDPARTYEXP			(unsigned long)0xffffff1e
#define PACKETTYPE_REMOVEPARTYPOINT		(unsigned long)0xffffff1f

#define PACKETTYPE_PARTYCHANGEITEMMODE	(unsigned long)0xffffff20
#define PACKETTYPE_PARTYCHANGEEXPMODE	(unsigned long)0xffffff21

#define	PACKETTYPE_SETPARTYEXP	(unsigned long)0xffffff22

#define PACKETTYPE_DUELREQUEST		(unsigned long)0xffffff23
#define PACKETTYPE_DUELYES			(unsigned long)0xffffff24
#define PACKETTYPE_DUELNO			(unsigned long)0xffffff25

#define PACKETTYPE_DUELPARTYREQUEST		(unsigned long)0xffffff26
#define PACKETTYPE_DUELPARTYYES			(unsigned long)0xffffff27
#define PACKETTYPE_DUELPARTYNO			(unsigned long)0xffffff28
#define	PACKETTYPE_PLAYERANGLE			(unsigned long)0xffffff29

#define	PACKETTYPE_SETPARTYDUEL		(unsigned long)0xffffff2a

#define PACKETTYPE_QUERYPLAYERLISTSTRING 	(unsigned long)0xffffff2b
#define PACKETTYPE_MOVERFOCOUS			(unsigned long)0xffffff2d

#define	PACKETTYPE_PARTYCHANGELEADER	(unsigned long)0xffffff2f

#define	PACKETTYPE_GUILD				(unsigned long)0xffffff30
#define	PACKETTYPE_CREATE_GUILD			(unsigned long)0xffffff31
#define	PACKETTYPE_DESTROY_GUILD		(unsigned long)0xffffff32
#define	PACKETTYPE_ADD_GUILD_MEMBER		(unsigned long)0xffffff33
#define	PACKETTYPE_REMOVE_GUILD_MEMBER	(unsigned long)0xffffff34
#define	PACKETTYPE_GUILD_INVITE			(unsigned long)0xffffff35
#define	PACKETTYPE_IGNORE_GUILD_INVITE	(unsigned long)0xffffff36

#define	PACKETTYPE_QUERYPLAYERSTRING	(unsigned long)0xffffff37
#define	PACKETTYPE_GLOBAL_DATA	(unsigned long)0xffffff38

#define	PACKETTYPE_GUILD_CHAT			(unsigned long)0xffffff39
#define	PACKETTYPE_GUILD_MEMBER_LEVEL	(unsigned long)0xffffff3a
#define PACKETTYPE_GUILD_BANK_QUERY		(unsigned long)0xffffff3b
#define PACKETTYPE_GUILD_BANK			(unsigned long)0xffffff3c
#define PACKETTYPE_GUILD_BANK_UPDATE	(unsigned long)0xffffff3d
#define PACKETTYPE_GUILD_BANK_WND_CLOSE	(unsigned long)0xffffff3e
#define PACKETTYPE_GUILD_BANK_MOVEITEM	(unsigned long)0xffffff3f


#define PACKETTYPE_OPENBANKWND		(unsigned long)0xffffff40
#define PACKETTYPE_CLOSEBANKWND		(unsigned long)0xffffff41
#define PACKETTYPE_PUTITEMBACK		(unsigned long)0xffffff42
#define	PACKETTYPE_PUTGOLDBACK		(unsigned long)0xffffff43
#define PACKETTYPE_GETITEMBACK		(unsigned long)0xffffff44
#define	PACKETTYPE_GETGOLDBACK		(unsigned long)0xffffff45
#define PACKETTYPE_MOVEBANKITEM		(unsigned long)0xffffff46
#define PACKETTYPE_CHANGEBANKPASS	(unsigned long)0xffffff47
#define PACKETTYPE_CONFIRMBANK		(unsigned long)0xffffff48
#define PACKETTYPE_PUTBACKTOBANK	(unsigned long)0xffffff49

#define PACKETTYPE_ENVIRONMENTSNOW	(unsigned long)0xffffff50
#define PACKETTYPE_ENVIRONMENTRAIN	(unsigned long)0xffffff51
#define PACKETTYPE_FALLRAIN			(unsigned long)0xffffff52
#define PACKETTYPE_FALLSNOW			(unsigned long)0xffffff53
#define PACKETTYPE_STOPRAIN			(unsigned long)0xffffff54
#define PACKETTYPE_STOPSNOW			(unsigned long)0xffffff55
#define PACKETTYPE_ENVIRONMENTALL	(unsigned long)0xffffff56
#define PACKETTYPE_PARTYCHAT		(unsigned long)0xffffff59
#define PACKETTYPE_BLOCK			(unsigned long)0xffffff5a	// Friend Block
#ifdef __S_NEW_SKILL_2
#define PACKETTYPE_SAVE_SKILL		(unsigned long)0xffffff5b
#endif // __S_NEW_SKILL_2

#define PACKETTYPE_ADDFRIEND		(unsigned long)0xffffff60
#define PACKETTYPE_ADDFRIENDREQEST  (unsigned long)0xffffff61
#define PACKETTYPE_ADDFRIENDCANCEL  (unsigned long)0xffffff62
#define PACKETTYPE_GETFRIENDNAME	(unsigned long)0xffffff63
#define PACKETTYPE_GETFRIENDSTATE	(unsigned long)0xffffff64
#define PACKETTYPE_ADDFRIENDJOIN	(unsigned long)0xffffff65
#define PACKETTYPE_ADDFRIENDLOGOUT	(unsigned long)0xffffff66
#define PACKETTYPE_SETFRIENDSTATE	(unsigned long)0xffffff67
#define PACKETTYPE_FRIENDINTERCEPTSTATE		(unsigned long)0xffffff68
#define PACKETTYPE_FRIENDNOINTERCEPT		(unsigned long)0xffffff69
#define PACKETTYPE_REMOVEFRIEND		(unsigned long)0xffffff6a
#define PACKETTYPE_ADDFRIENDNAMEREQEST		(unsigned long)0xffffff6b
#define PACKETTYPE_ADDFRIENDNAMENOTFOUND	(unsigned long)0xffffff6c
#define PACKETTYPE_REMOVEFRIENDSTATE		(unsigned long)0xffffff6d
#define PACKETTYPE_ONEFRIEMDSTATE			(unsigned long)0xffffff6e

#define PACKETTYPE_PARTYNAME			(unsigned long)0xffffff70
#define PACKETTYPE_ADDPARTYNAME		(unsigned long)0xffffff71

#define	PACKETTYPE_QUERYGETDESTOBJ	(unsigned long)0xffffff72
#define	PACKETTYPE_GETDESTOBJ	(unsigned long)0xffffff73

#define	PACKETTYPE_GUILD_CLASS			(unsigned long)0xffffff74
#define	PACKETTYPE_GUILD_NICKNAME		(unsigned long)0xffffff75
#define PACKETTYPE_GUILD_MEMBERTIME		(unsigned long)0xffffff76
#define PACKETTYPE_GUILD_MEMBERLOGOUT	(unsigned long)0xffffff77
#define PACKETTYPE_RESURRECTION_OK	(unsigned long)0xffffff78
#define PACKETTYPE_RESURRECTION_CANCEL	(unsigned long)0xffffff79

#define PACKETTYPE_STATEMODE		(unsigned long)0xffffff7a
#define PACKETTYPE_MODE				(unsigned long)0xffffff7b

#define	PACKETTYPE_CHEERING		(unsigned long)0xffffff7c

#ifdef __S_RECOMMEND_EVE
#define PACKETTYPE_EVE_RECOMMEND	(unsigned long)0xffffff7d
#endif // __S_RECOMMEND_EVE

#define PACKETTYPE_SAVE_CONCURRENT_USER_NUMBER (unsigned long)0x0f000f00
#define PACKETTYPE_SAVE_CONCURRENT_FIELD_USER_NUMBER (unsigned long)0x0f000f01
#define	PACKETTYPE_LOG_PLAY_CONNECT	(unsigned long)0x0f000f02
#define PACKETTYPE_LOG_PLAY_DEATH	(unsigned long)0x0f000f03
#define PACKETTYPE_LOG_LEVELUP		(unsigned long)0x0f000f04
#define PACKETTYPE_LOG_SERVER_DEATH	(unsigned long)0x0f000f05
#define PACKETTYPE_LOG_UNIQUEITEM	(unsigned long)0x0f000f06
#define PACKETTYPE_LOG_ALLITEM		(unsigned long)0x0f000f07
#define PACKETTYPE_LOG_QUEST		(unsigned long)0x0f000f08
#define PACKETTYPE_LOG_GAMEMASTER_CHAT	(unsigned long)0x0f000f09
#define PACKETTYPE_LOG_PK_PVP		(unsigned long)0x0f000f0a
#define PACKETTYPE_LOG_SCHOOL		(unsigned long)0x0f000f0b
#define PACKETTYPE_PREVENT_LOGIN	(unsigned long)0x0f000f0c

#define	PACKETTYPE_FREQUENCY_LOG	(unsigned long)0x0f000f0f

#define	PACKETTYPE_MONITOR_ID	(unsigned long)0xf0008000
#define	PACKETTYPE_CONSOLE_COMMAND	(unsigned long)0xf0008001
#define	PACKETTYPE_CONSOLE_OVERVIEW	(unsigned long)0xf0008002
#define	PACKETTYPE_CONSOLE_FILEHEADER (unsigned long)0xf0008003
#define	PACKETTYPE_CONSOLE_FILEDATA   (unsigned long)0xf0008004
#define PACKETTYPE_CONSOLE_UPLOADACK  (unsigned long)0xf0008005
#define PACKETTYPE_CONSOLE_EXPAND	(unsigned long)0xf0008006
#define PACKETTYPE_CONSOLE_SYNCACK	(unsigned long)0xf0008007
#define PACKETTYPE_CONSOLE_EXPANDACK (unsigned long)0xf0008008
#define	PACKETTYPE_CONSOLE_OVERVIEW2	(unsigned long)0xf0008009

#define	PACKETTYPE_PROCESS_CREATED	(unsigned long)0xf0009000
#define	PACKETTYPE_PROCESS_TERMINATED	(unsigned long)0xf0009001
#define	PACKETTYPE_MY_PROCESS	(unsigned long)0xf0009002
#define	PACKETTYPE_CREATE_PROCESS	(unsigned long)0xf000a000
#define	PACKETTYPE_TERMINATE_PROCESS	(unsigned long)0xf000a001
#define	PACKETTYPE_TERMINATE_PROCESS_AND_QUIT	(unsigned long)0xf000a002
#define	PACKETTYPE_PROCESS_MODE		(unsigned long)0xf000a003
#define	PACKETTYPE_ADD_ACCOUNT		(unsigned long)0xf000b000
#define	PACKETTYPE_REMOVE_ACCOUNT	(unsigned long)0xf000b001
#define	PACKETTYPE_ROUTE			(unsigned long)0xf000b002
#define	PACKETTYPE_DESTROY_PLAYER	(unsigned long)0xf000b003
#define PACKETTYPE_ERRORCODE		(unsigned long)0xf000b004
#define PACKETTYPE_PROCESS_CREATED2		(unsigned long)0xf000b005 
#define	PACKETTYPE_PROCESS_TERMINATED2	(unsigned long)0xf000b006
#define	PACKETTYPE_MY_PROCESS2			(unsigned long)0xf0009007
#define PACKETTYPE_RENEWEVNET		(unsigned long)0xf000b005

// TAG관련 
#define PACKETTYPE_TAG				(unsigned long)0xf000b006
#define PACKETTYPE_SENDTAG			(unsigned long)0xf000b007
#define PACKETTYPE_INSERTTAG_RESULT	(unsigned long)0xf000b008

#define PACKETTYPE_PARTYLEVEL		(unsigned long)0xf000b009

#define PACKETTYPE_WC_GUILDLOGO		(unsigned long)0xf000b00a	// 로고변경 
#define PACKETTYPE_WC_GUILDCONTRIBUTION    (unsigned long)0xf000b00b	// 공헌 (페냐와 PXP공헌 횟수)
#define PACKETTYPE_WC_GUILDNOTICE   (unsigned long)0xf000b00c	// 공지사항 변경 
#define PACKETTYPE_CW_GUILDLOGO		(unsigned long)0xf000b00d	// 로고변경 
#define PACKETTYPE_CW_GUILDCONTRIBUTION	(unsigned long)0xf000b00e	// 공헌 
#define PACKETTYPE_CW_GUILDNOTICE   (unsigned long)0xf000b00f	// 공지사항 변경 

#define	PACKETTYPE_NW_GUILDLOGO			(unsigned long)0xf000b010	// 로고 변경 
#define	PACKETTYPE_NW_GUILDCONTRIBUTION	(unsigned long)0xf000b011	// 공헌도 
#define	PACKETTYPE_NW_GUILDNOTICE		(unsigned long)0xf000b012	// 공지사항 
#define	PACKETTYPE_FOCUSOBJ				(unsigned long)0xf000b013	

#define	PACKETTYPE_CALL_USPLOGGINGQUEST		(unsigned long)0xf000b014

// World에서 Neuz로는 snapshop으로 

#define PACKETTYPE_CD_GUILD_LOGO			(unsigned long)0xf000b016	// 로고 
#define PACKETTYPE_WD_GUILD_CONTRIBUTION	(unsigned long)0xf000b017	// 공헌 
#define PACKETTYPE_CD_GUILD_NOTICE			(unsigned long)0xf000b018	// 공지 

#define PACKETTYPE_GUILD_BANK_WND			(unsigned long)0xf000b020	// 길드 창고 오픈 
#define PACKETTYPE_PUTITEMGUILDBANK			(unsigned long)0xf000b021	// 길드 창고 아이템 넣기(풋~~ ^^;;; )
#define PACKETTYPE_GETITEMGUILDBANK			(unsigned long)0xf000b022	
#define PACKETTYPE_GUILD_MSG_CONTROL		(unsigned long)0xf000b023	

#define PACKETTYPE_GUILD_AUTHORITY			(unsigned long)0xf000b026	// 길드 권한 설정( Authority가 권한이라니~ 꾹~~ ㅋㄷㅋㄷ )
#define PACKETTYPE_GUILD_PENYA				(unsigned long)0xf000b027	// 길드 PENYA 설정
#define PACKETTYPE_GUILD_DB_REALPENYA		(unsigned long)0xf000b028	// 길드 PENYA 실제로 길드원들에 주기
#define PACKETTYPE_GUILD_GAMELOGIN			(unsigned long)0xf000b029	// 길드원이 게임에 들어옴
#define PACKETTYPE_GUILD_GAMEJOIN			(unsigned long)0xf000b030	// 게임에 들어왔으므로 나에게 길드원 정보를 줌
#define PACKETTYPE_CHANGEGUILDJOBLEVEL		(unsigned long)0xf000b031	// 전직했을때/레벨업했을시 길드원들에게 알려줌
#define PACKETTYPE_GUILD_SETNAME			(unsigned long)0xf000b032	// 길드이름을 바꿈

#define PACKETTYPE_GUILD_ERROR				(unsigned long)0xf000b035	// 길드 에러 정보 보내기
#define	PACKETTYPE_DECL_GUILD_WAR			(unsigned long)0xf000b036	// 길드 전쟁 선포
#define	PACKETTYPE_ACPT_GUILD_WAR			(unsigned long)0xf000b037	// 길드 전쟁 시작
#define PACKETTYPE_NC_ADDVOTE				(unsigned long)0xf000b038	// 투표 입력 
#define PACKETTYPE_NC_REMOVEVOTE			(unsigned long)0xf000b039	// 투표 취소 
#define PACKETTYPE_NC_CLOSEVOTE				(unsigned long)0xf000b03a	// 투표 종료 
#define PACKETTYPE_NC_CASTVOTE				(unsigned long)0xf000b03b	// 투표하기 

#define PACKETTYPE_CD_ADDVOTE				(unsigned long)0xf000b03c   // 투표 입력(CORE -> DB)
#define PACKETTYPE_CD_REMOVEVOTE			(unsigned long)0xf000b03d	// 투표 취소(CORE -> DB)
#define PACKETTYPE_CD_CLOSEVOTE				(unsigned long)0xf000b03e	// 투표 종료(CORE -> DB)
#define PACKETTYPE_CD_CASTVOTE				(unsigned long)0xf000b03f	// 투표하기 (CORE -> DB)
#define PACKETTYPE_DC_ADDVOTERESULT			(unsigned long)0xf000b040	// 투표 입력결과 (DB -> CORE)
#define PACKETTYPE_CW_ADDVOTERESULT			(unsigned long)0xf000b041	// 투표 입력결과 (CORE -> WORLD)
#define PACKETTYPE_CW_MODIFYVOTE			(unsigned long)0xf000b042	// 투표 변경  (CORE -> WORLD)

#define PACKETTYPE_UPDATE_GUILD_RANKING		(unsigned long)0xf000b043	// 길드랭킹을 업데이트하기
#define PACKETTYPE_UPDATE_GUILD_RANKING_END	(unsigned long)0xf000b044	// 길드랭킹을 업데이트 완료		
#define PACKETTYPE_UPDATE_GUILD_RANKING_DB	(unsigned long)0xf000b04d	// 길드랭킹을 업데이트하기
#define PACKETTYPE_REQUEST_GUILD_RANKING	(unsigned long)0xf000b04e	// 길드랭킹을 요청하기

#define	PACKETTYPE_WAR_DEAD		(unsigned long)0Xf000b045	// 길드전 사망
#define	PACKETTYPE_WAR_END	(unsigned long)0xf000b046	// 종전
#define	PACKETTYPE_SURRENDER	(unsigned long)0xf000b047	// 항복
#define	PACKETTYPE_QUERY_TRUCE	(unsigned long)0xf000b048	// 정전 요청
#define	PACKETTYPE_ACPT_TRUCE	(unsigned long)0xf000b049	// 정전 수락
#define	PACKETTYPE_WAR_TIMEOUT	(unsigned long)0xf000b04a	// 전시 종료
#define	PACKETTYPE_WAR_MASTER_ABSENT	(unsigned long)0xf000b04b	// 길드 마스터 부재
#define	PACKETTYPE_SCHOOL_REPORT	(unsigned long)0xf000b04c
#define PACKETTYPE_COMMERCIALELEM	(unsigned long)0xf000b04f
#define	PACKETTYPE_UPGRADEBASE		(unsigned long)0xf000b050
#define	PACKETTYPE_ENCHANT			(unsigned long)0xf000b024
#define	PACKETTYPE_PIERCING			(unsigned long)0xf000b025
#define	PACKETTYPE_PIERCING_SIZE	(unsigned long)0xf000d008
#define	PACKETTYPE_QUERYEQUIP		(unsigned long)0xf000d009
#define	PACKETTYPE_QUERYEQUIPSETTING	(unsigned long)0xf000d00a
// (unsigned long)0xf000d00b
#define PACHETTYPE_ITEMTRANSY		(unsigned long)0xf000d00c
#define PACKETTYPE_RANDOMSCROLL		(unsigned long)0xf000d00d
#define PACKETTYPE_REMVOE_ATTRIBUTE (unsigned long)0xf000d00b // 속성제련 제거
#define PACKETTYPE_CHANGE_ATTRIBUTE (unsigned long)0xf000d00e // 속성제련 변경

#define	PACKETTYPE_UPDATEGUILDQUEST	(unsigned long)0xf000b051
#define	PACKETTYPE_BUYING_INFO	(unsigned long)0xf000b052
#define	PACKETTYPE_QUERYGUILDQUEST	(unsigned long)0xf000b053

#define PACKETTYPE_ENTERCHTTING		(unsigned long)0xf000b054
#define PACKETTYPE_CHATTING			(unsigned long)0xf000b055

#define	PACKETTYPE_INSERTGUILDQUEST		(unsigned long)0xf000b056

#define PACKETTYPE_COMMONPLACE			(unsigned long)0xf000b057

#define PACKETTYPE_OPENCHATTINGROOM		(unsigned long)0xf000b058
#define PACKETTYPE_CLOSECHATTINGROOM	(unsigned long)0xf000b059

#define	PACKETTYPE_DELETEGUILDQUEST		(unsigned long)0xf000b05a

#define	PACKETTYPE_SERVER_ENABLE	(unsigned long)0xf000b05b

//#define	PACKETTYPE_OPEN_BATTLESERVER	(unsigned long)0xf000d000
//#define	PACKETTYPE_CLOSE_BATTLESERVER	(unsigned long)0xf000d001
#define	PACKETTYPE_JOIN_BATTLESERVER	(unsigned long)0xf000d002
#define	PACKETTYPE_BASEGAMESETTING		(unsigned long)0xf000d003
#define PACKETTYPE_MONSTERRESPAWNSETTING		(unsigned long)0xf000d004
#define PACKETTYPE_MONSTERPROPGAMESETTING		(unsigned long)0xf000d005
#define PACKETTYPE_GAMEMASTER_CHATTING			(unsigned long)0xf000d006

#define PACKETTYPE_RELOAD_PROJECT				(unsigned long)0xf000d007
#define PACKETTYPE_CREATESFXOBJ					(unsigned long)0xf000d00f
#define	PACKETTYPE_RETURNSCROLL					(unsigned long)0xf000d010
#define	PACKETTYPE_EVENT0913						(unsigned long)0xf000d011
#define	PACKETTYPE_EVENT1206						(unsigned long)0xf000d01a
#define	PACKETTYPE_EVENT							(unsigned long)0xf000d01b
#define	PACKETTYPE_ADD_GUILDCOMBAT					(unsigned long)0xf000d021
#define	PACKETTYPE_IN_GUILDCOMBAT					(unsigned long)0xf000d022
#define	PACKETTYPE_OUT_GUILDCOMBAT					(unsigned long)0xf000d023
#define	PACKETTYPE_JOIN_GUILDCOMBAT					(unsigned long)0xf000d024
#define	PACKETTYPE_RESULT_GUILDCOMBAT				(unsigned long)0xf000d025
#define PACKETTYPE_GUILDCOMBAT_STATE				(unsigned long)0xf000d026
#define PACKETTYPE_SELECTPLAYER_GUILDCOMBAT			(unsigned long)0xf000d027
#define PACKETTYPE_REQUEST_STATUS					(unsigned long)0xf000d028
#define PACKETTYPE_REMOVEPARTY_GUILDCOMBAT			(unsigned long)0xf000d029
#define PACKETTYPE_ADDPARTY_GUILDCOMBAT				(unsigned long)0xf000d02a
#define PACKETTYPE_SELECTMAP_GUILDCOMBAT			(unsigned long)0xf000d02b
#define PACKETTYPE_START_GUILDCOMBAT				(unsigned long)0xf000d02c
#define PACKETTYPE_GETPENYAGUILD_GUILDCOMBAT		(unsigned long)0xf000d02d
#define PACKETTYPE_GETPENYAPLAYER_GUILDCOMBAT		(unsigned long)0xf000d02e
#if __VER < 8 // #ifndef __GUILDCOMBAT_85
#define PACKETTYPE_GETITEM_GUILDCOMBAT				(unsigned long)0xf000d02f
#endif // __VER < 8
#define	PACKETTYPE_TELE_GUILDCOMBAT					(unsigned long)0xf000d030
#define PACKETTYPE_PLAYERPOINT_GUILDCOMBAT			(unsigned long)0xf000d031
#define PACKETTYPE_CONTINUE_GUILDCOMBAT				(unsigned long)0xf000d032
#define PACKETTYPE_SUMMON_FRIEND				(unsigned long)0xf000e001
#define PACKETTYPE_SUMMON_FRIEND_CONFIRM		(unsigned long)0xf000e002
#define PACKETTYPE_SUMMON_PARTY					(unsigned long)0xf000e003
#define PACKETTYPE_SUMMON_PARTY_CONFIRM			(unsigned long)0xf000e004
#define PACKETTYPE_SUMMON_FRIEND_CANCEL			(unsigned long)0xf000e005

#define	PACKETTYPE_CHG_MASTER	(unsigned long)0xf000f000

#define	PACKETTYPE_GLOBALGIFTBOX	(unsigned long)0xf000f001
#define	PACKETTYPE_QUERYGLOBALGIFTBOX	(unsigned long)0xf000f002
#define	PACKETTYPE_RESTOREGLOBALGIFTBOX	(unsigned long)0xf000f003

#define	PACKETTYPE_QUERY_REMOVE_GUILD_BANK_TBL	(unsigned long)0xf000f004
#define	PACKETTYPE_EVENT_GENERIC	(unsigned long)0xf000f005
#define	PACKETTYPE_EVENT_FLAG		(unsigned long)0xf000f006
#define	PACKETTYPE_CALL_XXX_MULTI_SERVER	(unsigned long)0xf000f007

#define	PACKETTYPE_CTRL_COOLTIME_CANCEL	(unsigned long)0xf000f008
#define	PACKETTYPE_LOG_EXPBOX	(unsigned long)0xf000f009

#define	PACKETTYPE_TRAFIC_LOG	(unsigned long)0xf000f00a

#define PACKETTYPE_KAWIBAWIBO_START			(unsigned long)0xf000f100
#define PACKETTYPE_KAWIBAWIBO_GETITEM		(unsigned long)0xf000f101
#define PACKETTYPE_REASSEMBLE_START			(unsigned long)0xf000f102
#define PACKETTYPE_REASSEMBLE_OPENWND		(unsigned long)0xf000f103
#define PACKETTYPE_ALPHABET_OPENWND			(unsigned long)0xf000f104
#define PACKETTYPE_ALPHABET_START			(unsigned long)0xf000f105
#define PACKETTYPE_FIVESYSTEM_OPENWND		(unsigned long)0xf000f106
#define PACKETTYPE_FIVESYSTEM_DESTROYWND	(unsigned long)0xf000f107
#define PACKETTYPE_FIVESYSTEM_BET			(unsigned long)0xf000f108
#define PACKETTYPE_FIVESYSTEM_START			(unsigned long)0xf000f109

#define PACKETTYPE_ULTIMATE_MAKEITEM		(unsigned long)0xf000f110
#define PACKETTYPE_ULTIMATE_MAKEGEM			(unsigned long)0xf000f111
#define PACKETTYPE_ULTIMATE_TRANSWEAPON		(unsigned long)0xf000f112
#define PACKETTYPE_ULTIMATE_ENCHANTWEAPON	(unsigned long)0xf000f113
#define PACKETTYPE_ULTIMATE_SETGEM			(unsigned long)0xf000f114
#define PACKETTYPE_ULTIMATE_REMOVEGEM		(unsigned long)0xf000f115

#define PACKETTYPE_EXCHANGE					(unsigned long)0xf000f116
// 이벤트(루아 스크립트)
#define PACKETTYPE_EVENTLUA_STATE			(unsigned long)0xf000f117
#define PACKETTYPE_EVENTLUA_CHANGED			(unsigned long)0xf000f118
#define PACKETTYPE_EVENTLUA_NEEDSTATE		(unsigned long)0xf000f119
#define PACKETTYPE_EVENTLUA_NOTICE			(unsigned long)0xf000f120

#define PACKETTYPE_LEGENDSKILLUP_START		(unsigned long)0xf000f500
#define PACKETTYPE_MODIFY_STATUS			(unsigned long)0xf000f501

#define	PACKETTYPE_PET_RELEASE		(unsigned long)0xf000f600
#define	PACKETTYPE_USE_PET_FEED	(unsigned long)0xf000f601
#define	PACKETTYPE_PET_TAMER_MISTAKE	(unsigned long)0xf000f602
#define	PACKETTYPE_PET_TAMER_MIRACLE	(unsigned long)0xf000f603
#define	PACKETTYPE_FEED_POCKET_INACTIVE		(unsigned long)0xf000f604
#define	PACKETTYPE_MAKE_PET_FEED	(unsigned long)0xf000f605
#define	PACKETTYPE_CALL_USP_PET_LOG	(unsigned long)0xf000f606

#define PACKETTYPE_GC1TO1_TENDEROPENWND	(unsigned long)0xf000f700
#define	PACKETTYPE_GC1TO1_TENDERVIEW	(unsigned long)0xf000f701
#define PACKETTYPE_GC1TO1_TENDER		(unsigned long)0xf000f702
#define PACKETTYPE_GC1TO1_TENDERCANCEL	(unsigned long)0xf000f703
#define PACKETTYPE_GC1TO1_OPEN			(unsigned long)0xf000f704
#define PACKETTYPE_GC1TO1_MEMBERLINEUPOPENWND (unsigned long)0xf000f705
#define PACKETTYPE_GC1TO1_MEMBERLINEUP (unsigned long)0xf000f706
#define PACKETTYPE_GC1TO1_TELEPORTTONPC (unsigned long)0xf000f707
#define	PACKETTYPE_GC1TO1_TELEPORTTOSTAGE	(unsigned long)0xf000f708
#define	PACKETTYPE_GC1TO1_TENDERFAILED	(unsigned long)0xf000f709
#define PACKETTYPE_GC1TO1_STATETODB		(unsigned long)0xf000f710
#define	PACKETTYPE_GC1TO1_TENDERTODB	(unsigned long)0xf000f711
#define PACKETTYPE_GC1TO1_TENDERTOSRVR	(unsigned long)0xf000f712
#define	PACKETTYPE_GC1TO1_LINEUPTODB	(unsigned long)0xf000f713
#define PACKETTYPE_GC1TO1_WARPERSONTODB	(unsigned long)0xf000f714
#define PACKETTYPE_GC1TO1_WARGUILDTODB	(unsigned long)0xf000f715

#define	PACKETTYPE_QUERY_START_COLLECTING	(unsigned long)0xf000f800
#define	PACKETTYPE_QUERY_STOP_COLLECTING	(unsigned long)0xf000f801
#define	PACKETTYPE_QUERY_PLAYER_DATA	(unsigned long)0xf000f802
#define	PACKETTYPE_ALL_PLAYER_DATA	(unsigned long)0xf000f803
#define	PACKETTYPE_ADD_PLAYER_DATA	(unsigned long)0xf000f804
#define	PACKETTYPE_DELETE_PLAYER_DATA	(unsigned long)0xf000f805
#define	PACKETTYPE_QUERY_PLAYER_DATA2	(unsigned long)0xf000f807
#define	PACKETTYPE_UPDATE_PLAYER_DATA	(unsigned long)0xf000f808

#define PACKETTYPE_PIERCINGREMOVE		(unsigned long)0xf000f809

#define PACKETTYPE_GUILDLOG_VIEW		(unsigned long)0xf000f810
#define PACKETTYPE_SEALCHAR_REQ			(unsigned long)0xf000f811
#define PACKETTYPE_SEALCHARCONM_REQ			(unsigned long)0xf000f812

#define PACKETTYPE_NPC_BUFF				(unsigned long)0xf000f813

#define PACKETTYPE_SEALCHARGET_REQ			(unsigned long)0xf000f814
#define PACKETTYPE_SEALCHARSET_REQ			(unsigned long)0xf000f815

#define	PACKETTYPE_ADD_MESSENGER	(unsigned long)0x70000000
#define	PACKETTYPE_DELETE_MESSENGER	(unsigned long)0x70000001
#define	PACKETTYPE_UPDATE_MESSENGER	(unsigned long)0x70000002
#define	PACKETTYPE_ERROR_STRING	(unsigned long)0x70000003
#define	PACKETTYPE_DO_USE_ITEM_TARGET	(unsigned long)0x70000004
#define	PACKETTYPE_REMOVE_ITEM_LEVEL_DOWN	(unsigned long)0x70000005
#define	PACKETTYPE_AVAIL_POCKET		(unsigned long)0x70000006
#define	PACKETTYPE_MOVE_ITEM_POCKET		(unsigned long)0x70000007
#define	PACKETTYPE_AWAKENING		(unsigned long)0x70000008

#define	PACKETTYPE_QUE_PETRESURRECTION		(unsigned long)0x70000009
#define	PACKETTYPE_BLESSEDNESS_CANCEL		(unsigned long)0x7000000a

#define	PACKETTYPE_LOGOUT		(unsigned long)0x7000000b

#define	PACKETTYPE_ARENA_ENTER		(unsigned long)0x70000010
#define	PACKETTYPE_ARENA_EXIT		(unsigned long)0x70000011

#define PACKETTYPE_SECRETROOM_TENDER				(unsigned long)0x70000100
#define PACKETTYPE_SECRETROOM_LINEUPMEMBER			(unsigned long)0x70000101
#define PACKETTYPE_SECRETROOM_TENDEROPENWND			(unsigned long)0x70000102
#define PACKETTYPE_SECRETROOM_LINEUPOPENWND			(unsigned long)0x70000103
#define PACKETTYPE_SECRETROOM_ENTRANCE				(unsigned long)0x70000104
#define PACKETTYPE_SECRETROOM_TELEPORTTONPC			(unsigned long)0x70000105
#define PACKETTYPE_SECRETROOM_TENDERVIEW			(unsigned long)0x70000106
#define PACKETTYPE_SECRETROOM_TENDERCANCELRETURN	(unsigned long)0x70000107
#define PACKETTYPE_SECRETROOM_TENDER_INSERTTODB		(unsigned long)0x70000108
#define PACKETTYPE_SECRETROOM_TENDER_UPDATETODB		(unsigned long)0x70000109
#define PACKETTYPE_SECRETROOM_LINEUP_INSERTTODB		(unsigned long)0x70000110
#define PACKETTYPE_SECRETROOM_INFO_CLEAR			(unsigned long)0x70000111
#define PACKETTYPE_SECRETROOM_TENDERINFO_TO_WSERVER	(unsigned long)0x70000112
#define PACKETTYPE_SECRETROOM_CLOSED				(unsigned long)0x70000113
#define PACKETTYPE_SECRETROOM_TELEPORTTODUNGEON		(unsigned long)0x70000114

#define PACKETTYPE_TAX_ALLINFO						(unsigned long)0x70001000
#define PACKETTYPE_TAX_SET_SECRETROOM_WINNER		(unsigned long)0x70001001
#define PACKETTYPE_TAX_SET_LORD						(unsigned long)0x70001002
#define PACKETTYPE_TAX_SET_TAXRATE					(unsigned long)0x70001003
#define PACKETTYPE_TAX_ADDSALESTAX					(unsigned long)0x70001004
#define PACKETTYPE_TAX_ADDPURCHASEAX				(unsigned long)0x70001005
#define PACKETTYPE_TAX_ADDTAX						(unsigned long)0x70001006
#define PACKETTYPE_TAX_APPLY_TAXRATE_NOW			(unsigned long)0x70001007
#define PACKETTYPE_HEAVENTOWER_TELEPORT				(unsigned long)0x70001008

#define PACKETTYPE_RAINBOWRACE_PREVRANKING_OPENWND	(unsigned long)0x70002000
#define PACKETTYPE_RAINBOWRACE_APPLICATION_OPENWND	(unsigned long)0x70002001
#define PACKETTYPE_RAINBOWRACE_APPLICATION			(unsigned long)0x70002002
#define PACKETTYPE_RAINBOWRACE_MINIGAME_PACKET		(unsigned long)0x70002003
#define PACKETTYPE_RAINBOWRACE_REQ_FINISH			(unsigned long)0x70002004

#define PACKETTYPE_RAINBOWRACE_LOADDBTOWORLD		(unsigned long)0x70002005
#define PACKETTYPE_RAINBOWRACE_APPTODB				(unsigned long)0x70002006
#define PACKETTYPE_RAINBOWRACE_FAILEDTODB			(unsigned long)0x70002007
#define PACKETTYPE_RAINBOWRACE_RANKINGTODB			(unsigned long)0x70002008


#define PACKETTYPE_HOUSING_LOADINFO					(unsigned long)0x70003000
#define PACKETTYPE_HOUSING_FURNITURELIST			(unsigned long)0x70003001
#define PACKETTYPE_HOUSING_SETUPFURNITURE			(unsigned long)0x70003002
#define PACKETTYPE_HOUSING_DBFAILED					(unsigned long)0x70003003
#define PACKETTYPE_HOUSING_VISITROOM				(unsigned long)0x70003004
#define PACKETTYPE_HOUSING_SETVISITALLOW			(unsigned long)0x70003005
#define PACKETTYPE_HOUSING_REQVISITABLELIST			(unsigned long)0x70003006
#define PACKETTYPE_HOUSING_GOOUT					(unsigned long)0x70003007
#define PACKETTYPE_HOUSING_GM_REMOVEALL				(unsigned long)0x70003008


#define PACKETTYPE_HONOR_LIST_REQ					(unsigned long)0x70004000
#define PACKETTYPE_HONOR_CHANGE_REQ					(unsigned long)0x70004001
#define PACKETTYPE_LOG_GETHONORTIME					(unsigned long)0x70004002

#define PACKETTYPE_QUESTHELPER_REQNPCPOS			(unsigned long)0x70005000

#define PACKETTYPE_FUNNYCOIN_REQ_USE				(unsigned long)0x70006000
#define PACKETTYPE_FUNNYCOIN_ACK_USE				(unsigned long)0x70006001

#define PACKETTYPE_PCBANG_SETAPPLY					(unsigned long)0x70006010

#define PACKETTYPE_TIMELIMIT_INFO					(unsigned long)0x70006020
#define PACKETTYPE_TIMELIMIT_UPDATE					(unsigned long)0x70006021
#define PACKETTYPE_TIMELIMIT_RESET					(unsigned long)0x70006022

#define PACKETTYPE_SMELT_SAFETY						(unsigned long)0x70007000

#define	PACKETTYPE_ELECTION_ADD_DEPOSIT	(unsigned long)0x8FFF0000
#define	PACKETTYPE_ELECTION_SET_PLEDGE	(unsigned long)0x8FFF0001
#define	PACKETTYPE_ELECTION_INC_VOTE	(unsigned long)0x8FFF0002
#define	PACKETTYPE_ELECTION_BEGIN_CANDIDACY	(unsigned long)0x8FFF0003
#define	PACKETTYPE_ELECTION_BEGIN_VOTE	(unsigned long)0x8FFF0004
#define	PACKETTYPE_ELECTION_END_VOTE	(unsigned long)0x8FFF0005
#define	PACKETTYPE_ELECTION_PROCESS		(unsigned long)0x8FFF0006
#define	PACKETTYPE_LORD	(unsigned long)0x8FFF0007
#define	PACKETTYPE_L_EVENT_CREATE	(unsigned long)0x8FFF0008
#define	PACKETTYPE_LORD_SKILL_USE	(unsigned long)0x8FFF0009
#define	PACKETTYPE_L_EVENT_INITIALIZE	(unsigned long)0x8FFF000A
#define	PACKETTYPE_LORD_SKILL_TICK	(unsigned long)0x8FFF000B
#define	PACKETTYPE_L_EVENT_TICK		(unsigned long)0x8FFF000C
#define	PACKETTYPE_TRANSFORM_ITEM	(unsigned long)0x8FFF000D
#define	PACKETTYPE_TUTORIAL_STATE		(unsigned long)0x8FFF000E

#define	PACKETTYPE_PICKUP_PET_AWAKENING_CANCEL	(unsigned long)0x8FFF000F
#define	PACKETTYPE_OPTION_ENABLE_RENDER_MASK	(unsigned long)0x8FFF0010

#define	PACKETTYPE_DO_USE_ITEM_INPUT	(unsigned long)0x8FFFFF00
#define	PACKETTYPE_CLEAR_PET_NAME	(unsigned long)0x8FFFFF01

#define	PACKETTYPE_PROPOSE	(unsigned long)0x8FFFF000
#define	PACKETTYPE_REFUSE	(unsigned long)0x8FFFF001
#define	PACKETTYPE_COUPLE	(unsigned long)0x8FFFF002
#define	PACKETTYPE_DECOUPLE		(unsigned long)0x8FFFF003
#define	PACKETTYPE_ALL_COUPLES	(unsigned long)0x8FFFF004
#define	PACKETTYPE_CLEAR_PROPOSE	(unsigned long)0x8FFFF005
#define	PACKETTYPE_ADD_COUPLE_EXPERIENCE	(unsigned long)0x8FFFF006

#define PACKETTYPE_INSTANCEDUNGEON_ALLINFO			(unsigned long)0x00001000
#define PACKETTYPE_INSTANCEDUNGEON_CREATE			(unsigned long)0x00001001
#define PACKETTYPE_INSTANCEDUNGEON_DESTROY			(unsigned long)0x00001002
#define PACKETTYPE_INSTANCEDUNGEON_SETCOOLTIME		(unsigned long)0x00001003
#define PACKETTYPE_INSTANCEDUNGEON_DELETECOOLTIME	(unsigned long)0x00001005
#define	PACKETTYPE_INSTANCEDUNGEON_LOG				(unsigned long)0x0f001100

#define	PACKETTYPE_MAP_KEY							(unsigned long)0xFFFFF000

#define PACKETTYPE_QUIZ_CHANGED						(unsigned long)0xFF000000
#define PACKETTYPE_QUIZ_STATE						(unsigned long)0xFF000001
#define PACKETTYPE_QUIZ_NOTICE						(unsigned long)0xFF000002
#define PACKETTYPE_QUIZ_ENTRANCE					(unsigned long)0xFF000003
#define PACKETTYPE_QUIZ_TELEPORT					(unsigned long)0xFF000004
#define PACKETTYPE_QUIZ_LOADQUIZ					(unsigned long)0xFF000005
#define PACKETTYPE_QUIZ_PRIZEITEM					(unsigned long)0xFF000006
#define PACKETTYPE_QUIZ_SELECT						(unsigned long)0xFF000007
#define PACKETTYPE_QUIZ_OPEN						(unsigned long)0xFF000008

#define PACKETTYPE_VISPET_REMOVEVIS					(unsigned long)0x88000000
#define PACKETTYPE_VISPET_SWAPVIS					(unsigned long)0x88000001

#define PACKETTYPE_GUILDHOUSE_LOAD					(unsigned long)0x88100000
#define PACKETTYPE_GUILDHOUSE_BUY					(unsigned long)0x88100001
#define	PACKETTYPE_GUILDHOUSE_REMOVE				(unsigned long)0x88100002
#define PACKETTYPE_GUILDHOUSE_PACKET				(unsigned long)0x88100003
#define PACKETTYPE_GUILDHOUSE_ALLINFO				(unsigned long)0x88100004
#define PACKETTYPE_GUILDHOSUE_RESTPOINT				(unsigned long)0x88100005
#define PACKETTYPE_GUILDHOUSE_ENTER					(unsigned long)0x88100006
#define PACKETTYPE_GUILDHOUSE_GOOUT					(unsigned long)0x88100007

#define PACKETTYPE_GUILDFURNITURE_LOG				(unsigned long)0x88100008

#define PACKETTYPE_TELEPORTER						(unsigned long)0x88100100

#define PACKETTYPE_QUEST_CHECK						(unsigned long)0x88100110

#define PACKETTYPE_CAMPUS_ALL						(unsigned long)0x88100120
#define PACKETTYPE_CAMPUS_INVITE					(unsigned long)0x88100121
#define PACKETTYPE_CAMPUS_ACCEPT					(unsigned long)0x88100122
#define PACKETTYPE_CAMPUS_REFUSE					(unsigned long)0x88100123
#define PACKETTYPE_CAMPUS_ADD_MEMBER				(unsigned long)0x88100124
#define PACKETTYPE_CAMPUS_REMOVE_MEMBER				(unsigned long)0x88100125
#define PACKETTYPE_CAMPUS_UPDATE_POINT				(unsigned long)0x88100126

#define PACKETTYPE_LOGIN_PROTECT_NUMPAD				(unsigned long)0x88100200
#define PACKETTYPE_LOGIN_PROTECT_CERT				(unsigned long)0x88100201

#define PACKETTYPE_ERROR_LOG_TO_DB					(unsigned long)0x88100210

#define PACKETTYPE_COLLECTION_CERTIFY				(unsigned long)0x88100220


//	mulcom	BEGIN100405	각성 보호의 두루마리
#define PACKETTYPE_ITEM_SELECT_AWAKENING_VALUE		(unsigned long)0x88100221
//	mulcom	END100405	각성 보호의 두루마리

#define PACKETTYPE_GUILDHOUSE_TENDER_MAINWND		(unsigned long)0x88100230
#define PACKETTYPE_GUILDHOUSE_TENDER_INFOWND		(unsigned long)0x88100231
#define PACKETTYPE_GUILDHOUSE_TENDER_INFO			(unsigned long)0x88100232
#define PACKETTYPE_GUILDHOUSE_TENDER_JOIN			(unsigned long)0x88100233
#define PACKETTYPE_GUILDHOUSE_TENDER_STATE			(unsigned long)0x88100234
#define PACKETTYPE_GUILDHOUSE_TENDER_RESULT			(unsigned long)0x88100235
#define PACKETTYPE_GUILDHOUSE_GRADE_UPDATE			(unsigned long)0x88100236
#define PACKETTYPE_GUILDHOUSE_LEVEL_UPDATE			(unsigned long)0x88100237
#define PACKETTYPE_GUILDHOUSE_EXPIRED				(unsigned long)0x88100238

#define PACKETTYPE_QUERYMAILBOX_REQ					(unsigned long)0x88100240
#define PACKETTYPE_QUERYMAILBOX_COUNT				(unsigned long)0x88100241


/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

#define SNAPSHOTTYPE_CHAT	(unsigned short)0x0001
#define SNAPSHOTTYPE_ACTMSG	(unsigned short)0x0002
#define SNAPSHOTTYPE_CREATEITEM	(unsigned short)0x0003
#define	SNAPSHOTTYPE_MOVEITEM	(unsigned short)0x0004

#define	SNAPSHOTTYPE_TRADEPUTERROR (unsigned short)0x0005
#define	SNAPSHOTTYPE_DOEQUIP	(unsigned short)0x0006
#define	SNAPSHOTTYPE_TRADE	(unsigned short)0x0007
#define	SNAPSHOTTYPE_TRADEPUT	(unsigned short)0x0008
#define	SNAPSHOTTYPE_TRADEPULL	(unsigned short)0x0009
#define	SNAPSHOTTYPE_TRADEOK	(unsigned short)0x000a
#define	SNAPSHOTTYPE_TRADECANCEL	(unsigned short)0x000b
#define	SNAPSHOTTYPE_TRADECONSENT	(unsigned short)0x000c
#define SNAPSHOTTYPE_SYNCITEM	(unsigned short)0x000d
#define SNAPSHOTTYPE_SETPOSANGLE	(unsigned short)0x000e
#define	SNAPSHOTTYPE_CREATESFXOBJ	(unsigned short)0x000f

#define SNAPSHOTTYPE_SETPOS	(unsigned short)0x0010
#define SNAPSHOTTYPE_SETLEVEL	(unsigned short)0x0011
#define	SNAPSHOTTYPE_SETEXPERIENCE	(unsigned short)0x0012
#define	SNAPSHOTTYPE_DAMAGE	(unsigned short)0x0013
#define	SNAPSHOTTYPE_OPENSHOPWND	(unsigned short)0x0014
#define	SNAPSHOTTYPE_VENDOR	(unsigned short)0x0015
#define	SNAPSHOTTYPE_UPDATE_VENDOR	(unsigned short)0x0016
#define	SNAPSHOTTYPE_UPDATE_MOVER	(unsigned short)0x0017
#define	SNAPSHOTTYPE_UPDATE_ITEM	(unsigned short)0x0018
#define	SNAPSHOTTYPE_USESKILL	(unsigned short)0x0019
#define SNAPSHOTTYPE_CLEAR_USESKILL	(unsigned short)0x001a
#define	SNAPSHOTTYPE_QUERYGETPOS		(unsigned short)0x001b

#define	SNAPSHOTTYPE_SETDESTPARAM	(unsigned short)0x001c
#define	SNAPSHOTTYPE_RESETDESTPARAM	(unsigned short)0x001d
#define	SNAPSHOTTYPE_SETPOINTPARAM	(unsigned short)0x001e

#define	SNAPSHOTTYPE_GETPOS		(unsigned short)0x001f

#define	SNAPSHOTTYPE_TRADEPUTGOLD	(unsigned short)0x0020
#define	SNAPSHOTTYPE_TRADECLEARGOLD	(unsigned short)0x0021
#define	SNAPSHOTTYPE_CONFIRMTRADE	(unsigned short)0x0022
#define	SNAPSHOTTYPE_CONFIRMTRADECANCEL	(unsigned short)0x0023

#define	SNAPSHOTTYPE_RUNSCRIPTFUNC	(unsigned short)0x0024

#define	SNAPSHOTTYPE_SETSKILLLEVEL	(unsigned short)0x0026
#define	SNAPSHOTTYPE_RESURRECTION_MESSAGE	(unsigned short)0x0027

#if __VER < 8 // __S8_PK
#define	SNAPSHOTTYPE_SET_SLAUGHTER_POINT	(unsigned short)0x0028
#endif // __VER < 8 // __S8_PK

#define	SNAPSHOTTYPE_SETFXP			(unsigned short)0x0029
#define SNAPSHOTTYPE_SETFLIGHTLEVEL	(unsigned short)0x002a

#define	SNAPSHOTTYPE_TRADELASTCONFIRM	(unsigned short)0x002b
#define	SNAPSHOTTYPE_TRADELASTCONFIRMOK	(unsigned short)0x002c

#define	SNAPSHOTTYPE_SCHOOL_REPORT	(unsigned short)0x002d

#define SNAPSHOTTYPE_GAMERATE		(unsigned short)0x002e
#define SNAPSHOTTYPE_EVENTMESSAGE	(unsigned short)0x002f

#define SNAPSHOTTYPE_DUELREQUEST (unsigned short)0x0030
#define SNAPSHOTTYPE_DUELSTART	 (unsigned short)0x0031
#define SNAPSHOTTYPE_DUELNO		 (unsigned short)0x0032
#define SNAPSHOTTYPE_DUELCANCEL	 (unsigned short)0x0033
#define SNAPSHOTTYPE_DUELPARTYREQUEST (unsigned short)0x0034
#define SNAPSHOTTYPE_DUELPARTYSTART	 (unsigned short)0x0035
#define SNAPSHOTTYPE_DUELPARTYNO		 (unsigned short)0x0036
#define SNAPSHOTTYPE_DUELPARTYCANCEL	 (unsigned short)0x0037
#define SNAPSHOTTYPE_DUELPARTYRESULT	 (unsigned short)0x0038
#define SNAPSHOTTYPE_SETSCALE			(unsigned short)0x0039
#define SNAPSHOTTYPE_REMOVEQUEST		(unsigned short)0x003a
#define SNAPSHOTTYPE_MOVERFOCUS		(unsigned short)0x003b
#define SNAPSHOTTYPE_PARTYMAPINFO	(unsigned short)0x003c

#define SNAPSHOTTYPE_SM_MODE_ALL	(unsigned short)0x003e
#define	SNAPSHOTTYPE_SM_MODE		(unsigned short)0x003f

#define SNAPSHOTTYPE_SETFAME	(unsigned short)0x0040
#define SNAPSHOTTYPE_CORRREQ	(unsigned short)0x0041

#define	SNAPSHOTTYPE_PVENDOR_OPEN	(unsigned short)0x0042
#define	SNAPSHOTTYPE_PVENDOR_CLOSE	(unsigned short)0x0043
#define	SNAPSHOTTYPE_REGISTER_PVENDOR_ITEM	(unsigned short)0x0044
#define	SNAPSHOTTYPE_PVENDOR_ITEM	(unsigned short)0x0045
#define	SNAPSHOTTYPE_PVENDOR_ITEM_NUM	(unsigned short)0x0046
#define	SNAPSHOTTYPE_UNREGISTER_PVENDOR_ITEM	(unsigned short)0x0047
#define	SNAPSHOTTYPE_SET_HAIR	(unsigned short)0x0048

#define	SNAPSHOTTYPE_QUERYGETDESTOBJ	(unsigned short)0x0049
#define	SNAPSHOTTYPE_GETDESTOBJ		(unsigned short)0x004a
#define SNAPSHOTTYPE_SETFUEL		(unsigned short)0x004b
#define SNAPSHOTTYPE_SETSKILLSTATE	(unsigned short)0x004c

#define SNAPSHOTTYPE_CHANGEFACE		(unsigned short)0x004d

#define SNAPSHOTTYPE_MONSTERPROP	(unsigned short)0x004e
#define SNAPSHOTTYPE_GMCHAT			(unsigned short)0x004f

#define SNAPSHOTTYPE_PUTITEMBANK		(unsigned short)0x0050
#define SNAPSHOTTYPE_GETITEMBANK		(unsigned short)0x0051
#define SNAPSHOTTYPE_PUTGOLDBANK		(unsigned short)0x0052
#define SNAPSHOTTYPE_MOVEBANKITEM		(unsigned short)0x0053
#define SNAPSHOTTYPE_UPDATE_BANKITEM	(unsigned short)0x0054
#define	SNAPSHOTTYPE_BANKISFULL			(unsigned short)0x0055
#define SNAPSHOTTYPE_BANKWINDOW			(unsigned short)0x0056
#define SNAPSHOTTYPE_CHANGEBANKPASS		(unsigned short)0x0057
#define SNAPSHOTTYPE_CONFIRMBANKPASS	(unsigned short)0x0058
#define SNAPSHOTTYPE_SETSTUN			(unsigned short)0x0059
#define SNAPSHOTTYPE_SETPOISON			(unsigned short)0x005a
#define SNAPSHOTTYPE_SETDARK			(unsigned short)0x005b
#define SNAPSHOTTYPE_SENDACTMSG			(unsigned short)0x005c

#define SNAPSHOTTYPE_RESISTSMMODE		(unsigned short)0x005d
#define SNAPSHOTTYPE_COMMERCIALELEM		(unsigned short)0x005e
#define SNAPSHOTTYPE_PUSHPOWER			(unsigned short)0x005f
#define SNAPSHOTTYPE_ENVIRONMENT		(unsigned short)0x0060
#define SNAPSHOTTYPE_ENVIRONMENTSNOW	(unsigned short)0x0061
#define SNAPSHOTTYPE_ENVIRONMENTRAIN	(unsigned short)0x0062
#define SNAPSHOTTYPE_ENVIRONMENTALL		(unsigned short)0x0063
#if __VER >= 8 // __S8_PK
#define SNAPSHOTTYPE_PK_RELATION		(unsigned short)0x0065
#else // __VER >= 8 // __S8_PK
#define SNAPSHOTTYPE_UPDATE_PLAYER_ENEMY	(unsigned short)0x0065
#endif // __VER >= 8 // __S8_PK
#define SNAPSHOTTYPE_SETDUEL			(unsigned short)0x0066
#define SNAPSHOTTYPE_DUELCOUNT			(unsigned short)0x0067
#define SNAPSHOTTYPE_ADDPARTYNAME		(unsigned short)0x0068
#define SNAPSHOTTYPE_PARTYCHAT			(unsigned short)0x0069
#define SNAPSHOTTYPE_SETSTATE			(unsigned short)0x006a
#define SNAPSHOTTYPE_CMDSETSKILLLEVEL	(unsigned short)0x006b
//#define SNAPSHOTTYPE_DO_COLLECT			(unsigned short)0x006c
//#define SNAPSHOTTYPE_STOP_COLLECT		(unsigned short)0x006d
#define SNAPSHOTTYPE_ACTIVESKILL		(unsigned short)0x006e
#define SNAPSHOTTYPE_RESETBUFFSKILL		(unsigned short)0x006f
#define SNAPSHOTTYPE_ADDFRIEND			(unsigned short)0x0070
#define SNAPSHOTTYPE_ADDFRIENDREQEST    (unsigned short)0x0071
#define SNAPSHOTTYPE_ADDFRIENDCANCEL	(unsigned short)0x0072
#define SNAPSHOTTYPE_ADDGETFRIENDNAME	(unsigned short)0x0073
#define SNAPSHOTTYPE_ADDFRIENDGAMEJOIN	(unsigned short)0x0074
#define SNAPSHOTTYPE_REMOVEFRIEND		(unsigned short)0x0075
#define SNAPSHOTTYPE_ADDFRIENDERROR		(unsigned short)0x0076
#define SNAPSHOTTYPE_ADDFRIENDCHANGEJOB	(unsigned short)0x0077
#define SNAPSHOTTYPE_ADDGAMEJOIN		(unsigned short)0x0078
#define SNAPSHOTTYPE_ADDPARTYCHANGELEADER	(unsigned short)0x0079

#define	SNAPSHOTTYPE_SET_WAR	(unsigned short)0x007a
#define SNAPSHOTTYPE_CHATTING	(unsigned short)0x007b
#define SNAPSHOTTYPE_INITSKILLPOINT		(unsigned short)0x007c
#define SNAPSHOTTYPE_DOUSESKILLPOINT	(unsigned short)0x007d
#define SNAPSHOTTYPE_COMMONPlACE		(unsigned short)0x007e
#define SNAPSHOTTYPE_END_RECOVERMODE	(unsigned short)0x007f
#define SNAPSHOTTYPE_WANTED_INFO		(unsigned short)0x0080

#define SNAPSHOTTYPE_ERRORPARTY			(unsigned short)0x0081
#define SNAPSHOTTYPE_PARTYMEMBER		(unsigned short)0x0082
#define SNAPSHOTTYPE_PARTYREQEST		(unsigned short)0x0083
#define SNAPSHOTTYPE_PARTYREQESTCANCEL	(unsigned short)0x0084
#define SNAPSHOTTYPE_PARTYEXP			(unsigned short)0x0085
#define SNAPSHOTTYPE_PARTYMEMBERJOB		(unsigned short)0x0086
#define SNAPSHOTTYPE_PARTYMEMBERLEVEL	(unsigned short)0x0087
#define SNAPSHOTTYPE_PARTYCHANGETROUP	(unsigned short)0x0088
#define SNAPSHOTTYPE_PARTYCHANGENAME	(unsigned short)0x0089
#define SNAPSHOTTYPE_PARTYSKILL_CALL	(unsigned short)0x008a
#define SNAPSHOTTYPE_PARTYSKILL_BLITZ	(unsigned short)0x008b
#define SNAPSHOTTYPE_PARTYSKILL_RETREAT	(unsigned short)0x008c
#define SNAPSHOTTYPE_SETPARTYMODE				(unsigned short)0x008d
#define SNAPSHOTTYPE_PARTYSKILL_SPHERECIRCLE	(unsigned short)0x008e
#define SNAPSHOTTYPE_PARTYCHANGEITEMMODE	(unsigned short)0x008f
#define SNAPSHOTTYPE_PARTYCHANGEEXPMODE	(unsigned short)0x0090
#define	SNAPSHOTTYPE_SET_PARTY_MEMBER_PARAM	(unsigned short)0x0091
#define SNAPSHOTTYPE_DO_ESCAPE				(unsigned short)0x0092
#define	SNAPSHOTTYPE_SNOOP	(unsigned short)0x0093
#define	SNAPSHOTTYPE_DEFINEDTEXT1	(unsigned short)0x0094
#define	SNAPSHOTTYPE_DEFINEDTEXT	(unsigned short)0x0095
#define SNAPSHOTTYPE_GAMETIMER	(unsigned short)0x0096
#define SNAPSHOTTYPE_TASKBAR	(unsigned short)0x0097
#define	SNAPSHOTTYPE_MOTION		(unsigned short)0x0098
#define	SNAPSHOTTYPE_QUERYPLAYERSTRING	(unsigned short)0x0099
#define	SNAPSHOTTYPE_GUILD_INVITE	(unsigned short)0x009a
#define	SNAPSHOTTYPE_SET_GUILD		(unsigned short)0x009b
#define	SNAPSHOTTYPE_CREATE_GUILD	(unsigned short)0x009c
#define	SNAPSHOTTYPE_DESTROY_GUILD	(unsigned short)0x009d
#define	SNAPSHOTTYPE_GUILD			(unsigned short)0x009e
#define	SNAPSHOTTYPE_ALL_GUILDS		(unsigned short)0x009f

#define SNAPSHOTTYPE_TEXT	(unsigned short)0x00a0
#define SNAPSHOTTYPE_REVIVAL	(unsigned short)0x00a1
#define SNAPSHOTTYPE_REVIVAL_TO_LODESTAR	(unsigned short)0x00a2
#define SNAPSHOTTYPE_REVIVAL_TO_LODELIGHT	(unsigned short)0x00a3
#define	SNAPSHOTTYPE_SET_STAT_LEVEL		(unsigned short)0x00a4
#define	SNAPSHOTTYPE_SET_JOB_LEVEL	(unsigned short)0x00a5
#define	SNAPSHOTTYPE_SET_GROWTH_LEARNING_POINT	(unsigned short)0x00a6

#define SNAPSHOTTYPE_SET_JOB_SKILL		(unsigned short)0x00a7
#define SNAPSHOTTYPE_SET_NEAR_JOB_SKILL (unsigned short)0x00a8
#define SNAPSHOTTYPE_RETURNSAY			(unsigned short)0x00a9				// 귓속말에서 되돌아오는것 : 찾을수가 없음, 귓속말 내용
#define SNAPSHOTTYPE_CREATESFXALLOW		(unsigned short)0x00aa
#define	SNAPSHOTTYPE_REMOVEITEMATID		(unsigned short)0x00ab
#define	SNAPSHOTTYPE_QUERYEQUIP			(unsigned short)0x00ac
#define SNAPSHOTTYPE_RETURNSCORLL		(unsigned short)0x00ad
#define SNAPSHOTTYPE_SETTARGET			(unsigned short)0x00ae
#define SNAPSHOTTYPE_FOCUSOBJ			(unsigned short)0x00af

#define SNAPSHOTTYPE_SETQUEST	(unsigned short)0x00b0
#define	SNAPSHOTTYPE_FLYFF_EVENT	(unsigned short)0x00b2
#define	SNAPSHOTTYPE_SET_LOCAL_EVENT	(unsigned short)0x00b3

#define	SNAPSHOTTYPE_SETCHEERPARAM	(unsigned short)0x00b4

#define SNAPSHOTTYPE_SETGUILDQUEST		(unsigned short)0x00b5
#define SNAPSHOTTYPE_REMOVEGUILDQUEST	(unsigned short)0x00b6
#ifdef __S_SERVER_UNIFY
#define SNAPSHOTTYPE_ALLACTION			(unsigned short)0x00b7
#else // __S_SERVER_UNIFY
#define SNAPSHOTTYPE_DIAG_TEXT				(unsigned short)0x00b7
#endif // __S_SERVER_UNIFY
#define SNAPSHOTTYPE_GUILDCOMBAT		(unsigned short)0x00b8
#define SNAPSHOTTYPE_DEFINEDCAPTION		(unsigned short)0x00b9
#define SNAPSHOTTYPE_QUEST_TEXT_TIME	(unsigned short)0x00ba
#define	SNAPSHOTTYPE_QUERYPLAYERLISTSTRING	(unsigned short)0x00bb
#define	SNAPSHOTTYPE_CHATTEXT	(unsigned short)0x00bc

#define	SNAPSHOTTYPE_EXPBOXINFO		(unsigned short)0x00bd
#define	SNAPSHOTTYPE_EXPBOXCOLLTIME	(unsigned short)0x00be
#define	SNAPSHOTTYPE_EXPBOXCOLLTIMECANCEL	(unsigned short)0x00bf


#define	SNAPSHOTTYPE_PLAYERPOS	(unsigned short)0x00c0
#define	SNAPSHOTTYPE_DESTPOS	(unsigned short)0x00c1
#define	SNAPSHOTTYPE_MOVERSETDESTOBJ	(unsigned short)0x00c2
#define	SNAPSHOTTYPE_DESTANGLE	(unsigned short)0x00c3
#define	SNAPSHOTTYPE_MOVINGACTMSG	(unsigned short)0x00c4
#define SNAPSHOTTYPE_SETACTIONPOINT	(unsigned short)0x00c5
#define SNAPSHOTTYPE_SETNAVIPOINT	(unsigned short)0x00c6
#define	SNAPSHOTTYPE_MOVERDEATH		(unsigned short)0x00c7
#define	SNAPSHOTTYPE_MOVERCORR	(unsigned short)0x00c8
#define	SNAPSHOTTYPE_MOVERCORR2	(unsigned short)0x00c9
#define	SNAPSHOTTYPE_MOVERMOVED	(unsigned short)0x00ca
#define	SNAPSHOTTYPE_MOVERBEHAVIOR	(unsigned short)0x00cb
#define	SNAPSHOTTYPE_MOVERMOVED2	(unsigned short)0x00cc
#define	SNAPSHOTTYPE_MOVERBEHAVIOR2		(unsigned short)0x00cd
#define	SNAPSHOTTYPE_MOVERANGLE		(unsigned short)0x00ce
#define	SNAPSHOTTYPE_SETMOVEPATTERN	(unsigned short)0x00cf

#define	SNAPSHOTTYPE_SHOUT	(unsigned short)0x00d0
#define	SNAPSHOTTYPE_PLAYMUSIC	(unsigned short)0x00d1
#define	SNAPSHOTTYPE_PLAYSOUND	(unsigned short)0x00d2
#define	SNAPSHOTTYPE_MODIFYMODE	(unsigned short)0x00d3

#define	SNAPSHOTTYPE_DOAPPLYUSESKILL	(unsigned short)0x00d7
#define	SNAPSHOTTYPE_COMMONSKILL		(unsigned short)0x00dd
#define	SNAPSHOTTYPE_STATEMODE		(unsigned short)0x00df
#define	SNAPSHOTTYPE_MELEE_ATTACK	(unsigned short)0x00e0
#define	SNAPSHOTTYPE_MAGIC_ATTACK	(unsigned short)0x00e1
#define	SNAPSHOTTYPE_RANGE_ATTACK	(unsigned short)0x00e2
#define	SNAPSHOTTYPE_SP_ATTACK		(unsigned short)0x00e3
#define	SNAPSHOTTYPE_MELEE_ATTACK2	(unsigned short)0x00e4
#define SNAPSHOTTYPE_ENDSKILLQUEUE	(unsigned short)0x00e5

#define	SNAPSHOTTYPE_POSTMAIL	(unsigned short)0x00e6
#define	SNAPSHOTTYPE_REMOVEMAIL		(unsigned short)0x00e7
#define	SNAPSHOTTYPE_QUERYMAILBOX	(unsigned short)0x00e9

#define	SNAPSHOTTYPE_CRIME			(unsigned short)0x00ea
#define SNAPSHOTTYPE_RESURRECTION	(unsigned short)0x00eb
#define SNAPSHOTTYPE_SHIP_ACTMSG	(unsigned short)0x00ec
#define SNAPSHOTTYPE_SUMMON			(unsigned short)0x00ed
#define SNAPSHOTTYPE_MOTION_ARRIVE			(unsigned short)0x00ee

#define	SNAPSHOTTYPE_ADD_OBJ		(unsigned short)0x00f0
#define SNAPSHOTTYPE_DEL_OBJ		(unsigned short)0x00f1
#define SNAPSHOTTYPE_REPLACE		(unsigned short)0x00f2
#define	SNAPSHOTTYPE_REMOVE_GUILD_BANK_ITEM		(unsigned short)0x00f3
#define SNAPSHOTTYPE_REMOVEALLSKILLINFULENCE	(unsigned short)0x00f4

#define SNAPSHOTTYPE_DISGUISE		(unsigned short)0x00f5
#define SNAPSHOTTYPE_NODISGUISE		(unsigned short)0x00f6
#define SNAPSHOTTYPE_TAG			(unsigned short)0x00f7

#define SNAPSHOTTYPE_REMOVESKILLINFULENCE	(unsigned short)0x00f8
#define SNAPSHOTTYPE_ADDREGION	(unsigned short)0x00f9
#define SNAPSHOTTYPE_GUILD_BANK_WND		(unsigned short)(0x00FA)
#define SNAPSHOTTYPE_PUTITEMGUILDBANK	(unsigned short)(0x00EF)
#define SNAPSHOTTYPE_GETITEMGUILDBANK	(unsigned short)(0x00D4)
#define SNAPSHOTTYPE_GUILD_LOGO		(unsigned short)0x00fb				// 로고 
#define SNAPSHOTTYPE_GUILD_CONTRIBUTION	(unsigned short)0x00fc			// 공헌도 
#define SNAPSHOTTYPE_GUILD_NOTICE		(unsigned short)0x00fd			// 공지사항 
#define	SNAPSHOTTYPE_GUILD_AUTHORITY	(unsigned short)0x00fe			// 권한 변경 
#define SNAPSHOTTYPE_GUILD_PENYA		(unsigned short)0x00ff			// 페냐 변경 
#define SNAPSHOTTYPE_GUILD_REAL_PENYA	(unsigned short)(0x00D5)		// 월급 받음
#define SNAPSHOTTYPE_GUILD_CHANGEJOBLEVEL	(unsigned short)(0x00D6)	// 레벨이나 직업이 바뀜
#define SNAPSHOTTYPE_GUILD_ADDVOTE		(unsigned short)(0x00D8)	// 투표가 추가됨 
#define SNAPSHOTTYPE_GUILD_MODIFYVOTE   (unsigned short)(0x00D9)	// 투표의 변경(제거, 종료, 투표함)
#define	SNAPSHOTTYPE_WAR	(unsigned short)0x00da	// 전쟁

#define SNAPSHOTTYPE_REQUEST_GUILDRANK	(unsigned short)(0x00db)
#define	SNAPSHOTTYPE_SEX_CHANGE		(unsigned short)(0x00dc)
#define	SNAPSHOTTYPE_WORLDMSG		(unsigned short)(0x00de)

#define	SNAPSHOTTYPE_CALLTHEROLL	(unsigned short)(0x003d)

#if __VER >= 8 //__CSC_VER8_5
#define SNAPSHOTTYPE_ANGEL	(unsigned short)(0x00b1)
#endif //__CSC_VER8_5

#define SNAPSHOTTYPE_MINIGAME		(unsigned short)0x00e8

#define SNAPSHOTTYPE_ULTIMATE		(unsigned short)0x0100

#define SNAPSHOTTYPE_EXCHANGE		(unsigned short)0x0101

#define SNAPSHOTTYPE_PET_CALL		(unsigned short)0x0110
#define SNAPSHOTTYPE_PET_RELEASE	(unsigned short)0x0111
#define	SNAPSHOTTYPE_PET_LEVELUP	(unsigned short)0x0112
#define	SNAPSHOTTYPE_PET_SET_EXP	(unsigned short)0x0113
#define	SNAPSHOTTYPE_PET			(unsigned short)0x0114
#define	SNAPSHOTTYPE_PET_STATE		(unsigned short)0x0115
#define	SNAPSHOTTYPE_PET_FEED		(unsigned short)0x0116
#define SNAPSHOTTYPE_PET_MILL		(unsigned short)0x0117
#define	SNAPSHOTTYPE_SET_SPEED_FACTOR	(unsigned short)0x0118

#define SNAPSHOTTYPE_LEGENDSKILLUP_RESULT		(unsigned short)0x0120

#define SNAPSHOTTYPE_EVENTLUA_DESC	(unsigned short)0x0121

#define SNAPSHOTTYPE_REMOVE_ATTRIBUTE (unsigned short)0x122

#define	SNAPSHOTTYPE_START_COLLECTING	(unsigned short)0x123
#define SNAPSHOTTYPE_STOP_COLLECTING	(unsigned short)0x124

#define SNAPSHOTTYPE_GC1TO1_NOWSTATE		(unsigned short)0x0125
#define SNAPSHOTTYPE_GC1TO1_TENDEROPENWND	(unsigned short)0x0126
#define	SNAPSHOTTYPE_GC1TO1_TENDERVIEW		(unsigned short)0x0127
#define SNAPSHOTTYPE_GC1TO1_MEMBERLINEUPOPENWND (unsigned short)0x0128
#define SNAPSHOTTYPE_GC1TO1_WARRESULT		(unsigned short)0x0129

#define	SNAPSHOTTYPE_UPDATE_ITEM_EX		(unsigned short)0x0140
#define	SNAPSHOTTYPE_QUERY_PLAYER_DATA	(unsigned short)0x0141

#define	SNAPSHOTTYPE_GUILDLOG_VIEW	(unsigned short)0x0142

#define	SNAPSHOTTYPE_RESTART_COLLECTING	(unsigned short)0x0143

#define	SNAPSHOTTYPE_SEALCHAR_REQ	(unsigned short)0x0144

#define	SNAPSHOTTYPE_SEALCHARGET_REQ	(unsigned short)0x0145

#define SNAPSHOTTYPE_EVENT_COUPON		(unsigned short)0x0146

//
#define	SNAPSHOTTYPE_POCKET_ATTRIBUTE	(unsigned short)0x0200
#define	SNAPSHOTTYPE_POCKET_ADD_ITEM	(unsigned short)0x0201
#define	SNAPSHOTTYPE_POCKET_REMOVE_ITEM	(unsigned short)0x0202

#define	SNAPSHOTTYPE_QUE_PETRESURRECTION_RESULT	(unsigned short)0x0203

#define	SNAPSHOTTYPE_REMOVESFXOBJ	(unsigned short)0x0204

#define SNAPSHOTTYPE_SECRETROOM_MNG_STATE		(unsigned short)0x0300
#define SNAPSHOTTYPE_SECRETROOM_INFO			(unsigned short)0x0301
#define SNAPSHOTTYPE_SECRETROOM_TENDEROPENWND	(unsigned short)0x0302
#define SNAPSHOTTYPE_SECRETROOM_LINEUPOPENWND	(unsigned short)0x0303
#define SNAPSHOTTYPE_SECRETROOM_TENDERVIEW		(unsigned short)0x0304

#define SNAPSHOTTYPE_TAX_ALLINFO				(unsigned short)0x0400
#define SNAPSHOTTYPE_TAX_SETTAXRATE_OPENWND		(unsigned short)0x0401

#define	SNAPSHOTTYPE_ELECTION_ADD_DEPOSIT	(unsigned short)0x8F00
#define	SNAPSHOTTYPE_ELECTION_SET_PLEDGE	(unsigned short)0x8F01
#define	SNAPSHOTTYPE_ELECTION_INC_VOTE	(unsigned short)0x8F02
#define	SNAPSHOTTYPE_ELECTION_BEGIN_CANDIDACY	(unsigned short)0x8F03
#define	SNAPSHOTTYPE_ELECTION_BEGIN_VOTE	(unsigned short)0x8F04
#define	SNAPSHOTTYPE_ELECTION_END_VOTE	(unsigned short)0x8F05
#define	SNAPSHOTTYPE_LORD	(unsigned short)0x8F06
#define	SNAPSHOTTYPE_L_EVENT	(unsigned short)0x8F07
#define	SNAPSHOTTYPE_L_EVENT_CREATE		(unsigned short)0x8F08
#define	SNAPSHOTTYPE_L_EVENT_INITIALIZE		(unsigned short)0x8F09
#define	SNAPSHOTTYPE_LORD_SKILL_TICK		(unsigned short)0x8F0A
#define	SNAPSHOTTYPE_L_EVENT_TICK		(unsigned short)0x8F0B
#define	SNAPSHOTTYPE_SET_TUTORIAL_STATE		(unsigned short)0x8F0C
#define	SNAPSHOTTYPE_LORD_SKILL_USE		(unsigned short)0x8F0D

#define	SNAPSHOTTYPE_RAINBOWRACE_PREVRANKING_OPENWND	(unsigned short)0x9000
#define	SNAPSHOTTYPE_RAINBOWRACE_APPLICATION_OPENWND	(unsigned short)0x9001
#define SNAPSHOTTYPE_RAINBOWRACE_NOWSTATE				(unsigned short)0x9002
#define	SNAPSHOTTYPE_RAINBOWRACE_MINIGAMESTATE			(unsigned short)0x9003
#define	SNAPSHOTTYPE_RAINBOWRACE_MINIGAMEEXTSTATE		(unsigned short)0x9004

#define	SNAPSHOTTYPE_SET_PET_NAME	(unsigned short)0x9100


#define SNAPSHOTTYPE_HOUSING_ALLINFO				(unsigned short)0x9200
#define SNAPSHOTTYPE_HOUSING_FURNITURELIST			(unsigned short)0x9201
#define SNAPSHOTTYPE_HOUSING_SETUPFURNITURE			(unsigned short)0x9202
#define SNAPSHOTTYPE_HOUSING_PAPERINGINFO			(unsigned short)0x9203
#define SNAPSHOTTYPE_HOUSING_SETVISITALLOW			(unsigned short)0x9204
#define SNAPSHOTTYPE_HOUSING_VISITABLELIST			(unsigned short)0x9205

#define SNAPSHOTTYPE_HONOR_LIST_ACK					(unsigned short)0x9300
#define SNAPSHOTTYPE_HONOR_CHANGE_ACK				(unsigned short)0x9301

#define SNAPSHOTTYPE_QUESTHELPER_NPCPOS				(unsigned short)0x9400

#define SNAPSHOTTYPE_CLEAR_TARGET					(unsigned short)0x9500

#define SNAPSHOTTYPE_COUPLE		(unsigned short)0x9700
#define	SNAPSHOTTYPE_PROPOSE_RESULT		(unsigned short)0x9701
#define SNAPSHOTTYPE_COUPLE_RESULT	(unsigned short)0x9703
#define SNAPSHOTTYPE_DECOUPLE_RESULT	(unsigned short)0x9704
#define SNAPSHOTTYPE_ADD_COUPLE_EXPERIENCE	(unsigned short)0x9705

#ifdef __SPEED_SYNC_0108		// ResetDestParam speed 수정
#define	SNAPSHOTTYPE_RESETDESTPARAM_SYNC			(unsigned short)0x9800
#endif // __SPEED_SYNC_0108		// ResetDestParam speed 수정
#define SNAPSHOTTYPE_PCBANG_INFO					(unsigned short)0x9810

#define SNAPSHOTTYPE_ACCOUNT_PLAYTIME				(unsigned short)0x9820

#define SNAPSHOTTYPE_SMELT_SAFETY					(unsigned short)0x9900

#define SNAPSHOTTYPE_WORLD_READINFO						(unsigned short)0x9910

#define SNAPSHOTTYPE_QUIZ_STATE						(unsigned short)0x9920
#define SNAPSHOTTYPE_QUIZ_MESSAGE					(unsigned short)0x9921
#define SNAPSHOTTYPE_QUIZ_QUESTION					(unsigned short)0x9922

#define SNAPSHOTTYPE_MOVER_CHANGESFX				(unsigned short)0x8800
#define SNAPSHOTTYPE_VISPET_ACTIVATE				(unsigned short)0x8801

#define SNAPSHOTTYPE_GUILDHOUSE_PACKET				(unsigned short)0x8810
#define SNAPSHOTTYPE_GUILDHOUSE_ALLINFO				(unsigned short)0x8812
#define	SNAPSHOTTYPE_GUILDHOUSE_REMOVE				(unsigned short)0x8813
#define	SNAPSHOTTYPE_GUILDHOUSE_RESTPOINT			(unsigned short)0x8814

#define SNAPSHOTTYPE_QUEST_CHECKED					(unsigned short)0x8820

#define SNAPSHOTTYPE_CAMPUS_INVITE					(unsigned short)0x8830
#define SNAPSHOTTYPE_CAMPUS_UPDATE					(unsigned short)0x8831
#define SNAPSHOTTYPE_CAMPUS_REMOVE					(unsigned short)0x8832
#define SNAPSHOTTYPE_CAMPUS_UPDATE_POINT			(unsigned short)0x8833

//	mulcom	BEGIN100405	각성 보호의 두루마리
#define SNAPSHOTTYPE_ITEM_SELECT_AWAKENING_VALUE	(unsigned short)0x8834
//	mulcom	END100405	각성 보호의 두루마리

#define SNAPSHOTTYPE_GUILDHOUSE_TENDER_MAINWND		(unsigned short)0x8840
#define SNAPSHOTTYPE_GUILDHOUSE_TENDER_INFOWND		(unsigned short)0x8841
#define SNAPSHOTTYPE_GUILDHOUSE_TENDER_RESULT		(unsigned short)0x8842

#define	SNAPSHOTTYPE_QUERYMAILBOX_REQ				(unsigned short)0x8860

////////////////////////////////////////////////////////////////////////////
//  ERROR define
////////////////////////////////////////////////////////////////////////////
#define ERROR_OK							0L
#define ERROR_DEFAULT						1L

#define	ERROR_ACCOUNT_EXISTS				100L
#define ERROR_INVALID_SERVICE_PLAYER		102L
#define ERROR_DUPLICATE_ACCOUNT				103L
#define ERROR_ILLEGAL_ACCESS				104L
#define ERROR_DUPLICATE_SLOT				105L
#define ERROR_SLOT_OUTOFRANGE				106L
#define	ERROR_ILLEGAL_VER					107L
#define	ERROR_OVERFLOW						108L
#define	ERROR_EXTERNAL_ADDR					109L
#define	ERROR_INVALID_CLOCK					110L
#define ERROR_INVALID_NAME_CHARACTER		111L
#define	ERROR_WARTIME						112L
#define ERROR_BLOCKGOLD_ACCOUNT				119L
#define ERROR_FLYFF_PASSWORD				120L	// 비밀 번호가 틀립니다.
#define ERROR_FLYFF_ACCOUNT					121L	// 잘못된 계정입니다.
#define ERROR_FLYFF_AUTH					122L	// 실명인증후 게임접속이 가능합니다
#define ERROR_FLYFF_PERMIT					123L    // 프리프는 12세 이상 이용가 이므로 게임접속을 할수 없습니다.
#define ERROR_FLYFF_NEED_AGREEMENT			124L	// 14세 미만 가입자 분들은 부모님 동의서를 보내주셔야
#define ERROR_FLYFF_NO_MEMBERSHIP			125L	// 웹에서 탈퇴한 계정입니다
#define ERROR_BILLING_INFO_FAILED			126L	// 빌링 정보 없음
#define ERROR_BILLING_DATABASE_ERROR		127L	// 빌링 DB 에러 
#define ERROR_BILLING_TIME_OVER				128L	// 빌링 사용시간 만료 
#define ERROR_BILLING_OTHER_ERROR			129L	// 빌링 기타 다른 에러 
#define ERROR_BILLING_DISCONNECTED			130L	// 서버 접속 해제
#define ERROR_TOO_LATE_PLAY					131L	// 태국 22시에서 06시까지만 플레이 가능합니다.
#define ERROR_IP_CUT						132L	// 해외에서 국내로 못들어옴
#define ERROR_FLYFF_DB_JOB_ING				133L	// 실시간 데이터 작업 유저		
#define ERROR_15SEC_PREVENT					134L	// 15초간 로그인 금지 
#define ERROR_15MIN_PREVENT					135L	// 15분간 로그인 금지 
#define ERROR_CERT_GENERAL					136L	// CERT 일반 오류 	
#define	ERROR_FLYFF_EXPIRED_SESSION_PASSWORD	137L	// 세션 패스워드 만료
#define	ERROR_FLYFF_RESOURCE_MODIFIED	138L	// 세션 패스워드 만료

// Party
#define ERROR_NOLEADER						200L
#define ERROR_FULLPARTY						201L
#define ERROR_NOPARTY						202L
#define ERROR_DIFFRENTPARTY					203L
#define ERROR_DIFFERNTUSERNAME				204L
#define ERROR_NOTPARTYPOINT					205L
#define ERROR_NOTPARTYSKILL					206L
#define ERROR_NOTTARGET						207L
#define ERROR_NOTMAMBER						208L
#define ERROR_SAMLEADER						209L

// NOERR_XXX 와 ERROR_XXX 는 중복되면 안된다.
#define	NOERR_REGISTER_ACCOUNT				500L		
#define NOERR_LOGIN							501L


#define	ERROR_NODE_NOT_FOUND				1000L

////////////////////////////////////////////////////////////////////////////
//  QPS_ define
////////////////////////////////////////////////////////////////////////////
#define	QPS_GUILD_MASTER	(unsigned char)0x00
#define	QPS_GUILD_MEMBER	(unsigned char)0x01
#define	QPS_POST	(unsigned char)0x02
#define	QPS_GENERIC	(unsigned char)0x03
#define	QPS_GUILDCOMBAT_RANKING	(unsigned char)0x04


typedef	struct	_runscriptfunc
{
	char	lpszVal1[1024];
	char	lpszVal2[1024];
	unsigned long	dwVal1;
	unsigned long   dwVal2;
	unsigned short	wFuncType;
	D3DXVECTOR3 vPos;
}	RunScriptFunc, * PRunScriptFunc;

#define	FUNCTYPE_ADDKEY			(unsigned short)0x0010
#define	FUNCTYPE_REMOVEKEY		(unsigned short)0x0011
#define	FUNCTYPE_SAY			(unsigned short)0x0012
#define	FUNCTYPE_ADDANSWER		(unsigned short)0x0013
#define	FUNCTYPE_SETMARK		(unsigned short)0x0014
#define	FUNCTYPE_GOMARK			(unsigned short)0x0015
#define	FUNCTYPE_EXIT			(unsigned short)0x0016
#define	FUNCTYPE_ENDSAY			(unsigned short)0x0017
#define	FUNCTYPE_CREATEGUILD	(unsigned short)0x0018
#define	FUNCTYPE_DESTROYGUILD	(unsigned short)0x0019
#define	FUNCTYPE_INITSTAT		(unsigned short)0x001a
#define	FUNCTYPE_SETNAVIGATOR	(unsigned short)0x001b
#define	FUNCTYPE_REMOVENAVIGATOR	(unsigned long)0x001c
#define	FUNCTYPE_REMOVEALLKEY	(unsigned short)0x001d
#define	FUNCTYPE_SAYQUEST		(unsigned short)0x001e
#define	FUNCTYPE_INITSTR		(unsigned short)0x0020
#define	FUNCTYPE_INITSTA		(unsigned short)0x0021
#define	FUNCTYPE_INITDEX		(unsigned short)0x0022
#define	FUNCTYPE_INITINT		(unsigned short)0x0023
#define	FUNCTYPE_QUERYSETPLAYERNAME	(unsigned long)0x0024
#define	FUNCTYPE_QUERYSETGUILDNAME	(unsigned long)0x0025
#define	FUNCTYPE_NEWQUEST		(unsigned short)0x0026
#define	FUNCTYPE_CURRQUEST		(unsigned short)0x0027

// port num
#define	PN_PATCHSRVR	1100
#define	PN_WORLDSRVR	3000
#define	PN_CERTIFIER	23000
#define	PN_CORESRVR	    4000
#define	PN_CACHESRVR	5400
#define	PN_DBSRVR_0		7000	// l
//#define	PN_DBSRVR_1		7100	// w
#define	PN_DBSRVR_1		24000	// w
#define PN_DBSRVR_2		7200	// core
#define	PN_LOGINSRVR	28000
#define	PN_PATCH_DISTRIBUTOR_0	9000
#define	PN_PATCH_DISTRIBUTOR_1	9100

#define	PN_MONITOR_0	11000
#define	PN_MONITOR_1	11001
#define	PN_MONITOR_2	11002

#define	PN_ACCOUNTSRVR_0	12000
#define	PN_ACCOUNTSRVR_1	12001
#define	PN_ACCOUNTSRVR_2	12002

#define	PN_ACCUMULATOR		13000

#define	PN_ADBILL		29000

#define PN_COLLECTION	26000


// PACKETTYPE_ADD_ACCOUNT의 리턴 코드
const unsigned char ACCOUNT_DUPLIACTE = 0;			// 중복 에러 
const unsigned char ACCOUNT_CHECK_OK = 1;			// 성공
const unsigned char ACCOUNT_EXTERNAL_ADDR = 2;			// 허용되지 않는 주소 
const unsigned char ACCOUNT_OVERFLOW = 3;			// 인원초과
const unsigned char ACCOUNT_BILLING_INFO_FAILED = 4;	// 빌링 정보 없음 
const unsigned char ACCOUNT_BILLING_DATABASE_ERROR = 5;	// 빌링 DB 에러 
const unsigned char ACCOUNT_BILLING_TIME_OVER = 6;	// 빌링 사용시간 만료 
const unsigned char ACCOUNT_BILLING_OTHER_ERROR = 7;	// 빌링 기타 다른 에러 
const unsigned char ACCOUNT_BILLING_DISCONNECTED = 8;  // 빌링 서버가 끊겨 있는 경우
const unsigned char ACCOUNT_IPCUT_ADDR = 9;  // 빌링 서버가 끊겨 있는 경우

const unsigned char ACCOUNT_BILLING_WAIT_ACK = 128;  // 빌링응답을 기다리는 경우 

//
// m_cbAccountFlag의 의미 
//
const unsigned char ACCOUNT_FLAG_18 = 0x01;		// 18세이상 
const unsigned char ACCOUNT_FLAG_SCHOOLEVENT = 0x02;		// 학교대항전 이벤트 참가가능자?
const unsigned char ACCOUNT_FLAG_UNREGISTER18 = 0x04;		// 미등록 성인 
const unsigned char ACCOUNT_FLAG_HANGAME = 0x08;		// 한게임 사용자


#define CHATTING_ENTERMEMBER		(unsigned char)0x00
#define CHATTING_NEWCHATTING		(unsigned char)0x01
#define CHATTING_DELETECHATTING		(unsigned char)0x02
#define CHATTING_REMOVEMEMBER		(unsigned char)0x03
#define CHATTING_CHATTING			(unsigned char)0x04
#define CHATTING_CHATTINGROOMSTATE	(unsigned char)0x05

#define GAME_RATE_SHOPCOST		(unsigned char)0x00
#define GAME_RATE_ITEMDROP		(unsigned char)0x01
#define GAME_RATE_GOLDDROP		(unsigned char)0x02
#define GAME_RATE_MONSTEREXP	(unsigned char)0x03
#define GAME_RATE_MONSTERHIT	(unsigned char)0x04
#define GAME_RATE_X3			(unsigned char)0x05	// GAME_RATE_ITEMDROP, //GAME_RATE_GOLDDROP, //GAME_RATE_MONSTEREXP
#define GAME_RATE_REBIRTH		(unsigned char)0x06
#define GAME_RATE_HITPOINT		(unsigned char)0x07
#define GAME_RATE_AGGRESSIVE	(unsigned char)0x08
#define GAME_RATE_RESPAWN		(unsigned char)0x09
#define GAME_SKILL_VAGSP		(unsigned char)0x10
#define GAME_SKILL_EXPERTSP		(unsigned char)0x11
#define GAME_SKILL_PROSP		(unsigned char)0x12

#define GAME_RATE_SHOP_BUY		(unsigned char)0x13
#define GAME_RATE_SHOP_SELL		(unsigned char)0x14


const unsigned char COMMONPLACE_ACTION = 0x00;
const unsigned char COMMONPLACE_ALPHA = 0x01;
const unsigned char COMMONPLACE_QUAKE = 0x02;
//const unsigned char COMMONPLACE_ERROR	= 0x03;


//현상금 처리관련 
const unsigned char CT_WANTED_REQ_LIST = 0x00;		// Core->Trans 리스트 요청 
const unsigned char CT_WANTED_SET = 0x01;		// Core->Trans 현상금 설정 

#if __VER < 8 // __S8_PK
const unsigned char ADD_PLAYER_ENEMY = 0x00;		// 플레이어 적 추가 
const unsigned char DEL_PLAYER_ENEMY = 0x01;		// 플레이어 적 제거 
#endif // __VER < 8 // __S8_PK
#endif //__MSGHDR_H__

const unsigned char GC_WINGUILD = 0x00;		// 이긴 길드 정보
const unsigned char GC_IN_WINDOW = 0x01;		// 대전 신청 창 띄움
const unsigned char GC_IN_APP = 0x02;		// 대전 신청
const unsigned char GC_IN_COMPLETE = 0x03;		// 길드대전 신청 완료
const unsigned char GC_REQUEST_STATUS = 0x04;		// 대전 신청 현황
const unsigned char GC_SELECTPLAYER = 0x05;		// 길드대전 선택 캐릭터 창 띄움
const unsigned char GC_SELECTWARPOS = 0x06;		// 대전위치 윈도우 띄움
const unsigned char GC_BESTPLAYER = 0x07;		// BEST Player 정보
const unsigned char GC_ISREQUEST = 0x08;		// 신청중인지?
const unsigned char GC_USERSTATE = 0x10;		// 유저상태
const unsigned char GC_WARPLAYERLIST = 0x11;		// 전쟁유저 정보
const unsigned char GC_GUILDSTATUS = 0x20;		// 자신의 길드 상황
const unsigned char GC_GUILDPRECEDENCE = 0x21;		// 길드 순위
const unsigned char GC_PLAYERPRECEDENCE = 0x22;		// 개인 순위
const unsigned char GC_GCSTATE = 0x30;		// 대전 상황	
const unsigned char GC_NEXTTIMESTATE = 0x31;		// 대전시 다음 시간과 상태 넘겨주기
const unsigned char GC_ENTERTIME = 0x32;		// 대전시 들어갈수 있는 시간
const unsigned char GC_DIAGMESSAGE = 0x33;		// 다이얼 로그 메세지
const unsigned char GC_TELE = 0x34;		// 텔레포트
const unsigned char GC_LOG = 0x35;		// 대전 로그( 대전이 끝나면 나오게~ )
const unsigned char GC_LOG_REALTIME = 0x36;		// 대전 로그( 실시간으로 채팅창에 나오게~ )
const unsigned char GC_GETPENYAGUILD = 0x40;		// 길드 신청금액 및 보상
const unsigned char GC_GETPENYAPLAYER = 0x41;		// 베스트 플레이어 보상
const unsigned char GC_PLAYERPOINT = 0x42;		// 길드대전 플레이어 포인트

const unsigned char SOMMON_FRIEND = 0x00;		// 친구 소환
const unsigned char SOMMON_FRIEND_CONFIRM = 0x01;		// 친구 소환 확인
const unsigned char SOMMON_PARTY_CONFIRM = 0x10;		// 파티 소환

const unsigned char LOG_SKILLPOINT_GET_QUEST = 0x00;		// 스킬포인트 얻기 - 퀘스트
const unsigned char LOG_SKILLPOINT_GET_HUNT = 0x01;		// 스킬포인트 얻기 - 몬스터
const unsigned char LOG_SKILLPOINT_USE = 0x10;		// 스킬포인트 쓰기 - 스킬레벨업

const unsigned char PK_PINK = 0x00;		// 핑크상태 전송
const unsigned char PK_PROPENSITY = 0x01;		// 카오성향 전송
const unsigned char PK_PKVALUE = 0x02;		// 카오수치 전송

//const unsigned char ANGEL_WNDCREATE			= 0x00;		// 엔젤창
const unsigned char ANGEL_INFO = 0x01;		// 엔젤 정보 주기(경험치, 레벨)

const unsigned char TEXT_GENERAL = 0x01;		// OnText
const unsigned char TEXT_DIAG = 0x02;		// OnDiagText

const unsigned char MINIGAME_KAWIBAWIBO_RESUTLT = 0x01;		// 가위바위보 결과값
const unsigned char MINIGAME_REASSEMBLE_RESULT = 0x02;		// 퍼즐 결과
const unsigned char MINIGAME_REASSEMBLE_OPENWND = 0x03;		// 퍼즐창 열때 카드아이템 값들을 얻기위해...
const unsigned char MINIGAME_ALPHABET_OPENWND = 0x04;		// 알파벳 창 열때 공개 글자와 위치
const unsigned char MINIGAME_ALPHABET_RESULT = 0x05;		// 알파벳 맞추기 결과
const unsigned char MINIGAME_FIVESYSTEM_OPENWND = 0x06;		// 오곱 창 열때 최소, 최대, 배수
const unsigned char MINIGAME_FIVESYSTEM_RESULT = 0x07;		// 오곱 결과


const unsigned char ULTIMATE_MAKEITEM = 0x01;		// 빛나는 오리칼쿰 생성
const unsigned char ULTIMATE_TRANSWEAPON = 0x02;		// 무기 변환(일반->유니크, 유니크->얼터멋)
const unsigned char ULTIMATE_MAKEGEM = 0x03;		// 보석 생성(아이템 쪼개기)
const unsigned char ULTIMATE_SETGEM = 0x04;		// 보석 합성
const unsigned char ULTIMATE_REMOVEGEM = 0x05;		// 보석 제거
const unsigned char ULTIMATE_ENCHANTWEAPON = 0x06;		// 얼터멋 웨폰 제련

const unsigned char EXCHANGE_RESULT = 0x01;		// 교환 스크립트 결과

const unsigned char LEGENDSKILL_RESULT = 0x01;		// legend 스킬 결과

const unsigned char GI_LOG_VIEW_ITEM_ADD = 0x01;		// 길드창고 아이템추가 로그
const unsigned char GI_LOG_VIEW_ITEM_REMOVE = 0x02;		// 길드창고 아이템제거 로그
const unsigned char GI_LOG_VIEW_MONEY_ADD = 0x03;		// 길드창고 돈추가 로그
const unsigned char GI_LOG_VIEW_MONEY_REMOVE = 0x04;		// 길드창고 돈추가 로그

const unsigned char SECRETROOM_GUILDLIST = 0x01;
const unsigned char SECRETROOM_KILLCOUNT = 0x02;
const unsigned char SECRETROOM_WARSTATE = 0x03;
