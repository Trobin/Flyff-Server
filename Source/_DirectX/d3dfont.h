//-----------------------------------------------------------------------------
// File: D3DFont.h
//
// Desc: Texture-based font class
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#ifndef D3DFONT_H
#define D3DFONT_H
#include <tchar.h>
#include <D3D9.h>
#include <d3dx9core.h>

// Font creation flags
#define D3DFONT_BOLD        0x0001
#define D3DFONT_ITALIC      0x0002
#define D3DFONT_ZENABLE     0x0004

// Font rendering flags
#define D3DFONT_CENTERED_X  0x0001
#define D3DFONT_CENTERED_Y  0x0002
#define D3DFONT_TWOSIDED    0x0004
#define D3DFONT_FILTERED    0x0008


// Hangul High : 0xb0 ~ 0xc8
// Hangul Low  : 0xa0 ~ 0xff

//#define EXTRA_HANGUL     0xa4
//#define MIN_HANGUL_HIGH  0xb0
//#define MIN_HANGUL_LOW   0xa0
//#define MAX_HANGUL_HIGH  0xc9
//#define MAX_HANGUL_LOW   0x100
//#define NUM_HANGUL_HIGH  (MAX_HANGUL_HIGH - MIN_HANGUL_HIGH)
//#define NUM_HANGUL_LOW   (MAX_HANGUL_LOW  - MIN_HANGUL_LOW )

//#define MAX_ASCII        96
//#define NUM_HANGUL       (NUM_HANGUL_HIGH * MAX_HANGUL_LOW + 50000)

typedef struct tagFontTexture
{
	//int nTextureCoord;
	LPDIRECT3DTEXTURE9 pTexture;
	float tx1;
	float ty1;
	float tx2;
	float ty2;
} FONTTEX, * LPFONTTEX;

#if defined( __CLIENT)
#include "EditString.h"
#endif


typedef struct tagGlyphInfo
{
	char text[6];
	unsigned short wCodePage;
	bool operator < (const tagGlyphInfo& glyphInfo) const { return memcmp(this, &glyphInfo, sizeof(tagGlyphInfo)) < 0; }
} GLYPHINFO;

void    SetCodePage(int nLang);

//-----------------------------------------------------------------------------
// Name: class CD3DFontAPI
// Desc: Texture-based font class for doing text in a 3D scene.
//-----------------------------------------------------------------------------
class CD3DFontAPI
{
	TCHAR   m_strFontName[80];            // Font properties
	unsigned long   m_dwFontHeight;
	unsigned long   m_dwFontFlags;

	unsigned long   m_bCaption;
	LPDIRECT3DDEVICE9       m_pd3dDevice; // A D3DDevice used for rendering
	float   m_fTextScale;
	unsigned long   m_dwSpacing;                  // Character pixel spacing per side

	/////////////////////////////////////////
	LPD3DXFONT    m_pFont;
	//LPD3DXFONT    m_pFont2;
	//LPD3DXFONT    m_pStatsFont;
	LPD3DXMESH    m_pMesh3DText;
	//WCHAR*        m_strTextBuffer;

	ID3DXSprite* m_pD3DXSprite;

	//	int           m_nFontSize;
		/////////////////////////////////////////

public:
	int   m_nOutLine;
	unsigned long m_dwColor;
	unsigned long m_dwBgColor;
	unsigned long m_dwFlags;

public:
	SIZE m_sizeBitmap;

	//LPFONTTEX GetFontOffset( unsigned short wText ) { if( wText >= m_aFontOffset.GetSize() ) return NULL; return (LPFONTTEX)m_aFontOffset.GetAt( wText ); }
	// 2D and 3D text drawing functions
#if defined( __CLIENT)
	virtual HRESULT DrawText(float x, float y, unsigned long dwColor,
		CEditString& editString, int nPos = 0, int nLine = 0, int nLineSpace = 0, unsigned long dwFlags = 0L);
	virtual HRESULT DrawText(float x, float y, float fXScale, float fYScale, unsigned long dwColor,
		CEditString& editString, int nPos = 0, int nLine = 0, int nLineSpace = 0, unsigned long dwFlags = 0L);
#endif 

	virtual HRESULT DrawText(float x, float y, unsigned long dwColor,
		const TCHAR* strText, unsigned long dwFlags = 0L);
	virtual HRESULT DrawText(float x, float y, float fXScale, float fYScale, unsigned long dwColor,
		const TCHAR* strText, unsigned long dwFlags = 0L);
	HRESULT DrawTextScaled(float x, float y, float z,
		float fXScale, float fYScale, unsigned long dwColor,
		const TCHAR* strText, unsigned long dwFlags = 0L);
	HRESULT Render3DText(const TCHAR* strText, unsigned long dwColor, unsigned long dwFlags = 0L);

	// Function to get extent of text
	virtual HRESULT GetTextExtent(const TCHAR* strText, SIZE* pSize);

	// Initializing and destroying device-dependent objects
	virtual HRESULT InitDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	virtual HRESULT RestoreDeviceObjects();
	virtual HRESULT InvalidateDeviceObjects();
	virtual HRESULT DeleteDeviceObjects();

	// Constructor / destructor
	CD3DFontAPI();
	CD3DFontAPI(const TCHAR* strFontName, unsigned long dwHeight, unsigned long dwFlags = 0L);
	virtual ~CD3DFontAPI();

	// add
	unsigned long m_dwMaxHeight;

	unsigned long GetMaxHeight() { return m_dwMaxHeight; }
};
class CD3DFont //: public CD3DFontAPI
{
	char    m_strFontName[80];            // Font properties
	unsigned long   m_dwFontHeight;
	unsigned long   m_dwFontFlags;

	unsigned long   m_bCaption;
	LPDIRECT3DDEVICE9       m_pd3dDevice; // A D3DDevice used for rendering
	LPDIRECT3DVERTEXBUFFER9 m_pVB;        // VertexBuffer for rendering text
	unsigned long   m_dwTexWidth;                 // Texture dimensions
	unsigned long   m_dwTexHeight;
	float   m_fTextScale;
	unsigned long   m_dwSpacing;                  // Character pixel spacing per side

	// Stateblocks for setting and restoring render states
	LPDIRECT3DSTATEBLOCK9 m_pStateBlockSaved;
	LPDIRECT3DSTATEBLOCK9 m_pStateBlockDrawText;

	HRESULT   CreateGDIFont(HDC hDC, HFONT* pFont);
	HRESULT   PaintAlphabet(HDC hDC, unsigned long* pBitmapBits, bool bMeasureOnly = false);
	void CopyToTexture(LPDIRECT3DTEXTURE9 pTex, CSize size, unsigned long dwBgColor);
	HFONT   m_hFont;
	HFONT   m_hFontOld;
	HDC     m_hDC;
	HBITMAP m_hbmBitmap;
	HGDIOBJ m_hbmOld;
	unsigned long* m_pBitmapBits;

	LPDIRECT3DTEXTURE9 m_pCurTexture;
	int m_nCurX, m_nCurY;

	void MakeOutLine(int nWidth, unsigned short* pDst16);
	LPDIRECT3DTEXTURE9 CreateTexture();
public:
	CPtrArray m_apTexture;

	unsigned long  m_dwColor;
	unsigned long  m_dwBgColor;
	unsigned long  m_nOutLine;
	unsigned long  m_dwFlags;

	SIZE m_sizeBitmap;

	unsigned short	m_wCodePage;

	FONTTEX* GetFontTex(const char* begin, const char* end, unsigned short wCodePage);
	HFONT    GetFont(unsigned short wCodePage);

	typedef	map<GLYPHINFO, FONTTEX>	font_tex_map;
	typedef	font_tex_map::iterator	font_tex_map_iter;

	font_tex_map	m_fontTex;
	unsigned long			m_dwNumTriangles;

	typedef map<unsigned short, HFONT>		font_map;
	typedef font_map::iterator		font_map_iter;

	font_map		m_fontMap;

	HRESULT DrawText(float x, float y, unsigned long dwColor,
		const char* strText, unsigned long dwFlags = 0L, unsigned short wCodePage = 0);
	HRESULT DrawText(float x, float y, float fXScale, float fYScale, unsigned long dwColor,
		const char* strText, unsigned long dwFlags = 0L, unsigned short wCodePage = 0);

#if defined( __CLIENT)
	HRESULT DrawText(float x, float y, unsigned long dwColor,
		CEditString& editString, int nPos = 0, int nLine = 0, int nLineSpace = 0, unsigned long dwFlags = 0L);
	HRESULT DrawText(float x, float y, float fXScale, float fYScale, unsigned long dwColor,
		CEditString& editString, int nPos = 0, int nLine = 0, int nLineSpace = 0, unsigned long dwFlags = 0L);
	HRESULT GetTextExtent_EditString(CEditString& editString, SIZE* pSize, int nLineBegin = 0, int nLineCount = -1, unsigned short wCodePage = 0);
	CSize GetTextExtent_EditString(CEditString& editString, int nLineBegin = 0, int nLineCount = -1, unsigned short wCodePage = 0) { SIZE size; GetTextExtent_EditString(editString, &size, nLineBegin, nLineCount, wCodePage); return size; }
#endif
	// Function to get extent of text
	HRESULT GetTextExtent(const char* strText, SIZE* pSize, unsigned short wCodePage = 0);
	CSize GetTextExtent(const char* pszText) { SIZE size; GetTextExtent(pszText, &size); return size; }

	// Initializing and destroying device-dependent objects
	HRESULT InitDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT RestoreDeviceObjects();
	HRESULT InvalidateDeviceObjects();
	HRESULT DeleteDeviceObjects();

	// Constructor / destructor
	CD3DFont(const char* strFontName, unsigned long dwHeight, unsigned long dwFlags = 0L);
	~CD3DFont();

	// add
	unsigned long m_dwMaxHeight;

	unsigned long GetMaxHeight() { return m_dwMaxHeight; }
};

class CD3DFontAPIVTN : public CD3DFontAPI
{
public:
	CD3DFont* m_pD3DFont;

public:
	CD3DFontAPIVTN(const TCHAR* strFontName, unsigned long dwHeight, unsigned long dwFlags = 0L);
	~CD3DFontAPIVTN();

public:
#if defined( __CLIENT)
	HRESULT DrawText(float x, float y, unsigned long dwColor, CEditString& editString, int nPos = 0, int nLine = 0, int nLineSpace = 0, unsigned long dwFlags = 0L);
	HRESULT DrawText(float x, float y, float fXScale, float fYScale, unsigned long dwColor, CEditString& editString, int nPos = 0, int nLine = 0, int nLineSpace = 0, unsigned long dwFlags = 0L);
#endif
	HRESULT DrawText(float x, float y, unsigned long dwColor, const TCHAR* strText, unsigned long dwFlags = 0L);
	HRESULT DrawText(float x, float y, float fXScale, float fYScale, unsigned long dwColor, const TCHAR* strText, unsigned long dwFlags = 0L);

	// Function to get extent of text
	HRESULT GetTextExtent(const TCHAR* strText, SIZE* pSize);

	// Initializing and destroying device-dependent objects
	HRESULT InitDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT RestoreDeviceObjects();
	HRESULT InvalidateDeviceObjects();
	HRESULT DeleteDeviceObjects();

	unsigned long GetMaxHeight();
};
#endif