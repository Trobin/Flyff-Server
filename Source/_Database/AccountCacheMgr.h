#ifndef _ACCOUNTCACHEMANAGER_H_
#define _ACCOUNTCACHEMANAGER_H_

struct ACCOUNT_CACHE;
class CAccountCacheMgr
{
private:
	map<string, ACCOUNT_CACHE*>	m_2Account;
	//	map<unsigned long, string>			m_2AccountPtr;
	MemPooler<ACCOUNT_CACHE>* m_pAccountMemPooler;
	list< ACCOUNT_CACHE* >		m_list;
	int							m_nAlloc;
public:
	CAccountCacheMgr();
	ACCOUNT_CACHE* Find(const char* szAccount);
	void			Clear();
	void			AddMover(unsigned long idPlayer, const char* szAccount, int nSlot);
	void			RemoveMover(unsigned long idPlayer, const char* szAccount);
	void			AddAccount(unsigned long idPlayer, const char* szAccount, bool bCache, ACCOUNT_CACHE* pCache);
	//	ACCOUNT_CACHE*	_Find( unsigned long idPlayer );
	ACCOUNT_CACHE* GetAccount(const char* szAccount, bool* pbCacheHit);
#ifdef __INVALID_LOGIN_0320
	void	ChangeMultiServer(const char* szAccount, unsigned long uMultiServer);
	int		InitMultiServer(unsigned long uMultiServer);
#endif	// __INVALID_LOGIN_0320
};


#endif // _ACCOUNTCACHEMANAGER_H_