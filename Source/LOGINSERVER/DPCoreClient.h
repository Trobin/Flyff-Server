#ifndef __DPCORECLIENT_H__
#define __DPCORECLIENT_H__

#pragma once

#include "DPMng.h"

#undef	theClass
#define	theClass	CDPCoreClient
#undef theParameters
#define theParameters	CAr & ar

class CDPCoreClient : public CDPMng
{

public:
	CDPCoreClient();
	virtual	~CDPCoreClient();

	// Operations
	virtual	void SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	virtual void UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);

	void	SendToServer(LPVOID lpMsg, unsigned long dwMsgSize)
	{
		Send(lpMsg, dwMsgSize, DPID_SERVERPLAYER);
	}

	void	SendPreJoin(unsigned long dwAuthKey, const TCHAR* lpszAccount, unsigned long idPlayer, const TCHAR* lpszPlayer);
	void	SendLeave(unsigned long idPlayer);
	void	QueryTickCount(void);
	void	OnQueryTickCount(CAr& ar);

	USES_PFNENTRIES;
	// Handlers
	void	OnPreJoin(CAr& ar);	// result
	void	OnQueryRemovePlayer(CAr& ar);
};

#endif	// __DPCORECLIENT_H__