#ifndef __DPDATABASECLIENT_H__
#define __DPDATABASECLIENT_H__

#pragma once

#include "DPMng.h"

#undef	theClass
#define	theClass	CDPDatabaseClient
#undef theParameters
#define theParameters	CAr & ar, DPID, LPBYTE, unsigned long

class CDPDatabaseClient : public CDPMng
{
public:
	//	Constructions
	CDPDatabaseClient();
	virtual	~CDPDatabaseClient();
	//	Overrides
	virtual	void SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	virtual void UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	//	Operations	
	void	SendToServer(DPID idFrom, LPVOID lpMsg, unsigned long dwMsgSize);
	void	SendLeave(const char* lpszAccount, unsigned long idPlayer, unsigned long dwPlayTime);
	void	SendCloseError(const char* lpszAccount);

	void	SendGetPlayerList(DPID idFrom, const char* lpszAccount, const char* lpszPassword, unsigned long dwAuthKey, unsigned long uIdofMulti);

	USES_PFNENTRIES;
	//	Handlers
	void	OnPlayerList(CAr& ar, DPID dpid, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnCloseExistingConnection(CAr& ar, DPID dpid, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnFail(CAr& ar, DPID dpid, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnOneHourNotify(CAr& ar, DPID dpid, LPBYTE lpBuf, unsigned long uBufSize);

#if __VER >= 15 // __2ND_PASSWORD_SYSTEM
	void	OnLoginProtect(CAr& ar, DPID dpid, LPBYTE lpBuf, unsigned long uBufSize);
public:
	void	SendLoginProtect(const char* lpszAccount, const char* lpszPlayer, unsigned long idPlayer, int nBankPW, DPID dpid);
#endif // __2ND_PASSWORD_SYSTEM
};

inline void CDPDatabaseClient::SendToServer(DPID idFrom, LPVOID lpMsg, unsigned long dwMsgSize)
{
	*(UNALIGNED DPID*)lpMsg = idFrom;	Send(lpMsg, dwMsgSize, DPID_SERVERPLAYER);
}

#endif	// __DPDATABASECLIENT_H__