// LoginProtect.h: interface for the CLoginProtect class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOGINPROTECT_H__818DA819_B42E_4A41_9CEF_53B0C13A0F51__INCLUDED_)
#define AFX_LOGINPROTECT_H__818DA819_B42E_4A41_9CEF_53B0C13A0F51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if __VER >= 15 // __2ND_PASSWORD_SYSTEM
#include <dplay.h>

#define	MAX_NUMPADID	999
#define	MAX_SECRETNUM	9999

enum LOGIN_CHECK
{
	LOGIN_OK,				// 틀린 적이 없거나, 처음 
	LOGIN_3TIMES_ERROR,		// 3회 틀림 
};

class CLoginProtect
{
public:
	CLoginProtect();
	~CLoginProtect();

	static CLoginProtect* GetInstance();

	void	Clear();
	int		GetNumPad2PW(unsigned long idNumPad, unsigned long uSecretNum);
	unsigned long	GetNumPadId(DPID dpId);
	unsigned long	SetNumPadId(DPID dpId, bool bLogin);
	LOGIN_CHECK	Check(unsigned long idPlayer);
	void	SetError(unsigned long idPlayer, bool bLogin);

private:
	struct LOGIN_CACHE
	{
		int		nError;
		time_t	tmError;
		LOGIN_CACHE() : nError(0), tmError(time(NULL)) {};
	};
	typedef map<unsigned long, LOGIN_CACHE>	MAP_CACHE;
	MAP_CACHE	m_mapCache;

	typedef map<DPID, unsigned long>	MAP_NUMPAD_ID;
	MAP_NUMPAD_ID	m_mapNumPadId;
};

#endif // __2ND_PASSWORD_SYSTEM
#endif // !defined(AFX_LOGINPROTECT_H__818DA819_B42E_4A41_9CEF_53B0C13A0F51__INCLUDED_)
