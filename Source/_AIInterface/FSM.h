#if !defined(__FSM_H__)
#define __FSM_H__

// forward reference
class CFSMstate;

class CFSM
{
private:
	map< int, CFSMstate* >	m_map;

public:
	CFSM();
	~CFSM();

	CFSMstate* GetState(int nStateID);
	bool AddState(CFSMstate* pState);
	bool GetTransition(int nStateID, int nInput, int& nOutput);
};

enum FSM_TYPE
{
	FSM_TYPE_0,
	FSM_TYPE_1,
	FSM_TYPE_2,
	FSM_TYPE_MAX
};

extern CFSM* GetFSM(unsigned long type);

#endif // !defined(__FSM_H__)
