#ifndef __AI_MONSTER2_H
#define __AI_MONSTER2_H

#include "AIInterface.h"

class CObj;

class CAIMonster2 : public CAIInterface
{
public:
	CAIMonster2();
	CAIMonster2(CObj* pObj);
	virtual ~CAIMonster2();

	virtual void	RouteMessage();
	virtual void	SendAIMsg(unsigned long dwMessage, unsigned long dwParam1 = 0, unsigned long dwParam2 = 0);
	virtual void	PostAIMsg(unsigned long dwMessage, unsigned long dwParam1 = 0, unsigned long dwParam2 = 0);
	virtual void	InitAI();

	unsigned long			GetState() { return m_nState; }

private:
	unsigned long			m_dwFsmType;
	unsigned long			m_nState;
	unsigned long			m_dwTick;
	D3DXVECTOR3		m_vPosBegin;

	unsigned long			m_idLastAttacker;		// 나를 공격한 자 id 
	OBJID			m_idTarget;				// 공격 대상 id 
	D3DXVECTOR3		m_vTarget;				// 공격 대상 위치 
	int				m_nAttackType;			// 공격 방식
	float			m_fAttackRange;			// 공격 범위 
	unsigned long			m_dwReattack;			// 재 공격 시각 

	void	Init();
	bool	Check(int nInput, unsigned long dwCurTick, unsigned long dwValue);		// 주기적 검사( polling )
	bool	IsControllable();
	void	ProcessAIMsg(int nInput, int nOutput, unsigned long dwParam1, unsigned long dwParam2);
	void	OnBeginState(int nInput, unsigned long dwParam1, unsigned long dwParam2);
	void	OnEndState(int nInput, unsigned long dwParam1, unsigned long dwParam2);
	bool	BeginAttack();
	void	MoveToDst(const D3DXVECTOR3& vDst);
	bool	SelectTarget();
	bool	Search();
	bool	IsInValidTarget();
};

#endif