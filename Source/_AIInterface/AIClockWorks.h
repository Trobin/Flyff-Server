#ifndef __AI_CLOCKWORKS_H
#define __AI_CLOCKWORKS_H

#include "mempooler.h"

class CAIClockWorks : public CAIInterface
{
	D3DXVECTOR3 m_vPosBegin;		// 최초 생성된 좌표.
	int		m_nEvent;				// 현재 패턴 상태.
	int		m_nAttackType;			// 공격 방식.
	unsigned long	m_tmReattack;			// 재공격 타이머.
	unsigned long	m_tmAddReattack;		// 재공격 타이머의 랜덤변위값.(이 랜덤값으로 더 빨리 쏘는 경우도 생김)
	unsigned long	m_tmTrace;				// 추적시간.
	unsigned long	m_tmTimeOver;			// 예상치 못한 상황으로 더이상 공격을 안하던가 하는경우를 감별
	OBJID	m_idTarget;				// 공격 대상
	D3DXVECTOR3	m_vTarget;			// 공격 위치.
	int		m_nAppearCnt;			// 등장 카운터.

	OBJID	m_idLastAttacker;		// 날 마지막으로 공격한 쉐리.

	bool	m_bDefenseMode;			// 방어태세 모드.

	bool	MoveProcessIdle();
	bool	MoveProcessRage();
	bool	MoveProcessRunaway();

	bool	StopProcessIdle();

	bool	SelectTarget(void);		// 리어택 타이밍이 됬을때 타겟을 선정함.

	void Init(void);
	void Destroy(void);

public:

	CAIClockWorks();
	CAIClockWorks(CObj* pObj);
	virtual ~CAIClockWorks();

	virtual void InitAI();

	int GetEvent(void) { return m_nEvent; }
	bool	StateInit(const AIMSG& msg);
	bool	StateIdle(const AIMSG& msg);
	bool	StateWander(const AIMSG& msg);
	bool	StateRunaway(const AIMSG& msg);
	bool	StateEvade(const AIMSG& msg);
	bool	StateRage(const AIMSG& msg);

public:
#ifndef __VM_0820
#ifndef __MEM_TRACE
	static	MemPooler<CAIClockWorks>* m_pPool;
	void* operator new(size_t nSize) { return CAIClockWorks::m_pPool->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CAIClockWorks::m_pPool->Alloc(); }
	void	operator delete(void* lpMem) { CAIClockWorks::m_pPool->Free((CAIClockWorks*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CAIClockWorks::m_pPool->Free((CAIClockWorks*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820

	DECLARE_AISTATE()
};

#endif
