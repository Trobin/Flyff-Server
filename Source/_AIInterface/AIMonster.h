#ifndef __AI_MONSTER_H
#define __AI_MONSTER_H

#include "mempooler.h"
#include "Mover.h"
#include "PathObj.h"

class CAIMonster : public CAIInterface
{
protected:
	unsigned long 			m_tmMove;
	unsigned long			m_tmAttackDelay;
	unsigned long			m_tmAttack;
	unsigned long			m_tmSummon;					// 소환 타이머.
	unsigned long			m_tmHelp;					// 도움 요청 타이머.
	unsigned long			m_tmReturnToBegin;			// 리젠지점으로 돌아가는 시간측정.

	D3DXVECTOR3		m_vPosBegin;
	D3DXVECTOR3		m_vDestPos;
	D3DXVECTOR3		m_vOldPos;					// 이전 프레임 좌표

#if __VER >= 9	//__AI_0509
	D3DXVECTOR3		m_vPosDamage;
#endif	// __AI_0509

	_VECINFO		m_vecPatrolInfo;
	float			m_fAngleBegine;

	bool			m_bReturnToBegin;
	bool			m_bGoTarget;
	bool			m_bTargetNoMovePos;
	bool			m_bFirstRunaway;
	bool			m_bCallHelper;
	bool			m_bRangeAttack;				// 레인지 어택이냐 아니냐.
	bool			m_bLootMove;				// 루팅하러 가는중.

	unsigned long			m_dwIdTarget;
	unsigned long			m_dwAtkMethod;
#if __VER >= 12 // __NEW_SUMMON_RULE
	vector<OBJID>	m_vecIdSummon;					// 소환된 몬스터는 아이디가 채워진다.
#else // __NEW_SUMMON_RULE
	OBJID			m_idSummon[MAX_SUMMON];	// 소환된 몬스터는 아이디가 채워진다.
#endif // __NEW_SUMMON_RULE
	unsigned long			m_idLootItem;				// 루팅할 아이템.

	unsigned long			GetAtkMethod_Near();
	unsigned long			GetAtkMethod_Far();
	unsigned long			GetAtkRange(unsigned long dwAtkMethod);
	ItemProp* GetAtkProp(unsigned long dwAtkMethod);
	unsigned long			GetAtkItemId(unsigned long dwAtkMethod);
	void			DoAttack(unsigned long dwAtkMethod, CMover* pTarget);
	void			DoReturnToBegin(bool bReturn = true);
	void			MoveToDst(OBJID dwIdTarget);
	void			MoveToDst(const D3DXVECTOR3& vDst);
	void			MoveToRandom(unsigned int nState);
	void			CallHelper(const MoverProp* pMoverProp);
	void			SetStop(unsigned long dwTime);
	bool			IsEndStop();
	bool			IsMove() { return GetMover()->m_pActMover->IsMove(); }
	bool			IsInRange(D3DXVECTOR3& vDistant, float fRange);
	int				SelectAttackType(CMover* pTarget);	// AI에 따른 공격방식을 선택.
	bool			MoveProcessIdle(const AIMSG& msg);
	bool			MoveProcessRage(const AIMSG& msg);
	bool			SubAttackChance(const AIMSG& msg, CMover* pTarget);
	bool			MoveProcessRunaway();
	bool			StopProcessIdle();
	void			SubSummonProcess(CMover* pTarget);
	bool			SubItemLoot();
	void			Init();
	bool			StateInit(const AIMSG& msg);
	bool			StateIdle(const AIMSG& msg);
	bool			StateWander(const AIMSG& msg);
	bool			StateRunaway(const AIMSG& msg);
	bool			StateEvade(const AIMSG& msg);
	bool			StateRage(const AIMSG& msg);

	bool			StateRagePatrol(const AIMSG& msg);
	bool			MoveProcessRagePatrol(const AIMSG& msg);
	bool			StateStand(const AIMSG& msg);
	bool			StatePatrol(const AIMSG& msg);
	bool			MoveProcessStand(const AIMSG& msg);
	bool			MoveProcessPatrol(const AIMSG& msg);

	void	SetTarget(OBJID dwIdTarget, unsigned long uParty);

#if __VER >= 9	//__AI_0509
public:
	virtual	bool	IsReturnToBegin(void) { return m_bReturnToBegin; }
protected:
#endif	// __AI_0509

public:
	CAIMonster();
	CAIMonster(CObj* pObj);
	virtual ~CAIMonster();

	virtual void	InitAI();

#ifndef __VM_0820
#ifndef __MEM_TRACE
	static	MemPooler<CAIMonster>* m_pPool;
	void* operator new(size_t nSize) { return CAIMonster::m_pPool->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CAIMonster::m_pPool->Alloc(); }
	void	operator delete(void* lpMem) { CAIMonster::m_pPool->Free((CAIMonster*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CAIMonster::m_pPool->Free((CAIMonster*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820

	DECLARE_AISTATE()
};


#if __VER >= 12 // __MONSTER_SKILL
struct __MONSTERSKILL
{
	int		nAtkMethod;
	unsigned long	dwSkillID;
	unsigned long	dwSkillLV;
	int		nHitCount;
	int		nRange;
	int		nApplyProbabilty;
	unsigned long	dwSkillTime;
	bool	bIgnoreSkillProb;
	__MONSTERSKILL()
	{
		nAtkMethod = 0;
		dwSkillID = NULL_ID;
		dwSkillLV = 0;
		nHitCount = 0;
		nRange = 0;
		nApplyProbabilty = 0;
		dwSkillTime = 0;
		bIgnoreSkillProb = 0;
	}
};

typedef map<unsigned long, vector<__MONSTERSKILL> > MAPMONSTERSKILL;
typedef map<unsigned long, unsigned long> MAPATKMETHOD;

const unsigned long ATK_MELEE = 1;
const unsigned long ATK_RANGE = 2;

class CMonsterSkill
{
private:
	CMonsterSkill();
public:
	~CMonsterSkill();
	static CMonsterSkill* GetInstance(void);

	void	LoadScript();
	void	Clear();

	unsigned long	GetMonsterSkillLevel(CMover* pAttacker, unsigned long dwSkillId);
	bool	ApplySkill(CMover* pAttacker, CMover* pTarget, unsigned long dwAtkMethod);
#if __VER >= 14 // __INSTANCE_DUNGEON
	bool	MonsterTransform(CMover* pMover, int nMoverHP);
#endif // __INSTANCE_DUNGEON

private:
	MAPMONSTERSKILL		m_mapMonsterSkill;
};
#endif // __MONSTER_SKILL

#endif