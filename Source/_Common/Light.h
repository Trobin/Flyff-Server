#ifndef __LIGHT_H
#define __LIGHT_H

class CWorld;

class CLight : public D3DLIGHT9
{
public:
	CString m_strKey;
	unsigned long m_dwIndex;
	//public:
	CLight();
	CLight(const char* lpszKey, D3DLIGHTTYPE ltType, float x, float y, float z);
	~CLight() { }
	void SetLight(const char* lpszKey, D3DLIGHTTYPE ltType, float x, float y, float z);
	void Appear(LPDIRECT3DDEVICE9 pd3dDevice, bool bEnable = true);
	void SetPos(D3DXVECTOR3 vPos) { Position = vPos; }
	void SetPos(float x, float y, float z) { Position.x = x; Position.y = y; Position.z = z; }
	void SetDir(D3DXVECTOR3 vDir) { Direction = vDir; }
	void SetDir(float x, float y, float z) { Direction.x = x; Direction.y = y; Direction.z = z; }
};
#endif