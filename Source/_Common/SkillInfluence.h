#ifndef __SKILLINFLUENCE_H__
#define	__SKILLINFLUENCE_H__

#define	BUFF_ITEM	0
#define	BUFF_SKILL	1
#define	BUFF_PET	2
#define	BUFF_ITEM2	3
#define	BUFF_EQUIP	4	// 장착류 아이템에 특정 DST(DST_GIFTBOX)에 대해 버프 아이콘만 출력
#define	BUFF_NULL_ID	(unsigned short)0xFFFF
#define	MAX_SKILLINFLUENCE	64
#define	MAX_SKILLBUFF_COUNT	28

#ifndef __BUFF_1107

#include "ar.h"

typedef	struct	tagSKILLINFLUENCE
{
	unsigned short	wType;			// 0:아이템 1:스킬 2:기타?
	unsigned short	wID;			// 아이템이나 스킬의 프로퍼티 ID
	unsigned long	dwLevel;		// 스킬 레벨 - 저장
	unsigned long	tmCount;		// 남은 시간(카운트).  - 저장
	unsigned long	tmTime;			// 시작 타이머.
	bool	bEffect;		// 지속효과이펙트를 가지고 있을때 그것이 로딩됐는지.. false면 로딩해야한다.
#ifdef __PVPDEBUFSKILL
	unsigned long	dwAttackerID;	// 스킬 시전자 ID
#endif // __PVPDEBUFSKILL
}

SKILLINFLUENCE, * LPSKILLINFLUENCE;

class CMover;
class CSkillInfluence
{
private:
#if __VER < 8 //__CSC_VER8_3
	SKILLINFLUENCE* m_pEmptyNode;	// 비어있는 공간의 인덱스.
#endif //__CSC_VER8_3
	CMover* m_pMover;
	//	CRIT_SEC	m_AddRemoveLock;

	void	RemoveSkillInfluence(SKILLINFLUENCE* pSkillInfluence);

public:
	//	bool	IsEmpty();
#if !( defined( __CORESERVER ) || defined( __DBSERVER ) ) 	
#ifdef __PVPDEBUFSKILL
	bool	RemoveSkillInfluenceFromID(OBJID dwAttackerID);
#endif // __PVPDEBUFSKILL
	bool	RemoveSkillInfluence(unsigned short wType, unsigned short wID);
	bool	RemoveSkillInfluenceState(unsigned long dwChrState);
#if __VER >= 11 // __MA_VER11_06				// 확율스킬 효과수정 world,neuz
	bool	RemoveSkillInfluenceDestParam(unsigned long dwDestParam);
#endif // __MA_VER11_06				// 확율스킬 효과수정 world,neuz

	bool    RemoveAllSkillInfluence();
	bool	RemoveAllSkillDebuff(void);
	bool	RemoveAllSkillBuff(void);
#if __VER >= 11 // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz
	bool	RemoveAllBuff(void);
#endif // __MA_VER11_05	// 케릭터 봉인 거래 기능 world,database,neuz
	bool	RemoveOneSkillBuff(void);


#endif // #if !( defined( __CORESERVER ) || defined( __DBSERVER ) ) 
	//	Constructions
	CSkillInfluence();
	virtual	~CSkillInfluence();

	SKILLINFLUENCE		m_aSkillInfluence[MAX_SKILLINFLUENCE];

	LPSKILLINFLUENCE	FindPtr(unsigned short wType, unsigned short wID);
	void	Init(void);
	void	Destroy(void) {}

	void	Serialize(CAr& ar);

	//	Operations
	void	SetMover(CMover* pMover);
#ifdef __PVPDEBUFSKILL
	bool	Set(unsigned short wType, unsigned short wID, unsigned long dwLevel, unsigned long dwTime, unsigned long dwAttackerID = NULL_ID);
	bool	Set(SKILLINFLUENCE* pNode, unsigned short wType, unsigned short wID, unsigned long dwLevel, unsigned long dwTime, unsigned long dwAttackerID);
#else // __PVPDEBUFSKILL
	bool	Set(unsigned short wType, unsigned short wID, unsigned long dwLevel, unsigned long dwTime);
	bool	Set(SKILLINFLUENCE* pNode, unsigned short wType, unsigned short wID, unsigned long dwLevel, unsigned long dwTime);
#endif // __PVPDEBUFSKILL

	LPSKILLINFLUENCE	Get(int nIdx);
#if !( defined( __CORESERVER ) || defined( __DBSERVER ) ) 
	void	Process(void);
#endif // #if !( defined( __CORESERVER ) || defined( __DBSERVER ) ) 
	void	Reset(void);		// 클라로 버프정보를 다시 갱신하도록 타이머를 클리어 시킴.
	bool	HasSkill(unsigned short wType, unsigned short wID);

#if __VER >= 9	// __PET_0410
	bool	HasPet(void) { return FindPet() != NULL; }
	SKILLINFLUENCE* FindPet(void);
	bool	RemovePet(void);
#endif	// __PET_0410

#ifdef __CLIENT
	unsigned long	GetDisguise(void);
#endif // __CLIENT
	bool	HasLikeItemBuf(unsigned long dwItemKind3);
	void	RemoveLikeItemBuf(unsigned long dwItemKind3);
	SKILLINFLUENCE* Find(unsigned short wType, unsigned short wID);
	SKILLINFLUENCE* GetItemBuf(unsigned long dwItemKind3);

private:
	//	Attributes
	void	Remove(SKILLINFLUENCE* pNode);
	bool	LikeItemBuf(unsigned long dwItemKind3);
#if __VER >= 8 //__CSC_VER8_3
	SKILLINFLUENCE* SortSkillArray();
#ifdef __PVPDEBUFSKILL
	bool InsertBuff(SKILLINFLUENCE* pNode, unsigned short wType, unsigned short wID, unsigned long dwLevel, unsigned long dwTime, unsigned long dwAttackerID);
#else // __PVPDEBUFSKILL
	bool InsertBuff(SKILLINFLUENCE* pNode, unsigned short wType, unsigned short wID, unsigned long dwLevel, unsigned long dwTime);
#endif // __PVPDEBUFSKILL
#endif //__CSC_VER8_3
};

//
inline void CSkillInfluence::SetMover(CMover* pMover)
{
	m_pMover = pMover;
}

//
inline SKILLINFLUENCE* CSkillInfluence::Find(unsigned short wType, unsigned short wID)
{
	int		i = MAX_SKILLINFLUENCE;
	SKILLINFLUENCE* pList = m_aSkillInfluence, * pNode;

	while (i--)
	{
		pNode = pList++;
		if (pNode->wType == wType && pNode->wID == wID)	// 같은걸 찾음.
			return pNode;
	}

	return NULL;
}

#if __VER >= 9	// __PET_0410
inline SKILLINFLUENCE* CSkillInfluence::FindPet(void)
{
	int	i = MAX_SKILLINFLUENCE;
	SKILLINFLUENCE* pList = m_aSkillInfluence, * pNode;
	while (i--)
	{
		pNode = pList++;
		if (pNode->wType == BUFF_PET)
			return pNode;
	}
	return NULL;
}
#endif	// __PET_0410

//
inline bool CSkillInfluence::HasSkill(unsigned short wType, unsigned short wID)
{
	return Find(wType, wID) != NULL;
}

//
inline LPSKILLINFLUENCE CSkillInfluence::FindPtr(unsigned short wType, unsigned short wID)
{
	return(Find(wType, wID));
}

//
inline LPSKILLINFLUENCE CSkillInfluence::Get(int nIdx)
{
	if (nIdx < 0 || nIdx >= MAX_SKILLINFLUENCE)
	{
		Error("SKILLINFLUENCE::Get() : 범위를 넘어섬 %d", nIdx);
		return(NULL);
	}

	return(m_aSkillInfluence + nIdx);
}

//
inline void CSkillInfluence::Remove(SKILLINFLUENCE* pNode)
{
	pNode->wType = 0;
	pNode->wID = 0;
	pNode->bEffect = 0;
	pNode->tmTime = 0;
	pNode->tmCount = 0;
#if __VER < 8 //__CSC_VER8_3
	m_pEmptyNode = pNode;		// 지운 노드는 비어있으므로 그것을 받아둠.
#endif //__CSC_VER8_3
}

#endif	// __BUFF_1107

#endif	// __SKILLINFLUENCE_H__