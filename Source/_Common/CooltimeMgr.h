#ifndef	__COOLTIMEMGR_H__
#define __COOLTIMEMGR_H__

const int MAX_COOLTIME_TYPE = 3;

struct ItemProp;

// 쿨타임 정보를 관리하는 클래스 
class CCooltimeMgr
{
public:
	CCooltimeMgr();
	virtual ~CCooltimeMgr();

	static unsigned long		GetGroup(ItemProp* pItemProp);

	bool				CanUse(unsigned long dwGroup);
	void				SetTime(unsigned long dwGroup, unsigned long dwCoolTime);
	unsigned long				GetTime(unsigned long dwGroup)
	{
		assert(dwGroup > 0);
		return m_times[dwGroup - 1];
	}

	unsigned long				GetBase(unsigned long dwGroup)
	{
		assert(dwGroup > 0);
		return m_bases[dwGroup - 1];
	}

protected:
	unsigned long				m_times[MAX_COOLTIME_TYPE];		// 최종 사용가능 시각 	
	unsigned long				m_bases[MAX_COOLTIME_TYPE];		// 이벤트 발생 시각 ( 아이템을 먹은 시각 ) 
};

#endif	__COOLTIMEMGR_H__