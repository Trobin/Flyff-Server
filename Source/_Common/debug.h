#ifndef __DEBUG_H
#define __DEBUG_H


#ifdef __IDC
extern void DEBUGOUT2(const char* lpszStr, const char* szFileName = _T("..\\error.txt"));
#else
extern void DEBUGOUT2(const char* lpszStr, const char* szFileName = _T("error.txt"));
#endif

extern void FILEOUT(const char* lpszFileName, const char* lpszFormat, ...);
extern void WriteLog(const char* lpszFormat, ...);
extern void WriteError(const char* lpszFormat, ...);

extern void OUTPUTDEBUGSTRING(const char* lpszFormat, ...);

extern const char* MakeFileNameDate(const char* lpszFileName);

#endif