#ifndef __WEATHER_H
#define __WEATHER_H

///////////////////////////////////

#define WEATHER_NONE       0
#define WEATHER_THUNDER    1
#define WEATHER_RAIN       2
#define WEATHER_SNOW       3

#define WEATHER_SPRING     0
#define WEATHER_SUMMER     1
#define WEATHER_FALL       2
#define WEATHER_WINTER     3

////////////////////////////////// 

class CWeather;

class CSnow : public CObj
{
public:
	static int m_nSnowNum;
	CWeather* m_pWeather;
	CObj* m_pObjCenter;
	float m_fFallSpeed;
	float m_fDirSpeed;
	CSnow(CWeather* pWeather, CObj* pObj);
	~CSnow();
	virtual void Process();
	virtual void Render(LPDIRECT3DDEVICE9 pd3dDevice);
};
//////////////////////////////////

class CRain : public CObj
{
public:
	static int m_nRainNum;
	CWeather* m_pWeather;
	CObj* m_pObjCenter;
	float m_fFallSpeed;
	float m_fDirSpeed;
	CRain(CWeather* pWeather, CObj* pObj);
	~CRain();
	virtual void Process();
	virtual void Render(LPDIRECT3DDEVICE9 pd3dDevice);
};
//////////////////////////////////

class CFireRain : public CObj
{
public:
	static int m_nFireRainNum;
	CWeather* m_pWeather;
	CObj* m_pObjCenter;
	int m_nFallSpeed;
	int m_nDirSpeed;
	CFireRain(CObj* pObj);
	~CFireRain();
	virtual void Process();
	virtual void Render(LPDIRECT3DDEVICE9 pd3dDevice);
};
class CWeather
{
	void ControlWeather();
public:
	CDate* m_pDate;
	int  m_nMaxWindSpeed;
	int  m_nMaxCloud;
	int  m_nEffect;
	int  m_nSeason;
	int  m_nCloud;
	int  m_nTemperature;
	int  m_nHumidity;
	int  m_nWind;
	int  m_nWindSpeed;
	int  m_nThunderLight;
	int  m_nWeatherCnt;
	bool m_bFixCloud;
	bool m_bFixTemperature;
	bool m_bFixHumidity;
	bool m_bFixWind;
	bool m_bFixWindSpeed;
	unsigned long m_dwWeather; // rain, snow,
	/*
	bool m_bFireRain;
	bool m_bRain;
	bool m_bSnow;
	//bool m_bIndoor         ;
*/
	CWeather(CDate* pDate);
	CWeather();
	~CWeather();
	void SetWeather(CTime* pTime, int nSeason, int nCloud, int nTemperature, int nHumidity, int nWind);
	void SetWeather(int nSeason, int nCloud, int nTemperature, int nHumidity, int nWind);
	bool IsThunder();
	virtual void Process(CWorld* pWorld);
	virtual void Render(LPDIRECT3DDEVICE9 pd3dDevice);
};
#endif