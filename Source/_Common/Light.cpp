#include "stdafx.h"

unsigned long  aaa = 100;
CLight::CLight()
{
	m_dwIndex = 0;
	m_dwIndex = aaa;
	aaa++;
}
CLight::CLight(const char* lpszKey, D3DLIGHTTYPE ltType, float x, float y, float z)
{
	m_strKey = lpszKey;
	D3DUtil_InitLight(*this, ltType, x, y, z);
	m_dwIndex = aaa;
	aaa++;
}
void CLight::SetLight(const char* lpszKey, D3DLIGHTTYPE ltType, float x, float y, float z)
{
	m_strKey = lpszKey;
	D3DUtil_InitLight(*this, ltType, x, y, z);
	//m_dwIndex = aaa;
}
void CLight::Appear(LPDIRECT3DDEVICE9 pd3dDevice, bool bEnable)
{
	if (bEnable)
	{
		if (D3D_OK != pd3dDevice->SetLight(m_dwIndex, this))
		{
			assert(0);
		}
	}
	if (D3D_OK != pd3dDevice->LightEnable(m_dwIndex, bEnable))
	{
		assert(0);
	}
}
