#include "stdafx.h"
#include "ResManager.h"

LPWNDCTRL WNDAPPLET::GetAt(unsigned long dwWndId)
{
	LPWNDCTRL pWndCtrl = NULL;
	for (int i = 0; i < ptrCtrlArray.GetSize(); i++)
	{
		pWndCtrl = (LPWNDCTRL)ptrCtrlArray.GetAt(i);
		if (pWndCtrl->dwWndId == dwWndId)
			return pWndCtrl;
	}
	return NULL;
}

CResManager::CResManager()
{
}

CResManager::~CResManager()
{
	Free();
}

void CResManager::Free()
{
	unsigned long dwAppletId;
	LPWNDAPPLET LPWNDAPPLET;
	POSITION pos = m_mapWndApplet.GetStartPosition();
	while (pos)
	{
		m_mapWndApplet.GetNextAssoc(pos, (void*&)dwAppletId, (void*&)LPWNDAPPLET);
		for (int i = 0; i < LPWNDAPPLET->ptrCtrlArray.GetSize(); i++)
			safe_delete((LPWNDCTRL)LPWNDAPPLET->ptrCtrlArray.GetAt(i));
		SAFE_DELETE(LPWNDAPPLET);
	}
}

LPWNDAPPLET CResManager::GetAt(unsigned long dwAppletId)
{
	LPWNDAPPLET LPWNDAPPLET = NULL;
	m_mapWndApplet.Lookup((void*)dwAppletId, (void*&)LPWNDAPPLET);
	return LPWNDAPPLET;
}

LPWNDCTRL CResManager::GetAtControl(unsigned long dwAppletId, unsigned long dwCtrlId)
{
	LPWNDAPPLET lpWndApplet = GetAt(dwAppletId);
	if (lpWndApplet)
		return lpWndApplet->GetAt(dwCtrlId);
	return NULL;
}

CString CResManager::GetLangApplet(CScript& scanner, LPWNDAPPLET lpWndApplet, bool bToolTip)
{
	CString string;
	scanner.GetToken();	// {
	scanner.GetToken();
	string = scanner.token;
	scanner.GetToken();	// }

	return string;
}
CString CResManager::GetLangCtrl(CScript& scanner, LPWNDCTRL lpWndCtrl, bool bToolTip)
{

	CString string;
	scanner.GetToken();	// {
	scanner.GetToken();
	string = scanner.token;
	scanner.GetToken();	// }

	return string;
}

bool CResManager::Load(const char* lpszName)
{
	CScript scanner;
	if (scanner.Load(lpszName, false) == false)
		return false;

	unsigned long dwWndType;
	scanner.GetToken_NoDef();

	while (scanner.tok != FINISHED)
	{
		LPWNDAPPLET pWndApplet = new WNDAPPLET;
		pWndApplet->pWndBase = NULL;
		pWndApplet->strDefine = scanner.token;
		pWndApplet->dwWndId = CScript::GetDefineNum(scanner.token);

		scanner.GetToken();
		pWndApplet->strTexture = scanner.token;
		pWndApplet->bTile = scanner.GetNumber();
		CSize size;
		pWndApplet->size.cx = scanner.GetNumber();
		pWndApplet->size.cy = scanner.GetNumber();
		pWndApplet->dwWndStyle = scanner.GetNumber();
		pWndApplet->d3dFormat = (D3DFORMAT)scanner.GetNumber();

		// 타이틀 
		pWndApplet->strTitle = GetLangApplet(scanner, pWndApplet, false);
		// 핼프 키 
		pWndApplet->strToolTip = GetLangApplet(scanner, pWndApplet, true);


		// HelpKey
		m_mapWndApplet.SetAt((void*)pWndApplet->dwWndId, pWndApplet);
		scanner.GetToken(); // skip {
		dwWndType = scanner.GetNumber();
		while (*scanner.token != '}')
		{
			LPWNDCTRL pWndCtrl = new WNDCTRL;
			pWndCtrl->dwWndType = dwWndType;
			scanner.GetToken_NoDef();
			pWndCtrl->strDefine = scanner.token;///Char;
			for (int z = 0; z < pWndApplet->ptrCtrlArray.GetSize(); z++)
			{
				if (((LPWNDCTRL)pWndApplet->ptrCtrlArray.GetAt(z))->strDefine == pWndCtrl->strDefine)
				{
					CString string;
					string.Format("%s에서 ID 충돌 %s ", pWndApplet->strDefine, pWndCtrl->strDefine);
					AfxMessageBox(string);
				}
			}
			pWndCtrl->dwWndId = CScript::GetDefineNum(scanner.token);
			scanner.GetToken();
			pWndCtrl->strTexture = scanner.token;//token;
			pWndCtrl->bTile = scanner.GetNumber();
			pWndCtrl->rect.left = scanner.GetNumber();
			pWndCtrl->rect.top = scanner.GetNumber();
			pWndCtrl->rect.right = scanner.GetNumber();
			pWndCtrl->rect.bottom = scanner.GetNumber();
			pWndCtrl->dwWndStyle = scanner.GetNumber();

			// visible, Group, Disabled, TabStop
			pWndCtrl->m_bVisible = scanner.GetNumber();
			pWndCtrl->m_bGroup = scanner.GetNumber();
			pWndCtrl->m_bDisabled = scanner.GetNumber();
			pWndCtrl->m_bTabStop = scanner.GetNumber();

			// 타이틀 
			pWndCtrl->strTitle = GetLangCtrl(scanner, pWndCtrl, false);
			// 핼프 키 
			pWndCtrl->strToolTip = GetLangCtrl(scanner, pWndCtrl, true);

			pWndApplet->ptrCtrlArray.Add(pWndCtrl);
			dwWndType = scanner.GetNumber();
		}
		scanner.GetToken_NoDef();
	}

	return true;
}
