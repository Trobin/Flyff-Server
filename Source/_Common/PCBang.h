// PCBang.h: interface for the CPCBang class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PCBANG_H__DD46F01A_2342_4D10_9A65_C6F56352353C__INCLUDED_)
#define AFX_PCBANG_H__DD46F01A_2342_4D10_9A65_C6F56352353C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if __VER >= 14 // __PCBANG
#ifdef __WORLDSERVER
#include "User.h"
#endif // __WORLDSERVER

class CPCBangInfo
{
#ifdef __WORLDSERVER
public:
	CPCBangInfo(unsigned long dwPCBangClass, unsigned long dwConnectTime);
	~CPCBangInfo();

	void	SetDisconnect(unsigned long dwTime) { m_dwDisconnectTime = dwTime; }
	bool	ProcessRemove(unsigned long dwPlayerId);
	void	UpdateInfo();
private:
	unsigned long	m_dwUnitTime;
	unsigned long	m_dwDisconnectTime;
#endif // __WORLDSERVER
public:
	unsigned long	GetPCBangClass() { return m_dwPCBangClass; }
	float	GetExpFactor() { return m_fExpFactor; }
	float	GetPieceItemDropFactor() { return m_fPieceItemDropFactor; }
	unsigned long	GetUnitTime() { return (g_tmCurrent - m_dwConnectTime) / MIN(60); }
	void	Serialize(CAr& ar);
#ifdef __CLIENT
	CPCBangInfo();
	~CPCBangInfo();
	static CPCBangInfo* GetInstance();
	unsigned long	GetConnectTime() { return (g_tmCurrent - m_dwConnectTime) / SEC(1); }
#endif // __CLIENT

private:
	unsigned long	m_dwPCBangClass;
	unsigned long	m_dwConnectTime;
	float	m_fExpFactor;
	float	m_fPieceItemDropFactor;
};


#ifdef __WORLDSERVER
typedef map<unsigned long, CPCBangInfo> MAPPBI;

class CPCBang
{
public:
	CPCBang();
	~CPCBang();
	static CPCBang* GetInstance();

	bool	LoadScript();

	void	SetPCBangPlayer(CUser* pUser, unsigned long dwPCBangClass);	// PC방 유저를 목록에 추가한다.
	void	DestroyPCBangPlayer(unsigned long dwPlayerId);	// 접속해제 시간을 저장한다.
	unsigned long	GetPCBangClass(unsigned long dwPlayerId);
	void	ProcessPCBang();							// 접속 해제 후 10분이 경과된 유저를 초기화 한다.
	float	GetExpInfo(unsigned long dwHour);
	float	GetExpFactor(CUser* pUser);				// 증가될 경험치를 얻어온다.
	float	GetPartyExpFactor(CUser* apUser[], int nMemberSize);
	float	GetPieceItemDropInfo(unsigned long dwHour);
	float	GetPieceItemDropFactor(CUser* pUser);		// 증가될 아이템 드롭률을 가져온다.
	void	SetApply(bool bApply);
	bool	IsApply() { return m_bApply; }

private:
	CPCBangInfo* GetPCBangInfo(unsigned long dwPlayerId);			// PC방 유저인가?
	int		GetPlayTime(unsigned long dwConnectTime) { return (g_tmCurrent - dwConnectTime) / MIN(60); }	// 접속이후 총 플레이 시간

	MAPPBI m_mapPCBang;	// PC방 사용자 정보
	vector<float>	m_vecfExp;			// 시간대별 경험치
	vector<float>	m_vecfDropRate;		// 시간대별 아이템 드롭률
	bool	m_bApply;
};
#endif // __WORLDSERVER

#endif // __PCBANG

#endif // !defined(AFX_PCBANG_H__DD46F01A_2342_4D10_9A65_C6F56352353C__INCLUDED_)