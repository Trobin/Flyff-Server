//#include "stdafx.h"
#include "CPoint.h"

CPoint::CPoint() : x(0), y(0)
{

}

CPoint::CPoint(int ox, int oy) : x(ox), y(oy)
{

}

CPoint CPoint::operator+(CPoint other)
{
	return CPoint(this->x + other.x, this->y + other.y);
}

void CPoint::operator+=(CPoint other)
{
	this->x += other.x;
	this->y += other.y;
}