#include "StdAfx.h"
#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT
#include "UserMarkPositionInfo.h"
//-----------------------------------------------------------------------------
CUserMarkPositionInfo::CUserMarkPositionInfo(void) :
	m_strName(_T("")),
	m_fPositionX(0.0F),
	m_fPositionY(0.0F),
	m_dwID(0)
{
}
//-----------------------------------------------------------------------------
CUserMarkPositionInfo::~CUserMarkPositionInfo(void)
{
}
//-----------------------------------------------------------------------------
void CUserMarkPositionInfo::SetID(unsigned long dwID)
{
	m_dwID = dwID;
}
//-----------------------------------------------------------------------------
unsigned long CUserMarkPositionInfo::GetID(void) const
{
	return m_dwID;
}
//-----------------------------------------------------------------------------
void CUserMarkPositionInfo::SetName(const CString& strName)
{
	m_strName = strName;
}
//-----------------------------------------------------------------------------
const CString& CUserMarkPositionInfo::GetName(void) const
{
	return m_strName;
}
//-----------------------------------------------------------------------------
void CUserMarkPositionInfo::SetPositionX(float fPositionX)
{
	m_fPositionX = fPositionX;
}
//-----------------------------------------------------------------------------
float CUserMarkPositionInfo::GetPositionX(void) const
{
	return m_fPositionX;
}
//-----------------------------------------------------------------------------
void CUserMarkPositionInfo::SetPositionY(float fPositionY)
{
	m_fPositionY = fPositionY;
}
//-----------------------------------------------------------------------------
float CUserMarkPositionInfo::GetPositionY(void) const
{
	return m_fPositionY;
}
//-----------------------------------------------------------------------------
#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM