#ifndef __TICKET_H__
#define	__TICKET_H__

typedef	struct	_TicketProp
{
	unsigned long	dwWorldId;
	D3DXVECTOR3	vPos;
}	TicketProp, * PTicketProp;

#ifdef __AZRIA_1023
typedef struct	_LayerStruct
{
	unsigned long dwWorldId;
	int		nExpand;
}	LayerStruct;
typedef vector<LayerStruct>	VLS;
class CLayerProperty
{
public:
	CLayerProperty();
	virtual ~CLayerProperty();
	bool	LoadScript();
	int	GetExpanedLayer(unsigned long dwWorldId);
private:
	VLS	m_vLayers;
};
#endif	// __AZRIA_1023

class CTicketProperty
{
public:
	CTicketProperty();
	virtual	~CTicketProperty();
	static	CTicketProperty* GetInstance();
	TicketProp* GetTicketProp(unsigned long dwItemId);
	bool	IsTarget(unsigned long dwWorldId);
	bool	LoadScript();
#ifdef __AZRIA_1023
	int		GetExpanedLayer(unsigned long dwWorldId) { return m_lp.GetExpanedLayer(dwWorldId); }
#endif	// __AZRIA_1023
private:
	map<unsigned long, TicketProp>	m_mapTicketProp;
#ifdef __AZRIA_1023
	CLayerProperty	m_lp;
#endif // __AZRIA_1023
};
#endif	// __TICKET_H__