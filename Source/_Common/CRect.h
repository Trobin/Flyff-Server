#pragma once

#include "CSize.h"

class CRect
{
public:
	int left;
	int top;
	int right;
	int bottom;

	CRect();
	CRect(int l, int t, int r, int b);

	int width();
	int height();
	CSize size();
};

