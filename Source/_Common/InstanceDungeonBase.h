// InstanceDungeonBase.h: interface for the CInstanceDungeonBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INSTANCEDUNGEONBASE_H__5F92E1D4_2A9F_4602_8D26_35E328B9C160__INCLUDED_)
#define AFX_INSTANCEDUNGEONBASE_H__5F92E1D4_2A9F_4602_8D26_35E328B9C160__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if __VER >= 14 // __INSTANCE_DUNGEON
#include "ar.h"
#ifdef __WORLDSERVER
#include "defineText.h"
#endif // __WORLDSERVER

enum { IDTYPE_SOLO = 0, IDTYPE_PARTY, IDTYPE_GUILD, IDTYPE_MAX };
enum { ID_NORMAL = 0, ID_MIDBOSS, ID_BOSS };
const unsigned short CLASS_NORMAL = 0x0001, CLASS_MASTER = 0x0002, CLASS_HERO = 0x0004;
const int COOLTIMECOUNT = 10;

typedef struct INSTACNEDUNGEON_INFO
{
	unsigned long	dwWorldId;
	unsigned long	uMultiKey;
#ifdef __WORLDSERVER
	int		nState;
	int		nPlayerCount;
	int		nKillCount;
#endif // __WORLDSERVER
	INSTACNEDUNGEON_INFO(unsigned long dwWI, unsigned long uMK) : dwWorldId(dwWI), uMultiKey(uMK)
#ifdef __WORLDSERVER
		, nState(ID_NORMAL), nPlayerCount(0), nKillCount(0)
#endif // __WORLDSERVER
	{}
	void	Serialize(CAr& ar)
	{
		if (ar.IsStoring())
			ar << dwWorldId << uMultiKey;
		else
			ar >> dwWorldId >> uMultiKey;
	}
} ID_INFO;

typedef vector<ID_INFO> VEC_IDINFO;
typedef map<unsigned long, VEC_IDINFO> MAP_IDBASE;

struct COOLTIME_INFO
{
	unsigned long dwWorldId;
	unsigned long dwDungeonId;
	unsigned long dwCoolTime;
	COOLTIME_INFO() : dwWorldId(NULL_ID), dwDungeonId(NULL_ID), dwCoolTime(0) {}
	COOLTIME_INFO(unsigned long dwWI, unsigned long dwDI, unsigned long dwCT) : dwWorldId(dwWI), dwDungeonId(dwDI), dwCoolTime(dwCT) {}
	void	Serialize(CAr& ar)
	{
		if (ar.IsStoring())
			ar << dwWorldId << dwDungeonId << (dwCoolTime - GetTickCount());
		else
		{
			ar >> dwWorldId >> dwDungeonId >> dwCoolTime;
			dwCoolTime += GetTickCount();
		}
	}
};
typedef vector<COOLTIME_INFO> VEC_CTINFO;
typedef map<unsigned long, VEC_CTINFO> MAP_CTINFO;

//////////////////////////////////////////////////////////////////////
// CInstanceDungeonBase
//////////////////////////////////////////////////////////////////////
class CInstanceDungeonBase
{
public:
	CInstanceDungeonBase(int nIDType);
	virtual ~CInstanceDungeonBase();

	int		GetType() { return m_nIDType; }
	void	SerializeAllInfo(CAr& ar);

	bool	CreateDungeon(ID_INFO ID_Info, unsigned long dwDungeonId);
	bool	DestroyDungeon(ID_INFO ID_Info, unsigned long dwDungeonId);
	void	DestroyAllDungeonByDungeonID(unsigned long dwDungeonId);
	void	DestroyAllDungeonByMultiKey(unsigned long uMultiKey);		//현재까지는 CoreServer만 사용.

	void	SetDungeonCoolTimeInfo(COOLTIME_INFO CT_Info, unsigned long dwPlayerId);
	void	ResetDungeonCoolTimeInfo(unsigned long dwWorldId, unsigned long dwDungeonId);

private:
	bool	CreateDungeonLayer(ID_INFO& ID_Info, unsigned long dwLayer);
	bool	DestroyDungeonLayer(ID_INFO& ID_Info, unsigned long dwLayer);

	VEC_IDINFO* GetDungeonVector(unsigned long dwDungeonId);
	ID_INFO* GetDungeonInfo(unsigned long dwDungeonId, unsigned long dwWorldId);

	VEC_CTINFO* GetCoolTimeVector(unsigned long dwPlayerId);
	COOLTIME_INFO* GetCoolTimeInfo(unsigned long dwPlayerId, unsigned long dwWorldId);

	int				m_nIDType;
	MAP_IDBASE		m_mapID;
	MAP_CTINFO		m_mapCTInfo;

#ifdef __CORESERVER
public:
	void	DungeonCoolTimeCountProcess();
	void	UpdateDungeonCoolTimeInfo();

private:
	int		m_nCoolTimeCount;
#endif // __CORESERVER

#ifdef __WORLDSERVER
private:
	struct DUNGEON_DATA
	{
		struct MONSTER
		{
			int nState;
			unsigned long dwMonsterId;
			bool bRed;
			D3DXVECTOR3 vPos;
		};

		unsigned long	dwClass;
		int		nMinLevel;
		int		nMaxLevel;
		unsigned long	dwCoolTime;
		map<int, D3DXVECTOR3> mapTeleportPos;
		vector<MONSTER> vecMonster;
		map<int, int>	mapObjCount;
	};
	typedef map<unsigned long, DUNGEON_DATA> MAP_IDDATA;
	MAP_IDDATA m_mapDungeonData;

	int			GetState(unsigned long dwWorldId, unsigned long dwDungeonId);
	void		SetNextState(ID_INFO* pInfo, unsigned long dwDungeonId);
	D3DXVECTOR3 GetTeleportPos(unsigned long dwWorldId, unsigned long dwDungeonId);
	bool		CheckClassLevel(CUser* pUser, unsigned long dwWorldId);
	void		CreateMonster(unsigned long dwDungeonId, unsigned long dwWorldId);
	bool		IsNowStateMonster(unsigned long dwWorldId, unsigned long dwDungeonId, unsigned long dwMonsterId);
	int			GetObjCount(unsigned long dwWorldId, unsigned long dwDungeonId);
	unsigned long		GetCoolTime(unsigned long dwWorldId);
	bool		IncreasePlayerCount(unsigned long dwDungeonId, unsigned long dwWorldId);
	bool		DecreasePlayerCount(CUser* pUser, unsigned long dwDungeonId, unsigned long dwWorldId);

	BYTE		m_nMaxInstanceDungeon;

protected:
	bool	TeleportToDungeon(CUser* pUser, unsigned long dwWorldId, unsigned long dwDungeonId);
	bool	LeaveToOutside(CUser* pUser, unsigned long dwWorldId, unsigned long dwDungeonId);
	void	SetLeaveMarkingPos(CUser* pUser, unsigned long dwWorldId, D3DXVECTOR3 vPos);

public:
	void	LoadScript(const char* szFileName);
	virtual bool EnteranceDungeon(CUser* pUser, unsigned long dwWorldId) = false;
	virtual bool LeaveDungeon(CUser* pUser, unsigned long dwWorldId) = false;

	void	SetInstanceDungeonKill(unsigned long dwWorldId, unsigned long dwDungeonId, unsigned long dwMonsterId);
	void	DeleteDungeonCoolTimeInfo(unsigned long dwPlayerId);
#endif // __WORLDSERVER
};


//////////////////////////////////////////////////////////////////////
// CInstanceDungeonHelper
//////////////////////////////////////////////////////////////////////
class CInstanceDungeonHelper
{
public:
	CInstanceDungeonHelper();
	~CInstanceDungeonHelper();
	static CInstanceDungeonHelper* GetInstance();

	void OnCreateDungeon(int nType, ID_INFO ID_Info, unsigned long dwDungeonId);
	void OnDestroyDungeon(int nType, ID_INFO ID_Info, unsigned long dwDungeonId);
	void OnSetDungeonCoolTimeInfo(unsigned long uKey, int nType, COOLTIME_INFO CT_Info, unsigned long dwPlayerId);
	void OnResetDungeonCoolTimeInfo(int nType, unsigned long dwWorldId, unsigned long dwDungeonId);

	void SendInstanceDungeonCreate(int nType, unsigned long dwDungeonId, ID_INFO& ID_Info);
	void SendInstanceDungeonDestroy(int nType, unsigned long dwDungeonId, ID_INFO& ID_Info);
	void SendInstanceDungeonSetCoolTimeInfo(unsigned long uKey, int nType, unsigned long dwPlayerId, COOLTIME_INFO& CT_Info);

	void DestroyAllDungeonByMultiKey(unsigned long uMultiKey);

private:
	CInstanceDungeonBase* GetDungeonPtr(int nType);

	typedef map<unsigned long, int> MAP_WORLD2TYPE;
	MAP_WORLD2TYPE	m_mapDungeonType;

#ifdef __CORESERVER
public:
	void SendInstanceDungeonAllInfo(unsigned long dpId);
	void SendInstanceDungeonDeleteCoolTimeInfo(int nType, unsigned long dwPlayerId);
#endif // __CORESERVER
#ifdef __WORLDSERVER
public:
	void	OnInstanceDungeonAllInfo(CAr& ar);
	bool	IsDungeonType(unsigned long dwWorldId, int nType) { return GetType(dwWorldId) == nType; }
	bool	IsInstanceDungeon(unsigned long dwWorldId) { return GetType(dwWorldId) != NULL_ID; }
	void	SetDungeonType(unsigned long dwWorldId, int nType);
	bool	EnteranceDungeon(CUser* pUser, unsigned long dwWorldId);
	bool	LeaveDungeon(CUser* pUser, unsigned long dwWorldId);
	void	GoOut(CUser* pUser);
	void	SetInstanceDungeonKill(unsigned long dwWorldId, unsigned long dwDungeonId, unsigned long dwMonsterId);
	void	OnDeleteDungeonCoolTimeInfo(int nType, unsigned long dwPlayerId);

	BYTE	m_nIDLayerNum;

private:
	void	SetCertifyKey(unsigned long uMultiKey = NULL_ID);
	bool	IsCertifying() { return m_uCertifyKey != NULL_ID; }
	int		GetType(unsigned long dwWorldId);

	unsigned long	m_uCertifyKey;
#endif // __WORLDSERVER
};

#endif // __INSTANCE_DUNGEON

#endif // !defined(AFX_INSTANCEDUNGEONBASE_H__5F92E1D4_2A9F_4602_8D26_35E328B9C160__INCLUDED_)
