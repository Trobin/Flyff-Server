#ifndef __BUFF_H
#define	__BUFF_H

#ifdef __BUFF_1107

#include "ar.h"

#include <map>
using	namespace	std;

#define	RBF_COMMON	(unsigned long)0x00000001
#define	RBF_IK3	(unsigned long)0x00000002
#define	RBF_GOODSKILL	(unsigned long)0x00000004
#define	RBF_CHRSTATE	(unsigned long)0x00000008
#define	RBF_DSTPARAM	(unsigned long)0x00000010
#define	RBF_DEBUF		(unsigned long)0x00000020
#define	RBF_ATTACKER	(unsigned long)0x00000040
#define	RBF_UNCONDITIONAL	(unsigned long)0x01000000
#define	RBF_ONCE	(unsigned long)0x10000000

typedef unsigned long OBJID;

class CBuffMgr;
class IBuff
{
public:
	IBuff();
	virtual	~IBuff() {}
	////////////////////////////////////////
	unsigned short	GetType() { return m_wType; }
	unsigned short	GetId() { return m_wId; }
	unsigned long	GetLevel() { return m_dwLevel; }
	unsigned long	GetTotal() { return m_tmTotal; }
	void	SetType(unsigned short wType) { m_wType = wType; }
	void	SetId(unsigned short wId) { m_wId = wId; }
	void	SetTotal(unsigned long tmTotal);
	void	SetLevel(unsigned long dwLevel) { m_dwLevel = dwLevel; }
	void	SetAttacker(OBJID oiAttacker) { m_oiAttacker = oiAttacker; }
	unsigned long	GetInst() { return m_tmInst; }
	void	SetInst(unsigned long tmInst) { m_tmInst = tmInst; }
	virtual	void	Serialize(CAr& ar, CMover* pMover);
	virtual	void	SerializeLevel(CAr& ar);
	virtual	IBuff& operator=(const IBuff& buff);
	unsigned long	GetRemain();
	void	SetRemove() { m_bRemove = true; }
	bool	GetRemove() { return m_bRemove; }
	////////////////////////////////////////
#ifndef __DBSERVER
public:
	virtual bool	Process(CMover* pMover, unsigned long tmCurrent);
	virtual	bool	IsIk1(unsigned long dwIk1) { return false; }
	virtual	bool	IsIk3(unsigned long dwIk3) { return false; }
	virtual	bool	IsAddable(CMover* pMover);
	virtual	void	AddTotal(unsigned long tmTotal);
	virtual	ItemProp* GetProp() { return NULL; }
	virtual	void	Release(CBuffMgr* pBuffMgr) {}
	virtual	bool	IsRemovable(CMover* pMover) { return true; }
	virtual	unsigned long	GetDisguise() { return NULL_ID; }
	OBJID	GetAttacker() { return m_oiAttacker; }
	virtual	bool	IsCommon() { return true; }
	virtual	bool	IsGood(CMover* pMover) { return false; }
	virtual	bool	HasDstParam(CMover*, unsigned long dwDstParam) { return false; }
	virtual	bool	HasChrState(CMover*, unsigned long dwChrState) { return false; }
	virtual	bool	IsDebuf(CMover* pMover) { return false; }
	virtual	bool	IsAttacker(CMover*, OBJID oiAttacker) { return false; }
protected:
	virtual	bool	IsInvalid(CMover*) { return false; }
	virtual	void	Apply(CMover* pMover) {}
	virtual	bool	Timeover(CMover* pMover, unsigned long tmCurrent);
private:
	bool	Expire(CMover* pMover, unsigned long tmCurrent);
#endif	// __DBSERVER
	////////////////////////////////////////
#ifdef __WORLDSERVER
public:
	virtual	void	OnExpire(CMover* pMover) {}
#endif	// __WORLDSERVER
	////////////////////////////////////////
#ifdef __CLIENT
public:
	bool	HasSFX() { return m_bSFX; }
	void	SetSFX() { m_bSFX = true; }
	virtual	bool	IsCommercial() { return false; }
private:
	void	CreateSFX(CMover* pMover);
#endif	// __CLIENT
	////////////////////////////////////////
private:
	unsigned short	m_wType;
	unsigned short	m_wId;
	unsigned long	m_dwLevel;
	unsigned long	m_tmTotal;
	unsigned long	m_tmInst;
#ifdef __CLIENT
	bool	m_bSFX;
#endif	// __CLIENT
	OBJID	m_oiAttacker;
	bool	m_bRemove;
};

#ifndef __DBSERVER
class IBuffItemBase : public IBuff
{
public:
	IBuffItemBase() {}
	virtual	~IBuffItemBase() {}
	virtual	ItemProp* GetProp();
	virtual void Release(CBuffMgr* pBuffMgr);
	virtual	bool	IsCommon();
	virtual	bool	HasChrState(CMover*, unsigned long dwChrState);
	virtual	bool	IsDebuf(CMover* pMover);
	////////////////////////////////////////
#ifdef __CLIENT
	bool	IsCommercial();
#endif	// __CLIENT
};

class CBuffItem : public IBuffItemBase
{
public:
	CBuffItem() {}
	virtual	~CBuffItem() {}
	virtual	void	AddTotal(unsigned long tmTotal);
	virtual	bool	IsRemovable(CMover* pMover);
	virtual	unsigned long	GetDisguise();
	virtual	bool	IsIk1(unsigned long dwIk1);
	virtual	bool	IsIk3(unsigned long dwIk3);
protected:
	virtual	void	Apply(CMover* pMover);
	////////////////////////////////////////
#ifdef __WORLDSERVER
public:
	virtual	void	OnExpire(CMover* pMover);
#endif	// __WORLDSERVER
};

class CBuffSkill : public IBuff
{
public:
	CBuffSkill() {}
	virtual	~CBuffSkill() {}
	virtual	ItemProp* GetProp();
	virtual	bool	IsAddable(CMover* pMover);
	virtual void Release(CBuffMgr* pBuffMgr);
	virtual	bool	IsRemovable(CMover* pMover);
	virtual	bool	IsGood(CMover* pMover);
	virtual	bool	HasDstParam(CMover* pMover, unsigned long dwDstParam);
	virtual	bool	HasChrState(CMover* pMover, unsigned long dwChrState);
	virtual	bool	IsDebuf(CMover* pMover);
	virtual	bool	IsAttacker(CMover* pMover, OBJID oiAttacker);
protected:
	virtual	bool	IsInvalid(CMover* pMover);
	virtual	void	Apply(CMover* pMover);
	////////////////////////////////////////
#ifdef __WORLDSERVER
public:
	virtual	void	OnExpire(CMover* pMover);
#endif	// __WORLDSERVER
};

class CBuffPet : public IBuff
{
public:
	CBuffPet() {}
	virtual	~CBuffPet() {}
	virtual	bool	IsRemovable(CMover* pMover) { return false; }
	virtual	bool	IsCommon() { return false; }
protected:
	virtual	void	Apply(CMover* pMover);
	virtual void Release(CBuffMgr* pBuffMgr);
};

class CBuffItem2 : public IBuffItemBase
{
public:
	CBuffItem2() {}
	virtual	~CBuffItem2() {}
	virtual	void	SerializeLevel(CAr& ar);
	virtual	bool	Timeover(CMover* pMover, unsigned long tmCurrent);
};

class CBuffEquip : public IBuff
{
public:
	CBuffEquip() {}
	virtual	~CBuffEquip() {}
	virtual	bool	IsRemovable(CMover* pMover) { return false; }
	virtual	bool	IsCommon() { return false; }
};
#endif	// __DBSERVER

typedef	map<unsigned long, IBuff*>	MAPBUFF;

#ifdef __CLIENT
class	CWndWorld;
#endif	// __CLIENT
class CMover;
class CBuffMgr
{
public:
	CBuffMgr(CMover* pMover);
	virtual	~CBuffMgr();
	void	Clear();
	CMover* GetMover() { return const_cast<CMover*>(m_pMover); }
	void	Serialize(CAr& ar);
	CBuffMgr& operator=(CBuffMgr& bm);
	virtual	IBuff* CreateBuff(unsigned short wType);
protected:
	virtual	unsigned long	GetCurrentTime() { return timeGetTime(); }
#ifdef __DBSERVER
public:
	bool	Add(IBuff* pBuff) { return m_mapBuffs.insert(MAPBUFF::value_type(MAKELONG(pBuff->GetId(), pBuff->GetType()), pBuff)).second; }
#else	// __DBSERVER
private:
	bool	Add(IBuff* pBuff) { return m_mapBuffs.insert(MAPBUFF::value_type(MAKELONG(pBuff->GetId(), pBuff->GetType()), pBuff)).second; }
#endif	// __DBSERVER
	////////////////////////////////////////
#ifndef __DBSERVER
public:
	void	Process();
	bool	HasBuff(unsigned short wType, unsigned short wId);
	void	RemoveBuff(IBuff* pBuff, bool bFake);
	void	RemoveBuff(unsigned short wType, unsigned short wId, bool bFake = true);
	bool	HasBuffByIk3(unsigned long dwIk3);
	bool	HasPet() { return GetBuffPet() != NULL; }
	void	RemoveBuffPet();
	IBuff* GetBuffPet();

	IBuff* GetBuff(unsigned short wType, unsigned short wId);
	IBuff* GetBuffByIk3(unsigned long dwIk3);
	void	RemoveBuffs(unsigned long dwFlags, unsigned long dwParam = 0);
	void	ClearInst();
	unsigned long	GetDisguise();
	bool	AddBuff(unsigned short wType, unsigned short wId, unsigned long dwLevel, unsigned long tmTotal, OBJID oiAttacker = 0xFFFFFFFF);
protected:
	bool	AddBuff(IBuff* pBuff);
private:
	bool	Overwrite(IBuff* pBuff);
	void	PrepareBS(IBuff* pBuff);
	bool	IsBSFull();
	IBuff* GetFirstBS();
#endif	// __DBSERVER
	////////////////////////////////////////
#ifdef __WORLDSERVER
public:
	void	RemoveBuffPet(IBuff* pBuff);
#endif	// __WORLDSERVER
	////////////////////////////////////////
#ifdef __CLIENT
public:
	int		GetCommercialCount();
#endif	// __CLIENT
	////////////////////////////////////////
#ifdef __DBSERVER
public:
	void	ToString(char* szString);
#endif	// __DBSERVER
	////////////////////////////////////////
private:
	MAPBUFF	m_mapBuffs;
	const CMover* m_pMover;
#ifdef __CLIENT
	friend	class	CWndCommItemCtrl;
	friend	class	CWndBuffStatus;
	friend	class	CWndMgr;
	friend	class	CWndWorld;
	friend	class	CMover;
#endif	// __CLIENT
	// chipi_090217
	size_t	GetRemoveBuffSize();
};

#endif	// __BUFF_1107

#endif	// __BUFF_H
