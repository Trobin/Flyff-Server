#ifndef __PARTICLEMNG_H__
#define __PARTICLEMNG_H__

struct POINTVERTEX
{
	D3DXVECTOR3 v;
	D3DCOLOR    color;

	static const unsigned long FVF;
};

struct PARTICLE
{
	D3DXVECTOR3 m_vPos;       // Current position
	D3DXVECTOR3 m_vVel;       // Current velocity

	D3DXCOLOR   m_clrDiffuse; // Initial diffuse color
	D3DXCOLOR   m_clrFade;    // Faded diffuse color
	float       m_fFade;      // Fade progression
	float		m_fGroundY;

	PARTICLE* m_pNext;      // Next particle in list
};

class CParticles
{
	PARTICLE* m_pPool;
	int			m_nPoolPtr;
protected:
	//    float     m_fRadius;
	int		m_nType;
	unsigned long	m_dwBase;
	unsigned long	m_dwFlush;
	unsigned long	m_dwDiscard;
	//	float	m_fTime;

	unsigned long     m_dwParticles;
	unsigned long     m_dwParticlesLim;
	PARTICLE* m_pParticles;
	PARTICLE* m_pParticlesFree;

	// Geometry
	LPDIRECT3DVERTEXBUFFER9 m_pVB;

	void Init(void);
	void Destroy(void);

public:
	bool	m_bActive;
	bool	m_bGravity;
	float	m_fSize;
	LPDIRECT3DTEXTURE9 m_pParticleTexture;

	CParticles();
	CParticles(unsigned long dwFlush, unsigned long dwDiscard, int nType);
	~CParticles();

	void Create(unsigned long dwFlush, unsigned long dwDiscard, int nType);

	HRESULT InitDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice, const char* szFileName);
	//	HRESULT InitDeviceObjects( LPDIRECT3DDEVICE9 pd3dDevice );
	HRESULT RestoreDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT InvalidateDeviceObjects();

	HRESULT CreateParticle(int nType, const D3DXVECTOR3& vPos, const D3DXVECTOR3& vVel, float fGroundY);
	HRESULT Update(void);
	HRESULT Update2(void);

	HRESULT Render(LPDIRECT3DDEVICE9 pd3dDevice);
};

#define		MAX_PARTICLE_TYPE	32		// 최대 파티클 종류.

class CParticleMng
{
	LPDIRECT3DDEVICE9 m_pd3dDevice;
	int		m_nMaxType;
	CParticles m_Particles[MAX_PARTICLE_TYPE];

	void	Init(void);
	void	Destroy(void);
public:
	bool	m_bActive;

	CParticleMng();
	~CParticleMng();

	HRESULT InitDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice) { m_pd3dDevice = pd3dDevice; return S_OK; }
	HRESULT RestoreDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT InvalidateDeviceObjects();

	CParticles* CreateParticle(int nType, const D3DXVECTOR3& vPos, const D3DXVECTOR3& vVel, float fGroundY);

	void	Process(void);
	void	Render(LPDIRECT3DDEVICE9 pd3dDevice);

};

extern D3DXCOLOR g_clrColor;
extern unsigned long g_clrColorFade;

//extern CParticles		g_Particle;
//extern CParticles		g_Particle2;

extern CParticleMng	g_ParticleMng;

#endif // PARTICLE
