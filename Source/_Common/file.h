#ifndef __CFileIO_H
#define __CFileIO_H

#include <TCHAR.H>
#include <stdio.h>
#include <io.h>


class CFileIO
{
public:
	FILE* fp;

	CFileIO(const char* lpszFileName, TCHAR* mode) { Open(lpszFileName, mode); }
	CFileIO() { fp = NULL; }

	virtual ~CFileIO() { Close(); }
	virtual bool  Open(const char* lpszFileName, TCHAR* mode);
	virtual bool Close();
	virtual int	 Flush() { return fflush(fp); }
	virtual char  GetC() { return (char)getc(fp); }
	virtual unsigned short  GetW() { return getwc(fp); }
	virtual unsigned long GetDW() { unsigned long dw; fread(&dw, sizeof(dw), 1, fp); return dw; }
	virtual long Tell() { return ftell(fp); }
	virtual int  Seek(long offset, int whence) { return fseek(fp, offset, whence); }
	virtual long GetLength() { return _filelength(Handle()); }
	virtual size_t Read(void* ptr, size_t size, size_t n = 1) { return fread(ptr, size, n, fp); }
	virtual LPVOID Read();

	int  Handle() { return fileno(fp); }
	int  Error() { return ferror(fp); }
	int  Eof() { return feof(fp); }
	int  PutC(char c) { return putc(c, fp); }
	int  PutW(unsigned short w) { return putw(w, fp); }
	int  PutDW(unsigned long dw) { return fwrite(&dw, sizeof(dw), 1, fp); }
	size_t Write(LPVOID ptr, size_t size, size_t n = 1) { return fwrite(ptr, size, n, fp); }
	int PutString(const char* lpszString) { return _ftprintf(fp, lpszString); }
	int PutWideString(const char* lpszString);
};

#if defined( __CLIENT )
struct RESOURCE
{
	TCHAR szResourceFile[128];
	unsigned long dwOffset;
	unsigned long dwFileSize;
	unsigned char  byEncryptionKey;
	bool  bEncryption;
};
class CResFile : public CFileIO
{
public:
	CResFile(const char* lpszFileName, TCHAR* mode);
	CResFile();

private:
	CFile m_File;
	bool m_bResouceInFile;
	char m_szFileName[_MAX_FNAME];
	unsigned long m_nFileSize;
	unsigned long m_nFileBeginPosition; // 시작 위치
	unsigned long m_nFileCurrentPosition; // 현재 위치
	unsigned long m_nFileEndPosition; // 끝 위치
	bool m_bEncryption;
	unsigned char m_byEncryptionKey;
	static void AddResource(TCHAR* lpResName);

public:

#ifdef __SECURITY_0628
	static	char	m_szResVer[100];
	static	map<string, string>	m_mapAuth;
	static	void	LoadAuthFile(void);
#endif	// __SECURITY_0628

	static CMapStringToPtr m_mapResource;

	static unsigned char Encryption(unsigned char byEncryptionKey, unsigned char byData)
	{
		byData = (byData << 4) | (byData >> 4);
		return (~byData) ^ byEncryptionKey;
	}
	static unsigned char Decryption(unsigned char byEncryptionKey, unsigned char byData)
	{
		byData = ~byData ^ byEncryptionKey;
		return (byData << 4) | (byData >> 4);
	}

	bool IsEncryption() { return m_bEncryption; }
	virtual ~CResFile() { Close(); }
	virtual bool Open(const char* lpszFileName, TCHAR* mode);
	virtual bool Close(void);

	virtual LPVOID Read();
	virtual size_t Read(void* ptr, size_t size, size_t n = 1);
	virtual long GetLength();
	virtual int  Seek(long offset, int whence);
	virtual long Tell();

	virtual char  GetC();
	virtual unsigned short  GetW();
	virtual unsigned long GetDW();

	virtual int	 Flush();

	//bool FindFile(char *szSerchPath, const char* lpszFileName, TCHAR *mode, int f );
	//bool FindFileFromResource( char *filepath, const char* lpszFileName );

	static void ScanResource(const char* lpszRootPath);
	static void FreeResource();
};
#else
// 클라이언트가 아니면 
#define CResFile CFileIO
#endif


class CFileFinder
{
	POSITION m_pos;
	long m_lHandle;
	char m_szFilespec[MAX_PATH];
	bool m_bResFile;
public:
	CFileFinder();
	~CFileFinder();
	bool WildCmp(const char* lpszWild, const char* lpszString);

	bool FindFirst(const char* lpFilespec, struct _finddata_t* fileinfo);
	bool FindNext(struct _finddata_t* fileinfo);
	void FindClose();
};


#endif