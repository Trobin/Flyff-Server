#ifndef __CREATEMONSTER_H__
#define	__CREATEMONSTER_H__

#if __VER >= 12 // __NEW_ITEMCREATEMON_SERVER

struct CREATE_MONSTER_PROP
{
	unsigned long dwKeepTime;
	map<unsigned long, int> mapMonster;

	CREATE_MONSTER_PROP() : dwKeepTime(0) {}

	unsigned long GetRandomMonsterId()
	{
		int nRandom = xRandom(100);
		map<unsigned long, int>::iterator it = mapMonster.begin();
		for (; it != mapMonster.end(); it++)
		{
			if (nRandom < it->second)
				break;
			nRandom -= it->second;
		}

		return it != mapMonster.end() ? it->first : NULL_ID;
	}
};

struct CREATE_MONSTER_INFO
{
	unsigned long dwOwnerId;
	unsigned long dwEndTick;
	char  chState;

	CREATE_MONSTER_INFO() : dwOwnerId(NULL_ID), dwEndTick(0), chState('N') {}
};

typedef map<unsigned long, CREATE_MONSTER_PROP*> MAPPROP;
typedef map<unsigned long, CREATE_MONSTER_INFO> MAPINFO;

class CCreateMonster
{
public:
	//	Constructions
	CCreateMonster();
	~CCreateMonster();

	static CCreateMonster* GetInstance(void);
	void LoadScript();
	void SetState(OBJID objId, const char chState);
	void ProcessRemoveMonster();
	bool IsAttackAble(CUser* pUser, CMover* pTarget, bool bTextOut = false);
	void CreateMonster(CUser* pUser, unsigned long dwObjId, D3DXVECTOR3 vPos);
	void RemoveInfo(CMover* pMover);


private:
	CREATE_MONSTER_PROP* GetCreateMonsterProp(CItemElem* pItemElem);

	int m_nMaxCreateNum;
	MAPPROP m_mapCreateMonsterProp;
	MAPINFO m_mapCreateMonsterInfo;

};
#endif // __NEW_ITEMCREATEMON_SERVER



///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
#if __VER < 12 // __NEW_ITEMCREATEMON_SERVER
class CCreateMonster
{
	struct __MONSTERPERSENT {
		unsigned long dwMonsterId;
		short nPersent;
	};
public:
	//	Constructions
	CCreateMonster();
	~CCreateMonster();
	unsigned long GetCreateMonster(unsigned long dwItemId);
	map< unsigned long, vector< __MONSTERPERSENT > > pMonsterItem;
#ifdef __WORLDSERVER
	bool LoadScript(LPCSTR lpszFileName);
#endif // __WORLDSERVER
};

#endif // __NEW_ITEMCREATEMON_SERVER

#endif	//	__CREATEMONSTER_H__