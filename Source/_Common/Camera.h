#ifndef __CAMERA_H
#define __CAMERA_H

class CWorld;
class CObj;

#define CAMSTY_NONE    0
#define CAMSTY_HIT     1
#define CAMSTY_DAMAGE  2

extern D3DXMATRIX g_matView;
extern D3DXMATRIX g_matInvView;

class CCamera
{
	//protected:
public:
	D3DXVECTOR3    m_vPos; // 카메라가 있는 위치 
	//D3DXVECTOR3    m_vPos2       ; // 카메라가 있는 위치 2

	D3DXMATRIX     m_matView; // View 행렬 저장 
	D3DXMATRIX     m_matInvView; // View 역행렬 저장 

	D3DXVECTOR3    m_vLookAt; // 카메라가 보는 방향

	D3DXVECTOR3    m_vPosDest; // 카메라 이동 목표
	D3DXVECTOR3    m_vLookAtDest; // 카메라 보는 위치 이동 목표 

	D3DXVECTOR3    m_vOffset; // 캐릭터 위치에서 카메라 상대 위치
	D3DXVECTOR3    m_vOffsetDest; // 캐릭터 위치에서 카메라 상대 위치 이동 목표

	unsigned long          m_dwCamStyle;
	float m_fRotx, m_fRoty; // 마우스 조작에 의한 x,y축 회전값의 목표점
	float m_fCurRotx, m_fCurRoty; // 현재 마우스 조작에 의한 x,y축 회전값. 서서히 목표값에 가까와진다.

	int		m_nQuakeSec;
	float	m_fQuakeSize;		// 진동크기. 진동폭을 넣는다. 1.0f는 1미터폭으로 흔들린다.

	CCamera();

	void Reset();
	void SetPos(D3DXVECTOR3& vPos) { m_vPos = vPos; }
	D3DXVECTOR3 GetPos() { return m_vPos; }

	void	SetQuake(int nSec, float fSize = 0.06f);

	virtual	void Process(LPDIRECT3DDEVICE9 pd3dDevice, float fFactor = 15.0f);
	virtual	void Transform(LPDIRECT3DDEVICE9 pd3dDevice, CWorld* pWorld);
	virtual void ControlCamera(unsigned long dwStyle);
};

class CBackCamera : public CCamera
{
	bool m_bLock;

public:
	float m_fLength1;
	float m_fLength2;
	bool  m_bOld;
	bool  m_bStart;
	float m_fZoom;

#if __VER >= 13 // __HOUSING
#define CM_NORMAL	0
#define CM_MYROOM	1

	int m_nCamMode;
	bool SetCamMode(int nType);
#endif	// __HOUSING
	CBackCamera();

	bool IsLock() { return m_bLock; }
	void Lock();
	void Unlock();

	int  GetAnglePie(float fAngle);

	virtual	void Process(LPDIRECT3DDEVICE9 pd3dDevice, float fFactor = 15.0f);
	virtual void ControlCamera(unsigned long dwStyle);
};

//
class CFlyCamera : public CCamera
{
	bool m_bLock;
	float	m_fAngle;		// 카메라가 보는 좌/우 방향.
	float	m_fAngleY;		// 카메라가 보는 위/아래 방향

public:
	CObj* pObjTarget;
	float m_fZoom;

	CFlyCamera();

	bool IsLock() { return m_bLock; }
	void Lock();
	void Unlock();

	virtual	void Process(LPDIRECT3DDEVICE9 pd3dDevice, float fFactor = 15.0f);
	virtual void ControlCamera(unsigned long dwStyle);
};

class CToolCamera : public CCamera
{
public:
	//D3DXVECTOR3        m_vPosition;
	D3DXVECTOR3        m_vVelocity;
	float              m_fYaw;
	float              m_fYawVelocity;
	float              m_fPitch;
	float              m_fPitchVelocity;
	//D3DXMATRIXA16         m_matView;
	D3DXMATRIXA16         m_matOrientation;
	CToolCamera();
	virtual	void Process(LPDIRECT3DDEVICE9 pd3dDevice, float fFactor = 15.0f);
	virtual	void Transform(LPDIRECT3DDEVICE9 pd3dDevice, CWorld* pWorld);

};

#endif