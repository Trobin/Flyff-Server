#ifndef __2DRENDER_H
#define __2DRENDER_H

#ifdef __CLIENT
#include "mempooler.h"
#endif
#ifndef __WORLDSERVER
#include "EditString.h"
#endif	// __CLEINT


#include "xUtil.h"

struct IDeviceRes
{
	virtual void Invalidate() = 0;
	virtual bool SetInvalidate(LPDIRECT3DDEVICE9 pd3dDevice) = 0;
};

class CRectClip : public CRect
{
public:
	CRectClip() { }
	CRectClip(int l, int t, int r, int b) : CRect(l, t, r, b) { }
	CRectClip(const RECT& srcRect) : CRect(srcRect) { }
	CRectClip(LPCRECT lpSrcRect) : CRect(lpSrcRect) { }
	CRectClip(POINT point, SIZE size) : CRect(point, size) { }
	CRectClip(POINT topLeft, POINT bottomRight) : CRect(topLeft, bottomRight) { }

	bool Clipping(CRect& rect) const;
	//bool Clipping(CRect& os,Rect& is);

		//bool Clipping(CPtSz& os,CPtSz& is) const;

		//bool PtSzLapRect(CPtSz ptSz) const;
		//bool PtSzInRect (CPtSz ptSz) const;
	bool RectLapRect(CRect rect) const;
	bool RectInRect(CRect rect) const;
};

#define D2DTEXRENDERFLAG_90    1
#define D2DTEXRENDERFLAG_180   2
#define D2DTEXRENDERFLAG_HFLIP 3
#define D2DTEXRENDERFLAG_VFLIP 4

struct DRAWVERTEX
{
	D3DXVECTOR3 vec;//AT x, y, z;
	float rhw;
	unsigned long color;
	//	float u, v;
};
struct TEXTUREVERTEX
{
	D3DXVECTOR3 vec;//
	//float x, y, z, 
	float rhw, u, v;
};
struct TEXTUREVERTEX2
{
	D3DXVECTOR3 vec;//
	//float x, y, z, 
	float rhw;
	unsigned long color;
	float u, v;
};
#define D3DFVF_DRAWVERTEX (D3DFVF_XYZRHW|D3DFVF_DIFFUSE/*|D3DFVF_TEX1*/)	// FVF가 맞지않아 경고가 떠서 지웠음.-xuzhu-
#define D3DFVF_TEXTUREVERTEX (D3DFVF_XYZRHW|D3DFVF_TEX1)
#define D3DFVF_TEXTUREVERTEX2 (D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1)

inline void SetDrawVertex(DRAWVERTEX* pVertices, float x, float y, unsigned long dwColor)
{
	pVertices->vec = D3DXVECTOR3(x, y, 0);
	pVertices->rhw = 1.0f;
	pVertices->color = dwColor;
}
inline void SetTextureVertex(TEXTUREVERTEX* pVertices, float x, float y, float u, float v)
{
	//pVertices->x     = (float)x - 0.5f;
	//pVertices->y     = (float)y - 0.5f;
	//pVertices->z     = 0.0f;
	pVertices->vec = D3DXVECTOR3(x - 0.5f, y - 0.5f, 0);
	pVertices->rhw = 1.0f;
	pVertices->u = u;
	pVertices->v = v;
}
inline void SetTextureVertex2(TEXTUREVERTEX2* pVertices, float x, float y, float u, float v, unsigned long dwColor)
{
	//pVertices->x     = (float)x - 0.5f;
	//pVertices->y     = (float)y - 0.5f;
	//pVertices->z     = 0.0f;
	pVertices->vec = D3DXVECTOR3(x - 0.5f, y - 0.5f, 0);
	pVertices->rhw = 1.0f;
	pVertices->u = u;
	pVertices->v = v;
	pVertices->color = dwColor;
}

class CTexture;
class CTexturePack;


class C2DRender
{
	LPDIRECT3DVERTEXBUFFER9 m_pVBRect;
	LPDIRECT3DVERTEXBUFFER9 m_pVBFillRect;
	LPDIRECT3DVERTEXBUFFER9 m_pVBRoundRect;
	LPDIRECT3DVERTEXBUFFER9 m_pVBFillTriangle;
	LPDIRECT3DVERTEXBUFFER9 m_pVBTriangle;
	LPDIRECT3DVERTEXBUFFER9 m_pVBLine;
	LPDIRECT3DVERTEXBUFFER9 m_pVBPixel;
	//LPDIRECT3DVERTEXBUFFER9 m_pVBTexture;

public:
	LPDIRECT3DDEVICE9 m_pd3dDevice; // The D3D rendering device
	unsigned long      m_dwTextColor;
	CD3DFont* m_pFont;
	CPoint     m_ptOrigin; // 뷰포트 시작 지점 
	CRectClip  m_clipRect;

	C2DRender();
	~C2DRender();

	CD3DFont* GetFont() { return m_pFont; }
	void SetFont(CD3DFont* pFont) { m_pFont = pFont; }

	// 뷰포트 관련 
	CPoint GetViewportOrg() const { return m_ptOrigin; }
	void SetViewportOrg(POINT pt) { m_ptOrigin = pt; }
	void SetViewportOrg(int x, int y) { m_ptOrigin = CPoint(x, y); }
	void SetViewport(CRect rect);
	void SetViewport(int nLeft, int nTop, int nRight, int nBottom);

	bool ResizeRectVB(CRect* pRect, unsigned long dwColorLT, unsigned long dwColorRT, unsigned long dwColorLB, unsigned long dwColorRB, LPDIRECT3DVERTEXBUFFER9 pVB);
	bool RenderRect(CRect rect, unsigned long dwColorLT, unsigned long dwColorRT, unsigned long dwColorLB, unsigned long dwColorRB);
	bool RenderRect(CRect rect, unsigned long dwColor) { return RenderRect(rect, dwColor, dwColor, dwColor, dwColor); }
	bool RenderResizeRect(CRect rect, int nThick);

	bool ResizeFillRectVB(CRect* pRect, unsigned long dwColorLT, unsigned long dwColorRT, unsigned long dwColorLB, unsigned long dwColorRB, LPDIRECT3DVERTEXBUFFER9 pVB, LPDIRECT3DTEXTURE9 m_pTexture = NULL);
	bool RenderFillRect(CRect rect, unsigned long dwColorLT, unsigned long dwColorRT, unsigned long dwColorLB, unsigned long dwColorRB, LPDIRECT3DTEXTURE9 m_pTexture = NULL);
	bool RenderFillRect(CRect rect, unsigned long dwColor, LPDIRECT3DTEXTURE9 m_pTexture = NULL) { return RenderFillRect(rect, dwColor, dwColor, dwColor, dwColor, m_pTexture); }

	bool ResizeRoundRectVB(CRect* pRect, unsigned long dwColorLT, unsigned long dwColorRT, unsigned long dwColorLB, unsigned long dwColorRB, LPDIRECT3DVERTEXBUFFER9 pVB, LPDIRECT3DTEXTURE9 m_pTexture = NULL);
	bool RenderRoundRect(CRect rect, unsigned long dwColorLT, unsigned long dwColorRT, unsigned long dwColorLB, unsigned long dwColorRB, LPDIRECT3DTEXTURE9 m_pTexture = NULL);
	bool RenderRoundRect(CRect rect, unsigned long dwColor, LPDIRECT3DTEXTURE9 m_pTexture = NULL) { return RenderRoundRect(rect, dwColor, dwColor, dwColor, dwColor, m_pTexture); }

	bool RenderLine(CPoint pt1, CPoint pt2, unsigned long dwColor1, unsigned long dwColor2);
	bool RenderLine(CPoint pt1, CPoint pt2, unsigned long dwColor) { return RenderLine(pt1, pt2, dwColor, dwColor); }

	bool RenderTriangle(CPoint pt1, CPoint pt2, CPoint pt3, unsigned long dwColor1, unsigned long dwColor2, unsigned long dwColor3);
	bool RenderTriangle(CPoint pt1, CPoint pt2, CPoint pt3, unsigned long dwColor) { return RenderTriangle(pt1, pt2, pt3, dwColor, dwColor, dwColor); }

	bool RenderFillTriangle(CPoint pt1, CPoint pt2, CPoint pt3, unsigned long dwColor1, unsigned long dwColor2, unsigned long dwColor3);
	bool RenderFillTriangle(CPoint pt1, CPoint pt2, CPoint pt3, unsigned long dwColor) { return RenderFillTriangle(pt1, pt2, pt3, dwColor, dwColor, dwColor); }

	bool RenderTextureEx(CPoint pt, CPoint pt2, CTexture* pTexture, unsigned long dwBlendFactorAlhpa, float fScaleX, float fScaleY, bool bAnti = true);
	bool RenderTexture(CPoint pt, CTexture* pTexture, unsigned long dwBlendFactorAlhpa = 255, float fScaleX = 1.0, float fScaleY = 1.0);
	bool RenderTextureRotate(CPoint pt, CTexture* pTexture, unsigned long dwBlendFactorAlhpa, float fScaleX, float fScaleY, float fRadian);

	//added by gmpbigsun: 회전축 변경 
	bool RenderTextureRotate(CPoint pt, CTexture* pTexture, unsigned long dwBlendFactorAlhpa, float fRadian, bool bCenter, float fScaleX, float fScaleY);

	bool RenderTexture2(CPoint pt, CTexture* pTexture, float fScaleX = 1.0, float fScaleY = 1.0, D3DCOLOR coolor = 0xffffffff);
	bool RenderTextureEx2(CPoint pt, CPoint pt2, CTexture* pTexture, unsigned long dwBlendFactorAlhpa, float fScaleX, float fScaleY, D3DCOLOR color);
	bool RenderTextureColor(CPoint pt, CTexture* pTexture, float fScaleX, float fScaleY, D3DCOLOR color);

	void SetTextColor(unsigned long dwColor) { m_dwTextColor = dwColor; }
	unsigned long GetTextColor() { return m_dwTextColor; }
#ifdef __CLIENT
	void TextOut_EditString(int x, int y, CEditString& strEditString, int nPos = 0, int nLines = 0, int nLineSpace = 0);
#endif
	void TextOut(int x, int y, const char* pszString, unsigned long dwColor = 0xffffffff, unsigned long dwShadowColor = 0x00000000);
	void TextOut(int x, int y, int nValue, unsigned long dwColor = 0xffffffff, unsigned long dwShadowColor = 0x00000000);
	void TextOut(int x, int y, float fXScale, float fYScale, const char* pszString, unsigned long dwColor = 0xffffffff, unsigned long dwShadowColor = 0x00000000);

	HRESULT InitDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT RestoreDeviceObjects(D3DSURFACE_DESC* pd3dsdBackBuffer);
	HRESULT InvalidateDeviceObjects();
	HRESULT DeleteDeviceObjects();
};
class CTexture : public IDeviceRes
{
	bool m_bAutoFree;
public:
	SIZE  m_sizePitch;
	SIZE  m_size;
	float m_fuLT, m_fvLT;
	float m_fuRT, m_fvRT;
	float m_fuLB, m_fvLB;
	float m_fuRB, m_fvRB;
	POINT m_ptCenter;
	LPDIRECT3DTEXTURE9 m_pTexture;
#ifdef _DEBUG
	CString m_strFileName;
#endif

#ifdef __YDEBUG
	CString		m_strTexFileName;
	D3DCOLOR	m_d3dKeyColor;
	unsigned int		m_nLevels;
	bool		m_bMyLoader;
	unsigned long		m_dwUsage;

	D3DPOOL		m_Pool;
	D3DFORMAT	m_Format;
#endif //__YDEBUG

	CTexture();
	~CTexture();

	void Invalidate();
	bool SetInvalidate(LPDIRECT3DDEVICE9 pd3dDevice);

	bool DeleteDeviceObjects();
	void SetAutoFree(bool bAuto) { m_bAutoFree = bAuto; }


	LPDIRECT3DTEXTURE9 GetTexture() { return m_pTexture; }
	bool CreateTexture(LPDIRECT3DDEVICE9 pd3dDevice, int nWidth, int nHeight,
		unsigned int Levels, unsigned long Usage, D3DFORMAT Format, D3DPOOL Pool = D3DPOOL_DEFAULT);
	bool LoadTexture(LPDIRECT3DDEVICE9 pd3dDevice, const char* pFileName, D3DCOLOR d3dKeyColor, bool bMyLoader = false);
	//bool LoadTextureFromRes( LPDIRECT3DDEVICE9 pd3dDevice, const char* pFileName, D3DCOLOR d3dKeyColor, bool bMyLoader = false );
	void Render(C2DRender* p2DRender, CPoint pt, unsigned long dwBlendFactorAlhpa = 255) {
		p2DRender->RenderTexture(pt, this, dwBlendFactorAlhpa);
	}
	void Render(C2DRender* p2DRender, CPoint pt, CPoint pt2, unsigned long dwBlendFactorAlhpa = 255, float fscalX = 1.0, float fscalY = 1.0) {
		p2DRender->RenderTextureEx(pt, pt2, this, dwBlendFactorAlhpa, fscalX, fscalY);
	}
	void RenderEx2(C2DRender* p2DRender, CPoint pt, CPoint pt2, unsigned long dwBlendFactorAlhpa = 255, float fscalX = 1.0, float fscalY = 1.0, D3DCOLOR color = 0) {
		p2DRender->RenderTextureEx2(pt, pt2, this, dwBlendFactorAlhpa, fscalX, fscalY, color);
	}
	void RenderScal(C2DRender* p2DRender, CPoint pt, unsigned long dwBlendFactorAlhpa = 255, float fscalX = 1.0, float fscalY = 1.0) {
		p2DRender->RenderTexture(pt, this, dwBlendFactorAlhpa, fscalX, fscalY);
	}
	void RenderRotate(C2DRender* p2DRender, CPoint pt, float fRadian, unsigned long dwBlendFactorAlhpa = 255, float fscalX = 1.0, float fscalY = 1.0) {
		p2DRender->RenderTextureRotate(pt, this, dwBlendFactorAlhpa, fscalX, fscalY, fRadian);
	}

	//added by gmpbigsun : 회전축 변경가능 ( center or start point )
	void RenderRotate(C2DRender* p2DRender, CPoint pt, float fRadian, bool bCenter, unsigned long dwBlendFactorAlhpa = 255, float fscalX = 1.0, float fscalY = 1.0) {
		p2DRender->RenderTextureRotate(pt, this, dwBlendFactorAlhpa, fRadian, bCenter, fscalX, fscalY);
	}

	void Render2(C2DRender* p2DRender, CPoint pt, D3DCOLOR color, float fscalX = 1.0f, float fscalY = 1.0f) {
		p2DRender->RenderTexture2(pt, this, fscalX, fscalY, color);
	}
	void RenderScal2(C2DRender* p2DRender, CPoint pt, unsigned long dwBlendFactorAlhpa = 255, float fscalX = 1.0, float fscalY = 1.0, D3DCOLOR color = 0) {
		p2DRender->RenderTextureColor(pt, this, fscalX, fscalY, color);
	}

	//CSize ComputeSize( CSize size );
};
class CTexturePack
{
public:
	unsigned long m_dwNumber;
	CSize m_size;
	LPDIRECT3DTEXTURE9 m_pTexture;
	CTexture* m_ap2DTexture;

	CTexturePack();
	~CTexturePack();

	HRESULT		RestoreDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice);
	HRESULT		InvalidateDeviceObjects();

	bool DeleteDeviceObjects();
	unsigned long GetNumber() { return m_dwNumber; }
	void MakeVertex(C2DRender* p2DRender, CPoint point, int nIndex, TEXTUREVERTEX** ppVertices);
	void MakeVertex(C2DRender* p2DRender, CPoint point, int nIndex, TEXTUREVERTEX2** ppVertices, unsigned long dwColor);
	void Render(LPDIRECT3DDEVICE9 pd3dDevice, TEXTUREVERTEX* pVertices, int nVertexNum);
	void Render(LPDIRECT3DDEVICE9 pd3dDevice, TEXTUREVERTEX2* pVertices, int nVertexNum);

#if __VER >= 13 // __CSC_VER13_1
	virtual bool LoadScript(LPDIRECT3DDEVICE9 pd3dDevice, const char* pFileName);
#else //__CSC_VER13_1
	bool LoadScript(LPDIRECT3DDEVICE9 pd3dDevice, const char* pFileName);
#endif //__CSC_VER13_1

	CTexture* LoadTexture(LPDIRECT3DDEVICE9 pd3dDevice, const char* pFileName, D3DCOLOR d3dKeyColor);
	CTexture* GetAt(unsigned long dwIndex) {
		return &m_ap2DTexture[dwIndex];
	}
	void Render(C2DRender* p2DRender, CPoint pt, unsigned long dwIndex, unsigned long dwBlendFactorAlhpa = 255, float fScaleX = 1.0f, float fScaleY = 1.0f)
	{
		if (((int)dwIndex >= (int)m_dwNumber) || (int)dwIndex < 0)
		{
			const char* szErr = Error("CTexturePack::Render : 범위를 벗어남 %d", (int)dwIndex);
			//ADDERRORMSG( szErr );
			int* p = NULL;
			*p = 1;
		}
		p2DRender->RenderTexture(pt, &m_ap2DTexture[dwIndex], dwBlendFactorAlhpa, fScaleX, fScaleY);
	}
};
typedef map< string, CTexture* > CMapTexture;
typedef CMapTexture::value_type MapTexType;
typedef CMapTexture::iterator MapTexItor;

class CTextureMng
{
public:
	bool SetInvalidate(LPDIRECT3DDEVICE9 pd3dDevice);
	void Invalidate();
	CMapTexture m_mapTexture;
	//CMapStringToPtr m_mapTexture;
	CTextureMng();
	~CTextureMng();
	bool RemoveTexture(const char* pKey);
	bool DeleteDeviceObjects();
	CTexture* AddTexture(LPDIRECT3DDEVICE9 pd3dDevice, const char* pFileName, D3DCOLOR d3dKeyColor, bool bMyLoader = false);
	CTexture* AddTexture(LPDIRECT3DDEVICE9 pd3dDevice, const char* pKey, CTexture* pTexture);
	CTexture* GetAt(const char* pFileName);
};
#ifdef __CLIENT
// 몹, 플레이어 데미지 출력
class CDamageNum
{
public:
	unsigned long m_nFrame;
	unsigned long m_nNumber;
	unsigned long m_nAttribute;
	D3DXVECTOR3 m_vPos;
	float	m_fY, m_fDy;
	int		m_nState;
	int		m_nCnt;

	CDamageNum(D3DXVECTOR3 vPos, unsigned long nNumber, unsigned long nAttribute)
	{
		m_nFrame = 0; m_nNumber = nNumber; m_vPos = vPos; m_nAttribute = nAttribute; m_nState = 0; m_nCnt = 0;
	}
	~CDamageNum() {};

	void Process();
	void Render(CTexturePack* textPackNum);
#ifdef __CLIENT
#ifndef __VM_0820
#ifndef __MEM_TRACE
	static MemPooler<CDamageNum>* m_pPool;
	void* operator new(size_t nSize) { return CDamageNum::m_pPool->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CDamageNum::m_pPool->Alloc(); }
	void	operator delete(void* lpMem) { CDamageNum::m_pPool->Free((CDamageNum*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CDamageNum::m_pPool->Free((CDamageNum*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820
#endif	// __CLIENT
};

// 데미지 숫자 관리자
class CDamageNumMng
{
public:
	CPtrArray m_Array;
	D3DXMATRIX m_matProj, m_matView, m_matWorld;
	D3DVIEWPORT9 m_viewport;
	CTexturePack m_textPackNum;

	CDamageNumMng() { D3DXMatrixIdentity(&m_matWorld); };
	~CDamageNumMng();

	HRESULT		RestoreDeviceObjects();
	HRESULT		InvalidateDeviceObjects();

	bool DeleteDeviceObjects();
	void LoadTexture(LPDIRECT3DDEVICE9 pd3dDevice);
	void AddNumber(D3DXVECTOR3 vPos, unsigned long nNumber, unsigned long nAttribute); // 새로운 데미지 표시를 생성시킨다.
	void Process(); // 현재 생성된 데미지 표시들을 처리한다.
	void Render(); // 현재 생성된 데미지 표시들을 출력한다.
};
#endif
#endif