#ifndef __MUSIC_H__
#define __MUSIC_H__

class CMusicMng
{
public:
	struct MUSIC
	{
		TCHAR szMusicFileName[128];
	};
	CFixedArray< MUSIC > m_aMusic;

	bool LoadScript(const char* lpszFileName);
	LPTSTR GetFileName(unsigned long dwId);
};
void ProcessFadeMusic();
bool PlayMusic(const char* lpszFileName, int nLoopCount = 1);
bool PlayMusic(unsigned long dwIdMusic, int nLoopCount = 1);
bool PlayBGM(unsigned long dwIdMusic);
void LockMusic();
void StopMusic();
void SetVolume(float fVolume);
float GetVolume();
bool IsPlayingMusic();
bool IsStopMusic();
void InitCustomSound(bool bEnable = true);
void UnInitCustomSound();

extern CMusicMng g_MusicMng;

#endif
