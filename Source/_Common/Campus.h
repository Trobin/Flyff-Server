// Campus.h: interface for the CCampus class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAMPUS_H__D0177E19_6285_41A9_9AB5_6A145BBD08BC__INCLUDED_)
#define AFX_CAMPUS_H__D0177E19_6285_41A9_9AB5_6A145BBD08BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if __VER >= 15 // __CAMPUS
#include "ar.h"

#define		CAMPUS_MASTER			1
#define		CAMPUS_PUPIL			2
#define		MAX_PUPIL_NUM			3
#define		MIN_LV2_POINT			41
#define		MIN_LV3_POINT			101

class CCampusMember
{
public:
	CCampusMember();
	~CCampusMember();

	void	Serialize(CAr& ar);

	unsigned long	GetPlayerId() { return m_idPlayer; }
	void	SetPlayerId(unsigned long idPlayer) { m_idPlayer = idPlayer; }

	int		GetLevel() { return m_nMemberLv; }
	void	SetLevel(int nLevel) { m_nMemberLv = nLevel; }

private:
	unsigned long	m_idPlayer;
	int		m_nMemberLv;
};
typedef map<unsigned long, CCampusMember*>		MAP_CM;

class CCampus
{
public:
	CCampus();
	~CCampus();

	void	Clear();
	void	Serialize(CAr& ar);

	unsigned long	GetCampusId() { return m_idCampus; }
	void	SetCampusId(unsigned long idCampus) { m_idCampus = idCampus; }
	unsigned long	GetMaster() { return m_idMaster; }
	void	SetMaster(unsigned long idMaster) { m_idMaster = idMaster; }
	bool	IsMaster(unsigned long idPlayer) { return (idPlayer == m_idMaster); }
	int		GetMemberSize() { return m_mapCM.size(); }

	bool	IsPupil(unsigned long idPlayer);
	vector<unsigned long>	GetPupilPlayerId();
	int		GetPupilNum();

	vector<unsigned long>	GetAllMemberPlayerId();
	int		GetMemberLv(unsigned long idPlayer);
	bool	IsMember(unsigned long idPlayer);
	bool	AddMember(CCampusMember* pCM);
	bool	RemoveMember(unsigned long idPlayer);
	CCampusMember* GetMember(unsigned long idPlayer);


private:
	unsigned long	m_idCampus;
	unsigned long	m_idMaster;
	MAP_CM	m_mapCM;

#ifdef __WORLDSERVER
public:
	bool	IsChangeBuffLevel(unsigned long idPlayer);
	int		GetBuffLevel(unsigned long idPlayer);

private:
	int		m_nPreBuffLevel;
#endif // __WORLDSERVER
};
typedef map<unsigned long, CCampus*>	MAP_CAMPUS;
typedef map<unsigned long, unsigned long>		MAP_PID2CID;

class CCampusMng
{
public:
	CCampusMng();
	~CCampusMng();

	bool	IsEmpty() { return m_mapCampus.empty(); }
	void	Clear();
	void	Serialize(CAr& ar);

	unsigned long	AddCampus(CCampus* pCampus);
	bool	RemoveCampus(unsigned long idCampus);

	CCampus* GetCampus(unsigned long idCampus);

	bool	AddPlayerId2CampusId(unsigned long idPlayer, unsigned long idCampus);
	void	RemovePlayerId2CampusId(unsigned long idPlayer) { m_mapPid2Cid.erase(idPlayer); }
	unsigned long	GetCampusIdByPlayerId(unsigned long idPlayer);

private:
	unsigned long	m_idCampus;
	MAP_CAMPUS	m_mapCampus;
	MAP_PID2CID	m_mapPid2Cid;
};
#endif // __CAMPUS
#endif // !defined(AFX_CAMPUS_H__D0177E19_6285_41A9_9AB5_6A145BBD08BC__INCLUDED_)