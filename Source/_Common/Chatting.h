#ifndef __CHATTING_H__
#define	__CHATTING_H__

#include "mempooler.h"
#include <map>

#define MAX_CHATTINGMEMBER	512

class CChatting
{
	TCHAR	m_sChatting[33];					// ChattingRoom Name
	unsigned long	m_idChatting;
public:
	int		m_nSizeofMember;
	unsigned long	m_idMember[MAX_CHATTINGMEMBER];		// ChattingMember
#ifdef __CLIENT
	TCHAR	m_szName[MAX_CHATTINGMEMBER][MAX_NAME];
#endif // __CLIENT
	bool	m_bState;
	//	Constructions
	CChatting();
	~CChatting();

#ifdef __CLIENT
	bool	AddChattingMember(unsigned long uidPlayer, char* szName);
#else // __CLIENT
	bool	AddChattingMember(unsigned long uidPlayer);
#endif // __CLIENT
	bool	RemoveChattingMember(unsigned long uidPlayer);

	bool	IsChattingMember(unsigned long uidPlayer) { return(FindChattingMember(uidPlayer) >= 0); }
	int		FindChattingMember(unsigned long uidPlayer);

	void	ClearMember(void);

	void	Serialize(CAr& ar);
	int		GetChattingMember() { return m_nSizeofMember; }
public:
#ifndef __VM_0820
#ifndef __MEM_TRACE
	static	MemPooler<CChatting>* m_pPool;
	void* operator new(size_t nSize) { return CChatting::m_pPool->Alloc(); }
	void* operator new(size_t nSize, LPCSTR lpszFileName, int nLine) { return CChatting::m_pPool->Alloc(); }
	void	operator delete(void* lpMem) { CChatting::m_pPool->Free((CChatting*)lpMem); }
	void	operator delete(void* lpMem, LPCSTR lpszFileName, int nLine) { CChatting::m_pPool->Free((CChatting*)lpMem); }
#endif	// __MEM_TRACE
#endif	// __VM_0820
};

using	namespace	std;
typedef	map< unsigned long, CChatting*>	C2CharttingPtr;

class CChattingMng
{
	C2CharttingPtr	m_2ChatPtr;
public:
	CChattingMng();
	~CChattingMng();

	void	Clear(void);
	unsigned long	NewChattingRoom(unsigned long uChattingId);
	bool	DeleteChattingRoom(unsigned long uChattingId);
	CChatting* GetChttingRoom(unsigned long uidChtting);
};

#endif	//	_CHATTING_H