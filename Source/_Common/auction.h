#ifndef __AUCTION_H__
#define	__AUCTION_H__

class CUser;
class CAuction
{
public:
	CAuction();
	virtual	~CAuction();

	static	CAuction* GetInstance(void);

	void	ActionOff(CUser* CUser, BYTE byId, short nItemNum, unsigned long dw1, unsigned long dw2);
	void	Bid(int nAuctionId, unsigned long dwBid, bool bKnockdown);
	void	KnockDown(unsigned long idPlayer, int nAuctionId);
};

#endif	// __AUCTION_H__