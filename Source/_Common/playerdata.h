#ifndef __PLAYER_DATA_H__
#define	__PLAYER_DATA_H__

#include "ar.h"

typedef struct _sPlayerData
{
	BYTE	nJob;
	BYTE	nLevel;
	//	unsigned long	dwState;
	BYTE	nSex;
	int		nVer;
	BYTE	uLogin;
}	sPlayerData;

typedef struct	_PlayerData
{
	sPlayerData
		data;
	char	szPlayer[MAX_PLAYER];
	_PlayerData()
	{
		memset(this, 0, sizeof(_PlayerData));
	}
}	PlayerData;

typedef struct	_PDVer
{
	unsigned long	idPlayer;
	int		nVer;
	_PDVer()
	{
		idPlayer = nVer = 0;
	}
	_PDVer(unsigned long u, int n)
	{
		idPlayer = u;
		nVer = n;
	}
}	PDVer;

class CPlayerDataCenter
{
public:
	CPlayerDataCenter();
	virtual	~CPlayerDataCenter();
	static	CPlayerDataCenter* GetInstance(void);
	void		Clear();
#ifdef __CLIENT
	PlayerData* GetPlayerData(unsigned long idPlayer, bool bQuery = true);
#else	// __CLIENT
	PlayerData* GetPlayerData(unsigned long idPlayer);
#endif	// __CLIENT
	PlayerData* AddPlayerData(unsigned long idPlayer, PlayerData& pd);
#ifdef __CLIENT
	PlayerData* AddPlayerData(unsigned long idPlayer, const char* pszPlayer);
#endif	// __CLIENT
	bool	DeletePlayerData(unsigned long idPlayer);
	int		Serialize(CAr& ar, int nFirst = 0, int nCount = 0);
	//	void	GetPlayerString( unsigned long idPlayer, char* pszPlayer );
	const char* GetPlayerString(unsigned long idPlayer);
	void	ReplacePlayerString(unsigned long idPlayer, char* pszPlayer, char* pszOld);
	unsigned long	GetPlayerId(char* pszPlayer);
	int		size(void) { return m_mapPlayerData.size(); }

private:
	map<unsigned long, PlayerData*>	m_mapPlayerData;
	map<string, unsigned long>	m_mapPlayerStringToId;
public:
	CMclCritSec		m_Access;
};
#endif	// __PLAYER_DATA_H__