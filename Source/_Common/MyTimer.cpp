#include "stdafx.h"
#include "MyTimer.h"

#include <thread>
#include <chrono>

void Timer::setTimeout(std::function<void(void)> function, int delay) {
	this->active = true;
	std::thread t([=]() {
		if (!this->active.load()) {
			return;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(delay));
		if (!this->active.load()) {
			return;
		}
		function();
	});
	t.detach();
}

void Timer::setInterval(std::function<void(void)> function, int delay) {
	this->active = true;
	std::thread t([=]() {
		while (this->active.load()) {
			std::this_thread::sleep_for(std::chrono::milliseconds(delay));
			if (!this->active.load()) {
				return;
			}
			function();
		}
	});
	t.detach();
}

void Timer::stop() {
	this->active = false;
}