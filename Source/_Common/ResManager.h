#ifndef __RESMANAGER_H
#define __RESMANAGER_H

#include "data.h"
#include "lang.h"

#define DEF_CTRL_CHECK "ButtCheck.bmp"
#define DEF_CTRL_RADIO "ButtRadio.bmp"
#define DEF_CTRL_GROUP "WndEditTile200.tga"
#define DEF_CTRL_TAB1  "WndTabTile00.bmp"
#define DEF_CTRL_TAB2  "WndTabTile10.bmp"
#define DEF_WNDBASE    "WndTile00.tga"
#define DEF_CTRL_TEXT  "WndEditTile00.tga"

typedef struct tagWndCtrl
{
	unsigned long dwWndId;
	unsigned long dwWndType;
	unsigned long dwWndStyle;
	bool bTile;

	CString strDefine;
	CString strTitle;
	CString strToolTip;
	CString strTexture;
	CRect rect;
	int m_bVisible;
	bool m_bDisabled;
	bool m_bTabStop;
	bool m_bGroup;
} WNDCTRL, * LPWNDCTRL;

typedef struct tagWndApplet
{
	CWndBase* pWndBase;
	unsigned long dwWndId;
	unsigned long dwWndStyle;
	bool bTile;

	CString strTitle;
	CString strToolTip;
	D3DFORMAT d3dFormat;
	CString strDefine;
	CString strTexture;
	CPtrArray ptrCtrlArray;
	CSize size;

	LPWNDCTRL GetAt(unsigned long dwWndId);

} WNDAPPLET, * LPWNDAPPLET;

class CResManager
{
public:
	CResManager();
	~CResManager();

private:
	CMapPtrToPtr m_mapWndApplet;

public:
	void Free();
	bool Load(const char* lpszName);
	LPWNDAPPLET GetAt(unsigned long dwAppletId);
	LPWNDCTRL GetAtControl(unsigned long dwAppletId, unsigned long dwCtrlId);

private:
	CString GetLangApplet(CScript& scanner, LPWNDAPPLET lpWndApplet, bool bToolTip);
	CString GetLangCtrl(CScript& scanner, LPWNDCTRL lpWndCtrl, bool bToolTip);
};


#endif