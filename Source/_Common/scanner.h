#ifndef __SCANNER_H
#define __SCANNER_H

#include "data.h"

#ifndef NULL_ID
#define NULL_ID 0xffffffff
#endif

#define MAX_TOKENSTR 2048

/*
 *  Add additional C keyword tokens here
 */
enum Toke
{
	NONE_, ARG, VAR, IF, ELSE, FOR, DO, WHILE, BREAK, SWITCH, ANSWER, SELECT, YESNO,
	CASE, DEFAULT, GOTO, RETURN, EOL, DEFINE, INCLUDE, ENUM, FINISHED, END
};
/*
 *  Add additional double operatoes here (such as->)
 */
enum DoubleOps
{
	LT = 1, LE, GT, GE, EQ, NE, NT
};

enum TokenType
{
	TEMP, DELIMITER, IDENTIFIER, NUMBER, HEX, KEYWORD, STRING, BLOCK
};


////////////////////////////////////////////////////////////////////////////////
//
// Token : 토큰을 구분하여 읽어드림.
//
class CScanner
{
public:
	CScanner(bool bComma = false);
	CScanner(LPVOID p, bool bComma = false);
	virtual ~CScanner();

protected:
	bool		m_bComma;			// 구분자가 콤마인가?
	unsigned char		m_bMemFlag;			// 0은 로드, 1은 외부 포인트 
	int			m_nProgSize;
	unsigned long		m_dwDef;
	bool		Read(CFileIO* pFile, bool);

public:
	char* m_lpMark;
	char* m_pProg;
	char* m_pBuf;

	CString		Token;
	char* token;
	char		m_mszToken[MAX_TOKENSTR];
	int			tokenType;
	int			tok;
	int			m_nDoubleOps;
	unsigned long		dwValue;
	CString 	m_strFileName;
	bool		m_bErrorCheck;

	virtual void	SetMark() { m_lpMark = m_pProg; }
	virtual void	GoMark() { if (m_lpMark) m_pProg = m_lpMark; }
	virtual int		GetLineNum(LPVOID lpProg = NULL);
	virtual void	PutBack();
	virtual	int		GetToken(bool bComma = false);

	void			SetErrorCheck(bool bErrorCheck) { m_bErrorCheck = bErrorCheck; }
	bool			GetErrorCheck() { return m_bErrorCheck; }
	void			GetLastFull(); // 현재부터 끝까지 한번에 읽는다.
	void			Free();
	bool			Load(const char* lpszFileName, bool bMultiByte = true);
	bool			Load_FileIO(const char* lpszFileName, bool bMultiByte = true);
	void			SetProg(LPVOID pProg);
	int				GetNumber(bool bComma = false);
	__int64			GetInt64(bool bComma = false);
	float			GetFloat(bool bComma = false);
	unsigned long			GetHex(bool bComma = false);
	SERIALNUMBER	GetSerialNumber(bool bComma = false);
	EXPINTEGER		GetExpInteger(bool bComma = false) { return GetInt64(bComma); }
};


/////////////////////////////////////////////////////////////////////////////

extern unsigned short		g_codePage;

bool	IsComposibleTh(unsigned char prev, unsigned char curr, int mode);
const char* CharNextEx(const char* strText, unsigned short codePage = g_codePage);

/////////////////////////////////////////////////////////////////////////////
int WideCharToMultiByteEx(
	unsigned int     CodePage,
	unsigned long    dwFlags,
	LPCWSTR  lpWideCharStr,
	int      cchWideChar,
	LPSTR    lpMultiByteStr,
	int      cchMultiByte,
	LPCSTR   lpDefaultChar,
	LPBOOL   lpUsedDefaultChar);

int MultiByteToWideCharEx(
	unsigned int     CodePage,
	unsigned long    dwFlags,
	LPCSTR   lpMultiByteStr,
	int      cchMultiByte,
	LPWSTR   lpWideCharStr,
	int      cchWideChar);


bool IsCyrillic(const char chSrc);

#endif
