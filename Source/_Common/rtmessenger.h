#ifndef __RT_MESSENGER_H__
#define	__RT_MESSENGER_H__

#include "messenger.h"

typedef struct	_Friend
{
	bool	bBlock;
	unsigned long	dwState;
	_Friend()
	{
		bBlock = false;
		dwState = 0;
	}
}	Friend;

class CRTMessenger : public	map<unsigned long, Friend>
{
public:
	CRTMessenger();
	virtual	~CRTMessenger();
	//
	//	void	SetFriend( unsigned long idFriend, bool bBlock, unsigned long dwState );
	void	SetFriend(unsigned long idFriend, Friend* pFriend);
	void	RemoveFriend(unsigned long idFriend) { erase(idFriend); }
	Friend* GetFriend(unsigned long idFriend);

	void	SetBlock(unsigned long idFriend, bool bBlock);
	bool	IsBlock(unsigned long idFriend);

	unsigned long	GetState(void) { return m_dwState; }
	void	SetState(unsigned long dwState) { m_dwState = dwState; }

	//	void	Serialize( CAr & ar );
	int	Serialize(CAr& ar);

	virtual	CRTMessenger& CRTMessenger::operator =(CRTMessenger& rRTMessenger);
private:
	unsigned long	m_dwState;
};

#endif	// __RT_MESSENGER_H__
