#ifndef __PATHOBJ_H__
#define __PATHOBJ_H__

typedef struct _VECINFO
{
	D3DXVECTOR3		m_vDir;			// 방향벡터
	float			m_fLength;		// 거리
	unsigned long			m_dwState;		// 모션 종류
	unsigned long			m_dwDelay;		// 정지시간
	CTimer			m_ctStop;       // 정지시간
} _VECINFO;

class CPatrolPath
{
	map< unsigned long, vector<_VECINFO> > m_mapPatrolPath;
	map< unsigned long, bool	 > m_bReturn;

public:
	CPatrolPath();
	~CPatrolPath();

	static	CPatrolPath* GetInstance(void);

	bool			LoadPatrol(const char* szFileName);
	void			AddPatrolIndex(unsigned long dwIndex);
	bool			IsFirstPath(unsigned long dwIndex);
	void			Clear();
	void			SetReturn(unsigned long dwIndex, bool bReturn);
	void			AddPatrolPath(unsigned long dwIndex, _VECINFO vecInfo);
	void			GetNextPosInfo(CObj* pObj, const D3DXVECTOR3 vPos, D3DXVECTOR3& vDest, _VECINFO& _vecInfo);
};

#endif //__PATHOBJ_H__