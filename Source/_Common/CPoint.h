#pragma once


class CPoint
{
public:
	int x;
	int y;

	CPoint();
	CPoint(int x, int y);

	CPoint operator+(CPoint point);
	void operator+=(CPoint point);
};

