#ifndef __TARGA_H
#define __TARGA_H

#pragma pack(1)
typedef struct _TgaHeader
{
	UCHAR			IdLength;            // Image ID Field Length
	UCHAR			CmapType;            // Color Map Type
	UCHAR			ImageType;           // Image Type

	unsigned long		CmapIndex;           // First Entry Index
	unsigned long		CmapLength;          // Color Map Length
	UCHAR			CmapEntrySize;       // Color Map Entry Size

	unsigned long		X_Origin;            // X-origin of Image
	unsigned long		Y_Origin;            // Y-origin of Image
	unsigned long		ImageWidth;          // Image Width
	unsigned long		ImageHeight;         // Image Height
	UCHAR			PixelDepth;          // Pixel Depth
	UCHAR			ImagDesc;            // Image Descriptor
} TGAHEADER;

// Definitions for image types.
#define TGA_Null 0
#define TGA_Map 1
#define TGA_RGB 2
#define TGA_Mono 3
#define TGA_RLEMap 9
#define TGA_RLERGB 10
#define TGA_RLEMono 11
#define TGA_CompMap 32
#define TGA_CompMap4 33

#pragma pack()

class CTarga
{
	TGAHEADER m_header;
	LPBYTE m_pData;

	// Decoding

	bool GetPalette(LPRGBQUAD rgb);
	bool DecodeBitmap(LPBYTE pDst, LPBYTE pSrc, int nAlign);
	bool ReadChunk(CArchive& ar);

	LPBYTE m_pBuf;
	unsigned long  m_nFileSize;
	unsigned long	 m_nTgaDataIdx;
	int		 m_nReadPixelCount;

	//////////////////////////// targa original source

	void DoDecode(unsigned long** ppPal, int* pDestBPP, char* pDataSrc);
	// Fills TGAHEADER structure with the information in the file.
	void readTgaHeader(TGAHEADER* pTgaHead, char* pDataSrc);
	// Reads the TGA palette and creates a windows palette.
	void readPalette(int StartIndex, int Length, int EntrySize, unsigned long** ppPal, int DestBPP, char* pDataSrc);
	// Creates a standalone RGBA grayscale palette.
	void setGrayPalette(unsigned long** ppPal);
	// Determines compression type and calls readData.
	void readImage(TGAHEADER* pTgaHead, unsigned long** ppPal, int DestBPP, char* pDataSrc);
	// Reads image data line-by-line.
	void readData(TGAHEADER* pTgaHead, bool bCompressed, unsigned long* pPal, int DestBPP, char* pDataSrc);
	// Decodes one line of uncompressed image data.
	void expandUncompressedLine(UCHAR* pDest, int Width, bool	bReversed, int	bpp, char* pDataSrc, unsigned long* pPal, int DestBPP);
	// Decodes one line of compressed image data.
	void expandCompressedLine(UCHAR* pDest, int Width, bool	bReversed, int	bpp, char* pDataSrc, unsigned long* pPal, int DestBPP);
	// Reads one image pixel and returns it in RGBA format.
	unsigned long readPixel32(int bpp, char* pDataSrc, unsigned long* pPal);
	// Reads one image pixel and returns it in 8-bit format.
	UCHAR readPixel8(int bpp, char* pDataSrc);

	////////////////////////////////////////////////////////////////
	void CreateTarga();// CDC* pDc,CDibPalette* pPal);

public:
	CTarga();
	~CTarga();
	void FreeBuffer();
	//void CreateTarga( CDC* pDc,CDibPalette* pPal);
	bool Load(LPCSTR lpszFileName, LPBYTE* lppData, SIZE* pSize);
};
#endif