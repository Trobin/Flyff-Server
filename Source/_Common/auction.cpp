#include "stdafx.h"
#include "auction.h"

CAuction::CAuction()
{
}

CAuction::~CAuction()
{
}

CAuction* CAuction::GetInstance(void)
{
	static CAuction sAuction;
	return &sAuction;
}

void CAuction::ActionOff(CUser* pUser, BYTE byId, short nItemNum, unsigned long dw1, unsigned long dw2)
{
}

void CAuction::Bid(int nAuctionId, unsigned long dwBid, bool bKnockdown)
{
}

void CAuction::KnockDown(unsigned long idPlayer, int nAuctionId)
{
}
