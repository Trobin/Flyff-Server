// EventMonster.h: interface for the CEventMonster class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EVENTMONSTER_H__812603BA_A499_4C90_9975_D3C74CF95390__INCLUDED_)
#define AFX_EVENTMONSTER_H__812603BA_A499_4C90_9975_D3C74CF95390__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef __EVENT_MONSTER

class CEventMonster
{
public:
	struct __EVENTMONSTER
	{
		int nLevel;
		unsigned long dwLootTime;
		float fItemDropRange;
		bool bPet;
		bool bGiftBox;
	};

	CEventMonster();
	virtual ~CEventMonster();

	static CEventMonster* GetInstance(void);

	void	LoadScript();

	bool	SetEventMonster(unsigned long dwId);
	bool	IsEventMonster(unsigned long dwId);
	bool	IsPetAble();
	bool	IsGiftBoxAble();
	unsigned long	GetLootTime();
	int		GetLevelGap();
	float	GetItemDropRange();


	map< unsigned long, __EVENTMONSTER > m_mapEventMonster;
	map< unsigned long, __EVENTMONSTER >::iterator m_it;
};
#endif // __EVENT_MONSTER

#endif // !defined(AFX_EVENTMONSTER_H__812603BA_A499_4C90_9975_D3C74CF95390__INCLUDED_)