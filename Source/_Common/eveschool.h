#ifndef __EVE_SCHOOL_H__
#define	__EVE_SCHOOL_H__

#include "guild.h"

#define	MAX_SCHOOL	10

enum
{
	SBS_END, SBS_READY, SBS_START, SBS_START2,
};

typedef	struct	_SCHOOL_ENTRY
{
	unsigned long	id;
	char	lpName[MAX_G_NAME];
	int		nSurvivor;
	int		nSize;
	int		nLevel;
	int		nDead;
	_SCHOOL_ENTRY()
	{
		id = 0;
		*lpName = '\0';
		nSurvivor = 0;
		nSize = 0;
		nLevel = 0;
		nDead = 0;
	};
}
SCHOOL_ENTRY, * PSCHOOL_ENTRY;

class CGuildMng;

class	CEveSchool
{
public:
	CGuildMng* m_pSchoolMng;
	D3DXVECTOR3	m_avPos[MAX_SCHOOL];
	D3DXVECTOR3	m_vDefault;
	unsigned long	m_dwsbstart2;
public:
	CEveSchool();
	~CEveSchool() {}

	bool	Ready(void);
	bool	Start(void);
	bool	Start2(void);
	bool	End(void);
	bool	Report(void);

	bool	LoadPos(LPCSTR lpszFileName);
	D3DXVECTOR3		GetPos(unsigned long id);
	D3DXVECTOR3		GetPos(void) { return m_vDefault; }

	static	CEveSchool* GetInstance();
};


typedef struct __AUTO_OPEN
{
	bool bUseing;
	BYTE nHour;
	BYTE nMinute;
} __AUTO_OPEN;

class CGuildCombat
{
#ifdef __WORLDSERVER 	
	__AUTO_OPEN		__AutoOpen[7];
#endif // __WORLDSERVER

public:
	enum { OPEN_STATE, CLOSE_STATE, WAR_STATE, COMPLET_CLOSE_STATE, GM_COLSE_STATE };
	enum { NOTENTER_STATE = 100, NOTENTER_COUNT_STATE, ENTER_STATE, MAINTENANCE_STATE, WAR_WAR_STATE, WAR_CLOSE_STATE, WAR_CLOSE_WAIT_STATE, WAR_TELEPORT_STATE };
	enum { ALLMSG = 1000, GUILDMSG, JOINMSG, WORLDMSG, STATE, WARSTATE, WAIT };
	struct __REQUESTGUILD
	{
		unsigned long uidGuild;
		unsigned long dwPenya;
	};
	struct __JOINPLAYER
	{
		unsigned long	uidPlayer;		// 캐릭터 아이디
		int		nlife;			// 남은 생명
//		bool	bEntry;			// 참가 유/무
		int		nPoint;			// 포인트
		int		uKillidGuild;	// 전에 죽인 길드
		int		nMap;			// 대전 텔레포트 맵
		unsigned long	dwTelTime;		// 대전 텔레포트 시간
		__JOINPLAYER()
		{
			uidPlayer = 0;
			nlife = 0;
			nPoint = 0;
			uKillidGuild = 0;
			nMap = 0;
			dwTelTime = 0;
		}
	};
	struct __GCSENDITEM
	{
		int nWinCount;
		unsigned long dwItemId;
		int nItemNum;
		__GCSENDITEM()
		{
			nWinCount = 0;
			dwItemId = 0;
			nItemNum = 0;
		}
	};
	struct __GuildCombatMember
	{
#ifdef __S_BUG_GC
		unsigned long	uGuildId;
#endif // __S_BUG_GC
		vector<__JOINPLAYER*> vecGCSelectMember;	// 대전에 선택된 멤버 정보
//		set<unsigned long> GuildCombatJoinMember;		// 길드전에 참가한멤버
//		set<unsigned long> GuildCombatOutMember;		// 참가했는데 죽었던가 나간 멤버
//		float fAvgLevel;		// 평균 레벨
		unsigned long dwPenya;			// 참가비
		bool  bRequest;			// 참가 유/무
		unsigned long	m_uidDefender;	// 디펜더
		int	  nJoinCount;		// 참가 카운터
		int	  nWarCount;		// 전투 인원수
		int   nGuildPoint;		// 길드 포인트
		list<__JOINPLAYER*>	lspFifo;

		void Clear()
		{
#ifdef __S_BUG_GC
			uGuildId = 0;
#endif // __S_BUG_GC
			SelectMemberClear();
			dwPenya = 0;
			bRequest = false;
			m_uidDefender = 0;
			nJoinCount = 0;
			nWarCount = 0;
			nGuildPoint = 0;
		};
		void SelectMemberClear()
		{
			for (int veci = 0; veci < (int)(vecGCSelectMember.size()); ++veci)
			{
				__JOINPLAYER* pJoinPlayer = vecGCSelectMember[veci];
				SAFE_DELETE(pJoinPlayer);
			}
			vecGCSelectMember.clear();
			lspFifo.clear();
		}
	};
	struct __GuildCombatProcess
	{
		int		nState;
		unsigned long	dwTime;
		unsigned long	dwCommand;
		unsigned long	dwParam;
	};
	struct __GCRESULTVALUEGUILD
	{
		int	nCombatID;			// 길드대전 아이디
		unsigned long uidGuild;		// 길드 아이디
		__int64 nReturnCombatFee;	// 돌려받을 참여금
		__int64 nReward;				// 보상금
	};
	struct __GCRESULTVALUEPLAYER
	{
		int nCombatID;			// 길드대전 아이디
		unsigned long uidGuild;		// 길드 아이디
		unsigned long uidPlayer;		// 플레이어 아이디
		__int64 nReward;			// 보상금
	};
	struct __GCGETPOINT
	{
		unsigned long uidGuildAttack;
		unsigned long uidGuildDefence;
		unsigned long uidPlayerAttack;
		unsigned long uidPlayerDefence;
		int nPoint;
		bool bKillDiffernceGuild;
		bool bMaster;
		bool bDefender;
		bool bLastLife;

		__GCGETPOINT()
		{
			uidGuildAttack = uidGuildDefence = uidPlayerAttack = uidPlayerDefence = nPoint = 0;
			bKillDiffernceGuild = bMaster = bDefender = bLastLife = false;
		}
	};
	struct __GCPLAYERPOINT
	{
		unsigned long	uidPlayer;
		int		nJob;
		int		nPoint;

		__GCPLAYERPOINT()
		{
			uidPlayer = nJob = nPoint = 0;
		}
	};
	int		m_nGuildCombatIndex;
	unsigned long	m_uWinGuildId;
	int		m_nWinGuildCount;
	unsigned long	m_uBestPlayer;
	vector<__GCGETPOINT> m_vecGCGetPoint;
	vector<__GCPLAYERPOINT> m_vecGCPlayerPoint;
#ifdef __WORLDSERVER 
#ifdef __S_BUG_GC
	vector<__GuildCombatMember*> m_vecGuildCombatMem;
#else // __S_BUG_GC
	map<unsigned long, __GuildCombatMember*> m_GuildCombatMem;
#endif // __S_BUG_GC
	unsigned long	m_dwTime;
	int		m_nProcessGo;
	int	 	m_nProcessCount[25];
	__GuildCombatProcess GuildCombatProcess[250];
	int		m_nStopWar;	// 1이면 중간에 종료, 2이면 운영자가 종료

	int		m_nJoinPanya;		// 대전에 참가할수 있는 기본 Penya
	int		m_nGuildLevel;		// 대전에 참가할수 있는 최소 길드레벨
#if __VER >= 8 // __GUILDCOMBAT_85
	int		m_nMinGuild;		// 최소 전쟁을 할수 있는 길드 개수(최소 참여 길드 조건이 되야 길드 대전이 시작함)
	int		m_nMaxGCSendItem;
#endif // __VER >= 8 
	int		m_nMaxGuild;		// 대전에 참가할수 있는 길드
	int		m_nMaxJoinMember;	// 대전에 참가할수 있는 최대 유저
	int		m_nMaxPlayerLife;	// 대전에 참가한 유저의 최대 생명
	int		m_nMaxWarPlayer;	// 최대 선발대 유저
	int		m_nMaxMapTime;		// 대전 위치 설정 시간
	int		m_nMaxGuildPercent;	// 대전길드 상금 퍼센트
	int		m_nMaxPlayerPercent;// 베스트 플레이어 퍼센트 
	int		m_nRequestCanclePercent;	// 참가신청시 취소한 길드에게 돌려줄 퍼센트
	int		m_nNotRequestPercent;		// 참가시 입찰이 안된길드에게 돌려줄 퍼센트
	int		m_nItemPenya;				// 대전 상품 아이템 가격(용망토?)
	bool    m_bMutex;					// 길드대전 오픈 한번만...
	bool    m_bMutexMsg;					// 길드대전 오픈 한번만...
	CTimer   m_ctrMutexOut;
#if __VER >= 8 // __GUILDCOMBAT_85
	vector< CString > m_vecstrGuildMsg;
	vector<__GCSENDITEM>	vecGCSendItem;
#endif // __VER >= 8 

	vector<__REQUESTGUILD>	vecRequestRanking;	// 참가 순위
	vector<__GCRESULTVALUEGUILD>			m_GCResultValueGuild;		// 길드대전 결과값
	vector<__GCRESULTVALUEPLAYER>			m_GCResultValuePlayer;		// 길드대전 결과값
#endif // __WORLDSERVER
	int		m_nState;		// 길드워 상태
	int		m_nGCState;	// 전투 중일 때의 상태
#ifdef __CLIENT
	bool	m_bRequest;	// 신청 유무
	bool	IsRequest(void) { return m_bRequest; };
#endif // __CLIENT
public:
	//	Constructions
	CGuildCombat();
	virtual ~CGuildCombat();
	void	GuildCombatClear(int Clear = 1);
	void	GuildCombatGameClear();
	void	SelectPlayerClear(unsigned long uidGuild);
	void	AddvecGCGetPoint(unsigned long uidGuildAttack, unsigned long uidGuildDefence, unsigned long uidPlayerAttack, unsigned long uidPlayerDefence, int nPoint,
		bool bKillDiffernceGuild, bool bMaster, bool bDefender, bool bLastLife);
	void	AddvecGCPlayerPoint(unsigned long uidPlayer, int nJob, int nPoint);
#ifdef __WORLDSERVER
	bool	LoadScript(LPCSTR lpszFileName);
	void	JoinGuildCombat(unsigned long idGuild, unsigned long dwPenya, bool bRequest);
	void	AddSelectPlayer(unsigned long idGuild, unsigned long uidPlayer);
	void	GetSelectPlayer(unsigned long idGuild, vector<__JOINPLAYER>& vecSelectPlayer);
	void	OutGuildCombat(unsigned long idGuild);
	void	SetMaintenance();
	void	SetEnter();
	void	SetGuildCombatStart();
	void	SetGuildCombatClose(bool bGM = false);
	void	SetGuildCombatCloseWait(bool bGM = false);
	void	GuildCombatCloseTeleport();

	void	SetNpc(void);
	void	SetRequestRanking(void);
	void	SetDefender(unsigned long uidGuild, unsigned long uidDefender);
	void	SetPlayerChange(CUser* pUser, CUser* pLeader);
	unsigned long	GetDefender(unsigned long uidGuild);
	unsigned long	GetBestPlayer(unsigned long* dwGetGuildId, int* nGetPoint);
	unsigned long	GetRequstPenya(unsigned long uidGuild);
	void	GetPoint(CUser* pAttacker, CUser* pDefender);
	__int64	GetPrizePenya(int nFlag);
	bool	IsRequestWarGuild(unsigned long uidGuild, bool bAll);
	bool	IsSelectPlayer(CUser* pUser);

	void	JoinWar(CUser* pUser, int nMap = 99, bool bWar = true);
	void	OutWar(CUser* pUser, CUser* pLeader, bool bLogOut = false);
	void	JoinObserver(CUser* pUser);
	void	GuildCombatRequest(CUser* pUser, unsigned long dwPenya);
	void	GuildCombatCancel(CUser* pUser);
	void	GuildCombatOpen(void);
	void	GuildCombatEnter(CUser* pUser);
	void	SetSelectMap(CUser* pUser, int nMap);
	void	UserOutGuildCombatResult(CUser* pUser);
	void	GuildCombatResult(bool nResult = false, unsigned long idGuildWin = 0);
	void	Process();
	void	ProcessCommand();
	void	ProcessJoinWar();
	void	SendJoinMsg(const char* lpszString);
	void	SendGuildCombatEnterTime(void);
	void	SendGCLog(void);
	void	SerializeGCWarPlayerList(CAr& ar);

#if __VER >= 11 // __GUILDCOMBATCHIP
	void	GuildCombatResultRanking();
#endif // __GUILDCOMBATCHIP

	CTime	GetNextGuildCobmatTime(void);
	int		m_nDay;

#ifdef __S_BUG_GC
	__GuildCombatMember* FindGuildCombatMember(unsigned long GuildId);
#endif // __S_BUG_GC
#endif // __WORLDSERVER
};

#endif	// __EVE_SCHOOL_H__