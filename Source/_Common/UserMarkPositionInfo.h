#ifndef __USER_MARK_POSITION_INFO_H__
#define __USER_MARK_POSITION_INFO_H__

#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT
class CUserMarkPositionInfo
{
public:
	CUserMarkPositionInfo(void);
	~CUserMarkPositionInfo(void);

public:
	void SetID(unsigned long dwID);
	unsigned long GetID(void) const;
	void SetName(const CString& strName);
	const CString& GetName(void) const;
	void SetPositionX(float fPositionX);
	float GetPositionX(void) const;
	void SetPositionY(float fPositionY);
	float GetPositionY(void) const;

private:
	unsigned long m_dwID;
	CString m_strName;
	float m_fPositionX;
	float m_fPositionY;
};
typedef list<CUserMarkPositionInfo*> UserMarkPositionInfoList;
#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM

#endif // __MAP_POSITION_INFO_H__