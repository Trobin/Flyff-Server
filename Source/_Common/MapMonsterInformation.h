#ifndef __MAP_MONSTER_INFORMATION_H__
#define __MAP_MONSTER_INFORMATION_H__

#ifdef __IMPROVE_MAP_SYSTEM
#ifdef __CLIENT
class CMapMonsterInformation
{
public:
	CMapMonsterInformation(void);
	~CMapMonsterInformation(void);

public:
	void SetMonsterIDNumber(int nMonsterIDNumber);
	int GetMonsterIDNumber(void) const;
	void InsertMonsterID(unsigned long dwMonsterID);
	unsigned long GetMonsterID(int nIndex) const;
	vector< unsigned long >& GetMonsterIDVector(void);
	void SetDropItemID(unsigned long dwDropItemID);
	unsigned long GetDropItemID(void) const;
	void SetIconPositionRect(const CRect& rectIconPosition);
	const CRect& GetIconPositionRect(void) const;

private:
	int m_nMonsterIDNumber;
	vector< unsigned long > m_vecMonsterID;
	unsigned long m_dwDropItemID;
	CRect m_rectIconPosition;
};
typedef vector< CMapMonsterInformation* > MapMonsterInformationVector;
#endif // __CLIENT
#endif // __IMPROVE_MAP_SYSTEM

#endif // __MAP_MONSTER_ICON_INFORMATION_H__
