#ifndef		__YUTIL_H__
#define		__YUTIL_H__

class CFLASH
{
	bool	m_bAction;
	bool	m_bFade;
	int		m_nAlphaCount;
	int		m_nAlphaAcc;
	int     m_nTick;
	unsigned long	m_dwTime;
	unsigned long	m_dwEndTime;
	unsigned long	m_dwStopTime;
	bool	m_bStopTime;
	bool	m_bRunTime;


public:
	CFLASH();
	~CFLASH();

	void	Init();
	void    RenderFlash(C2DRender* p2DRender);
	void	SetTime(unsigned long dwStopTime, unsigned long dwTime, bool bAction);
	void	SetAction(bool bAction);
};

bool		IsForceAttack();
int			Point_In_Poly(vector<CPoint> vecMap, CPoint test_point);

#endif //__YUTIL_H__