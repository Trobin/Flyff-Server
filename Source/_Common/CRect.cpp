//#include "stdafx.h"
#include "CRect.h"

CRect::CRect() : left(0), top(0), right(0), bottom(0)
{

}

CRect::CRect(int l, int t, int r, int b) : left(l), top(t), right(r), bottom(b)
{

}

int CRect::width()
{
	return this->right - this->left;
}

int CRect::height()
{
	return this->bottom - this->left;
}

CSize CRect::size()
{
	return CSize(width(), height());
}