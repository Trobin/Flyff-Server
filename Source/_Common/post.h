#ifndef __POST_H__
#define	__POST_H__

#include <map>
#include <list>
using	namespace	std;

//	mulcom	BEGIN100420	메일 관련 사이즈 변경.
//#define	MAX_MAILTITLE	128
#define	MAX_MAILTITLE	32
//	mulcom	END100420	메일 관련 사이즈 변경.
#define	MAX_MAILTEXT	256

#ifdef __INTERNALSERVER
// 내부
#define MAX_KEEP_MAX_DAY		7   // 최대 보관일수 5일
#define MAX_KEEP_BASIC_DAY		2   // 기본 보관일수 1일
#else //__INTERNALSERVER
// 정섭
#define MAX_KEEP_MAX_DAY		15  // 최대 보관일수 15일
#define MAX_KEEP_BASIC_DAY		7	// 기본 보관일수 7일
#endif //__INTERNALSERVER

class CItemElem;
#ifdef __DBSERVER
class CMailBox;
#endif	// __DBSERVER
class CMail
{
public:
	CMail();
	CMail(unsigned long idSender, CItemElem* pItemElem, int nGold, char* szTitle, char* szText);
	virtual	~CMail();

	enum { mail, item, gold, read };

	void	Clear(void);
	unsigned long	GetCost(void);
#ifdef __DBSERVER
	void	SetMailBox(CMailBox* pMailBox) { m_pMailBox = pMailBox; }
	CMailBox* GetMailBox(void) { return m_pMailBox; }
#endif	// __DBSERVER
	void	Serialize(CAr& ar, bool bData = true);
public:
	unsigned long	m_nMail;
	unsigned long	m_idSender;
	CItemElem* m_pItemElem;
	unsigned long	m_nGold;
	time_t	m_tmCreate;
	BYTE	m_byRead;
	char	m_szTitle[MAX_MAILTITLE];
	char	m_szText[MAX_MAILTEXT];

	void	GetMailInfo(int* nKeepingDay, unsigned long* dwKeepingTime = NULL);

	static	unsigned long	s_nMail;
#ifdef __DBSERVER
private:
	CMailBox* m_pMailBox;
#endif	// __DBSERVER
};

#ifdef __DBSERVER
class CPost;
#endif	// __DBSERVER


typedef	std::vector<CMail*>			MailVector;
typedef MailVector::iterator		MailVectorItr;


class CMailBox : public MailVector
{
public:
	CMailBox();
	CMailBox(unsigned long idReceiver);
	virtual	~CMailBox();
	unsigned long	AddMail(CMail* pMail);
	void	Serialize(CAr& ar, bool bData = true);
#ifdef __DBSERVER
	void	Write(CAr& ar);
#endif	// __DBSERVER
#ifdef __WORLDSERVER
	void	Read(CAr& ar);




	//////////////////////////////////////////////////////////////////////////
	void	ReadReq(CAr& ar);
	//////////////////////////////////////////////////////////////////////////



#endif	// __WORLDSERVER
	CMail* GetMail(unsigned long nMail);
	bool	RemoveMail(unsigned long nMail);
	bool	RemoveMailItem(unsigned long nMail);
	bool	RemoveMailGold(unsigned long nMail);
	bool	ReadMail(unsigned long nMail);
	bool	IsStampedMailExists(void);
	void	Clear(void);
#ifdef __DBSERVER
	void	SetPost(CPost* pPost) { m_pPost = pPost; }
	CPost* GetPost(void) { return m_pPost; }
#endif	// __DBSERVER
	static	CMailBox* GetInstance(void);
private:
	MailVectorItr Find(unsigned long nMail);
#ifdef __DBSERVER
	CPost* m_pPost;
#endif	// __DBSERVER

public:
	unsigned long	m_idReceiver;
#ifdef __WORLDSERVER
	enum { nodata = 0, read = 1, data = 2 };
	int		m_nStatus;
#endif	// __WORLDSERVER
};

//	mulcom	BEGIN100420	메일 관련 사이즈 변경.
//#define	MAX_MAIL	100
#define	MAX_MAIL	50
//	mulcom	END100420	메일 관련 사이즈 변경.
class CPost
{
public:
	//	Constructions
	CPost();
	virtual	~CPost();
	void	Clear(void);
	//	Operations
	unsigned long	AddMail(unsigned long idReceiver, CMail* pMail);
	CMailBox* GetMailBox(unsigned long idReceiver);
	bool	AddMailBox(CMailBox* pMailBox);
	void	Serialize(CAr& ar, bool bData = true);
#ifdef __DBSERVER
	map< unsigned long, CMail* >	m_mapMail4Proc;
	CMclCritSec	m_csPost;
	void	Process(void);
#endif	// __DBSERVER
	static	CPost* GetInstance(void);
private:
	map< unsigned long, CMailBox* >	m_mapMailBox;
};
#endif	// __POST_H__