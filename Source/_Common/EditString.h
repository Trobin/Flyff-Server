#ifndef __EDITSTRING_H
#define __EDITSTRING_H 

#include "vutil.h"
inline bool IsWhite(TCHAR c)
{
	return (iswspace(c) || c == '\r' || c == '\n' || !iswprint(c)) && !IsDBCSLeadByte(c);
}
inline bool IsAlphaNum(TCHAR c)
{
	return iswalnum(c);
}
#define ESSTY_UNDERLINE     1
#define ESSTY_BOLD          2 
#define ESSTY_STRIKETHROUGH 4
#define	ESSTY_SHADOW		8
#define	ESSTY_BLOCK			16

class CD3DFont* pFont;

class CEditString : public CString
{
protected:
	void InitWordAlignment();

public:
	bool m_bWordAlign;
	CD3DFont* m_pFont;
	int m_nWidth;
	SIZE m_sizeFont;

	CDWordArray m_adwColor;
	CByteArray  m_abyStyle;
	CDWordArray m_adwLineOffset;
	CWordArray  m_awCodePage;

	CEditString();// : CString( );
	CEditString(const CString& stringSrc);// : CString( stringSrc );
	CEditString(const CEditString& stringSrc);// : CString( stringSrc );
	CEditString(TCHAR ch, int nRepeat = 1);// : CString( ch, nRepeat );
	CEditString(const char* lpch, int nLength);// : CString( lpch, nLength );
	CEditString(const unsigned char* psz);// : CString( psz );
	CEditString(LPCWSTR lpsz);// : CString( lpsz );
	CEditString(LPCSTR lpsz);// : CString( lpsz );

	~CEditString();

#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	void InitializeValue(void);
#endif // __IMPROVE_QUEST_INTERFACE
	void Init(int nWidth, SIZE sizeFont);
	void Init(CD3DFont* pFont, CRect* pRect);
	void Adjust(int nWidth, SIZE sizeFont);
	void Adjust(CD3DFont* pFont, CRect* pRect);
	void ParsingString(const char* lpsz, unsigned long dwColor, unsigned long dwStyle, unsigned short wCodePage, CString& string, CDWordArray& adwColor, CByteArray& abyStyle, CWordArray& awCodePage, unsigned long dwPStyle = 0x00000001);
	void SetAlpha(unsigned long dwColor);
	void SetColor(unsigned long dwColor);
	void SetStyle(unsigned long dwStyle);
	void SetCodePage(unsigned short codePage);
	void SetColor(int nPos, int nLength, unsigned long dwColor);
	void SetStyle(int nPos, int nLength, unsigned long dwStyle);
	void ClearStyle(int nPos, int nLength, unsigned long dwStyle);

	void GetTextFormat(CString& str);
	void ClearStyle(unsigned long dwStyle);
	void SetCodePage(int nPos, int nLength, unsigned short wCodePage);

	// ref-counted copy from another CString
	const CEditString& operator=(const CString& stringSrc) { SetString(stringSrc); return *this; }
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	const CEditString& operator=(const CEditString& stringSrc);
#else // __IMPROVE_QUEST_INTERFACE
	const CEditString& operator=(const CEditString& stringSrc) { SetEditString(stringSrc); return *this; }
#endif // __IMPROVE_QUEST_INTERFACE
	// set string content to single character
	//const CString& operator=(TCHAR ch) { SetString( ch ); return *this; } 
#ifdef _UNICODE
	const CEditString& operator=(char ch) { SetString(ch); return *this; }
#endif
	// copy string content from ANSI string (converts to TCHAR)
	const CEditString& operator=(LPCSTR lpsz) { SetString(lpsz); return *this; }
	// copy string content from UNICODE string (converts to TCHAR)
	//const CString& operator=( LPCWSTR lpsz ) { SetString( lpsz ); return *this; } 
	// copy string content from unsigned chars
	//const CString& operator=( const unsigned char* psz ) { SetString( psz ); return *this; } 

	// string concatenation

	// concatenate from another CString
	const CEditString& operator+=(const CString& string) { AddString(string); return *this; }
	const CEditString& operator+=(const CEditString& string) { AddEditString(string); return *this; }

	// concatenate a single character
	//const CString& operator+=( TCHAR ch ) { AddString( ch ); return *this; } 
#ifdef _UNICODE
	// concatenate an ANSI character after converting it to TCHAR
	const CString& operator+=(char ch) { SetString(ch); return *this; }
#endif
	// concatenate a UNICODE character after converting it to TCHAR
	const CString& operator+= (const char* lpsz) { SetString(lpsz); return *this; }
	const CString& operator+= (char* lpsz) { SetString(lpsz); return *this; }

	int Find(const char* pstr, int nStart) const;
	int Insert(int nIndex, TCHAR ch, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0);
	int Insert(int nIndex, const char* pstr, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0);
	int Delete(int nIndex, int nCount = 1);
	int DeleteLine(int nBeginLine, int nCount = 1);
	TCHAR GetAt(int nIndex) const;
	void SetAt(int nIndex, TCHAR ch, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0);
	void AddParsingString(const char* lpsz, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0, unsigned long dwPStyle = 0x00000001);
#if __VER >= 15 // __IMPROVE_QUEST_INTERFACE
	void SetParsingString(const char* lpsz, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0, unsigned long dwPStyle = 0x00000001, bool bNoInitAlign = false);
#else // __IMPROVE_QUEST_INTERFACE
	void SetParsingString(const char* lpsz, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0, unsigned long dwPStyle = 0x00000001);
#endif // __IMPROVE_QUEST_INTERFACE
	void AddString(const char* lpsz, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0);
	void AddString(char ch, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0);
	void SetString(const char* lpsz, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0);
	void SetString(char* lpsz, unsigned long dwColor = 0xff000000, unsigned long dwStyle = 0x00000000, unsigned short wCodePage = 0);
	void AddEditString(const CEditString& string);
	void SetEditString(const CEditString& string);
	void Empty();
	//void Reset( CD3DFont* pFont, CRect* rect, bool bHScroll = false, bool bVScroll = true);
	void Align(CD3DFont* pFont, int nBeginLine = 0);
	unsigned long GetLineCount();
	unsigned long GetLineOffset(unsigned long dwLine);
	unsigned long OffsetToLine(unsigned long dwOffset);
	CString GetLine(unsigned long dwLine);
	void GetLine(unsigned long dwLine, char* pszStr);
	CEditString GetEditString(unsigned long dwLine);
};


#endif