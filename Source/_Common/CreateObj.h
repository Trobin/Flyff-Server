#ifndef __CREATEOBJ_H__
#define __CREATEOBJ_H__

#ifdef __CLIENT

#include "Sfx.h"

// SFX�� ������Ű���� �̳��� ȣ���Ѵ�.
// 
// pd3dDevice d3d����̽�
// dwSfxObj SFX�� ID
// vPosSrc �� ��ġ
// idSrc ��� ID
// vPosDest ���� ��ġ
// idDest ������ ID
CSfx* CreateSfx(LPDIRECT3DDEVICE9 pd3dDevice, unsigned long dwSfxObj, D3DXVECTOR3& vPosSrc, OBJID idSrc = NULL_ID, D3DXVECTOR3& vPosDest = D3DXVECTOR3(0, 0, 0), OBJID idDest = NULL_ID, int nSec = 0);
CSfx* CreateItemReadySfx(LPDIRECT3DDEVICE9 pd3dDevice, unsigned long dwSfxObj, D3DXVECTOR3& vPosSrc, OBJID idSrc = NULL_ID, D3DXVECTOR3& vPosDest = D3DXVECTOR3(0, 0, 0), OBJID idDest = NULL_ID, int nSec = 0);
CSfxShoot* CreateShootSfx(LPDIRECT3DDEVICE9 pd3dDevice, unsigned long dwSfxObj, D3DXVECTOR3& vPosSrc, OBJID idSrc = NULL_ID, D3DXVECTOR3& vPosDest = D3DXVECTOR3(0, 0, 0), OBJID idDest = NULL_ID, int nSec = 0);
CSfx* CreateSfxYoYo(LPDIRECT3DDEVICE9 pd3dDevice, unsigned long dwSfxObj, D3DXVECTOR3& vPosSrc, OBJID idSrc = NULL_ID, D3DXVECTOR3& vPosDest = D3DXVECTOR3(0, 0, 0), OBJID idDest = NULL_ID, int nSec = 0);

#endif // __CLIENT

#ifdef __LAYER_1015
CMover* CreateMover(CWorld* pWorld, unsigned long dwID, const D3DXVECTOR3& vPos, int nLayer);
#else	// __LAYER_1015
CMover* CreateMover(CWorld* pWorld, unsigned long dwID, D3DXVECTOR3 vPos);
#endif	// __LAYER_1015
CObj* CreateObj(LPDIRECT3DDEVICE9 pd3dDevice, unsigned long dwObjType, unsigned long dwObjIndex, bool bInitProp = true);
CObj* ReadObj(CFileIO* pFile);

bool CheckVersion(FILE* fp, const char* lpszName, const char* lpszVersion);
bool WriteVersion(FILE* fp, const char* lpszName, const char* lpszVersion);
bool CheckVersion(FILE* fp, unsigned long dwVersion);
bool WriteVersion(FILE* fp, unsigned long dwVersion);

#endif // __CREATEOBJ_H__