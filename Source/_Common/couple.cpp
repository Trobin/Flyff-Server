#include "stdafx.h"
#include "couple.h"

#if __VER >= 13 // __COUPLE_1117

bool CCoupleTestCase::Test()
{
	/*
	// Load
	CCoupleProperty* ptr	= CCoupleProperty::Instance();
	bool bResult	= ptr->Initialize();
	assert( bResult );

	CCouple	c( 1, 2 );
	for( int nLevel = 1; nLevel < CCouple::eMaxLevel; nLevel++ )
	{
		assert( c.GetLevel() == nLevel );
		int nExperience	= ptr->GetExperienceRequired( c.GetLevel() );
		c.AddExperience( nExperience );
	}
	return false;
	*/
	/*
	// Level
	for( int nLevel = 1; nLevel <= CCouple::eMaxLevel; nLevel++ )
		TRACE( "Level: %d = Exp: %d\n", nLevel, ptr->GetTotalExperience( nLevel ) );
	// Item
	for( nLevel = 1; nLevel <= CCouple::eMaxLevel; nLevel++ )
	{
		VCI& vItems	= ptr->GetItems( nLevel );
		TRACE( "Level: %d: Item\n", nLevel );
		for( int i = 0; i < vItems.size(); i++ )
			TRACE( "\tnItem: %d, nFlags: %d, nLife: %d, nNum: %d\n", vItems[i].nItem, vItems[i].nFlags, vItems[i].nLife, vItems[i].nNum );
	}
	for( nLevel = 1; nLevel <= CCouple::eMaxLevel; nLevel++ )
	{
		VS& vSkills	= ptr->GetSkill( nLevel );
		TRACE( "Level: %d: Skill\n", nLevel );
		for( int i = 0; i < vSkills.size(); i++ )
			TRACE( "\tSkill: %d\n", vSkills[i] );
	}
	unsigned long idFirst	= 1;
	unsigned long idSecond	= 2;
	unsigned long idInvalid	= 0;

	CCoupleMgr* pMgr	= new CCoupleMgr;
	pMgr->Couple( idFirst, idSecond );
	assert( pMgr->GetCount() == 1 );
	pMgr->Decouple( idFirst );
	assert( pMgr->GetCount() == 0 );
	pMgr->Couple( idFirst, idSecond );
	assert( pMgr->GetCount() == 1 );
	pMgr->Decouple( idSecond );
	assert( pMgr->GetCount() == 0 );
	pMgr->Couple( idFirst, idSecond );
	assert( pMgr->GetCount() == 1 );
	bool bResult	= pMgr->Decouple( idInvalid );
	assert( bResult == false );
	assert( pMgr->GetCount() == 1 );
	CCouple* pCoupleByFirst	= pMgr->GetCouple( idFirst );
	assert( pCoupleByFirst );
	CCouple* pCoupleBySecond	= pMgr->GetCouple( idSecond );
	assert( pCoupleBySecond );
	assert( pCoupleByFirst == pCoupleBySecond );
	CCouple* pCouple	= pCoupleByFirst;
	assert( pCouple->GetLevel() == 1 );
	pCouple->AddExperience( 1 );
	assert( pCouple->GetExperience() == 1 );
	pCouple->AddExperience( 1 );
	assert( pCouple->GetExperience() == 2 );
	pMgr->OnTimer();
	assert( pCouple->GetExperience() == 3 );
*/
	return true;
}

////////////////////////////////////////////////////////////
CCouple::CCouple()
	:
	m_nExperience(0),
	m_idFirst(0),
	m_idSecond(0),
	m_nLevel(1)
{
}

CCouple::CCouple(unsigned long idFirst, unsigned long idSecond)
	:
	m_nExperience(0),
	m_idFirst(idFirst),
	m_idSecond(idSecond),
	m_nLevel(1)
{
}

bool CCouple::AddExperience(int nExperience)
{
	int nLevel = GetLevel();
	if (nLevel == eMaxLevel)
		return false;
	m_nExperience += nExperience;
	return nLevel != GetLevel(true);
}

unsigned long CCouple::GetPartner(unsigned long idPlayer)
{
	assert(HasPlayer(idPlayer));
	return m_idFirst == idPlayer ? m_idSecond : m_idFirst;
}

void CCouple::OnTimer()
{
	AddExperience(1);		//
}

void CCouple::Serialize(CAr& ar)
{
	if (ar.IsStoring())
	{
		ar << m_nExperience << m_idFirst << m_idSecond;
	}
	else
	{
		ar >> m_nExperience >> m_idFirst >> m_idSecond;
		GetLevel(true);
	}
}

int CCouple::GetLevel(bool bCalc)
{
	if (bCalc)
		m_nLevel = CCoupleProperty::Instance()->GetLevel(GetExperience());
	return m_nLevel;
}
////////////////////////////////////////////////////////////
CCoupleMgr::CCoupleMgr()
{
}

CCoupleMgr::~CCoupleMgr()
{
	Clear();
}

void CCoupleMgr::Clear()
{
	for (VCOUPLE::iterator i = m_vCouples.begin(); i != m_vCouples.end(); ++i)
		SAFE_DELETE(*i);
	m_mapPlayers.clear();
	m_vCouples.clear();
}

CCouple* CCoupleMgr::GetCouple(unsigned long idPlayer)
{
	MPC::iterator i = m_mapPlayers.find(idPlayer);
	if (i != m_mapPlayers.end())
		return i->second;
	return NULL;
}

void CCoupleMgr::Couple(unsigned long idFirst, unsigned long idSecond)
{
	CCouple* pCouple = new CCouple(idFirst, idSecond);
	Couple(pCouple);
}

bool CCoupleMgr::Decouple(unsigned long idFirst)
{
	for (VCOUPLE::iterator i = m_vCouples.begin(); i != m_vCouples.end(); ++i)
	{
		if ((*i)->HasPlayer(idFirst))
		{
			m_mapPlayers.erase(idFirst);
			m_mapPlayers.erase((*i)->GetPartner(idFirst));
			CCouple* pCouple = *i;
			m_vCouples.erase(i);
			SAFE_DELETE(pCouple);
			return true;
		}
	}
	return false;
}

void CCoupleMgr::OnTimer()
{
	for (VCOUPLE::iterator i2 = m_vCouples.begin(); i2 != m_vCouples.end(); ++i2)
		(*i2)->OnTimer();
}

void CCoupleMgr::Serialize(CAr& ar)
{
	if (ar.IsStoring())
	{
		ar << m_vCouples.size();
		for (VCOUPLE::iterator i = m_vCouples.begin(); i != m_vCouples.end(); ++i)
			(*i)->Serialize(ar);
	}
	else
	{
		Clear();
		size_t nSize;
		ar >> nSize;
		for (size_t i = 0; i < nSize; i++)
		{
			CCouple* pCouple = new CCouple;
			pCouple->Serialize(ar);
			Couple(pCouple);
		}
	}
}

void CCoupleMgr::Couple(CCouple* pCouple)
{
	m_vCouples.push_back(pCouple);
	bool bResult = m_mapPlayers.insert(MPC::value_type(pCouple->GetFirst(), pCouple)).second;
	assert(bResult);
	bResult = m_mapPlayers.insert(MPC::value_type(pCouple->GetSecond(), pCouple)).second;
	assert(bResult);
}

#if __VER >= 13 // __COUPLE_1202
CCoupleProperty::CCoupleProperty()
{
}

CCoupleProperty::~CCoupleProperty()
{
	m_vExp.clear();
	m_vItems.clear();
	m_vSkillKinds.clear();
	m_vSkills.clear();
}

bool CCoupleProperty::Initialize()
{
	CScript s;
	if (s.Load("couple.inc") == false)
		return false;
	s.GetToken();
	while (s.tok != FINISHED)
	{
		if (s.Token == _T("Level"))
			LoadLevel(s);
		else if (s.Token == _T("Item"))
			LoadItem(s);
		else if (s.Token == _T("SkillKind"))
			LoadSkillKind(s);
		else if (s.Token == _T("SkillLevel"))
			LoadSkillLevel(s);
		s.GetToken();
	}
	return true;
}

void CCoupleProperty::LoadLevel(CScript& s)
{
	s.GetToken();	// {
	int nExp = s.GetNumber();
	while (*s.token != '}')
	{
#ifndef __MAINSERVER
		nExp /= 100;	// 정식 서버가 아니라면 테스트를 위해서 시간을 1/100로 줄인다.
#endif	// __MAINSERVER
		m_vExp.push_back(nExp);
		nExp = s.GetNumber();
	}
	m_vItems.resize(m_vExp.size());
	m_vSkills.resize(m_vExp.size());
}

void CCoupleProperty::LoadItem(CScript& s)
{
	assert(m_vItems.size() > 0);
	s.GetToken();	// {
	int nLevel = s.GetNumber();
	while (*s.token != '}')
	{
		assert(nLevel <= (int)(m_vItems.size()));
		s.GetToken();	// {
		int nItem = s.GetNumber();
		while (*s.token != '}')
		{
			int	nSex = s.GetNumber();
			int nFlags = s.GetNumber();
			int nLife = s.GetNumber();
			int nNum = s.GetNumber();
			AddItem(nLevel, COUPLE_ITEM(nItem, nSex, nFlags, nLife, nNum));
			nItem = s.GetNumber();
		}
		nLevel = s.GetNumber();
	}
}

VCI& CCoupleProperty::GetItems(int nLevel)
{
	assert(nLevel <= (int)(m_vItems.size()));
	return m_vItems[nLevel - 1];
}

void CCoupleProperty::AddItem(int nLevel, const COUPLE_ITEM& ci)
{
	VCI& vci = GetItems(nLevel);
	vci.push_back(ci);
}

void CCoupleProperty::LoadSkillKind(CScript& s)
{
	s.GetToken();	// {
	int nSkill = s.GetNumber();
	while (*s.token != '}')
	{
		m_vSkillKinds.push_back(nSkill);
		nSkill = s.GetNumber();
	}
}

void CCoupleProperty::LoadSkillLevel(CScript& s)
{
	assert(m_vSkillKinds.size() > 0);
	assert(m_vSkills.size() > 0);
	s.GetToken();	// {
	int nLevel = s.GetNumber();
	while (*s.token != '}')
	{
		for (int i = 0; i < (int)(m_vSkillKinds.size()); i++)
		{
			int nSkillLevel = s.GetNumber();
			int nSkill = 0;
			if (nSkillLevel > 0)
				nSkill = m_vSkillKinds[i] + nSkillLevel - 1;
			m_vSkills[nLevel - 1].push_back(nSkill);
		}
		nLevel = s.GetNumber();
	}
	for (int i = 1; i < (int)(m_vSkills.size()); i++)
	{
		if (m_vSkills[i].empty())
			m_vSkills[i].assign(m_vSkills[i - 1].begin(), m_vSkills[i - 1].end());
	}
}

int CCoupleProperty::GetLevel(int nExperience)
{
	for (int i = 0; i < (int)(m_vExp.size()); i++)
	{
		if (nExperience < m_vExp[i])
			return i;
	}
	return 1;
}

VS& CCoupleProperty::GetSkill(int nLevel)
{
	assert(nLevel <= (int)(m_vSkills.size()));
	return m_vSkills[nLevel - 1];
}

CCoupleProperty* CCoupleProperty::Instance()
{
	static CCoupleProperty sCoupleProperty;
	return &sCoupleProperty;
}

int CCoupleProperty::GetTotalExperience(int nLevel)
{
	assert(nLevel <= (int)(m_vExp.size()));
	return m_vExp[nLevel - 1];
}

int CCoupleProperty::GetExperienceRequired(int nLevel)
{
	if (nLevel == CCouple::eMaxLevel)
		return 0;
	return GetTotalExperience(nLevel + 1) - GetTotalExperience(nLevel);
}

float	CCoupleProperty::GetExperienceRate(int nLevel, int nExperience)
{
	int nTotal = GetExperienceRequired(nLevel);
	if (nTotal == 0)
		return 0.0F;

	return static_cast<float>(nExperience - GetTotalExperience(nLevel)) / static_cast<float>(nTotal);
}
#endif	// __COUPLE_1202

#endif	// __COUPLE_1117