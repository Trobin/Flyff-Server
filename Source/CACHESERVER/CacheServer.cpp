// CacheServer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"

#include "DPCoreClient.h"
#include "DPClient.h"
#include "DPCacheSrvr.h"
#include "player.h"

#include <iostream>
#include "MyTimer.h"

#ifdef __PL_0917
#include "packetlog.h"
#endif	// __PL_0917

#ifdef __CRASH_0404
#include "crashstatus.h"
#endif	// __CRASH_0404

extern	CDPCoreClient	g_DPCoreClient;
extern	CDPClientArray	g_DPClientArray;
extern	CDPCacheSrvr	g_DPCacheSrvr;

char					g_szCoreAddr[16];
unsigned short g_uPort = PN_CACHESRVR;


bool Script(LPCSTR lpszFileName);

bool init()
{
	if (!Script("CacheServer.ini"))
	{
		return false;
	}

	if (!InitializeNetLib())
	{
		return false;
	}

	if (!g_DPCoreClient.ConnectToServer(g_szCoreAddr, PN_CORESRVR + 1))
	{
		return false;
	}

#ifdef __CRC
	if (!g_DPCacheSrvr.StartServer(g_uPort, false, CSock::crcRead))
#else	// __CRC
	if (!g_DPCacheSrvr.StartServer(g_uPort))
#endif	// __CRC
	{
		return false;
	}

	return true;
}

void callbackDestroyGarbage()
{
#ifdef __CRASH_0404
	CCrashStatus::GetInstance()->SetMainThreadTask(1);
#endif	// __CRASH_0404
	CPlayerMng::Instance()->DestroyGarbage();
#ifdef __CRASH_0404
	CCrashStatus::GetInstance()->SetMainThreadTask(0);
#endif	// __CRASH_0404
}

void exit()
{
	g_DPCoreClient.DeleteDPObject();
	g_DPClientArray.Free();
	g_DPCacheSrvr.DeleteDPObject();
	UninitializeNetLib();
}

bool Script(LPCSTR lpszFileName)
{
	CScanner s;

	if (s.Load(lpszFileName))
	{
		s.GetToken();
		while (s.tok != FINISHED)
		{
			if (s.Token == "Core")
			{
				s.GetToken();
				strcpy(g_szCoreAddr, s.Token);
			}
			else if (s.Token == "Port")
			{
				g_uPort = s.GetNumber();
			}
			s.GetToken();
		}
		return true;
	}
	std::cerr << "Can't open file, " << lpszFileName << "\n";

	return false;
}

int main(int argc, char** argv)
{
	bool resInit = init();
	std::cout << "Initialisation : " << resInit << "\n";
	Timer t;
	t.setInterval(&callbackDestroyGarbage, SEC(60));

	while (getchar() != 'q');

	t.stop();

	exit();

	return 0;
}