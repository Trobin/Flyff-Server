#ifndef __PLAYER_H__
#define __PLAYER_H__

#pragma once

#include <map>
using	namespace	std;
#include "dpclient.h"

class CPlayer
{
public:
	CPlayer(DPID dpidUser, unsigned long dwSerial);
	virtual	~CPlayer();
	unsigned long	GetAuthKey(void) { return m_dwAuthKey; }
	const char* GetPlayer(void) { return m_szPlayer; }
	const char* GetAccount(void) { return m_szAccount; }
	const char* GetPass(void) { return m_szPass; }
	CDPClient* GetClient(void) { return m_pClient; }
	void	SetClient(CDPClient* pClient) { m_pClient = pClient; }
	bool	IsAlive(void) { return m_bAlive; }
	void	SetAlive(bool bAlive) { m_bAlive = bAlive; }
	DPID	GetNetworkId(void) { return m_dpid; }
	unsigned long	GetPlayerId(void) { return m_idPlayer; }
	unsigned long	GetSerial(void) { return m_dwSerial; }
	bool	IsTimeover(unsigned long dwTick) { return !m_pClient && m_dwCreation < dwTick; }
	const char* GetAddr(void) { return m_lpAddr; }
	unsigned long	GetWorld(void) { return m_dwWorldId; };
	unsigned long		GetChannel(void) { return m_uChannel; }
	unsigned long	GetParty(void) { return m_idParty; }
	unsigned long	GetGuild(void) { return m_idGuild; }
	unsigned long GetWar(void) { return m_idWar; }
	BYTE	GetSlot(void) { return m_nSlot; }
	void	Join(CAr& ar);
	void	SetAddr(CDPMng* pdpMng) { pdpMng->GetPlayerAddr(GetNetworkId(), m_lpAddr); }
private:
	const	unsigned long	m_dwSerial;
	const	DPID	m_dpid;
	unsigned long	m_dwAuthKey;
	char	m_szPlayer[MAX_PLAYER];
	char	m_szAccount[MAX_ACCOUNT];
	char	m_szPass[MAX_PASSWORD];
	CDPClient* m_pClient;
	bool	m_bAlive;
	unsigned long	m_idPlayer;
	const	unsigned long	m_dwCreation;
	char	m_lpAddr[16];
	unsigned long	m_dwWorldId;
	unsigned long	m_uChannel;
	unsigned long	m_idParty;
	unsigned long	m_idGuild;
	unsigned long	m_idWar;
	BYTE	m_nSlot;
};

typedef	map<DPID, CPlayer*>	MPP;
class CPlayerMng
{
public:
	CMclCritSec	m_AddRemoveLock;

public:
	CPlayerMng();
	virtual	~CPlayerMng();
	void	Clear(void);
	bool	AddPlayer(DPID dpid);
	bool	RemovePlayer(DPID dpid);
	CPlayer* GetPlayer(DPID dpid);
	CPlayer* GetPlayerBySerial(unsigned long dwSerial);
	int		GetCount(void) { return m_mapPlayers.size(); }
	void	DestroyPlayer(CDPClient* pClient);
	void	DestroyGarbage(void);
	static	CPlayerMng* Instance(void);
private:
	void	DestroyPlayersOnChannel(CDPClient* pClient);
	void	DestroyAllPlayers(void);
	void	DestroyGarbagePlayer(CPlayer* pPlayer);
	void	SendKeepAlive(CPlayer* pPlayer);
private:
	long	m_lSerial;
	MPP		m_mapPlayers;
};

#endif	// __PLAYER_H__