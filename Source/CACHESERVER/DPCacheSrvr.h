#ifndef __DPCACHESRVR_H__
#define __DPCACHESRVR_H__

#include "DPMng.h"
#include "MsgHdr.h"

#undef	theClass
#define theClass	CDPCacheSrvr
#undef theParameters
#define theParameters	CAr & ar, DPID, LPBYTE, unsigned long

class CDPCacheSrvr : public CDPMng
{
private:

public:
	// Constructions
	CDPCacheSrvr();
	virtual	~CDPCacheSrvr();

	// Operations
	virtual void	SysMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);
	virtual void	UserMessageHandler(LPDPMSG_GENERIC lpMsg, unsigned long dwMsgSize, DPID idFrom);

	USES_PFNENTRIES;

	// Handlers
	void	OnAddConnection(CAr& ar, DPID dpid, LPBYTE lpBuf, unsigned long uBufSize);
	void	OnRemoveConnection(DPID dpid);
	void	OnKeepAlive(CAr& ar, DPID dpid, LPBYTE, unsigned long);
};


#endif