#ifndef __PACKET_LOG_H
#define __PACKET_LOG_H

#include <map>
#include <vector>
#include <dplay.h>

#include "CMclCritSec.h"

using namespace std;

typedef struct	_PacketRecvd
{
	unsigned long	dwHdr;
	unsigned long	dwPackets;
	unsigned long	dwBytes;
	_PacketRecvd(unsigned long h, unsigned long dwRecvd) : dwHdr(h), dwPackets(1), dwBytes(dwRecvd) {}
}	PacketRecvd;

typedef	map<unsigned long, PacketRecvd>	MPAR;

typedef struct	_PlayerRecvd
{
	DPID	dpid;
	unsigned long dwTotalBytes;
	unsigned long dwTotalPackets;
	MPAR mpar;
	_PlayerRecvd(DPID d, unsigned long dwBytes, MPAR& m) : dpid(d), dwTotalBytes(dwBytes), dwTotalPackets(1), mpar(m) {}
} PlayerRecvd;

typedef map<DPID, PlayerRecvd>	MPLR;
typedef vector<PacketRecvd>	VPAR;
typedef vector<PlayerRecvd>	VPLR;

class CPacketLog
{
private:
	CPacketLog();
public:
	virtual	~CPacketLog();
	static	CPacketLog* Instance();
	void	Add(DPID dpid, unsigned long dwHdr, unsigned long dwBytes);
	void	Print();
	void	Reset();

private:
	MPLR	m_players;
	CMclCritSec		m_AccessLock;
};

#endif	// __PACKET_LOG_H